platformio.ini:

[platformio]
src_dir = Src
lib_dir = Middlewares/ST

[env:disco_f407vg]
platform = ststm32
board = disco_f407vg
framework = stm32cube



After code compilation:

Create library.json in Middlewares/XX (eg /ST/AI) and set this content

{
    "name": "STM32_USB_Host_Library",
    "version": "0.0.0",
    "build": {
        "flags": [
            "-I $PROJECT_DIR/Inc",
            "-I Core/Inc",
            "-I Class/CDC/Inc"
        ]
    }
}

rename ai runtime .a library to libia.a