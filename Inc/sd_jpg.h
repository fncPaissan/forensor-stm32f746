#ifndef _SD_JPG_H
#define _SD_JPG_H


#include <stdio.h>
#include "ff.h"
#include "jpeglib.h"
#include "main.h"

#define JPEG_QUALITY 70
#define IMAGE_SIZE_WIDTH 320
#define IMAGE_SIZE_HEIGHT 240

#define MOTION_JPEG_FPS_MSEC 100

typedef uint32_t RET;

#define RET_OK           0x00000000
#define RET_NO_DATA      0x00000001
#define RET_DO_NOTHING   0x00000002
#define RET_ERR          0x80000001
#define RET_ERR_OF       0x80000002
#define RET_ERR_TIMEOUT  0x80000004
#define RET_ERR_STATUS   0x80000008
#define RET_ERR_PARAM    0x80000010
#define RET_ERR_FILE     0x80000020
#define RET_ERR_MEMORY   0x80000040


RET liveviewCtrl_capture();
RET liveviewCtrl_movieRecordStart(); // call this when start movie recording
RET liveviewCtrl_movieRecordFinish();  // call this when stop movie recording
RET liveviewCtrl_movieRecordFrame(); // call this every frame during movie recording

static RET liveviewCtrl_encodeJpegFrame();
static RET liveviewCtrl_writeFileStart(char* filename);
static RET liveviewCtrl_writeFileFinish();

static void LCD_LL_ConvertLineToRGB888(void *pSrc, void *pDst, uint16_t width);
static RET liveviewCtrl_generateFilename(char* filename, uint8_t numPos);

#endif /* _SD_JPG_H */