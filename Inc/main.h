/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f7xx_hal_uart.h"
#include "stm32f7xx_hal_sdram.h"
#include "stm32f7xx_hal_ltdc.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_sdram.h"
//#include "stm32746g_discovery_camera.h"
#include "stm32f7xx_ll_fmc.h"

#include "ov7670_driver.h"
#include "dma2d_drv.h"

#include "decoder_layer.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

//LCD Configuration
//layer 0: contains grayscale image, fills the layer window
//layer 1: contains movment data image and debug informations (temporary)
#define LCD_FRAME_BUFFER_0      0xC0000000

#define LAYER0_SIZE_X           320
#define LAYER0_SIZE_Y           240
#define LAYER0_BYTE_PER_PIXEL   2  //RGB565 image

#define LCD_FRAME_BUFFER_1      LCD_FRAME_BUFFER_0 + LAYER0_SIZE_X*LAYER0_SIZE_Y*LAYER0_BYTE_PER_PIXEL

#define LAYER1_SIZE_X           160
#define LAYER1_SIZE_Y           270
#define LAYER1_IMAGE_SIZE_X     160
#define LAYER1_IMAGE_SIZE_Y     120
#define LAYER1_BYTE_PER_PIXEL   2  

//Camera Configuration
#define CAMERA_FRAME_BUFFER     0xC0100000  
#define DWNSMP_FRAME_BUFFER     0xC0212C00

#define resolution              RESOLUTION_R320x240
#define CAMERA_xsize            320
#define CAMERA_ysize            240

#define MAX_FRAMERATE           8

//SD Configuration
#define SD_OUT_BUFFER           0xC0200000
#define SD_OUT_FPS              4

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void LCD_Print(char* text);
void CAMERA_UpdateMovementData(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
