#ifndef DMA2D_H
#define DMA2D_H

#ifdef __cplusplus
 extern "C" {
#endif 

#include "main.h"



void LUT_Set(void);
void TransferError(DMA2D_HandleTypeDef* hdma2d);
void TransferComplete(DMA2D_HandleTypeDef* hdma2d);
void DMA2D_Image_Downsample(uint8_t *pSrc, uint8_t *pDst, uint16_t width, uint16_t height, uint16_t out_width, uint16_t out_height, uint32_t original_format);
void Line_Downsample(void *pSrc, void *pDst, uint8_t stride, uint16_t length,  uint32_t original_format);
void DMA2D_Image_Show(void *pSrc, void *pDst, uint16_t width, uint16_t height, uint32_t original_format, uint16_t out_offs);

#ifdef __cplusplus
}
#endif

#endif /* DMA2D_H */