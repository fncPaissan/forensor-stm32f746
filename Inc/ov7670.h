/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ov7670_H
#define __ov7670_H

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "../Common/camera.h"

#define  ov7670_ID    0x76
#define  ov7670_ADDR  ((uint16_t)0x42)


/* ov7670 Registers definition */
#define ov7670_SENSOR_PIDH              0x0A
#define ov7670_SENSOR_PIDL              0x0B
#define ov7670_SENSOR_COM7              0x12
#define ov7670_SENSOR_TSLB              0x3A
#define ov7670_SENSOR_MTX1              0x4F
#define ov7670_SENSOR_MTX2              0x50
#define ov7670_SENSOR_MTX3              0x51
#define ov7670_SENSOR_MTX4              0x52
#define ov7670_SENSOR_MTX5              0x53
#define ov7670_SENSOR_MTX6              0x54
#define ov7670_SENSOR_BRTN              0x55
#define ov7670_SENSOR_CNST1             0x56
#define ov7670_SENSOR_CNST2             0x57

 
void     ov7670_Init(uint16_t DeviceAddr, uint32_t resolution);
void     ov7670_Config(uint16_t DeviceAddr, uint32_t feature, uint32_t value, uint32_t BR_value);
uint16_t ov7670_ReadID(uint16_t DeviceAddr);

void     CAMERA_IO_Init(void);
void     CAMERA_IO_Write(uint8_t addr, uint8_t reg, uint8_t value);
uint8_t  CAMERA_IO_Read(uint8_t addr, uint8_t reg);
void     CAMERA_Delay(uint32_t delay);

/* CAMERA driver structure */
extern CAMERA_DrvTypeDef   ov7670_drv;
/**
  * @}
  */    
#ifdef __cplusplus
}
#endif

#endif /* __ov7670_H */

