/* Includes ------------------------------------------------------------------*/
#include "decoder_layer.h"

//ai_float ANCHORS[BOX][2]= {{0.77,2.43}, {0.94,1.07}, {1.53,1.75}, {1.65,3.27}, {3.10,3.52}};
ai_float ANCHORS[BOX][2]={{0.60,1.91}, {0.77,0.86}, {1.22,2.70}, {1.27,1.46}, {2.38,2.77}};

static ai_float sigmoid(ai_float x){
  return 1/(1+expf(-x));
}

uint8_t decode_output_tensor(ai_float tensor[GRID_SZ][GRID_SZ][BOX*(5+CLASS)], ai_float boxes[MAX_BOXES][4], int labels[MAX_BOXES])
{ 
  for(uint8_t box_id=0; box_id<MAX_BOXES; box_id++){
      boxes[box_id][0]=0;
      boxes[box_id][2]=0;
      boxes[box_id][1]=0;
      boxes[box_id][3]=0;
  }
  uint8_t obj_count=0;
  ai_float out_x, out_y, out_w, out_h, xmin, ymin, xmax, ymax, obj_prob;
  for(uint8_t coord_x=0; coord_x<GRID_SZ; coord_x++){
    for(uint8_t coord_y=0; coord_y<GRID_SZ; coord_y++){
      for(uint8_t anchor_idx=0; anchor_idx<BOX; anchor_idx++){
        obj_prob=tensor[coord_y][coord_x][anchor_idx*(5+CLASS)+ 4];
        obj_prob=sigmoid(obj_prob);
        if (obj_prob>SCORE_THRESHOLD){

          //read box coordinates from output tensor
          out_x=tensor[coord_y][coord_x][anchor_idx*(5+CLASS)+ 0];
          out_y=tensor[coord_y][coord_x][anchor_idx*(5+CLASS)+ 1];
          out_w=tensor[coord_y][coord_x][anchor_idx*(5+CLASS)+ 2];
          out_h=tensor[coord_y][coord_x][anchor_idx*(5+CLASS)+ 3];

          //process coordinates
          out_x=sigmoid(out_x)+coord_x;
          out_y=sigmoid(out_y)+coord_y;
          out_w=expf(out_w)*ANCHORS[anchor_idx][0];
          out_h=expf(out_h)*ANCHORS[anchor_idx][1];

          //find corners
          xmin=(out_x - out_w/2) /GRID_SZ;
          ymin=(out_y - out_h/2) /GRID_SZ;
          xmax=(out_x + out_w/2) /GRID_SZ;
          ymax=(out_y + out_h/2) /GRID_SZ;

          //clip output in [0,1]
          if (xmin<0) xmin=0;
          if (xmax>0.99) xmax=0.99;
          if (ymin<0) ymin=0;
          if (ymax>0.99) ymax=0.99;

          //write output vector
          boxes[obj_count][0]=xmin;
          boxes[obj_count][1]=ymin;
          boxes[obj_count][2]=xmax;
          boxes[obj_count][3]=ymax;

          //increment counter
          obj_count++;

        }
      }
    }
  }

  return obj_count;

}
