/* Includes ------------------------------------------------------------------*/
#include "ov7670.h"



CAMERA_DrvTypeDef   ov7670_drv = 
{
  ov7670_Init,
  ov7670_ReadID,  
  ov7670_Config,
};

/* Initialization sequence for QVGA resolution (320x240) - I'm sorry, it's ugly, half is stolen from a random git repo and the other half derived from the datasheet registers */
const unsigned char ov7670_QVGA[][2]=
{

    // Image format
		{ 0x12, 0x11 },		// 0x14 = QVGA size, RGB mode; 0x11 = QVGA, raw bayer
		{ 0xc, 0x8 }, //
		{ 0x11, 0b1000000 }, //

		{ 0xb0, 0x84 },		//
    {0x40, 0x10},

		// Hardware window
		{ 0x32, 0x80 },		//HREF
		{ 0x17, 0x17 },		//HSTART
		{ 0x18, 0x05 },		//HSTOP
		{ 0x03, 0x0a },		//VREF
		{ 0x19, 0x02 },		//VSTART
		{ 0x1a, 0x7a },		//VSTOP

    // FPS and clock speed setup
    { 0x11, 0x80 },
    { 0x6b, 0x0a },
    { 0x2a, 0x00 },
    { 0x2b, 0x00 },
    { 0x92, 0x66 },
    { 0x93, 0x00 },
    { 0x3b, 0x0a }

};

/**
  * @brief  Initializes the ov7670 CAMERA component.
  * @param  DeviceAddr: Device address on communication Bus.
  * @param  resolution: Camera resolution
  * @retval None
  */
void ov7670_Init(uint16_t DeviceAddr, uint32_t resolution)
{
  uint32_t index;
  
  /* Initialize I2C */
  CAMERA_IO_Init();    
  
  /* Prepare the camera to be configured by resetting all its registers */
  CAMERA_IO_Write(DeviceAddr, ov7670_SENSOR_COM7, 0x80);
  CAMERA_Delay(200);
  
  /* Initialize ov7670 */
  switch (resolution)
  {   
  case CAMERA_R320x240:
    {
      for(index=0; index<(sizeof(ov7670_QVGA)/2); index++)
      {
        CAMERA_IO_Write(DeviceAddr, ov7670_QVGA[index][0], ov7670_QVGA[index][1]);
        CAMERA_Delay(10);
      } 
      break;
    }
  default:
    {
      break;
    }
  }
}


/**
  * @brief  Read the ov7670 Camera identity.
  * @param  DeviceAddr: Device address on communication Bus.
  * @retval the ov7670 ID
  */
uint16_t ov7670_ReadID(uint16_t DeviceAddr)
{
  /* Initialize I2C */
  CAMERA_IO_Init();
  
  /* Get the camera ID */
  return (CAMERA_IO_Read(DeviceAddr, ov7670_SENSOR_PIDH));
}

void ov7670_Config(uint16_t DeviceAddr, uint32_t feature, uint32_t value, uint32_t BR_value){
  ;
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
