#include "sd_bmp.h"

/* Private macro -------------------------------------------------------------*/
#define BMP_16BIT
//#define BMP_24BIT
/* Private variables ---------------------------------------------------------*/

FATFS filesys;        /* volume label */
char SDPath[4];

static void LCD_LL_ConvertFrameToRGB565(void *pSrc, void *pDst, uint16_t width, uint16_t height);
DMA2D_HandleTypeDef hdma2d_disco;
SD_HandleTypeDef hsd;

uint8_t image_buf[240][640] = {0};
/* save the picture count  */
uint32_t pic_counter = 0;



/*
bmp header 
image size :320*240
biBitCount :16
biCompression : BI_BITFIELDS = 3
*/
uint8_t bmp_header[70] = {
  0x42, 0x4D, 0x46, 0x58, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46, 0x00, 0x00, 0x00, 0x28, 0x00, 
  0x00, 0x00, 0x40, 0x01, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x01, 0x00, 0x10, 0x00, 0x03, 0x00, 
  0x00, 0x00, 0x00, 0x58, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x00, 0x00, 0xE0, 0x07, 0x00, 0x00, 0x1F, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};






/* Private function prototypes -----------------------------------------------*/
static int32_t set_pic_count(void);
static int32_t get_pic_count(void);

/**
  * @brief  Capture_Image_TO_Bmp
  * @param  None
  * @retval None
  */
int32_t Capture_Image_Bmp()
{
  int32_t  ret = -1;
  int32_t  i = 0;
  int32_t  j = 0;
  int16_t  data_temp = 0;
  uint32_t bw = 0;
  BSP_LCD_SelectLayer(0);
  char  file_str[30] = {0};
  FIL file;        /* File object */

  

  //LCD_LL_ConvertFrameToRGB565((uint32_t *)CAMERA_FRAME_BUFFER, (uint32_t *)SD_OUT_BUFFER, 320, 240);
  FATFS_LinkDriver(&SD_Driver, SDPath);
  /* mount the filesys */
  if (f_mount(&filesys, (TCHAR const*)SDPath, 0) != FR_OK) ;//return -1;
  HAL_Delay(10);

  //if ((HAL_SD_GetCardState(&hsd) != HAL_SD_CARD_READY) || (HAL_SD_GetCardState(&hsd) != HAL_SD_CARD_STANDBY)) return -1;

  sprintf(file_str, "pic%d.bmp",pic_counter);
  ret = f_open(&file, file_str, FA_WRITE | FA_CREATE_ALWAYS);
  if (ret) ;//return -1;

  /* write the bmp header */
  ret = f_write(&file, bmp_header, 70, &bw);
  HAL_Delay(5);
  BSP_LCD_SelectLayer(0);
  for (j = 0; j < 240; j++) {
    for(i=0;i<320;i++) { 
      data_temp=BSP_LCD_ReadPixel(i,240-j-1);
      image_buf[j][i*2+1] = (data_temp&0xff00) >> 8;
      image_buf[j][i*2+0] = data_temp & 0x00ff;
      }
  }
  ret = f_write(&file, image_buf, 640*240, &bw);
  if (ret) ;//return -1;

  ret = f_close(&file);
  if (ret) ;//return -1;

  FATFS_UnLinkDriver(SDPath);

  /* statistics the take pictures count */
  pic_counter++;
  //set_pic_count();

  return ret;
}

/**
  * @brief  init_picture_count
  * @param  None
  * @retval int32_t : picture count
  */
void init_picture_count(void)
{
  pic_counter = get_pic_count();
}


/**
  * @brief  get the bmp picture count	          
  * @param  None
  * @retval int32_t : picture count
  */
static int32_t get_pic_count(void)
{
//  int32_t ret = -1;
//  uint32_t bw = 0;
//  FIL file;		
//
//  /* mount the filesys */
//  if (f_mount(0, &filesys) != FR_OK) {
//    return -1;
//  }
//  Delay(10);
//
//  ret = f_open(&file, "counter.dat", FA_OPEN_EXISTING | FA_READ);
//  if (ret != FR_OK) {
//    f_close(&file);
//    ret = f_open(&file,"counter.dat", FA_CREATE_ALWAYS | FA_WRITE);
//    if (ret == FR_OK) {
//      pic_counter = 0;
//      ret = f_write(&file, &pic_counter, sizeof(uint32_t), &bw);
//      f_close(&file);
//      f_mount(0, NULL);
//      return pic_counter;
//    } else {
//      f_close(&file);
//      f_mount(0, NULL);
//      return -1;
//    }
//  } else {
//    ret = f_read(&file, &pic_counter, sizeof(uint32_t), &bw);
//    f_close(&file);
//    f_mount(0, NULL);
//    return pic_counter;
//  }
  return 0;
}

/**
  * @brief  save the bmp picture count	          
  * @param  None
  * @retval int32_t : picture count
  */
static int32_t set_pic_count(void)
{
//  int32_t ret = -1;
//  uint32_t bw = 0;
//  FIL file;		
//
//  /* mount the filesys */
//  if (f_mount(0, &filesys) != FR_OK) {
//    return -1;
//  }
//  Delay(10);
//
//  ret = f_open(&file, "counter.dat", FA_OPEN_EXISTING | FA_WRITE);
//  if (ret == FR_OK) {
//    ret = f_write(&file, &pic_counter, sizeof(uint32_t), &bw);
//    f_close(&file);
//    f_mount(0, NULL);
//    return pic_counter;
//  } else {
//    f_close(&file);
//    f_mount(0, NULL);
//    return -1;
//  }
  return 0;
}

static void LCD_LL_ConvertFrameToRGB565(void *pSrc, void *pDst, uint16_t width, uint16_t height)
{
    /* Enable DMA2D clock */
    __HAL_RCC_DMA2D_CLK_ENABLE();

    /* Configure the DMA2D Mode, Color Mode and output offset */
    hdma2d_disco.Init.Mode         = DMA2D_M2M_PFC;
    hdma2d_disco.Init.ColorMode    = DMA2D_RGB565;
    hdma2d_disco.Init.OutputOffset = 0;

    /* Foreground Configuration */
    hdma2d_disco.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
    hdma2d_disco.LayerCfg[1].InputAlpha = 0xFF;
    hdma2d_disco.LayerCfg[1].InputColorMode = CM_L8;
    hdma2d_disco.LayerCfg[1].InputOffset = 0;

    hdma2d_disco.Instance = DMA2D;

    /* DMA2D Initialization */
    if(HAL_DMA2D_Init(&hdma2d_disco) == HAL_OK) {
        if(HAL_DMA2D_ConfigLayer(&hdma2d_disco, 1) == HAL_OK) {
            if (HAL_DMA2D_Start(&hdma2d_disco, (uint32_t)pSrc, (uint32_t)pDst, width, height) == HAL_OK) {
                /* Polling For DMA transfer */
                HAL_DMA2D_PollForTransfer(&hdma2d_disco, 10);
            }
        }
    } else {

    }
}
/******************* COPYRIGHT 2012 Embest Tech. Co., Ltd. *****END OF FILE****/