/**
  ******************************************************************************
  * @file    network.c
  * @author  AST Embedded Analytics Research Platform
  * @date    Mon Apr 20 18:02:50 2020
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */


#include "network.h"

#include "ai_platform_interface.h"
#include "ai_math_helpers.h"

#include "core_common.h"
#include "layers.h"

#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#define AI_TOOLS_VERSION_MAJOR 5
#define AI_TOOLS_VERSION_MINOR 0
#define AI_TOOLS_VERSION_MICRO 0


#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#define AI_TOOLS_API_VERSION_MAJOR 1
#define AI_TOOLS_API_VERSION_MINOR 3
#define AI_TOOLS_API_VERSION_MICRO 0

#undef AI_NET_OBJ_INSTANCE
#define AI_NET_OBJ_INSTANCE g_network
 
#undef AI_NETWORK_MODEL_SIGNATURE
#define AI_NETWORK_MODEL_SIGNATURE     "df8c5e304e9732dca328e592261a2c4b"

#ifndef AI_TOOLS_REVISION_ID
#define AI_TOOLS_REVISION_ID     "(rev-5.0.0)"
#endif

#undef AI_TOOLS_DATE_TIME
#define AI_TOOLS_DATE_TIME   "Mon Apr 20 18:02:50 2020"

#undef AI_TOOLS_COMPILE_TIME
#define AI_TOOLS_COMPILE_TIME    __DATE__ " " __TIME__

#undef AI_NETWORK_N_BATCHES
#define AI_NETWORK_N_BATCHES         (1)

/**  Forward network declaration section  *************************************/
AI_STATIC ai_network AI_NET_OBJ_INSTANCE;


/**  Forward network array declarations  **************************************/
AI_STATIC ai_array conv2d_53_scratch0_array;   /* Array #0 */
AI_STATIC ai_array conv2d_52_scratch1_array;   /* Array #1 */
AI_STATIC ai_array conv2d_52_scratch0_array;   /* Array #2 */
AI_STATIC ai_array conv2d_50_scratch0_array;   /* Array #3 */
AI_STATIC ai_array conv2d_49_scratch1_array;   /* Array #4 */
AI_STATIC ai_array conv2d_49_scratch0_array;   /* Array #5 */
AI_STATIC ai_array conv2d_48_scratch1_array;   /* Array #6 */
AI_STATIC ai_array conv2d_48_scratch0_array;   /* Array #7 */
AI_STATIC ai_array conv2d_46_scratch0_array;   /* Array #8 */
AI_STATIC ai_array conv2d_45_scratch1_array;   /* Array #9 */
AI_STATIC ai_array conv2d_45_scratch0_array;   /* Array #10 */
AI_STATIC ai_array conv2d_44_scratch1_array;   /* Array #11 */
AI_STATIC ai_array conv2d_44_scratch0_array;   /* Array #12 */
AI_STATIC ai_array conv2d_43_scratch0_array;   /* Array #13 */
AI_STATIC ai_array conv2d_42_scratch1_array;   /* Array #14 */
AI_STATIC ai_array conv2d_42_scratch0_array;   /* Array #15 */
AI_STATIC ai_array conv2d_41_scratch1_array;   /* Array #16 */
AI_STATIC ai_array conv2d_41_scratch0_array;   /* Array #17 */
AI_STATIC ai_array conv2d_39_scratch0_array;   /* Array #18 */
AI_STATIC ai_array conv2d_38_scratch1_array;   /* Array #19 */
AI_STATIC ai_array conv2d_38_scratch0_array;   /* Array #20 */
AI_STATIC ai_array conv2d_37_scratch1_array;   /* Array #21 */
AI_STATIC ai_array conv2d_37_scratch0_array;   /* Array #22 */
AI_STATIC ai_array conv2d_35_scratch0_array;   /* Array #23 */
AI_STATIC ai_array conv2d_34_scratch1_array;   /* Array #24 */
AI_STATIC ai_array conv2d_34_scratch0_array;   /* Array #25 */
AI_STATIC ai_array conv2d_33_scratch1_array;   /* Array #26 */
AI_STATIC ai_array conv2d_33_scratch0_array;   /* Array #27 */
AI_STATIC ai_array conv2d_31_scratch0_array;   /* Array #28 */
AI_STATIC ai_array conv2d_30_scratch1_array;   /* Array #29 */
AI_STATIC ai_array conv2d_30_scratch0_array;   /* Array #30 */
AI_STATIC ai_array conv2d_29_scratch1_array;   /* Array #31 */
AI_STATIC ai_array conv2d_29_scratch0_array;   /* Array #32 */
AI_STATIC ai_array conv2d_28_scratch0_array;   /* Array #33 */
AI_STATIC ai_array conv2d_27_scratch1_array;   /* Array #34 */
AI_STATIC ai_array conv2d_27_scratch0_array;   /* Array #35 */
AI_STATIC ai_array conv2d_25_scratch1_array;   /* Array #36 */
AI_STATIC ai_array conv2d_25_scratch0_array;   /* Array #37 */
AI_STATIC ai_array conv2d_23_scratch0_array;   /* Array #38 */
AI_STATIC ai_array conv2d_22_scratch1_array;   /* Array #39 */
AI_STATIC ai_array conv2d_22_scratch0_array;   /* Array #40 */
AI_STATIC ai_array conv2d_21_scratch1_array;   /* Array #41 */
AI_STATIC ai_array conv2d_21_scratch0_array;   /* Array #42 */
AI_STATIC ai_array conv2d_19_scratch0_array;   /* Array #43 */
AI_STATIC ai_array conv2d_18_scratch1_array;   /* Array #44 */
AI_STATIC ai_array conv2d_18_scratch0_array;   /* Array #45 */
AI_STATIC ai_array conv2d_17_scratch1_array;   /* Array #46 */
AI_STATIC ai_array conv2d_17_scratch0_array;   /* Array #47 */
AI_STATIC ai_array conv2d_16_scratch0_array;   /* Array #48 */
AI_STATIC ai_array conv2d_15_scratch1_array;   /* Array #49 */
AI_STATIC ai_array conv2d_15_scratch0_array;   /* Array #50 */
AI_STATIC ai_array conv2d_13_scratch1_array;   /* Array #51 */
AI_STATIC ai_array conv2d_13_scratch0_array;   /* Array #52 */
AI_STATIC ai_array conv2d_11_scratch0_array;   /* Array #53 */
AI_STATIC ai_array conv2d_10_scratch1_array;   /* Array #54 */
AI_STATIC ai_array conv2d_10_scratch0_array;   /* Array #55 */
AI_STATIC ai_array conv2d_9_scratch1_array;   /* Array #56 */
AI_STATIC ai_array conv2d_9_scratch0_array;   /* Array #57 */
AI_STATIC ai_array conv2d_8_scratch0_array;   /* Array #58 */
AI_STATIC ai_array conv2d_7_scratch1_array;   /* Array #59 */
AI_STATIC ai_array conv2d_7_scratch0_array;   /* Array #60 */
AI_STATIC ai_array conv2d_5_scratch1_array;   /* Array #61 */
AI_STATIC ai_array conv2d_5_scratch0_array;   /* Array #62 */
AI_STATIC ai_array conv2d_4_scratch0_array;   /* Array #63 */
AI_STATIC ai_array conv2d_3_scratch1_array;   /* Array #64 */
AI_STATIC ai_array conv2d_3_scratch0_array;   /* Array #65 */
AI_STATIC ai_array conv2d_2_scratch1_array;   /* Array #66 */
AI_STATIC ai_array conv2d_2_scratch0_array;   /* Array #67 */
AI_STATIC ai_array conv2d_53_bias_array;   /* Array #68 */
AI_STATIC ai_array conv2d_53_weights_array;   /* Array #69 */
AI_STATIC ai_array conv2d_52_bias_array;   /* Array #70 */
AI_STATIC ai_array conv2d_52_weights_array;   /* Array #71 */
AI_STATIC ai_array conv2d_50_bias_array;   /* Array #72 */
AI_STATIC ai_array conv2d_50_weights_array;   /* Array #73 */
AI_STATIC ai_array conv2d_49_bias_array;   /* Array #74 */
AI_STATIC ai_array conv2d_49_weights_array;   /* Array #75 */
AI_STATIC ai_array conv2d_48_bias_array;   /* Array #76 */
AI_STATIC ai_array conv2d_48_weights_array;   /* Array #77 */
AI_STATIC ai_array conv2d_46_bias_array;   /* Array #78 */
AI_STATIC ai_array conv2d_46_weights_array;   /* Array #79 */
AI_STATIC ai_array conv2d_45_bias_array;   /* Array #80 */
AI_STATIC ai_array conv2d_45_weights_array;   /* Array #81 */
AI_STATIC ai_array conv2d_44_bias_array;   /* Array #82 */
AI_STATIC ai_array conv2d_44_weights_array;   /* Array #83 */
AI_STATIC ai_array conv2d_43_bias_array;   /* Array #84 */
AI_STATIC ai_array conv2d_43_weights_array;   /* Array #85 */
AI_STATIC ai_array conv2d_42_bias_array;   /* Array #86 */
AI_STATIC ai_array conv2d_42_weights_array;   /* Array #87 */
AI_STATIC ai_array conv2d_41_bias_array;   /* Array #88 */
AI_STATIC ai_array conv2d_41_weights_array;   /* Array #89 */
AI_STATIC ai_array conv2d_39_bias_array;   /* Array #90 */
AI_STATIC ai_array conv2d_39_weights_array;   /* Array #91 */
AI_STATIC ai_array conv2d_38_bias_array;   /* Array #92 */
AI_STATIC ai_array conv2d_38_weights_array;   /* Array #93 */
AI_STATIC ai_array conv2d_37_bias_array;   /* Array #94 */
AI_STATIC ai_array conv2d_37_weights_array;   /* Array #95 */
AI_STATIC ai_array conv2d_35_bias_array;   /* Array #96 */
AI_STATIC ai_array conv2d_35_weights_array;   /* Array #97 */
AI_STATIC ai_array conv2d_34_bias_array;   /* Array #98 */
AI_STATIC ai_array conv2d_34_weights_array;   /* Array #99 */
AI_STATIC ai_array conv2d_33_bias_array;   /* Array #100 */
AI_STATIC ai_array conv2d_33_weights_array;   /* Array #101 */
AI_STATIC ai_array conv2d_31_bias_array;   /* Array #102 */
AI_STATIC ai_array conv2d_31_weights_array;   /* Array #103 */
AI_STATIC ai_array conv2d_30_bias_array;   /* Array #104 */
AI_STATIC ai_array conv2d_30_weights_array;   /* Array #105 */
AI_STATIC ai_array conv2d_29_bias_array;   /* Array #106 */
AI_STATIC ai_array conv2d_29_weights_array;   /* Array #107 */
AI_STATIC ai_array conv2d_28_bias_array;   /* Array #108 */
AI_STATIC ai_array conv2d_28_weights_array;   /* Array #109 */
AI_STATIC ai_array conv2d_27_bias_array;   /* Array #110 */
AI_STATIC ai_array conv2d_27_weights_array;   /* Array #111 */
AI_STATIC ai_array conv2d_25_bias_array;   /* Array #112 */
AI_STATIC ai_array conv2d_25_weights_array;   /* Array #113 */
AI_STATIC ai_array conv2d_23_bias_array;   /* Array #114 */
AI_STATIC ai_array conv2d_23_weights_array;   /* Array #115 */
AI_STATIC ai_array conv2d_22_bias_array;   /* Array #116 */
AI_STATIC ai_array conv2d_22_weights_array;   /* Array #117 */
AI_STATIC ai_array conv2d_21_bias_array;   /* Array #118 */
AI_STATIC ai_array conv2d_21_weights_array;   /* Array #119 */
AI_STATIC ai_array conv2d_19_bias_array;   /* Array #120 */
AI_STATIC ai_array conv2d_19_weights_array;   /* Array #121 */
AI_STATIC ai_array conv2d_18_bias_array;   /* Array #122 */
AI_STATIC ai_array conv2d_18_weights_array;   /* Array #123 */
AI_STATIC ai_array conv2d_17_bias_array;   /* Array #124 */
AI_STATIC ai_array conv2d_17_weights_array;   /* Array #125 */
AI_STATIC ai_array conv2d_16_bias_array;   /* Array #126 */
AI_STATIC ai_array conv2d_16_weights_array;   /* Array #127 */
AI_STATIC ai_array conv2d_15_bias_array;   /* Array #128 */
AI_STATIC ai_array conv2d_15_weights_array;   /* Array #129 */
AI_STATIC ai_array conv2d_13_bias_array;   /* Array #130 */
AI_STATIC ai_array conv2d_13_weights_array;   /* Array #131 */
AI_STATIC ai_array conv2d_11_bias_array;   /* Array #132 */
AI_STATIC ai_array conv2d_11_weights_array;   /* Array #133 */
AI_STATIC ai_array conv2d_10_bias_array;   /* Array #134 */
AI_STATIC ai_array conv2d_10_weights_array;   /* Array #135 */
AI_STATIC ai_array conv2d_9_bias_array;   /* Array #136 */
AI_STATIC ai_array conv2d_9_weights_array;   /* Array #137 */
AI_STATIC ai_array conv2d_8_bias_array;   /* Array #138 */
AI_STATIC ai_array conv2d_8_weights_array;   /* Array #139 */
AI_STATIC ai_array conv2d_7_bias_array;   /* Array #140 */
AI_STATIC ai_array conv2d_7_weights_array;   /* Array #141 */
AI_STATIC ai_array conv2d_5_bias_array;   /* Array #142 */
AI_STATIC ai_array conv2d_5_weights_array;   /* Array #143 */
AI_STATIC ai_array conv2d_4_bias_array;   /* Array #144 */
AI_STATIC ai_array conv2d_4_weights_array;   /* Array #145 */
AI_STATIC ai_array conv2d_3_bias_array;   /* Array #146 */
AI_STATIC ai_array conv2d_3_weights_array;   /* Array #147 */
AI_STATIC ai_array conv2d_2_bias_array;   /* Array #148 */
AI_STATIC ai_array conv2d_2_weights_array;   /* Array #149 */
AI_STATIC ai_array input_0_output_array;   /* Array #150 */
AI_STATIC ai_array conversion_0_output_array;   /* Array #151 */
AI_STATIC ai_array conv2d_2_output_array;   /* Array #152 */
AI_STATIC ai_array conv2d_3_output_array;   /* Array #153 */
AI_STATIC ai_array conv2d_4_output_array;   /* Array #154 */
AI_STATIC ai_array conv2d_5_output_array;   /* Array #155 */
AI_STATIC ai_array conv2d_7_output_array;   /* Array #156 */
AI_STATIC ai_array conv2d_8_output_array;   /* Array #157 */
AI_STATIC ai_array conv2d_9_output_array;   /* Array #158 */
AI_STATIC ai_array conv2d_10_output_array;   /* Array #159 */
AI_STATIC ai_array conv2d_11_output_array;   /* Array #160 */
AI_STATIC ai_array eltwise_12_output_array;   /* Array #161 */
AI_STATIC ai_array conv2d_13_output_array;   /* Array #162 */
AI_STATIC ai_array conv2d_15_output_array;   /* Array #163 */
AI_STATIC ai_array conv2d_16_output_array;   /* Array #164 */
AI_STATIC ai_array conv2d_17_output_array;   /* Array #165 */
AI_STATIC ai_array conv2d_18_output_array;   /* Array #166 */
AI_STATIC ai_array conv2d_19_output_array;   /* Array #167 */
AI_STATIC ai_array eltwise_20_output_array;   /* Array #168 */
AI_STATIC ai_array conv2d_21_output_array;   /* Array #169 */
AI_STATIC ai_array conv2d_22_output_array;   /* Array #170 */
AI_STATIC ai_array conv2d_23_output_array;   /* Array #171 */
AI_STATIC ai_array eltwise_24_output_array;   /* Array #172 */
AI_STATIC ai_array conv2d_25_output_array;   /* Array #173 */
AI_STATIC ai_array conv2d_27_output_array;   /* Array #174 */
AI_STATIC ai_array conv2d_28_output_array;   /* Array #175 */
AI_STATIC ai_array conv2d_29_output_array;   /* Array #176 */
AI_STATIC ai_array conv2d_30_output_array;   /* Array #177 */
AI_STATIC ai_array conv2d_31_output_array;   /* Array #178 */
AI_STATIC ai_array eltwise_32_output_array;   /* Array #179 */
AI_STATIC ai_array conv2d_33_output_array;   /* Array #180 */
AI_STATIC ai_array conv2d_34_output_array;   /* Array #181 */
AI_STATIC ai_array conv2d_35_output_array;   /* Array #182 */
AI_STATIC ai_array eltwise_36_output_array;   /* Array #183 */
AI_STATIC ai_array conv2d_37_output_array;   /* Array #184 */
AI_STATIC ai_array conv2d_38_output_array;   /* Array #185 */
AI_STATIC ai_array conv2d_39_output_array;   /* Array #186 */
AI_STATIC ai_array eltwise_40_output_array;   /* Array #187 */
AI_STATIC ai_array conv2d_41_output_array;   /* Array #188 */
AI_STATIC ai_array conv2d_42_output_array;   /* Array #189 */
AI_STATIC ai_array conv2d_43_output_array;   /* Array #190 */
AI_STATIC ai_array conv2d_44_output_array;   /* Array #191 */
AI_STATIC ai_array conv2d_45_output_array;   /* Array #192 */
AI_STATIC ai_array conv2d_46_output_array;   /* Array #193 */
AI_STATIC ai_array eltwise_47_output_array;   /* Array #194 */
AI_STATIC ai_array conv2d_48_output_array;   /* Array #195 */
AI_STATIC ai_array conv2d_49_output_array;   /* Array #196 */
AI_STATIC ai_array conv2d_50_output_array;   /* Array #197 */
AI_STATIC ai_array eltwise_51_output_array;   /* Array #198 */
AI_STATIC ai_array conv2d_52_output_array;   /* Array #199 */
AI_STATIC ai_array conv2d_53_output_array;   /* Array #200 */
AI_STATIC ai_array conversion_55_output_array;   /* Array #201 */


/**  Forward network tensor declarations  *************************************/
AI_STATIC ai_tensor conv2d_53_scratch0;   /* Tensor #0 */
AI_STATIC ai_tensor conv2d_52_scratch1;   /* Tensor #1 */
AI_STATIC ai_tensor conv2d_52_scratch0;   /* Tensor #2 */
AI_STATIC ai_tensor conv2d_50_scratch0;   /* Tensor #3 */
AI_STATIC ai_tensor conv2d_49_scratch1;   /* Tensor #4 */
AI_STATIC ai_tensor conv2d_49_scratch0;   /* Tensor #5 */
AI_STATIC ai_tensor conv2d_48_scratch1;   /* Tensor #6 */
AI_STATIC ai_tensor conv2d_48_scratch0;   /* Tensor #7 */
AI_STATIC ai_tensor conv2d_46_scratch0;   /* Tensor #8 */
AI_STATIC ai_tensor conv2d_45_scratch1;   /* Tensor #9 */
AI_STATIC ai_tensor conv2d_45_scratch0;   /* Tensor #10 */
AI_STATIC ai_tensor conv2d_44_scratch1;   /* Tensor #11 */
AI_STATIC ai_tensor conv2d_44_scratch0;   /* Tensor #12 */
AI_STATIC ai_tensor conv2d_43_scratch0;   /* Tensor #13 */
AI_STATIC ai_tensor conv2d_42_scratch1;   /* Tensor #14 */
AI_STATIC ai_tensor conv2d_42_scratch0;   /* Tensor #15 */
AI_STATIC ai_tensor conv2d_41_scratch1;   /* Tensor #16 */
AI_STATIC ai_tensor conv2d_41_scratch0;   /* Tensor #17 */
AI_STATIC ai_tensor conv2d_39_scratch0;   /* Tensor #18 */
AI_STATIC ai_tensor conv2d_38_scratch1;   /* Tensor #19 */
AI_STATIC ai_tensor conv2d_38_scratch0;   /* Tensor #20 */
AI_STATIC ai_tensor conv2d_37_scratch1;   /* Tensor #21 */
AI_STATIC ai_tensor conv2d_37_scratch0;   /* Tensor #22 */
AI_STATIC ai_tensor conv2d_35_scratch0;   /* Tensor #23 */
AI_STATIC ai_tensor conv2d_34_scratch1;   /* Tensor #24 */
AI_STATIC ai_tensor conv2d_34_scratch0;   /* Tensor #25 */
AI_STATIC ai_tensor conv2d_33_scratch1;   /* Tensor #26 */
AI_STATIC ai_tensor conv2d_33_scratch0;   /* Tensor #27 */
AI_STATIC ai_tensor conv2d_31_scratch0;   /* Tensor #28 */
AI_STATIC ai_tensor conv2d_30_scratch1;   /* Tensor #29 */
AI_STATIC ai_tensor conv2d_30_scratch0;   /* Tensor #30 */
AI_STATIC ai_tensor conv2d_29_scratch1;   /* Tensor #31 */
AI_STATIC ai_tensor conv2d_29_scratch0;   /* Tensor #32 */
AI_STATIC ai_tensor conv2d_28_scratch0;   /* Tensor #33 */
AI_STATIC ai_tensor conv2d_27_scratch1;   /* Tensor #34 */
AI_STATIC ai_tensor conv2d_27_scratch0;   /* Tensor #35 */
AI_STATIC ai_tensor conv2d_25_scratch1;   /* Tensor #36 */
AI_STATIC ai_tensor conv2d_25_scratch0;   /* Tensor #37 */
AI_STATIC ai_tensor conv2d_23_scratch0;   /* Tensor #38 */
AI_STATIC ai_tensor conv2d_22_scratch1;   /* Tensor #39 */
AI_STATIC ai_tensor conv2d_22_scratch0;   /* Tensor #40 */
AI_STATIC ai_tensor conv2d_21_scratch1;   /* Tensor #41 */
AI_STATIC ai_tensor conv2d_21_scratch0;   /* Tensor #42 */
AI_STATIC ai_tensor conv2d_19_scratch0;   /* Tensor #43 */
AI_STATIC ai_tensor conv2d_18_scratch1;   /* Tensor #44 */
AI_STATIC ai_tensor conv2d_18_scratch0;   /* Tensor #45 */
AI_STATIC ai_tensor conv2d_17_scratch1;   /* Tensor #46 */
AI_STATIC ai_tensor conv2d_17_scratch0;   /* Tensor #47 */
AI_STATIC ai_tensor conv2d_16_scratch0;   /* Tensor #48 */
AI_STATIC ai_tensor conv2d_15_scratch1;   /* Tensor #49 */
AI_STATIC ai_tensor conv2d_15_scratch0;   /* Tensor #50 */
AI_STATIC ai_tensor conv2d_13_scratch1;   /* Tensor #51 */
AI_STATIC ai_tensor conv2d_13_scratch0;   /* Tensor #52 */
AI_STATIC ai_tensor conv2d_11_scratch0;   /* Tensor #53 */
AI_STATIC ai_tensor conv2d_10_scratch1;   /* Tensor #54 */
AI_STATIC ai_tensor conv2d_10_scratch0;   /* Tensor #55 */
AI_STATIC ai_tensor conv2d_9_scratch1;   /* Tensor #56 */
AI_STATIC ai_tensor conv2d_9_scratch0;   /* Tensor #57 */
AI_STATIC ai_tensor conv2d_8_scratch0;   /* Tensor #58 */
AI_STATIC ai_tensor conv2d_7_scratch1;   /* Tensor #59 */
AI_STATIC ai_tensor conv2d_7_scratch0;   /* Tensor #60 */
AI_STATIC ai_tensor conv2d_5_scratch1;   /* Tensor #61 */
AI_STATIC ai_tensor conv2d_5_scratch0;   /* Tensor #62 */
AI_STATIC ai_tensor conv2d_4_scratch0;   /* Tensor #63 */
AI_STATIC ai_tensor conv2d_3_scratch1;   /* Tensor #64 */
AI_STATIC ai_tensor conv2d_3_scratch0;   /* Tensor #65 */
AI_STATIC ai_tensor conv2d_2_scratch1;   /* Tensor #66 */
AI_STATIC ai_tensor conv2d_2_scratch0;   /* Tensor #67 */
AI_STATIC ai_tensor conv2d_53_bias;   /* Tensor #68 */
AI_STATIC ai_tensor conv2d_53_weights;   /* Tensor #69 */
AI_STATIC ai_tensor conv2d_52_bias;   /* Tensor #70 */
AI_STATIC ai_tensor conv2d_52_weights;   /* Tensor #71 */
AI_STATIC ai_tensor conv2d_50_bias;   /* Tensor #72 */
AI_STATIC ai_tensor conv2d_50_weights;   /* Tensor #73 */
AI_STATIC ai_tensor conv2d_49_bias;   /* Tensor #74 */
AI_STATIC ai_tensor conv2d_49_weights;   /* Tensor #75 */
AI_STATIC ai_tensor conv2d_48_bias;   /* Tensor #76 */
AI_STATIC ai_tensor conv2d_48_weights;   /* Tensor #77 */
AI_STATIC ai_tensor conv2d_46_bias;   /* Tensor #78 */
AI_STATIC ai_tensor conv2d_46_weights;   /* Tensor #79 */
AI_STATIC ai_tensor conv2d_45_bias;   /* Tensor #80 */
AI_STATIC ai_tensor conv2d_45_weights;   /* Tensor #81 */
AI_STATIC ai_tensor conv2d_44_bias;   /* Tensor #82 */
AI_STATIC ai_tensor conv2d_44_weights;   /* Tensor #83 */
AI_STATIC ai_tensor conv2d_43_bias;   /* Tensor #84 */
AI_STATIC ai_tensor conv2d_43_weights;   /* Tensor #85 */
AI_STATIC ai_tensor conv2d_42_bias;   /* Tensor #86 */
AI_STATIC ai_tensor conv2d_42_weights;   /* Tensor #87 */
AI_STATIC ai_tensor conv2d_41_bias;   /* Tensor #88 */
AI_STATIC ai_tensor conv2d_41_weights;   /* Tensor #89 */
AI_STATIC ai_tensor conv2d_39_bias;   /* Tensor #90 */
AI_STATIC ai_tensor conv2d_39_weights;   /* Tensor #91 */
AI_STATIC ai_tensor conv2d_38_bias;   /* Tensor #92 */
AI_STATIC ai_tensor conv2d_38_weights;   /* Tensor #93 */
AI_STATIC ai_tensor conv2d_37_bias;   /* Tensor #94 */
AI_STATIC ai_tensor conv2d_37_weights;   /* Tensor #95 */
AI_STATIC ai_tensor conv2d_35_bias;   /* Tensor #96 */
AI_STATIC ai_tensor conv2d_35_weights;   /* Tensor #97 */
AI_STATIC ai_tensor conv2d_34_bias;   /* Tensor #98 */
AI_STATIC ai_tensor conv2d_34_weights;   /* Tensor #99 */
AI_STATIC ai_tensor conv2d_33_bias;   /* Tensor #100 */
AI_STATIC ai_tensor conv2d_33_weights;   /* Tensor #101 */
AI_STATIC ai_tensor conv2d_31_bias;   /* Tensor #102 */
AI_STATIC ai_tensor conv2d_31_weights;   /* Tensor #103 */
AI_STATIC ai_tensor conv2d_30_bias;   /* Tensor #104 */
AI_STATIC ai_tensor conv2d_30_weights;   /* Tensor #105 */
AI_STATIC ai_tensor conv2d_29_bias;   /* Tensor #106 */
AI_STATIC ai_tensor conv2d_29_weights;   /* Tensor #107 */
AI_STATIC ai_tensor conv2d_28_bias;   /* Tensor #108 */
AI_STATIC ai_tensor conv2d_28_weights;   /* Tensor #109 */
AI_STATIC ai_tensor conv2d_27_bias;   /* Tensor #110 */
AI_STATIC ai_tensor conv2d_27_weights;   /* Tensor #111 */
AI_STATIC ai_tensor conv2d_25_bias;   /* Tensor #112 */
AI_STATIC ai_tensor conv2d_25_weights;   /* Tensor #113 */
AI_STATIC ai_tensor conv2d_23_bias;   /* Tensor #114 */
AI_STATIC ai_tensor conv2d_23_weights;   /* Tensor #115 */
AI_STATIC ai_tensor conv2d_22_bias;   /* Tensor #116 */
AI_STATIC ai_tensor conv2d_22_weights;   /* Tensor #117 */
AI_STATIC ai_tensor conv2d_21_bias;   /* Tensor #118 */
AI_STATIC ai_tensor conv2d_21_weights;   /* Tensor #119 */
AI_STATIC ai_tensor conv2d_19_bias;   /* Tensor #120 */
AI_STATIC ai_tensor conv2d_19_weights;   /* Tensor #121 */
AI_STATIC ai_tensor conv2d_18_bias;   /* Tensor #122 */
AI_STATIC ai_tensor conv2d_18_weights;   /* Tensor #123 */
AI_STATIC ai_tensor conv2d_17_bias;   /* Tensor #124 */
AI_STATIC ai_tensor conv2d_17_weights;   /* Tensor #125 */
AI_STATIC ai_tensor conv2d_16_bias;   /* Tensor #126 */
AI_STATIC ai_tensor conv2d_16_weights;   /* Tensor #127 */
AI_STATIC ai_tensor conv2d_15_bias;   /* Tensor #128 */
AI_STATIC ai_tensor conv2d_15_weights;   /* Tensor #129 */
AI_STATIC ai_tensor conv2d_13_bias;   /* Tensor #130 */
AI_STATIC ai_tensor conv2d_13_weights;   /* Tensor #131 */
AI_STATIC ai_tensor conv2d_11_bias;   /* Tensor #132 */
AI_STATIC ai_tensor conv2d_11_weights;   /* Tensor #133 */
AI_STATIC ai_tensor conv2d_10_bias;   /* Tensor #134 */
AI_STATIC ai_tensor conv2d_10_weights;   /* Tensor #135 */
AI_STATIC ai_tensor conv2d_9_bias;   /* Tensor #136 */
AI_STATIC ai_tensor conv2d_9_weights;   /* Tensor #137 */
AI_STATIC ai_tensor conv2d_8_bias;   /* Tensor #138 */
AI_STATIC ai_tensor conv2d_8_weights;   /* Tensor #139 */
AI_STATIC ai_tensor conv2d_7_bias;   /* Tensor #140 */
AI_STATIC ai_tensor conv2d_7_weights;   /* Tensor #141 */
AI_STATIC ai_tensor conv2d_5_bias;   /* Tensor #142 */
AI_STATIC ai_tensor conv2d_5_weights;   /* Tensor #143 */
AI_STATIC ai_tensor conv2d_4_bias;   /* Tensor #144 */
AI_STATIC ai_tensor conv2d_4_weights;   /* Tensor #145 */
AI_STATIC ai_tensor conv2d_3_bias;   /* Tensor #146 */
AI_STATIC ai_tensor conv2d_3_weights;   /* Tensor #147 */
AI_STATIC ai_tensor conv2d_2_bias;   /* Tensor #148 */
AI_STATIC ai_tensor conv2d_2_weights;   /* Tensor #149 */
AI_STATIC ai_tensor input_0_output;   /* Tensor #150 */
AI_STATIC ai_tensor conversion_0_output;   /* Tensor #151 */
AI_STATIC ai_tensor conv2d_2_output;   /* Tensor #152 */
AI_STATIC ai_tensor conv2d_3_output;   /* Tensor #153 */
AI_STATIC ai_tensor conv2d_4_output;   /* Tensor #154 */
AI_STATIC ai_tensor conv2d_5_output;   /* Tensor #155 */
AI_STATIC ai_tensor conv2d_7_output;   /* Tensor #156 */
AI_STATIC ai_tensor conv2d_8_output;   /* Tensor #157 */
AI_STATIC ai_tensor conv2d_9_output;   /* Tensor #158 */
AI_STATIC ai_tensor conv2d_10_output;   /* Tensor #159 */
AI_STATIC ai_tensor conv2d_11_output;   /* Tensor #160 */
AI_STATIC ai_tensor eltwise_12_output;   /* Tensor #161 */
AI_STATIC ai_tensor conv2d_13_output;   /* Tensor #162 */
AI_STATIC ai_tensor conv2d_15_output;   /* Tensor #163 */
AI_STATIC ai_tensor conv2d_16_output;   /* Tensor #164 */
AI_STATIC ai_tensor conv2d_17_output;   /* Tensor #165 */
AI_STATIC ai_tensor conv2d_18_output;   /* Tensor #166 */
AI_STATIC ai_tensor conv2d_19_output;   /* Tensor #167 */
AI_STATIC ai_tensor eltwise_20_output;   /* Tensor #168 */
AI_STATIC ai_tensor conv2d_21_output;   /* Tensor #169 */
AI_STATIC ai_tensor conv2d_22_output;   /* Tensor #170 */
AI_STATIC ai_tensor conv2d_23_output;   /* Tensor #171 */
AI_STATIC ai_tensor eltwise_24_output;   /* Tensor #172 */
AI_STATIC ai_tensor conv2d_25_output;   /* Tensor #173 */
AI_STATIC ai_tensor conv2d_27_output;   /* Tensor #174 */
AI_STATIC ai_tensor conv2d_28_output;   /* Tensor #175 */
AI_STATIC ai_tensor conv2d_29_output;   /* Tensor #176 */
AI_STATIC ai_tensor conv2d_30_output;   /* Tensor #177 */
AI_STATIC ai_tensor conv2d_31_output;   /* Tensor #178 */
AI_STATIC ai_tensor eltwise_32_output;   /* Tensor #179 */
AI_STATIC ai_tensor conv2d_33_output;   /* Tensor #180 */
AI_STATIC ai_tensor conv2d_34_output;   /* Tensor #181 */
AI_STATIC ai_tensor conv2d_35_output;   /* Tensor #182 */
AI_STATIC ai_tensor eltwise_36_output;   /* Tensor #183 */
AI_STATIC ai_tensor conv2d_37_output;   /* Tensor #184 */
AI_STATIC ai_tensor conv2d_38_output;   /* Tensor #185 */
AI_STATIC ai_tensor conv2d_39_output;   /* Tensor #186 */
AI_STATIC ai_tensor eltwise_40_output;   /* Tensor #187 */
AI_STATIC ai_tensor conv2d_41_output;   /* Tensor #188 */
AI_STATIC ai_tensor conv2d_42_output;   /* Tensor #189 */
AI_STATIC ai_tensor conv2d_43_output;   /* Tensor #190 */
AI_STATIC ai_tensor conv2d_44_output;   /* Tensor #191 */
AI_STATIC ai_tensor conv2d_45_output;   /* Tensor #192 */
AI_STATIC ai_tensor conv2d_46_output;   /* Tensor #193 */
AI_STATIC ai_tensor eltwise_47_output;   /* Tensor #194 */
AI_STATIC ai_tensor conv2d_48_output;   /* Tensor #195 */
AI_STATIC ai_tensor conv2d_49_output;   /* Tensor #196 */
AI_STATIC ai_tensor conv2d_50_output;   /* Tensor #197 */
AI_STATIC ai_tensor eltwise_51_output;   /* Tensor #198 */
AI_STATIC ai_tensor conv2d_52_output;   /* Tensor #199 */
AI_STATIC ai_tensor conv2d_53_output;   /* Tensor #200 */
AI_STATIC ai_tensor conversion_55_output;   /* Tensor #201 */


/**  Forward network tensor chain declarations  *******************************/
AI_STATIC_CONST ai_tensor_chain conversion_0_chain;   /* Chain #0 */
AI_STATIC_CONST ai_tensor_chain conv2d_2_chain;   /* Chain #1 */
AI_STATIC_CONST ai_tensor_chain conv2d_3_chain;   /* Chain #2 */
AI_STATIC_CONST ai_tensor_chain conv2d_4_chain;   /* Chain #3 */
AI_STATIC_CONST ai_tensor_chain conv2d_5_chain;   /* Chain #4 */
AI_STATIC_CONST ai_tensor_chain conv2d_7_chain;   /* Chain #5 */
AI_STATIC_CONST ai_tensor_chain conv2d_8_chain;   /* Chain #6 */
AI_STATIC_CONST ai_tensor_chain conv2d_9_chain;   /* Chain #7 */
AI_STATIC_CONST ai_tensor_chain conv2d_10_chain;   /* Chain #8 */
AI_STATIC_CONST ai_tensor_chain conv2d_11_chain;   /* Chain #9 */
AI_STATIC_CONST ai_tensor_chain eltwise_12_chain;   /* Chain #10 */
AI_STATIC_CONST ai_tensor_chain conv2d_13_chain;   /* Chain #11 */
AI_STATIC_CONST ai_tensor_chain conv2d_15_chain;   /* Chain #12 */
AI_STATIC_CONST ai_tensor_chain conv2d_16_chain;   /* Chain #13 */
AI_STATIC_CONST ai_tensor_chain conv2d_17_chain;   /* Chain #14 */
AI_STATIC_CONST ai_tensor_chain conv2d_18_chain;   /* Chain #15 */
AI_STATIC_CONST ai_tensor_chain conv2d_19_chain;   /* Chain #16 */
AI_STATIC_CONST ai_tensor_chain eltwise_20_chain;   /* Chain #17 */
AI_STATIC_CONST ai_tensor_chain conv2d_21_chain;   /* Chain #18 */
AI_STATIC_CONST ai_tensor_chain conv2d_22_chain;   /* Chain #19 */
AI_STATIC_CONST ai_tensor_chain conv2d_23_chain;   /* Chain #20 */
AI_STATIC_CONST ai_tensor_chain eltwise_24_chain;   /* Chain #21 */
AI_STATIC_CONST ai_tensor_chain conv2d_25_chain;   /* Chain #22 */
AI_STATIC_CONST ai_tensor_chain conv2d_27_chain;   /* Chain #23 */
AI_STATIC_CONST ai_tensor_chain conv2d_28_chain;   /* Chain #24 */
AI_STATIC_CONST ai_tensor_chain conv2d_29_chain;   /* Chain #25 */
AI_STATIC_CONST ai_tensor_chain conv2d_30_chain;   /* Chain #26 */
AI_STATIC_CONST ai_tensor_chain conv2d_31_chain;   /* Chain #27 */
AI_STATIC_CONST ai_tensor_chain eltwise_32_chain;   /* Chain #28 */
AI_STATIC_CONST ai_tensor_chain conv2d_33_chain;   /* Chain #29 */
AI_STATIC_CONST ai_tensor_chain conv2d_34_chain;   /* Chain #30 */
AI_STATIC_CONST ai_tensor_chain conv2d_35_chain;   /* Chain #31 */
AI_STATIC_CONST ai_tensor_chain eltwise_36_chain;   /* Chain #32 */
AI_STATIC_CONST ai_tensor_chain conv2d_37_chain;   /* Chain #33 */
AI_STATIC_CONST ai_tensor_chain conv2d_38_chain;   /* Chain #34 */
AI_STATIC_CONST ai_tensor_chain conv2d_39_chain;   /* Chain #35 */
AI_STATIC_CONST ai_tensor_chain eltwise_40_chain;   /* Chain #36 */
AI_STATIC_CONST ai_tensor_chain conv2d_41_chain;   /* Chain #37 */
AI_STATIC_CONST ai_tensor_chain conv2d_42_chain;   /* Chain #38 */
AI_STATIC_CONST ai_tensor_chain conv2d_43_chain;   /* Chain #39 */
AI_STATIC_CONST ai_tensor_chain conv2d_44_chain;   /* Chain #40 */
AI_STATIC_CONST ai_tensor_chain conv2d_45_chain;   /* Chain #41 */
AI_STATIC_CONST ai_tensor_chain conv2d_46_chain;   /* Chain #42 */
AI_STATIC_CONST ai_tensor_chain eltwise_47_chain;   /* Chain #43 */
AI_STATIC_CONST ai_tensor_chain conv2d_48_chain;   /* Chain #44 */
AI_STATIC_CONST ai_tensor_chain conv2d_49_chain;   /* Chain #45 */
AI_STATIC_CONST ai_tensor_chain conv2d_50_chain;   /* Chain #46 */
AI_STATIC_CONST ai_tensor_chain eltwise_51_chain;   /* Chain #47 */
AI_STATIC_CONST ai_tensor_chain conv2d_52_chain;   /* Chain #48 */
AI_STATIC_CONST ai_tensor_chain conv2d_53_chain;   /* Chain #49 */
AI_STATIC_CONST ai_tensor_chain conversion_55_chain;   /* Chain #50 */


/**  Forward network layer declarations  **************************************/
AI_STATIC ai_layer_nl conversion_0_layer; /* Layer #0 */
AI_STATIC ai_layer_conv2d conv2d_2_layer; /* Layer #1 */
AI_STATIC ai_layer_conv2d conv2d_3_layer; /* Layer #2 */
AI_STATIC ai_layer_conv2d conv2d_4_layer; /* Layer #3 */
AI_STATIC ai_layer_conv2d conv2d_5_layer; /* Layer #4 */
AI_STATIC ai_layer_conv2d conv2d_7_layer; /* Layer #5 */
AI_STATIC ai_layer_conv2d conv2d_8_layer; /* Layer #6 */
AI_STATIC ai_layer_conv2d conv2d_9_layer; /* Layer #7 */
AI_STATIC ai_layer_conv2d conv2d_10_layer; /* Layer #8 */
AI_STATIC ai_layer_conv2d conv2d_11_layer; /* Layer #9 */
AI_STATIC ai_layer_eltwise eltwise_12_layer; /* Layer #10 */
AI_STATIC ai_layer_conv2d conv2d_13_layer; /* Layer #11 */
AI_STATIC ai_layer_conv2d conv2d_15_layer; /* Layer #12 */
AI_STATIC ai_layer_conv2d conv2d_16_layer; /* Layer #13 */
AI_STATIC ai_layer_conv2d conv2d_17_layer; /* Layer #14 */
AI_STATIC ai_layer_conv2d conv2d_18_layer; /* Layer #15 */
AI_STATIC ai_layer_conv2d conv2d_19_layer; /* Layer #16 */
AI_STATIC ai_layer_eltwise eltwise_20_layer; /* Layer #17 */
AI_STATIC ai_layer_conv2d conv2d_21_layer; /* Layer #18 */
AI_STATIC ai_layer_conv2d conv2d_22_layer; /* Layer #19 */
AI_STATIC ai_layer_conv2d conv2d_23_layer; /* Layer #20 */
AI_STATIC ai_layer_eltwise eltwise_24_layer; /* Layer #21 */
AI_STATIC ai_layer_conv2d conv2d_25_layer; /* Layer #22 */
AI_STATIC ai_layer_conv2d conv2d_27_layer; /* Layer #23 */
AI_STATIC ai_layer_conv2d conv2d_28_layer; /* Layer #24 */
AI_STATIC ai_layer_conv2d conv2d_29_layer; /* Layer #25 */
AI_STATIC ai_layer_conv2d conv2d_30_layer; /* Layer #26 */
AI_STATIC ai_layer_conv2d conv2d_31_layer; /* Layer #27 */
AI_STATIC ai_layer_eltwise eltwise_32_layer; /* Layer #28 */
AI_STATIC ai_layer_conv2d conv2d_33_layer; /* Layer #29 */
AI_STATIC ai_layer_conv2d conv2d_34_layer; /* Layer #30 */
AI_STATIC ai_layer_conv2d conv2d_35_layer; /* Layer #31 */
AI_STATIC ai_layer_eltwise eltwise_36_layer; /* Layer #32 */
AI_STATIC ai_layer_conv2d conv2d_37_layer; /* Layer #33 */
AI_STATIC ai_layer_conv2d conv2d_38_layer; /* Layer #34 */
AI_STATIC ai_layer_conv2d conv2d_39_layer; /* Layer #35 */
AI_STATIC ai_layer_eltwise eltwise_40_layer; /* Layer #36 */
AI_STATIC ai_layer_conv2d conv2d_41_layer; /* Layer #37 */
AI_STATIC ai_layer_conv2d conv2d_42_layer; /* Layer #38 */
AI_STATIC ai_layer_conv2d conv2d_43_layer; /* Layer #39 */
AI_STATIC ai_layer_conv2d conv2d_44_layer; /* Layer #40 */
AI_STATIC ai_layer_conv2d conv2d_45_layer; /* Layer #41 */
AI_STATIC ai_layer_conv2d conv2d_46_layer; /* Layer #42 */
AI_STATIC ai_layer_eltwise eltwise_47_layer; /* Layer #43 */
AI_STATIC ai_layer_conv2d conv2d_48_layer; /* Layer #44 */
AI_STATIC ai_layer_conv2d conv2d_49_layer; /* Layer #45 */
AI_STATIC ai_layer_conv2d conv2d_50_layer; /* Layer #46 */
AI_STATIC ai_layer_eltwise eltwise_51_layer; /* Layer #47 */
AI_STATIC ai_layer_conv2d conv2d_52_layer; /* Layer #48 */
AI_STATIC ai_layer_conv2d conv2d_53_layer; /* Layer #49 */
AI_STATIC ai_layer_nl conversion_55_layer; /* Layer #50 */


/**  Array declarations section  **********************************************/
AI_ARRAY_OBJ_DECLARE(
    conv2d_53_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1502,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_52_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_52_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_50_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1632,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_49_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_49_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 10657,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_48_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_48_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_46_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1632,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_45_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_45_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 10657,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_44_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_44_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_43_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1248,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_42_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_42_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 7105,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_41_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_41_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 2048,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_39_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1088,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_38_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_38_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 7105,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_37_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_37_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 2048,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_35_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1088,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_34_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_34_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 7105,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_33_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_33_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 2048,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_31_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1088,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_30_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_30_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 7105,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_29_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_29_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 2048,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_28_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 704,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_27_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_27_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3553,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_25_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_25_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_23_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 544,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_22_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_22_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3553,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_21_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_21_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_19_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 544,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_18_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_18_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3553,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_17_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_17_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_16_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 544,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_15_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_15_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3553,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_13_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 24576,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_13_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_11_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 544,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_10_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 24576,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_10_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3553,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_9_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 24576,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_9_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_8_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 352,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 12288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1777,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 49152,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 16384,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 593,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_scratch1_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 16384,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_scratch0_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 332,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_53_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 35,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_53_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 10080,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_52_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_52_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 13824,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_50_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 48,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_50_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 13824,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_49_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_49_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 2592,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_48_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_48_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 13824,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_46_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 48,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_46_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 13824,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_45_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_45_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 2592,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_44_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_44_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 13824,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_43_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 48,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_43_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 9216,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_42_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_42_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1728,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_41_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_41_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_39_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 32,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_39_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_38_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_38_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1728,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_37_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_37_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_35_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 32,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_35_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_34_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_34_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1728,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_33_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_33_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_31_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 32,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_31_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_30_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_30_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1728,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_29_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_29_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_28_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 32,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_28_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_27_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_27_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 864,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_25_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_25_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_23_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_23_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_22_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_22_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 864,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_21_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_21_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_19_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_19_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_18_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_18_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 864,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_17_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_17_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_16_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_16_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_15_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_15_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 864,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_13_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_13_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_11_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_11_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_10_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_10_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 864,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_9_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 96,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_9_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_8_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_8_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 768,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 48,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 432,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 48,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 384,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 8,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 128,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_bias_array, AI_ARRAY_FORMAT_S32,
    NULL, NULL, 16,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_weights_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 432,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    input_0_output_array, AI_ARRAY_FORMAT_FLOAT|AI_FMT_FLAG_IS_IO,
    NULL, NULL, 12288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conversion_0_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 12288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 16384,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 16384,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_4_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 8192,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 49152,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 12288,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_8_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4096,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_9_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 24576,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_10_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 24576,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_11_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4096,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_12_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4096,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_13_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 24576,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_15_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_16_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_17_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_18_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_19_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_20_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_21_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_22_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_23_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_24_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1024,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_25_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 6144,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_27_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 1536,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_28_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_29_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_30_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_31_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_32_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_33_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_34_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_35_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_36_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_37_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_38_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_39_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_40_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 512,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_41_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_42_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 3072,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_43_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 768,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_44_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_45_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_46_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 768,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_47_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 768,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_48_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_49_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_50_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 768,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    eltwise_51_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 768,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_52_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 4608,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conv2d_53_output_array, AI_ARRAY_FORMAT_S8,
    NULL, NULL, 560,
     AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
    conversion_55_output_array, AI_ARRAY_FORMAT_FLOAT|AI_FMT_FLAG_IS_IO,
    NULL, NULL, 560,
     AI_STATIC)


AI_STATIC ai_intq_info_list conv2d_52_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #0 */
AI_STATIC ai_intq_info_list conv2d_49_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #1 */
AI_STATIC ai_intq_info_list conv2d_48_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #2 */
AI_STATIC ai_intq_info_list conv2d_45_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #3 */
AI_STATIC ai_intq_info_list conv2d_44_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #4 */
AI_STATIC ai_intq_info_list conv2d_42_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #5 */
AI_STATIC ai_intq_info_list conv2d_41_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #6 */
AI_STATIC ai_intq_info_list conv2d_38_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #7 */
AI_STATIC ai_intq_info_list conv2d_37_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #8 */
AI_STATIC ai_intq_info_list conv2d_34_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #9 */
AI_STATIC ai_intq_info_list conv2d_33_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #10 */
AI_STATIC ai_intq_info_list conv2d_30_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #11 */
AI_STATIC ai_intq_info_list conv2d_29_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #12 */
AI_STATIC ai_intq_info_list conv2d_27_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #13 */
AI_STATIC ai_intq_info_list conv2d_25_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #14 */
AI_STATIC ai_intq_info_list conv2d_22_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #15 */
AI_STATIC ai_intq_info_list conv2d_21_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #16 */
AI_STATIC ai_intq_info_list conv2d_18_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #17 */
AI_STATIC ai_intq_info_list conv2d_17_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #18 */
AI_STATIC ai_intq_info_list conv2d_15_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #19 */
AI_STATIC ai_intq_info_list conv2d_13_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #20 */
AI_STATIC ai_intq_info_list conv2d_10_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #21 */
AI_STATIC ai_intq_info_list conv2d_9_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #22 */
AI_STATIC ai_intq_info_list conv2d_7_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #23 */
AI_STATIC ai_intq_info_list conv2d_5_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #24 */
AI_STATIC ai_intq_info_list conv2d_3_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #25 */
AI_STATIC ai_intq_info_list conv2d_2_scratch1_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #26 */
AI_STATIC ai_intq_info_list conv2d_53_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 35, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(4.22969096689485e-05f, 6.208013655850664e-05f, 3.991892299382016e-05f, 2.4558365112170577e-05f, 5.402783062891103e-05f, 5.3520801884587854e-05f, 6.449104694183916e-05f, 4.392969640321098e-05f, 4.3263589759590104e-05f, 2.8554484742926434e-05f, 3.10051254928112e-05f, 5.665676872013137e-05f, 6.360147381201386e-05f, 6.136231240816414e-05f, 6.223948003025725e-05f, 4.743762474390678e-05f, 3.4103053621947765e-05f, 2.9497663490474224e-05f, 9.111131657846272e-05f, 5.493740536621772e-05f, 6.719509838148952e-05f, 4.515558612183668e-05f, 5.522702485905029e-05f, 3.7366178730735555e-05f, 3.03857395920204e-05f, 6.661553197773173e-05f, 6.988445238675922e-05f, 6.522417243104428e-05f, 5.446490104077384e-05f, 5.179647996556014e-05f, 3.0031111236894503e-05f, 2.7467596737551503e-05f, 5.663596311933361e-05f, 6.0467504226835445e-05f, 6.215664325281978e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #27 */
AI_STATIC ai_intq_info_list conv2d_53_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 35, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0017976186936721206f, 0.002638405654579401f, 0.001696554129011929f, 0.0010437305318191648f, 0.002296182792633772f, 0.0022746340837329626f, 0.0027408695314079523f, 0.0018670120043680072f, 0.0018387025920674205f, 0.0012135655852034688f, 0.0013177178334444761f, 0.0024079126305878162f, 0.002703062491491437f, 0.002607898321002722f, 0.0026451777666807175f, 0.0020160989370197058f, 0.0014493797207251191f, 0.0012536506401374936f, 0.0038722308818250895f, 0.002334839664399624f, 0.002855791477486491f, 0.0019191124010831118f, 0.0023471484892070293f, 0.0015880626160651445f, 0.0012913938844576478f, 0.0028311600908637047f, 0.002970089204609394f, 0.002772027160972357f, 0.0023147582542151213f, 0.0022013504058122635f, 0.0012763221748173237f, 0.0011673728004097939f, 0.002407028339803219f, 0.002569868927821517f, 0.002641657367348671f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #28 */
AI_STATIC ai_intq_info_list conv2d_52_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0003517412405926734f, 0.0002547377662267536f, 0.00018635625019669533f, 0.0002464023418724537f, 0.00025915270089171827f, 0.0001905654207803309f, 0.0001847480598371476f, 0.0002282378263771534f, 0.0004064930835738778f, 0.00026589108165353537f, 0.00013869044778402895f, 0.00024985274649225175f, 0.00023636796686332673f, 0.00023506261641159654f, 0.0001493784657213837f, 5.8506189816398546e-05f, 0.00011839038779726252f, 0.00020700950699392706f, 0.00022644423006568104f, 0.00017645145999267697f, 0.00019565100956242532f, 0.00020374658924993128f, 0.00020817609038203955f, 0.00028378324350342155f, 0.00012533817789517343f, 0.00023424274695571512f, 0.00023114905343391f, 0.00018765486311167479f, 0.00022133270977064967f, 0.00019144071848131716f, 0.00012297708599362522f, 0.00013163457333575934f, 0.00024117896100506186f, 0.00010461041529197246f, 0.0001916667097248137f, 0.00018345590797252953f, 0.00022664539574179798f, 0.00020601876894943416f, 0.00017347329412586987f, 0.00020939312526024878f, 0.000228268705541268f, 0.00015654732123948634f, 0.00033847070881165564f, 0.0003196659090463072f, 0.00017348384426441044f, 0.0001225970045197755f, 0.00023317051818594337f, 0.00011306796659482643f, 0.0002712397836148739f, 0.00021247545373626053f, 0.0002293332654517144f, 0.0002007993753068149f, 0.0002187475038226694f, 0.00033823720877990127f, 0.00023995789524633437f, 0.00022128423734102398f, 0.00018958254077006131f, 0.00023157820396590978f, 0.00026983549469150603f, 0.0004181496915407479f, 0.00023378670448437333f, 0.0001601546537131071f, 0.0003572554560378194f, 0.00018758888472802937f, 0.00015353561320807785f, 0.00015253742458298802f, 0.00017981859855353832f, 0.00015595472359564155f, 0.00028597997152246535f, 0.00014580757124349475f, 0.0002349427668377757f, 0.00022867158986628056f, 0.00022376196284312755f, 0.0002620466402731836f, 0.0003356040979269892f, 0.000265957904048264f, 0.000296142534352839f, 0.00022098170302342623f, 0.00014688208466395736f, 0.0002722960489336401f, 0.000252978119533509f, 0.00022096955217421055f, 0.0002139632124453783f, 0.00020236395357642323f, 0.00019237026572227478f, 0.0002079479454550892f, 0.00029849258135072887f, 0.00023334266734309494f, 0.00013456445594783872f, 0.00025892886333167553f, 0.00019253752543590963f, 0.00027395665529184043f, 0.00023057182261254638f, 0.00028017666772939265f, 0.00016606698045507073f, 0.00020339395268820226f, 0.00029616206302307546f, 0.00022198440274223685f, 0.0001157128281192854f, 0.00022003594494890422f, 0.00022655780776403844f, 0.00010509688581805676f, 0.0003165057278238237f, 0.0002592133532743901f, 8.648857328807935e-05f, 0.0002461975091136992f, 0.00027082115411758423f, 0.0001473742740927264f, 0.00025702620041556656f, 9.68819804256782e-05f, 0.0003424677415750921f, 0.00011785003152908757f, 0.0002052594645647332f, 0.00022145711409393698f, 0.0003441995941102505f, 0.00027227599639445543f, 0.0003551038389559835f, 0.0002228816010756418f, 0.0001821234036469832f, 0.00027599933673627675f, 0.00017693114932626486f, 0.00025535482564009726f, 0.0002362228842684999f, 0.00036542522138915956f, 0.00011270058166701347f, 0.00027648918330669403f, 0.00023847776174079627f, 0.00018891198851633817f, 0.00014826157712377608f, 0.0003146350500173867f, 0.00025648775044828653f, 0.00024036018294282258f, 0.00029228554922156036f, 0.0002554126549512148f, 0.0002233287668786943f, 0.00019798622815869749f, 0.00013770064106211066f, 0.00029725319473072886f, 0.00022202763648238033f, 0.0002730497217271477f, 0.0002783529052976519f, 0.00024401563860010356f, 0.0002113513182848692f, 0.00021218076290097088f, 0.00024175297585316002f, 0.00021742530225310475f, 0.00024583525373600423f, 7.800051389494911e-05f, 0.00032913649920374155f, 0.00018013824592344463f, 0.00015903594612609595f, 0.0002778393682092428f, 0.00015863717999309301f, 0.000161300617037341f, 0.00022177242499310523f, 0.00010238451068289578f, 0.0001885086967376992f, 0.00014262263721320778f, 0.00015090689703356475f, 0.0002585780166555196f, 0.00019510966376401484f, 0.00020604277960956097f, 0.0002776135806925595f, 0.00011845549306599423f, 0.0002322788059245795f, 0.000264930073171854f, 0.00028021869366057217f, 0.0002077707467833534f, 0.0001554812624817714f, 0.00021334046323318034f, 0.00015288151917047799f, 0.00021433879737742245f, 0.00021841833950020373f, 0.00023694217088632286f, 0.00039376935455948114f, 8.413435716647655e-05f, 0.00012251315638422966f, 0.00013528553245123476f, 0.0002778942871373147f, 0.00023948363377712667f, 0.00022562715457752347f, 0.0001618528476683423f, 0.00011393825116101652f, 0.0003408777993172407f, 0.00025906317750923336f, 0.0002426954306429252f, 0.00025866689975373447f, 0.00026529139722697437f, 0.0002339426428079605f, 0.00027339652297087014f, 0.00023258851433638483f, 0.00030076855910010636f, 0.00020959302491974086f, 0.0002141882141586393f, 0.00026061717653647065f, 0.00017922811093740165f, 0.00023377851175609976f, 0.00017360744823236018f, 0.000194590087630786f, 0.00024058607232291251f, 0.0003444039321038872f, 0.0002897036902140826f, 0.00018388600437901914f, 0.00010329417273169383f, 0.00016719168343115598f, 0.00015613232972100377f, 0.00017042222316376865f, 0.00014482461847364902f, 0.0002823218528646976f, 0.0003869206120725721f, 0.00028342712903395295f, 0.0002763726224657148f, 0.00019295256061013788f, 0.00021565718634519726f, 0.0003564988146536052f, 0.0002221528993686661f, 0.00018751685274764895f, 0.0001742134045343846f, 0.00011777969484683126f, 0.0002367795823374763f, 0.0002221178583567962f, 0.00024345492420252413f, 0.00013632822083309293f, 0.00014094420475885272f, 0.00017572889919392765f, 0.0002957365068141371f, 0.00021012268553022295f, 0.00024538813158869743f, 0.0002722696226555854f, 0.0002611014642752707f, 0.00024675976601429284f, 0.00015980772150214761f, 0.00020398330525495112f, 0.0001042258445522748f, 0.00038037236663512886f, 0.00020242997561581433f, 0.00016385465278290212f, 0.0001989091106224805f, 0.000167614096426405f, 0.00033666519448161125f, 0.00026916564092971385f, 0.0002771285071503371f, 0.0003110394754912704f, 0.00024239649064838886f, 0.0003552771813701838f, 0.00016608470468781888f, 0.00031197204953059554f, 0.0001706772018224001f, 0.00039733111043460667f, 0.0002946475869975984f, 0.00025053450372070074f, 0.00019595098274294287f, 0.000306904868921265f, 0.00019079420599155128f, 0.0001883630466181785f, 0.000258747604675591f, 9.431225771550089e-05f, 0.0001722250017337501f, 0.000111113564344123f, 0.00013601301179733127f, 0.00040648120921105146f, 0.00020821882935706526f, 0.00018852930224966258f, 0.00022354240354616195f, 0.0001709128700895235f, 0.00017052072507794946f, 0.00021220241615083069f, 0.00040205870755016804f, 0.00022169644944369793f, 0.00017823665984906256f, 0.0002666862856131047f, 0.00017882886459119618f, 0.00021049841598141938f, 0.00015912701201159507f, 0.0001545865525258705f, 0.00013802587636746466f, 0.00022774246463086456f, 0.00032486094278283417f, 0.00026430952129885554f, 0.00014772574650123715f, 0.00027553291874937713f, 0.00018117608851753175f, 0.00016207034059334546f, 0.0002989956992678344f, 0.00021717378695029765f, 0.00016048032557591796f, 0.00017776286404114217f, 0.000290437979856506f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #29 */
AI_STATIC ai_intq_info_list conv2d_52_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0007552165188826621f, 0.0005469422903843224f, 0.0004001217312179506f, 0.0005290454719215631f, 0.0005564215243794024f, 0.00040915916906669736f, 0.00039666882366873324f, 0.0004900448257103562f, 0.0008727730601094663f, 0.0005708893877454102f, 0.0002977794501930475f, 0.0005364537937566638f, 0.0005075008957646787f, 0.0005046981968916953f, 0.000320727500366047f, 0.00012561745825223625f, 0.00025419361190870404f, 0.0004444659280125052f, 0.0004861938359681517f, 0.00037885535857640207f, 0.0004200783441774547f, 0.0004374601994641125f, 0.0004469706618692726f, 0.0006093052797950804f, 0.00026911107124760747f, 0.0005029378808103502f, 0.0004962954553775489f, 0.00040290996548719704f, 0.0004752189852297306f, 0.00041103849071078f, 0.0002640416205395013f, 0.00028262994601391256f, 0.0005178304854780436f, 0.00022460690524894744f, 0.0004115237097721547f, 0.00039389447192661464f, 0.0004866257368121296f, 0.0004423387290444225f, 0.000372461014194414f, 0.0004495837492868304f, 0.0004901111242361367f, 0.00033611958497203887f, 0.0007267236360348761f, 0.000686348183080554f, 0.00037248365697450936f, 0.0002632255491334945f, 0.0005006357096135616f, 0.00024276594922412187f, 0.0005823734682053328f, 0.00045620175660587847f, 0.0004923968226648867f, 0.00043113227002322674f, 0.0004696683317888528f, 0.0007262222934514284f, 0.0005152087542228401f, 0.0004751149099320173f, 0.0004070488503202796f, 0.0004972168826498091f, 0.0005793583113700151f, 0.0008978007244877517f, 0.0005019587115384638f, 0.00034386481274850667f, 0.0007670559571124613f, 0.0004027683171443641f, 0.0003296532086096704f, 0.000327510031638667f, 0.00038608486647717655f, 0.0003348472237121314f, 0.0006140218465588987f, 0.00031306047458201647f, 0.0005044408608227968f, 0.0004909761482849717f, 0.0004804347991012037f, 0.0005626350175589323f, 0.0007205687579698861f, 0.000571032811421901f, 0.000635841628536582f, 0.0004744653415400535f, 0.0003153675643261522f, 0.0005846413550898433f, 0.000543164205737412f, 0.00047443926450796425f, 0.00045939607662148774f, 0.00043449155054986477f, 0.000413034314988181f, 0.00044648084440268576f, 0.0006408874178305268f, 0.0005010053282603621f, 0.0002889206225518137f, 0.0005559409037232399f, 0.00041339342715218663f, 0.0005882068071514368f, 0.0004950560978613794f, 0.0006015616818331182f, 0.00035655905958265066f, 0.00043670303421095014f, 0.0006358835962601006f, 0.00047661823919042945f, 0.0002484446740709245f, 0.0004724347381852567f, 0.00048643769696354866f, 0.00022565139806829393f, 0.0006795630324631929f, 0.0005565517349168658f, 0.0001856978633441031f, 0.0005286057130433619f, 0.000581474625505507f, 0.0003164243244100362f, 0.0005518557736650109f, 0.0002080133417621255f, 0.0007353055407293141f, 0.0002530334168113768f, 0.0004407084488775581f, 0.0004754861001856625f, 0.0007390239625237882f, 0.0005845982814207673f, 0.000762436306104064f, 0.0004785445926245302f, 0.00039103347808122635f, 0.0005925925797782838f, 0.00037988528492860496f, 0.0005482671549543738f, 0.0005071893683634698f, 0.0007845971267670393f, 0.00024197714810725302f, 0.0005936443340033293f, 0.0005120307905599475f, 0.00040560911293141544f, 0.00031832943204790354f, 0.0006755465874448419f, 0.0005506996531039476f, 0.000516072497703135f, 0.000627560424618423f, 0.0005483913701027632f, 0.00047950466978363693f, 0.00042509223567321897f, 0.00029565425938926637f, 0.0006382263381965458f, 0.00047671105130575597f, 0.0005862595280632377f, 0.0005976458778604865f, 0.0005239210440777242f, 0.0004537881468422711f, 0.0004555690393317491f, 0.0005190629162825644f, 0.0004668294859584421f, 0.0005278278840705752f, 0.0001674733211984858f, 0.0007066823309287429f, 0.00038677119300700724f, 0.00034146287362091243f, 0.0005965433083474636f, 0.0003406066680327058f, 0.00034632530878297985f, 0.0004761630843859166f, 0.00021982772159390152f, 0.0004047432157676667f, 0.0003062221803702414f, 0.00032400916097685695f, 0.0005551876383833587f, 0.00041891602450050414f, 0.00044239030103199184f, 0.0005960584967397153f, 0.00025433339760638773f, 0.0004987211432307959f, 0.0005688259843736887f, 0.0006016519037075341f, 0.0004461003700271249f, 0.0003338306851219386f, 0.0004580589884426445f, 0.00032824883237481117f, 0.00046020251465961337f, 0.00046896160347387195f, 0.000508733734022826f, 0.000845454225782305f, 0.00018064318282995373f, 0.00026304551283828914f, 0.00029046882991679013f, 0.0005966612370684743f, 0.0005141904694028199f, 0.0004844395152758807f, 0.0003475109697319567f, 0.00024463451700285077f, 0.000731891836039722f, 0.0005562293226830661f, 0.0005210864474065602f, 0.000555378501303494f, 0.0005696017760783434f, 0.0005022935220040381f, 0.0005870041204616427f, 0.0004993861075490713f, 0.0006457741255871952f, 0.0004500129434745759f, 0.0004598792002070695f, 0.0005595659022219479f, 0.0003848170454148203f, 0.000501941132824868f, 0.0003727490548044443f, 0.0004178004455752671f, 0.000516557483933866f, 0.0007394627318717539f, 0.000622016959823668f, 0.0003948179364670068f, 0.00022178083600010723f, 0.0003589738917071372f, 0.00033522857120260596f, 0.0003659101203083992f, 0.0003109500103164464f, 0.000606167537625879f, 0.0008307494572363794f, 0.0006085406639613211f, 0.0005933940992690623f, 0.0004142845282331109f, 0.00046303318231366575f, 0.0007654313812963665f, 0.00047697999980300665f, 0.00040261365938931704f, 0.0003740500833373517f, 0.0002528823970351368f, 0.0005083846626803279f, 0.000476904766401276f, 0.000522717135027051f, 0.000292707554763183f, 0.00030261845677159727f, 0.0003773039788939059f, 0.000634969852399081f, 0.00045115017564967275f, 0.0005268679233267903f, 0.0005845846026204526f, 0.0005606056656688452f, 0.0005298128817230463f, 0.000343119929311797f, 0.00043796843965537846f, 0.00022378120047505945f, 0.0008166898041963577f, 0.0004346333153080195f, 0.00035180902341380715f, 0.0004270737408660352f, 0.0003598808543756604f, 0.0007228470640257001f, 0.0005779201164841652f, 0.0005950169870629907f, 0.0006678265635855496f, 0.0005204445915296674f, 0.0007628084276802838f, 0.0003565970982890576f, 0.0006698289071209729f, 0.00036645756335929036f, 0.0008531016064807773f, 0.0006326318834908307f, 0.0005379176000133157f, 0.00042072238284163177f, 0.0006589492550119758f, 0.0004096503835171461f, 0.0004044304951094091f, 0.0005555517273023725f, 0.00020249593944754452f, 0.00036978081334382296f, 0.00023856968618929386f, 0.0002920307742897421f, 0.0008727475651539862f, 0.00044706245535053313f, 0.00040478745358996093f, 0.0004799633752554655f, 0.00036696356255561113f, 0.0003661216178443283f, 0.0004556155181489885f, 0.0008632521494291723f, 0.0004759999574162066f, 0.0003826883330475539f, 0.000572596734855324f, 0.00038395985029637814f, 0.00045195690472610295f, 0.00034165839315392077f, 0.0003319096576888114f, 0.00029635257669724524f, 0.0004889812553301454f, 0.000697502342518419f, 0.0005674936110153794f, 0.00031717895762994885f, 0.0005915911751799285f, 0.00038899952778592706f, 0.000347977940691635f, 0.000641967635601759f, 0.0004662894643843174f, 0.00034456406137906015f, 0.0003816710668615997f, 0.0006235935143195093f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #30 */
AI_STATIC ai_intq_info_list conv2d_50_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(8.034372876863927e-05f, 0.00013309706992004067f, 8.544905722374097e-05f, 0.00015216936299111694f, 0.00022406657808460295f, 0.0002250695979455486f, 0.00010686711175367236f, 0.00012757486547343433f, 8.723586506675929e-05f, 0.00015663534577470273f, 0.0002449359162710607f, 0.00015863284352235496f, 0.0002547031035646796f, 0.00018993066623806953f, 0.00022580334916710854f, 5.0117323553422466e-05f, 5.1221333706052974e-05f, 0.00017430786101613194f, 0.00016688666073605418f, 0.00025896631996147335f, 0.00022339784482028335f, 0.00011536657984834164f, 0.0001510948350187391f, 7.87102326285094e-05f, 0.0001231758506037295f, 0.00014972481585573405f, 0.00028257103986106813f, 0.00029668884235434234f, 0.0001856153248809278f, 0.00010925970855168998f, 0.00024187524104490876f, 0.0001587666047271341f, 8.759634511079639e-05f, 0.00015958405856508762f, 8.059607353061438e-05f, 0.0001937710912898183f, 8.960714330896735e-05f, 5.843035614816472e-05f, 9.472057718085125e-05f, 0.00023454378242604434f, 3.825257226708345e-05f, 0.00023035578487906605f, 0.0002204441261710599f, 0.0001856389280874282f, 0.00013699603732675314f, 8.036234794417396e-05f, 9.536387369735166e-05f, 0.0003055655979551375f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #31 */
AI_STATIC ai_intq_info_list conv2d_50_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0034146085381507874f, 0.005656625144183636f, 0.0036315848119556904f, 0.006467197556048632f, 0.009522829204797745f, 0.009565457701683044f, 0.004541852045804262f, 0.005421931855380535f, 0.0037075241561979055f, 0.006657002028077841f, 0.010409776121377945f, 0.00674189580604434f, 0.010824881494045258f, 0.008072053082287312f, 0.009596642106771469f, 0.002129986183717847f, 0.002176906680688262f, 0.0074080838821828365f, 0.007092683110386133f, 0.01100606843829155f, 0.009494408033788204f, 0.004903079476207495f, 0.006421530619263649f, 0.0033451849594712257f, 0.005234973505139351f, 0.006363304797559977f, 0.012009268626570702f, 0.012609275057911873f, 0.007888651452958584f, 0.004643537569791079f, 0.010279697366058826f, 0.006747580599039793f, 0.003722844645380974f, 0.0067823221907019615f, 0.0034253329504281282f, 0.008235271088778973f, 0.003808303503319621f, 0.0024832901544868946f, 0.0040256245993077755f, 0.009968110360205173f, 0.0016257342649623752f, 0.009790120646357536f, 0.009368875063955784f, 0.007889654487371445f, 0.005822331178933382f, 0.003415399696677923f, 0.0040529645048081875f, 0.01298653706908226f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #32 */
AI_STATIC ai_intq_info_list conv2d_49_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00018503399041946977f, 5.295178561937064e-05f, 0.00045351413427852094f, 0.00020871749438811094f, 7.716225809417665e-05f, 0.0003813762159552425f, 0.00011914909555343911f, 0.00021672187722288072f, 0.00016546895494684577f, 0.00010530660802032799f, 8.679671009304002e-05f, 0.00018026577890850604f, 0.00011410692241042852f, 0.00016146009147632867f, 0.0005123524460941553f, 0.0001619628892512992f, 0.0001335772394668311f, 0.00026319679454900324f, 7.878675387473777e-05f, 0.000164244047482498f, 0.00018001912394538522f, 0.0001138302541221492f, 0.00021335257042665035f, 0.00015596127195749432f, 0.00026998462271876633f, 0.00015096685092430562f, 0.0001334372500423342f, 8.255207649199292e-05f, 8.383150270674378e-05f, 6.598859908990562e-05f, 0.00015114899724721909f, 0.00023024395341053605f, 9.417438559466973e-05f, 0.00010390480019850656f, 6.298260996118188e-05f, 0.00017849591677077115f, 5.855402196175419e-05f, 0.00029777540476061404f, 0.0002701912890188396f, 8.928003808250651e-05f, 8.059622632572427e-05f, 0.00010313838720321655f, 0.00027025231975130737f, 8.65373804117553e-05f, 0.00010627671872498468f, 0.0003105364157818258f, 0.00011052405898226425f, 0.0001329092774540186f, 7.488663686672226e-05f, 0.00021701239165849984f, 0.00020178873091936111f, 0.00016823108308017254f, 0.00018177331367041916f, 0.00019579194486141205f, 0.00016645024879835546f, 0.00038702599704265594f, 0.00016785501793492585f, 0.00021954806288704276f, 0.0003071476530749351f, 0.00010436131560709327f, 0.0001536761992610991f, 0.0003477045684121549f, 6.213811138877645e-05f, 6.421455327654257e-05f, 0.00021234492305666208f, 0.00025582636590115726f, 0.0002660985046532005f, 0.0005796769401058555f, 0.00024046508769970387f, 9.893827518681064e-05f, 0.00012115863501094282f, 0.00016324988973792642f, 0.00016621297982055694f, 0.0001832365378504619f, 0.00013929945998825133f, 0.000217737746424973f, 0.00043110817205160856f, 0.0001117747524403967f, 0.00013073209265712649f, 0.00020424951799213886f, 0.00010942558583337814f, 0.0001078905479516834f, 0.000205207135877572f, 0.00022295030066743493f, 8.986592729343101e-05f, 6.479724106611684e-05f, 9.717370267026126e-05f, 9.947837679646909e-05f, 0.00016318650159519166f, 0.00017436455527786165f, 0.00021113190450705588f, 0.0002652201510500163f, 0.000160235955263488f, 0.0002311447315150872f, 0.00021339250088203698f, 0.00023580467677675188f, 0.0002707494713831693f, 0.00021323880355339497f, 0.0003699245280586183f, 0.0002640428429003805f, 0.0006458215648308396f, 7.545085827587172e-05f, 7.610006286995485e-05f, 0.00015149195678532124f, 0.0004454555455595255f, 0.00011304098006803542f, 0.00011050349712604657f, 0.00037836431874893606f, 0.00034935615258291364f, 0.0015842990251258016f, 0.00014709692914038897f, 0.00022081143106333911f, 0.0001415969745721668f, 7.309341890504584e-05f, 0.0002239556488348171f, 0.00018878687114920467f, 0.00014833554450888187f, 0.0002069243637379259f, 0.00028056566952727735f, 0.00024112524988595396f, 0.0002804212854243815f, 0.00021942669991403818f, 0.0001290241052629426f, 0.00025057996390387416f, 0.0001035764507832937f, 0.0006753745255991817f, 0.00015299148799385875f, 0.0005921237752772868f, 6.523034971905872e-05f, 0.00017867873248178512f, 0.0003240928635932505f, 0.00022216507932171226f, 6.96347706252709e-05f, 0.0002991134824696928f, 0.0003696449566632509f, 0.00018823939899448305f, 0.00018969259690493345f, 0.0003181640640832484f, 0.0001228556939167902f, 0.00019909307593479753f, 0.00034058550954796374f, 0.00011039199307560921f, 0.00018687588453758508f, 0.00018275428737979382f, 7.276063843164593e-05f, 6.384796870406717e-05f, 0.00026167341275140643f, 0.0002598101273179054f, 0.0001767297217156738f, 0.00029667417402379215f, 4.837649976252578e-05f, 0.0002562639710959047f, 0.0007357235299423337f, 0.0003278326184954494f, 9.51522306422703e-05f, 0.00026703806361183524f, 0.00036466066376306117f, 0.00013999250950291753f, 0.0006119816098362207f, 0.00033866026205942035f, 0.0005411271704360843f, 0.00011992880899924785f, 0.00011333697329973802f, 8.916572551243007e-05f, 0.0001316124980803579f, 0.00042787098209373653f, 0.00021772843319922686f, 8.449082088191062e-05f, 0.0001149272356997244f, 4.994726987206377e-05f, 0.00022418619482778013f, 0.00031161587685346603f, 0.0004888386465609074f, 0.00020678917644545436f, 0.0001376938307657838f, 0.0008430293528363109f, 0.00011595743126235902f, 0.00033579792943783104f, 0.00017279773601330817f, 0.00018158032617066056f, 8.907929441193119e-05f, 9.892620437312871e-05f, 0.00011794344754889607f, 0.00032801428460516036f, 0.00017896176723297685f, 0.00012902675371151417f, 0.0002630826202221215f, 0.0003844291786663234f, 0.0004142823163419962f, 6.970650429138914e-05f, 6.456011033151299e-05f, 0.0002127019688487053f, 8.724162034923211e-05f, 0.0003816240350715816f, 0.00018504235777072608f, 0.00019837103900499642f, 9.09616137505509e-05f, 0.00022257301316130906f, 0.00014037260552868247f, 0.00016409609816037118f, 0.00011575260577956215f, 0.00014939685934223235f, 0.00027745863189920783f, 7.254623051267117e-05f, 0.00016688782488927245f, 0.0002829825389198959f, 0.0002629517111927271f, 0.00023288927332032472f, 0.00030782297835685313f, 0.00012172790593467653f, 0.00033331793383695185f, 0.00028977470356039703f, 0.00010240270057693124f, 0.0001326599740423262f, 0.00012142647756263614f, 0.0001220888807438314f, 0.00012905971379950643f, 7.331692177103832e-05f, 0.00013308960478752851f, 0.000193728570593521f, 0.00021202124480623752f, 8.79918152350001e-05f, 0.00014362447836901993f, 0.00026914768386632204f, 0.0004473116132430732f, 6.583955837413669e-05f, 0.00013157984358258545f, 0.00031986398971639574f, 0.00027576208231039345f, 0.0001015109519357793f, 0.00019355217227712274f, 9.98832838376984e-05f, 0.00013158866204321384f, 6.795058288844302e-05f, 8.434683695668355e-05f, 0.00012966510257683694f, 0.00019029760733246803f, 0.000380252517061308f, 0.00010680696868803352f, 0.00016421015607193112f, 6.367302557919174e-05f, 0.00022293765505310148f, 0.00012802801211364567f, 6.98442236171104e-05f, 0.00011544450535438955f, 0.00022289658954832703f, 0.00023356005840469152f, 5.736561433877796e-05f, 0.00017870432930067182f, 0.00026004030951298773f, 0.00012381564010865986f, 0.00025849128724075854f, 9.744479029905051e-05f, 0.00010498500341782346f, 0.00019706365128513426f, 0.00021115955314598978f, 0.0003224329848308116f, 0.00025132225709967315f, 0.0009916179114952683f, 0.00023565891024190933f, 0.00017298851162195206f, 0.00015510033699683845f, 0.00015324814012274146f, 7.084176468197256e-05f, 0.00011375753820175305f, 0.0003346025769133121f, 0.00011559431732166559f, 0.00021818386449012905f, 0.00013944043894298375f, 0.00021883606677874923f, 0.0001795512216631323f, 5.315129237715155e-05f, 0.00015955518756527454f, 0.00014030499733053148f, 0.0004870256525464356f, 0.0002005660644499585f, 9.835948003455997e-05f, 0.0001151025207946077f, 0.0001554587361169979f, 0.00012064621114404872f, 0.00033678492764011025f, 8.674863784108311e-05f, 9.414126543560997e-05f, 0.00014401430962607265f, 0.00011745499796234071f, 0.00023324144422076643f, 0.0003916601126547903f, 0.00014279269089456648f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #33 */
AI_STATIC ai_intq_info_list conv2d_49_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00786394439637661f, 0.00225045089609921f, 0.019274350255727768f, 0.008870493620634079f, 0.003279395867139101f, 0.01620848849415779f, 0.005063836462795734f, 0.009210679680109024f, 0.00703243026509881f, 0.004475530702620745f, 0.0036888602189719677f, 0.007661295589059591f, 0.004849544260650873f, 0.006862053647637367f, 0.021774979308247566f, 0.006883422378450632f, 0.00567703228443861f, 0.011185863986611366f, 0.0033484369050711393f, 0.0069803716614842415f, 0.0076508126221597195f, 0.0048377858474850655f, 0.009067484177649021f, 0.006628354080021381f, 0.011474345810711384f, 0.006416091229766607f, 0.005671082995831966f, 0.003508463269099593f, 0.0035628387704491615f, 0.002804515417665243f, 0.006423832383006811f, 0.009785368107259274f, 0.004002411384135485f, 0.0044159539975225925f, 0.0026767610106617212f, 0.0075860763899981976f, 0.0024885458406060934f, 0.012655454687774181f, 0.011483130045235157f, 0.003794401418417692f, 0.003425339702516794f, 0.0043833814561367035f, 0.011485722847282887f, 0.0036778387147933245f, 0.004516760352998972f, 0.01319779735058546f, 0.004697272554039955f, 0.005648644175380468f, 0.0031826819758862257f, 0.009223026223480701f, 0.00857602059841156f, 0.007149820681661367f, 0.007725365459918976f, 0.008321157656610012f, 0.007074135355651379f, 0.016448603942990303f, 0.007133838254958391f, 0.009330792352557182f, 0.0130537748336792f, 0.0044353557750582695f, 0.006531238555908203f, 0.014777444303035736f, 0.00264086970128119f, 0.0027291185688227415f, 0.009024659171700478f, 0.010872620157897472f, 0.01130918599665165f, 0.024636268615722656f, 0.010219765827059746f, 0.004204876720905304f, 0.005149242002516985f, 0.006938119884580374f, 0.0070640514604747295f, 0.007787552662193775f, 0.005920226685702801f, 0.009253853932023048f, 0.01832209713757038f, 0.004750426858663559f, 0.005556113552302122f, 0.00868060439825058f, 0.00465058721601963f, 0.004585348069667816f, 0.008721303194761276f, 0.009475387632846832f, 0.0038193019572645426f, 0.0027538826689124107f, 0.00412988243624568f, 0.004227831028401852f, 0.0069354260340332985f, 0.007410493679344654f, 0.008973105810582638f, 0.011271855793893337f, 0.006810028105974197f, 0.009823651053011417f, 0.00906918104737997f, 0.010021698661148548f, 0.011506852693855762f, 0.009062648750841618f, 0.015721792355179787f, 0.01122182048857212f, 0.02744741551578045f, 0.0032066614367067814f, 0.003234252566471696f, 0.006438408046960831f, 0.018931860104203224f, 0.004804241470992565f, 0.0046963985078036785f, 0.01608048379421234f, 0.014847636222839355f, 0.06733270734548569f, 0.0062516191974282265f, 0.009384485892951488f, 0.006017871201038361f, 0.0031064702197909355f, 0.009518114849925041f, 0.008023441769182682f, 0.0063042608089745045f, 0.008794285356998444f, 0.011924040503799915f, 0.010247822850942612f, 0.011917904019355774f, 0.009325634688138962f, 0.0054835244081914425f, 0.010649648495018482f, 0.004401999060064554f, 0.028703417629003525f, 0.006502137985080481f, 0.025165259838104248f, 0.0027722897939383984f, 0.007593845948576927f, 0.013773947022855282f, 0.009442015551030636f, 0.0029594777151942253f, 0.012712322175502777f, 0.015709910541772842f, 0.008000174537301064f, 0.008061935193836689f, 0.013521972112357616f, 0.005221366882324219f, 0.008461455814540386f, 0.014474883675575256f, 0.004691659472882748f, 0.007942224852740765f, 0.007767057046294212f, 0.003092326922342181f, 0.002713538706302643f, 0.011121119372546673f, 0.011041929945349693f, 0.007511012721806765f, 0.012608652003109455f, 0.0020560012198984623f, 0.010891218669712543f, 0.031268250197172165f, 0.01393288653343916f, 0.004043969791382551f, 0.011349117383360863f, 0.015498078428208828f, 0.0059496816247701645f, 0.026009216904640198f, 0.014393060468137264f, 0.0229979045689106f, 0.005096974316984415f, 0.004816821310669184f, 0.003789543407037854f, 0.005593530833721161f, 0.018184516578912735f, 0.009253458119928837f, 0.0035908599384129047f, 0.004884407389909029f, 0.002122758887708187f, 0.00952791329473257f, 0.01324367430061102f, 0.02077564224600792f, 0.00878854002803564f, 0.00585198774933815f, 0.03582874685525894f, 0.004928190726786852f, 0.014271412044763565f, 0.00734390364959836f, 0.007717163767665625f, 0.0037858698051422834f, 0.004204363562166691f, 0.005012596491724253f, 0.013940607197582722f, 0.007605874910950661f, 0.005483636632561684f, 0.011181010864675045f, 0.01633824035525322f, 0.01760699786245823f, 0.002962526399642229f, 0.002743804594501853f, 0.009039833210408688f, 0.0037077688612043858f, 0.016219021752476692f, 0.007864300161600113f, 0.008430768735706806f, 0.003865868551656604f, 0.009459353052079678f, 0.005965835880488157f, 0.006974084302783012f, 0.004919485654681921f, 0.006349366623908281f, 0.01179199106991291f, 0.0030832148622721434f, 0.007092732470482588f, 0.012026757933199406f, 0.011175448074936867f, 0.009897793643176556f, 0.013082476332783699f, 0.005173435900360346f, 0.014166012406349182f, 0.012315425090491772f, 0.004352114629000425f, 0.005638048518449068f, 0.005160625092685223f, 0.005188777111470699f, 0.005485037807375193f, 0.0031159690115600824f, 0.005656308028846979f, 0.008233464322984219f, 0.009010902605950832f, 0.0037396519910544157f, 0.0061040399596095085f, 0.01143877673894167f, 0.019010743126273155f, 0.0027981812600046396f, 0.005592143163084984f, 0.013594219461083412f, 0.01171988807618618f, 0.004314215388149023f, 0.008225967176258564f, 0.004245039541274309f, 0.0055925180204212666f, 0.0028878997545689344f, 0.00358474045060575f, 0.00551076652482152f, 0.008087648078799248f, 0.01616073213517666f, 0.00453929603099823f, 0.006978931371122599f, 0.0027061034925282f, 0.00947485025972128f, 0.005441190674901009f, 0.0029683792963624f, 0.004906391259282827f, 0.00947310496121645f, 0.00992630235850811f, 0.0024380385875701904f, 0.007594933733344078f, 0.011051712557673454f, 0.005262164864689112f, 0.010985879227519035f, 0.004141403362154961f, 0.004461862612515688f, 0.008375205099582672f, 0.008974281139671803f, 0.013703401200473309f, 0.010681196115911007f, 0.04214375838637352f, 0.010015503503382206f, 0.007352011743932962f, 0.0065917642787098885f, 0.006513046100735664f, 0.003010774962604046f, 0.004834695253521204f, 0.014220609329640865f, 0.004912758246064186f, 0.00927281379699707f, 0.005926218349486589f, 0.00930053275078535f, 0.0076309265568852425f, 0.0022589298896491528f, 0.006781095638871193f, 0.005962962284684181f, 0.020698590204119682f, 0.00852405745536089f, 0.004180277697741985f, 0.004891857039183378f, 0.006606996059417725f, 0.005127463955432177f, 0.01431335974484682f, 0.003686817130073905f, 0.004001003690063953f, 0.006120608188211918f, 0.004991837311536074f, 0.009912760928273201f, 0.016645554453134537f, 0.0060686892829835415f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #34 */
AI_STATIC ai_intq_info_list conv2d_48_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(8.558583795093e-05f, 0.00011407981219235808f, 6.984209176152945e-05f, 0.00012159915786469355f, 7.229820766951889e-05f, 0.00010700643179006875f, 0.0001711398654151708f, 8.286067168228328e-05f, 7.375414861598983e-05f, 0.00015286031702999026f, 0.00010419837781228125f, 6.940401362953708e-05f, 0.00010882092465180904f, 9.451868390897289e-05f, 8.407901623286307e-05f, 9.326875442638993e-05f, 0.00014905500574968755f, 7.08272127667442e-05f, 0.00011629293294390664f, 0.00011487938900245354f, 8.950940537033603e-05f, 9.580521145835519e-05f, 0.0001308846694882959f, 0.00012278012582100928f, 0.0001799383171601221f, 0.0001235090894624591f, 8.343469380633906e-05f, 0.00010346204362576827f, 0.00017854275938589126f, 0.0001338196889264509f, 8.766022074269131e-05f, 0.00010822981130331755f, 0.0001434825680917129f, 9.601073543308303e-05f, 0.00018574083514977247f, 0.00015835002704989165f, 0.0001404040667694062f, 9.751104516908526e-05f, 6.903536268509924e-05f, 0.00021068927890155464f, 0.0001340231392532587f, 0.00011671183165162802f, 0.0001011295389616862f, 8.740756311453879e-05f, 0.00014611803635489196f, 4.387494846014306e-05f, 0.00012473649985622615f, 5.5554315622430295e-05f, 0.00018406558956485242f, 0.0002023249544436112f, 7.334323163377121e-05f, 0.0001804365892894566f, 0.00017625615873839706f, 0.00016149943985510617f, 0.00010458494944032282f, 7.681892020627856e-05f, 0.00023965696163941175f, 0.00016025864169932902f, 5.2403847803361714e-05f, 0.00011996569810435176f, 6.735204078722745e-05f, 0.00016118574421852827f, 0.00011436585191404447f, 0.00014155737881083041f, 0.00017109203326981515f, 7.131241000024602e-05f, 9.697713539935648e-05f, 5.0660666602198035e-05f, 0.0001548560830997303f, 0.00014553687651641667f, 0.00010306199692422524f, 0.00010445101361256093f, 0.00012652823352254927f, 0.00013239416875876486f, 0.00014520392869599164f, 9.25933345570229e-05f, 0.00010859285976039246f, 9.178757318295538e-05f, 0.00013762270100414753f, 5.293043068377301e-05f, 0.0001199475082103163f, 0.0001820758916437626f, 0.00018088097567670047f, 9.78371681412682e-05f, 0.00012870294449385256f, 2.8150539947091602e-05f, 0.00018891834770329297f, 0.00016723501903470606f, 0.00016142407548613846f, 0.00010553683387115598f, 0.00014184426981955767f, 9.949789091479033e-05f, 9.873729868559167e-05f, 9.707536810310557e-05f, 0.000173879336216487f, 0.00011263245687587187f, 6.536127330036834e-05f, 9.672166925156489e-05f, 6.86639832565561e-05f, 4.242589056957513e-05f, 1.026578956953017e-05f, 0.00023527763551101089f, 0.00010005977674154565f, 0.00015821828856132925f, 5.1117895054630935e-05f, 0.0001610618783161044f, 9.95596928987652e-05f, 0.00020090796169824898f, 7.479378109565005e-05f, 8.07358737802133e-05f, 0.00013501360081136227f, 0.00011276012810412794f, 0.00012608605902642012f, 0.00010125550033990294f, 0.00013686523016076535f, 0.0002295593440067023f, 6.872065569041297e-05f, 9.297759243054315e-05f, 7.658410322619602e-05f, 0.0001829613756854087f, 0.0001349076919723302f, 0.00012968844384886324f, 7.678756082896143e-05f, 0.00010246213059872389f, 0.0002020516258198768f, 0.00017327010573353618f, 0.00014004684635438025f, 5.5578308092663065e-05f, 0.000192287378013134f, 7.173437916208059e-05f, 5.532153954845853e-05f, 0.00011641867604339495f, 0.0001546589919598773f, 8.649385563330725e-05f, 0.00016872237029019743f, 0.00013102170487400144f, 0.00012904347386211157f, 6.79394870530814e-05f, 0.00012858817353844643f, 0.00011421537055866793f, 0.00013523345114663243f, 7.075635221553966e-05f, 8.599415741628036e-05f, 9.5748073363211e-05f, 0.00012830951891373843f, 0.00017987283354159445f, 0.00010040067718364298f, 0.00012596104352269322f, 0.00014829004066996276f, 7.31225882191211e-05f, 0.00012516797869466245f, 0.00014450667367782444f, 8.65984329720959e-05f, 0.0001556209899717942f, 9.367179882247001e-05f, 9.505556226940826e-05f, 0.000140641481266357f, 0.00014361029025167227f, 0.00010126472625415772f, 0.00013862007472198457f, 4.8848098231246695e-05f, 0.00013547053094953299f, 0.00010791511158458889f, 0.00011214290861971676f, 3.3096152037614956e-05f, 9.208445408148691e-05f, 0.0001540410885354504f, 0.0001492521696491167f, 0.00012374785728752613f, 0.00011551596981007606f, 0.00014057727821636945f, 0.00013551228039432317f, 9.27775472518988e-05f, 0.0001455056481063366f, 0.0001590936881257221f, 7.577966607641429e-05f, 0.00018921514856629074f, 7.77755631133914e-05f, 9.723350376589224e-05f, 0.00015525748312938958f, 5.559775672736578e-05f, 0.00015564716886729002f, 0.00019420443277340382f, 0.00015502962924074382f, 0.00012021479051327333f, 0.00013252260396257043f, 9.258135833078995e-05f, 0.00011479987006168813f, 0.00010556743654888123f, 0.00011573879601201043f, 0.00011000133963534608f, 9.893560491036624e-05f, 0.000132524233777076f, 0.00012273469474166632f, 5.9846104704774916e-05f, 5.991241414449178e-05f, 0.00011381946387700737f, 0.00014998581900727004f, 0.00012735705240629613f, 0.00014447847206611186f, 0.00011966759484494105f, 0.00011578475096030161f, 8.125906606437638e-05f, 0.00014212404494173825f, 0.0001091349549824372f, 0.00013456458691507578f, 0.00011560789425857365f, 5.1846182032022625e-05f, 5.395301559474319e-05f, 0.00012672293814830482f, 0.00015913024253677577f, 9.373048669658601e-05f, 0.00010993463365593925f, 0.0001885208475869149f, 0.00010734111856436357f, 0.00014430377632379532f, 0.00010591232421575114f, 0.0002686342049855739f, 0.00013267270696815103f, 9.219127241522074e-05f, 0.00012237468035891652f, 0.00011032330803573132f, 0.00015263102250173688f, 0.0001427672104910016f, 0.00017683861369732767f, 0.0001537500647827983f, 0.00017986998136620969f, 0.00014212327369023114f, 0.00016774673713371158f, 9.443237649975345e-05f, 7.075091707520187e-05f, 0.00010714741802075878f, 0.00012182901991764084f, 0.00015605537919327617f, 0.00018814091163221747f, 0.00012494617840275168f, 9.083210898097605e-05f, 0.00010809647210408002f, 9.851429058471695e-05f, 0.00018822195124812424f, 0.00012775497452821583f, 0.00019871757831424475f, 0.0001414219877915457f, 0.00011716772860381752f, 0.00014425396511796862f, 6.475522241089493e-05f, 0.00012311339378356934f, 0.00012007845361949876f, 6.678431964246556e-05f, 9.659548231866211e-05f, 0.00012917284038849175f, 9.758479427546263e-05f, 8.490858454024419e-05f, 0.00016133215103764087f, 8.340613567270339e-05f, 0.00012915303523186594f, 0.00010582667164271697f, 9.469380893278867e-05f, 6.687442510155961e-05f, 0.0001949403522303328f, 8.648620132589713e-05f, 6.990646943449974e-05f, 5.929442340857349e-05f, 0.00020176114048808813f, 8.339271880686283e-05f, 7.896786701167002e-05f, 0.00012128296657465398f, 0.00013111240696161985f, 0.0001186570298159495f, 0.00016068448894657195f, 0.00020357426546979696f, 0.00012062286987202242f, 0.00015171330596785992f, 0.00016041623894125223f, 3.5412107536103576e-05f, 0.00013081629003863782f, 0.00028278055833652616f, 0.00013141290401108563f, 8.633281686343253e-05f, 0.00010677552927518263f, 0.0001461389911128208f, 0.00019615869678091258f, 0.00018671109864953905f, 9.788163879420608e-05f, 0.0001628507161512971f, 7.103997631929815e-05f, 0.00010519805073272437f, 0.00011781047942349687f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #35 */
AI_STATIC ai_intq_info_list conv2d_48_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0003572123823687434f, 0.000476138578960672f, 0.0002915021905209869f, 0.0005075223161838949f, 0.00030175334541127086f, 0.0004466161772143096f, 0.0007142919930629432f, 0.0003458382561802864f, 0.00030783007969148457f, 0.0006379980477504432f, 0.0004348961228970438f, 0.0002896737714763731f, 0.0004541894013527781f, 0.0003944956697523594f, 0.0003509233065415174f, 0.0003892788081429899f, 0.0006221156800165772f, 0.0002956138050649315f, 0.00048537555267103016f, 0.00047947579878382385f, 0.0003735882928594947f, 0.0003998652973677963f, 0.0005462775588966906f, 0.0005124513409100473f, 0.0007510143332183361f, 0.0005154938553459942f, 0.00034823405439965427f, 0.00043182287481613457f, 0.0007451896672137082f, 0.0005585275357589126f, 0.00036587027716450393f, 0.0004517222405411303f, 0.0005988578195683658f, 0.00040072310366667807f, 0.0007752325036562979f, 0.000660910620354116f, 0.0005860089440830052f, 0.0004069849965162575f, 0.0002881351101677865f, 0.0008793606539256871f, 0.0005593766691163182f, 0.000487123936181888f, 0.0004220876144245267f, 0.0003648157580755651f, 0.0006098575540818274f, 0.000183122290764004f, 0.0005206167697906494f, 0.00023186883481685072f, 0.0007682404830120504f, 0.0008444502018392086f, 0.0003061150200664997f, 0.0007530939765274525f, 0.000735645997337997f, 0.0006740554235875607f, 0.00043650958104990423f, 0.00032062159152701497f, 0.001000263961032033f, 0.0006688766297884285f, 0.00021871962235309184f, 0.0005007046856917441f, 0.00028110938728787005f, 0.0006727461004629731f, 0.0004773324471898377f, 0.0005908226012252271f, 0.00071409234078601f, 0.00029763890779577196f, 0.0004047566035296768f, 0.00021144405764061958f, 0.0006463277968578041f, 0.0006074319244362414f, 0.00043015318806283176f, 0.0004359505546744913f, 0.0005280949408188462f, 0.0005525777814909816f, 0.0006060423329472542f, 0.0003864597820211202f, 0.0004532375023700297f, 0.00038309674710035324f, 0.0005744002992287278f, 0.00022091744176577777f, 0.0005006287829019129f, 0.000759935996029526f, 0.0007549487636424601f, 0.0004083461535628885f, 0.0005371716106310487f, 0.00011749281839001924f, 0.0007884945953264832f, 0.0006979941972531378f, 0.0006737408693879843f, 0.00044048248673789203f, 0.0005920199910178781f, 0.00041527755092829466f, 0.00041210305062122643f, 0.0004051665891893208f, 0.0007257257821038365f, 0.000470097700599581f, 0.0002728004474192858f, 0.0004036903555970639f, 0.0002865850692614913f, 0.00017707431106828153f, 4.284665556042455e-05f, 0.0009819858241826296f, 0.0004176227084826678f, 0.0006603607907891273f, 0.00021335241035558283f, 0.0006722291582264006f, 0.0004155354981776327f, 0.0008385360124520957f, 0.00031216919887810946f, 0.00033696991158649325f, 0.0005635106354020536f, 0.0004706305917352438f, 0.0005262494669295847f, 0.00042261334601789713f, 0.0005712388083338737f, 0.0009581192280165851f, 0.00028682162519544363f, 0.00038806357770226896f, 0.0003196415491402149f, 0.0007636317750439048f, 0.0005630685482174158f, 0.0005412848549894989f, 0.00032049071160145104f, 0.0004276495019439608f, 0.0008433093898929656f, 0.0007231830386444926f, 0.0005845180130563676f, 0.00023196898109745234f, 0.0008025559945963323f, 0.00029940009699203074f, 0.00023089729074854404f, 0.00048590038204565644f, 0.0006455052061937749f, 0.0003610021958593279f, 0.0007042019860818982f, 0.0005468495073728263f, 0.0005385929252952337f, 0.0002835612394846976f, 0.0005366926197893918f, 0.0004767043574247509f, 0.0005644282209686935f, 0.0002953180519398302f, 0.0003589165862649679f, 0.0003996268205810338f, 0.0005355295725166798f, 0.0007507410482503474f, 0.0004190455365460366f, 0.0005257276352494955f, 0.0006189229316078126f, 0.0003051940875593573f, 0.0005224175984039903f, 0.0006031321827322245f, 0.00036143866600468755f, 0.000649520312435925f, 0.0003909610095433891f, 0.0003967364609707147f, 0.0005869998713023961f, 0.0005993908853270113f, 0.0004226518503855914f, 0.0005785630783066154f, 0.00020387888071127236f, 0.0005654176929965615f, 0.0004504087846726179f, 0.000468054466182366f, 0.00013813447731081396f, 0.0003843358426820487f, 0.0006429262575693429f, 0.0006229385617189109f, 0.0005164904287084937f, 0.0004821327165700495f, 0.0005867319414392114f, 0.0005655919667333364f, 0.0003872286470141262f, 0.0006073015974834561f, 0.0006640144274570048f, 0.00031628404394723475f, 0.0007897333707660437f, 0.0003246143751312047f, 0.0004058266058564186f, 0.0006480031297542155f, 0.00023205015168059617f, 0.0006496296264231205f, 0.0008105572778731585f, 0.0006470521329902112f, 0.0005017443327233195f, 0.0005531138740479946f, 0.000386409810744226f, 0.00047914389870129526f, 0.00044061022344976664f, 0.00048306272947229445f, 0.0004591161268763244f, 0.0004129307344555855f, 0.0005531206843443215f, 0.0005122617585584521f, 0.0002497816167306155f, 0.0002500583650544286f, 0.0004750519583467394f, 0.0006260006339289248f, 0.0005315542221069336f, 0.0006030144286341965f, 0.0004994604969397187f, 0.0004832545528188348f, 0.000339153571985662f, 0.0005931876949034631f, 0.0004555000632535666f, 0.0005616365233436227f, 0.00048251639236696064f, 0.00021639207261614501f, 0.0002251854311907664f, 0.0005289075779728591f, 0.000664166989736259f, 0.00039120594738051295f, 0.0004588377196341753f, 0.0007868355605751276f, 0.0004480130737647414f, 0.0006022853194735944f, 0.00044204966980032623f, 0.0011212072568014264f, 0.0005537403631024063f, 0.0003847816842608154f, 0.000510759127791971f, 0.0004604599380400032f, 0.000637040997389704f, 0.0005958721158094704f, 0.0007380770402960479f, 0.0006417115801014006f, 0.0007507291156798601f, 0.0005931844934821129f, 0.0007001299527473748f, 0.0003941354516427964f, 0.0002952953800559044f, 0.00044720462756231427f, 0.0005084816948510706f, 0.0006513333646580577f, 0.000785249809268862f, 0.0005214918637648225f, 0.00037910891114734113f, 0.00045116571709513664f, 0.00041117225191555917f, 0.0007855879957787693f, 0.0005332150612957776f, 0.0008293939754366875f, 0.0005902575212530792f, 0.0004890267155133188f, 0.0006020774017088115f, 0.0002702709462027997f, 0.0005138423293828964f, 0.0005011752946302295f, 0.0002787398698274046f, 0.0004031636635772884f, 0.000539132859557867f, 0.00040729279862716794f, 0.0003543857019394636f, 0.0006733572226949036f, 0.00034811487421393394f, 0.0005390502046793699f, 0.00044169218745082617f, 0.00039522661245428026f, 0.00027911594952456653f, 0.0008136288379319012f, 0.0003609702398534864f, 0.0002917708770837635f, 0.00024747903808020055f, 0.0008420969825237989f, 0.0003480588784441352f, 0.0003295907226856798f, 0.0005062026320956647f, 0.0005472280899994075f, 0.000495242653414607f, 0.0006706540007144213f, 0.000849664444103837f, 0.0005034475470893085f, 0.0006332107004709542f, 0.0006695344345644116f, 0.0001478006597608328f, 0.0005459921667352319f, 0.001180250314064324f, 0.00054848229046911f, 0.0003603300719987601f, 0.00044565246207639575f, 0.0006099449819885194f, 0.0008187138591893017f, 0.0007792821270413697f, 0.0004085317486897111f, 0.0006796952802687883f, 0.00029650185024365783f, 0.0004390685062389821f, 0.0004917093901894987f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #36 */
AI_STATIC ai_intq_info_list conv2d_46_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(5.141832662047818e-05f, 0.00010711040522437543f, 5.0338694563834e-05f, 0.00013997901987750083f, 0.00015517568681389093f, 0.00016865457291714847f, 6.914213736308739e-05f, 0.00012400106061249971f, 0.0001597451773704961f, 0.00010225186997558922f, 0.00010552364983595908f, 0.0001148318697232753f, 0.00011713583808159456f, 0.00013243392459116876f, 0.00012876291293650866f, 7.16863723937422e-05f, 3.1366434996016324e-05f, 0.0001128267977037467f, 0.0001928302808664739f, 0.00017656727868597955f, 0.0001047354526235722f, 7.100678340066224e-05f, 0.00017225657938979566f, 0.00010032275895355269f, 0.00011473456106614321f, 0.00014991156058385968f, 0.00013203137496020645f, 0.00020066392607986927f, 0.00021892148652113974f, 6.788447353756055e-05f, 0.00022163669927977026f, 0.00012870393402408808f, 7.729129720246419e-05f, 0.00015308469301089644f, 8.659970626467839e-05f, 0.00018640189955476671f, 7.130278390832245e-05f, 3.878907227772288e-05f, 7.834979624021798e-05f, 0.0001965977717190981f, 4.939495192957111e-05f, 0.00010895841114688665f, 0.00021763486438430846f, 0.00023359974147751927f, 4.9838687118608505e-05f, 5.5228843848453835e-05f, 7.245445885928348e-05f, 0.00020858926291111857f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #37 */
AI_STATIC ai_intq_info_list conv2d_46_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.002185278804972768f, 0.0045521920546889305f, 0.0021393944043666124f, 0.005949108395725489f, 0.006594966631382704f, 0.007167819421738386f, 0.0029385406523942947f, 0.005270044784992933f, 0.006789169739931822f, 0.004345704335719347f, 0.004484754987061024f, 0.0048803542740643024f, 0.0049782730638980865f, 0.005628441460430622f, 0.005472423974424601f, 0.0030466706957668066f, 0.0013330734800547361f, 0.0047951387241482735f, 0.008195286616683006f, 0.007504109293222427f, 0.004451256711035967f, 0.0030177882872521877f, 0.0073209041729569435f, 0.004263717215508223f, 0.004876218736171722f, 0.006371241062879562f, 0.005611333064734936f, 0.008528216741979122f, 0.009304163046181202f, 0.0028850899543613195f, 0.009419559501111507f, 0.005469917319715023f, 0.003284880192950368f, 0.0065060993656516075f, 0.003680487396195531f, 0.007922080345451832f, 0.00303036835975945f, 0.0016485354863107204f, 0.0033298663329333067f, 0.008355405181646347f, 0.0020992853678762913f, 0.004630732350051403f, 0.009249481372535229f, 0.009927988983690739f, 0.0021181441843509674f, 0.0023472257889807224f, 0.0030793144833296537f, 0.00886504352092743f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #38 */
AI_STATIC ai_intq_info_list conv2d_45_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00023079752281773835f, 0.00020783033687621355f, 0.00017264143389184028f, 0.00019504778902046382f, 0.00017303755157627165f, 0.00014601543080061674f, 9.062027675099671e-05f, 0.00018437852850183845f, 0.00015578737657051533f, 0.00019670282199513167f, 0.00014946724695619196f, 0.00015991623513400555f, 0.0004992931499145925f, 0.000400072691263631f, 0.0003229381109122187f, 0.0007902777288109064f, 0.0002039473911281675f, 0.00041953628533519804f, 0.0001805012725526467f, 0.00017486265278421342f, 8.702126797288656e-05f, 0.00010497635958017781f, 0.00015024271851871163f, 0.00026410073041915894f, 0.0003960033063776791f, 0.00014107742754276842f, 0.00015159502800088376f, 0.0001436637539882213f, 7.850465044612065e-05f, 0.00018110949895344675f, 0.00022379125584848225f, 0.00016211203183047473f, 0.00017944886349141598f, 0.0001718733983580023f, 0.0005315424641594291f, 0.00044305509072728455f, 0.0003730514144990593f, 0.0001947939454112202f, 0.00021591877157334238f, 0.00020436730119399726f, 0.00016132283781189471f, 0.00014893856132403016f, 0.0001514499745098874f, 0.00014125395682640374f, 0.0001473784213885665f, 0.00012361897097434849f, 0.00012939961743541062f, 0.0001675657695159316f, 0.00028729881159961224f, 7.308890781132504e-05f, 0.0002926384622696787f, 0.00020082606351934373f, 0.00013225746806710958f, 0.00019547957344911993f, 0.000147370679769665f, 0.00020448149007279426f, 0.0002817169588524848f, 0.00020644029427785426f, 0.0001650159538257867f, 0.0004311231314204633f, 0.00022407915093936026f, 0.000183123120223172f, 0.00025019721942953765f, 0.00027514854446053505f, 0.00013279408449307084f, 5.833716568304226e-05f, 7.511397416237742e-05f, 0.0001535300543764606f, 0.00023377971956506371f, 0.00022248840832617134f, 0.0001733218232402578f, 0.00014157200348563492f, 9.341291297459975e-05f, 0.00012384948786348104f, 0.0002324984670849517f, 0.0002506462624296546f, 0.00024631788255646825f, 0.00014050921890884638f, 0.00027666124515235424f, 0.00015601803897880018f, 0.00010214349458692595f, 0.0001471011055400595f, 0.00018773712508846074f, 0.00016495701856911182f, 0.00019754048844333738f, 0.00018971538520418108f, 0.0001546537532703951f, 0.00021389801986515522f, 0.00019548178534023464f, 8.715638978173956e-05f, 0.0002448709565214813f, 0.00017881998792290688f, 0.000657270138617605f, 0.0001233639777638018f, 0.00016832102846819907f, 0.0003575113951228559f, 8.036561484914273e-05f, 0.0001815299183363095f, 0.0002725428785197437f, 5.719791806768626e-05f, 0.0005621021264232695f, 0.0003930593666154891f, 0.00043414271203801036f, 0.0001293425593758002f, 0.00017405007383786142f, 9.33894989429973e-05f, 0.0002825327683240175f, 0.00020138692343607545f, 0.0001734796242089942f, 0.00011705578799592331f, 0.00011971515050390735f, 0.00020885842968709767f, 0.00014219776494428515f, 0.00013687799219042063f, 0.00022539369820151478f, 0.00017105448932852596f, 0.00031586637487635016f, 0.00011219749285373837f, 0.0003081668692175299f, 0.00032591045601293445f, 0.00014177127741277218f, 6.440308061428368e-05f, 0.0006262416718527675f, 0.0002661805192474276f, 0.0003435826220083982f, 0.0001364036143058911f, 7.805546192685142e-05f, 0.00016387889627367258f, 0.0003801214916165918f, 0.00020990506163798273f, 0.00019771757069975138f, 0.00027855983353219926f, 0.0001217983226524666f, 0.0002099911798723042f, 0.00012666673865169287f, 0.00016561745724175125f, 0.00034945536754094064f, 0.0001324325130553916f, 0.0002899226965382695f, 7.606116560054943e-05f, 0.0002122404839610681f, 0.00013682829739991575f, 0.0007893198635429144f, 0.00014129639021120965f, 0.00011417607311159372f, 0.00011376871407264844f, 0.00014663810725323856f, 0.00017821100482251495f, 0.00022985295800026506f, 0.00020708954252768308f, 0.00034822625457309186f, 0.0005288485554046929f, 0.00022739304404240102f, 0.00034925478394143283f, 0.00024735310580581427f, 0.00016962847439572215f, 6.752912304364145e-05f, 7.866460509831086e-05f, 0.0004725274338852614f, 0.00016603227413725108f, 0.0002994687529280782f, 0.00020738301100209355f, 0.0001612710184417665f, 0.00018423465371597558f, 9.77041563601233e-05f, 0.0006006378680467606f, 0.00035586534067988396f, 8.466447616228834e-05f, 0.0002990064967889339f, 0.0001743907341733575f, 5.621737000183202e-05f, 0.00021671339345630258f, 0.00012968122609890997f, 0.00027294570463709533f, 0.0001935817999765277f, 0.00022132901358418167f, 0.00016390536620747298f, 0.00014155135431792587f, 0.0005210964009165764f, 0.00032711264793761075f, 0.00020057460642419755f, 7.596056093461812e-05f, 0.00014907680451869965f, 0.00020939158275723457f, 0.00012762170808855444f, 0.00022059926413930953f, 0.00013406573270913213f, 0.0001414786820532754f, 0.00045270100235939026f, 0.0001748661743476987f, 0.0001909740240080282f, 0.00034087756648659706f, 8.805387187749147e-05f, 0.00012634533050004393f, 0.00010706287139328197f, 0.0004133404290769249f, 0.0001359740417683497f, 0.00020656573178712279f, 0.0002519359113648534f, 0.0001864515506895259f, 0.00016724507440812886f, 8.619356231065467e-05f, 0.00046927377115935087f, 0.0002482574782334268f, 0.00027336343191564083f, 0.00020432713790796697f, 0.0002023574197664857f, 0.0001083421302610077f, 0.000266849878244102f, 0.00013471273996401578f, 0.00010160278179682791f, 0.00012185853120172396f, 0.00023243327450472862f, 0.00038254077662713826f, 0.0001423238863935694f, 0.0001725649635773152f, 0.0003949743404518813f, 0.0001864777586888522f, 0.00020375949679873884f, 0.00014812476001679897f, 0.00014711992116644979f, 0.00014895090134814382f, 0.00014820063370279968f, 0.000568730290979147f, 0.00013124039105605334f, 0.00013206733274273574f, 0.00023861219233367592f, 0.0001519413635833189f, 0.0004046419053338468f, 0.00011365753744030371f, 0.00010119553917320445f, 0.00013116282934788615f, 0.00022408459335565567f, 0.0002789729041978717f, 0.0004805890202987939f, 0.0004983465187251568f, 0.00025930721312761307f, 0.0002696307492442429f, 0.0002809670113492757f, 0.00025874399580061436f, 0.0001242950966116041f, 0.00024190181284211576f, 0.0004408514651004225f, 0.00010461147030582651f, 0.00022367778001353145f, 0.00017159462731797248f, 0.00020985619630664587f, 0.00017643620958551764f, 0.00014152871153783053f, 0.00020453965407796204f, 0.0002387779240962118f, 0.00041356051224283874f, 0.00010963153908960521f, 0.0003862476150970906f, 0.0002265080256620422f, 0.00045126231270842254f, 0.000176078945514746f, 0.0002681857440620661f, 0.0001590839819982648f, 0.00021441183343995363f, 0.00010688507609302178f, 0.00039455777732655406f, 9.591773414285854e-05f, 0.0003360435657668859f, 0.0005032551125623286f, 0.0001986515271710232f, 0.00027560145827010274f, 0.0002529770717956126f, 0.00014343751536216587f, 0.0001387635711580515f, 0.0002914090291596949f, 0.0003452122618909925f, 0.00015162258932832628f, 0.00011269513197476044f, 0.0002342642255825922f, 0.00011007810098817572f, 0.00011201799497939646f, 0.0004237736575305462f, 0.00038128267624415457f, 0.0002169829822378233f, 0.00014040316455066204f, 0.00015393966168630868f, 9.874864190351218e-05f, 0.00019991185399703681f, 0.00022950625861994922f, 0.0003193824668414891f, 0.0001848810788942501f, 0.00013682999997399747f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #39 */
AI_STATIC ai_intq_info_list conv2d_45_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.009808894246816635f, 0.008832789026200771f, 0.007337260991334915f, 0.008289530873298645f, 0.0073540955781936646f, 0.006205655634403229f, 0.003851361572742462f, 0.007836087606847286f, 0.006620963104069233f, 0.008359869942069054f, 0.006352357566356659f, 0.00679643964394927f, 0.02121995948255062f, 0.01700308918952942f, 0.013724870048463345f, 0.03358680382370949f, 0.008667764253914356f, 0.017830291762948036f, 0.0076713040471076965f, 0.007431662641465664f, 0.003698403714224696f, 0.004461495205760002f, 0.0063853152096271515f, 0.011224281042814255f, 0.016830140724778175f, 0.005995790474116802f, 0.006442788522690535f, 0.006105709355324507f, 0.003336447523906827f, 0.007697153836488724f, 0.009511128067970276f, 0.006889761425554752f, 0.007626576814800501f, 0.0073046195320785046f, 0.022590553387999535f, 0.018829841166734695f, 0.01585468463599682f, 0.0082787424325943f, 0.009176547639071941f, 0.008685610257089138f, 0.0068562207743525505f, 0.006329888477921486f, 0.006436623632907867f, 0.006003292743116617f, 0.006263582967221737f, 0.005253806244581938f, 0.005499483551830053f, 0.007121545262634754f, 0.012210199609398842f, 0.0031062786001712084f, 0.012437134981155396f, 0.008535107597708702f, 0.005620942451059818f, 0.008307881653308868f, 0.00626325374469161f, 0.00869046337902546f, 0.011972970329225063f, 0.008773712441325188f, 0.007013177964836359f, 0.01832273229956627f, 0.009523363783955574f, 0.007782732602208853f, 0.010633381083607674f, 0.011693812906742096f, 0.005643748212605715f, 0.0024793294724076986f, 0.003192343981936574f, 0.006525027099996805f, 0.009935637935996056f, 0.009455757215619087f, 0.007366177625954151f, 0.006016809958964586f, 0.003970048855990171f, 0.005263602826744318f, 0.009881184436380863f, 0.010652465745806694f, 0.01046850997954607f, 0.00597164174541831f, 0.011758103035390377f, 0.006630766671150923f, 0.004341098479926586f, 0.006251796614378691f, 0.007978827692568302f, 0.007010673172771931f, 0.008395470678806305f, 0.008062903769314289f, 0.006572784390300512f, 0.009090665727853775f, 0.008307975716888905f, 0.003704146482050419f, 0.010407015681266785f, 0.0075998492538928986f, 0.027933981269598007f, 0.005242968909442425f, 0.007153643760830164f, 0.015194234438240528f, 0.003415538463741541f, 0.007715021260082722f, 0.011583072133362293f, 0.002430911408737302f, 0.023889340460300446f, 0.016705023124814034f, 0.018451064825057983f, 0.005497058853507042f, 0.00739712780341506f, 0.003969053737819195f, 0.012007642537355423f, 0.00855894386768341f, 0.007372884079813957f, 0.004974870942533016f, 0.005087893921881914f, 0.008876482956111431f, 0.006043404806405306f, 0.005817314609885216f, 0.009579231962561607f, 0.007269815541803837f, 0.013424321077764034f, 0.0047683934681117535f, 0.01309709157794714f, 0.013851194642484188f, 0.006025278940796852f, 0.0027371307369321585f, 0.026615271344780922f, 0.011312671937048435f, 0.01460226159542799f, 0.00579715333878994f, 0.0033173570409417152f, 0.006964853033423424f, 0.016155162826180458f, 0.008920964784920216f, 0.008402996696531773f, 0.011838792823255062f, 0.005176428705453873f, 0.008924624882638454f, 0.0053833359852433205f, 0.007038741838186979f, 0.01485185232013464f, 0.005628381390124559f, 0.01232171431183815f, 0.0032325994689017534f, 0.00902022048830986f, 0.005815202370285988f, 0.033546093851327896f, 0.00600509624928236f, 0.004852483049035072f, 0.004835170228034258f, 0.006232119165360928f, 0.007573967333883047f, 0.00976875051856041f, 0.008801305666565895f, 0.014799615368247032f, 0.022476064041256905f, 0.009664203971624374f, 0.014843327924609184f, 0.01051250658929348f, 0.007209210190922022f, 0.002869987627491355f, 0.0033432457130402327f, 0.020082416012883186f, 0.00705637177452445f, 0.012727421708405018f, 0.008813777938485146f, 0.006854018196463585f, 0.007829972542822361f, 0.0041524264961481094f, 0.02552710846066475f, 0.015124276280403137f, 0.003598240204155445f, 0.012707775458693504f, 0.007411606144160032f, 0.002389238215982914f, 0.009210319258272648f, 0.005511451978236437f, 0.011600191704928875f, 0.008227226324379444f, 0.009406482800841331f, 0.006965978071093559f, 0.0060159326530992985f, 0.022146597504615784f, 0.013902286998927593f, 0.00852442067116499f, 0.0032283237669616938f, 0.006335764192044735f, 0.008899142034351826f, 0.005423922557383776f, 0.009375468827784061f, 0.005697793792933226f, 0.006012843921780586f, 0.019239792600274086f, 0.00743181211873889f, 0.008116396144032478f, 0.014487296342849731f, 0.0037422894965857267f, 0.005369676277041435f, 0.0045501720160245895f, 0.017566967755556107f, 0.005778896622359753f, 0.008779043331742287f, 0.010707275941967964f, 0.00792419072240591f, 0.007107915356755257f, 0.0036632262635976076f, 0.01994413509964943f, 0.010550943203270435f, 0.01161794550716877f, 0.008683903142809868f, 0.008600190281867981f, 0.004604540299624205f, 0.01134112011641264f, 0.005725291091948748f, 0.004318118095397949f, 0.005178987514227629f, 0.009878413751721382f, 0.016257982701063156f, 0.006048765033483505f, 0.007334010675549507f, 0.01678640954196453f, 0.00792530458420515f, 0.008659778162837029f, 0.006295301951467991f, 0.006252596620470285f, 0.006330413278192282f, 0.006298526655882597f, 0.02417103573679924f, 0.005577716510742903f, 0.00561286136507988f, 0.010141017846763134f, 0.006457507610321045f, 0.01719728112220764f, 0.004830445162951946f, 0.004300810396671295f, 0.005574420094490051f, 0.009523594751954079f, 0.011856348253786564f, 0.020425032824277878f, 0.021179726347327232f, 0.011020556092262268f, 0.011459306813776493f, 0.011941097676753998f, 0.010996619239449501f, 0.005282541271299124f, 0.010280827060341835f, 0.018736187368631363f, 0.0044459872879087925f, 0.009506305679678917f, 0.007292771711945534f, 0.008918887935578823f, 0.007498538587242365f, 0.006014970131218433f, 0.008692935109138489f, 0.01014806143939495f, 0.017576321959495544f, 0.004659340251237154f, 0.01641552336513996f, 0.009626590646803379f, 0.01917864754796028f, 0.007483354769647121f, 0.0113978935405612f, 0.006761069409549236f, 0.009112502448260784f, 0.004542615730315447f, 0.01676870509982109f, 0.004076503682881594f, 0.01428185123950243f, 0.02138834074139595f, 0.008442689664661884f, 0.011713061481714249f, 0.01075152587145567f, 0.0060960943810641766f, 0.0058974516578018665f, 0.012384883128106594f, 0.014671520330011845f, 0.006443960126489401f, 0.004789542872458696f, 0.009956229478120804f, 0.0046783192083239555f, 0.004760764539241791f, 0.018010379746556282f, 0.016204513609409332f, 0.009221776388585567f, 0.0059671346098184586f, 0.006542435381561518f, 0.004196817055344582f, 0.008496253751218319f, 0.009754016064107418f, 0.013573754578828812f, 0.007857445627450943f, 0.005815275013446808f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #40 */
AI_STATIC ai_intq_info_list conv2d_44_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00012233061715960503f, 0.00014042103430256248f, 0.000123688078019768f, 0.00012068767682649195f, 0.00012831056665163487f, 4.256516331224702e-05f, 0.00015639062621630728f, 0.0001735140976961702f, 0.00011255676508881152f, 7.732275844318792e-05f, 0.00014490628382191062f, 0.00010746465704869479f, 4.7183901187963784e-05f, 5.983395385555923e-05f, 2.168438004446216e-05f, 2.3629343559150584e-05f, 9.963972843252122e-05f, 8.52860976010561e-05f, 9.919354488374665e-05f, 5.137548578204587e-05f, 0.0002156293485313654f, 0.00010790323722176254f, 0.0001642636489123106f, 0.00015725760022178292f, 9.891960507957265e-05f, 0.00013518607011064887f, 9.810434130486101e-05f, 8.230615640059114e-05f, 0.00015132222324609756f, 0.00017367632244713604f, 0.00013757983106188476f, 0.00016352754028048366f, 0.00021818731329403818f, 0.00016430870164185762f, 0.00014776442549191415f, 0.00013984156248625368f, 9.006395703181624e-05f, 0.0001594942732481286f, 0.00010994241165462881f, 0.00012949587835464627f, 5.122291258885525e-05f, 0.00011310754780424759f, 0.00010868735989788547f, 8.478962990920991e-05f, 0.00013893719005864114f, 0.00012568503734655678f, 0.0001922594237839803f, 7.58367168600671e-05f, 0.00010761848534457386f, 0.00016637402586638927f, 3.2248644856736064e-05f, 5.257880184217356e-05f, 0.00017397929332219064f, 0.0001812802511267364f, 6.299695814959705e-05f, 7.659389666514471e-05f, 4.53954708063975e-05f, 0.0001229691260959953f, 9.183199290418997e-05f, 9.592826972948387e-05f, 8.321602945216e-05f, 7.407995144603774e-05f, 8.769935084274039e-05f, 4.175310823484324e-05f, 0.00020826401305384934f, 0.0001265784230781719f, 0.00011409619764890522f, 0.0001450330310035497f, 0.0001288481435040012f, 7.650686166016385e-05f, 0.00011642366007436067f, 9.937804861692712e-05f, 0.00011191816884092987f, 7.972794264787808e-05f, 0.00010292816295986995f, 7.388173980871215e-05f, 0.00013351210509426892f, 0.00012675460311584175f, 0.00010440892219776288f, 0.00010128146823262796f, 9.918786963680759e-05f, 0.00010938675404759124f, 9.19728190638125e-05f, 0.00011743201321223751f, 0.00012119409802835435f, 5.8747165894601494e-05f, 8.074445213424042e-05f, 7.80289774411358e-05f, 8.0257719673682e-05f, 0.00013491188292391598f, 9.738432709127665e-05f, 0.0001395050494465977f, 2.7802458134829067e-05f, 7.732466474408284e-05f, 0.00013556655903812498f, 0.00012143611093051732f, 9.053115354618058e-05f, 9.587384556652978e-05f, 5.32734629814513e-05f, 0.0001603143900865689f, 4.4238364353077486e-05f, 6.164822116261348e-05f, 5.427882933872752e-05f, 9.364556171931326e-05f, 7.851751433918253e-05f, 0.00017040185048244894f, 5.829145811730996e-05f, 0.00014726107474416494f, 0.00010699017002480105f, 0.00011304860527161509f, 0.00012879521818831563f, 0.00010009819379774854f, 0.00016809487715363503f, 9.5421499281656e-05f, 0.0002107367618009448f, 0.00012593601422850043f, 7.112642197171226e-05f, 9.394785593030974e-05f, 0.00010681358253350481f, 0.00011696897854562849f, 0.00014928521704860032f, 0.00016612984472885728f, 9.571134432917461e-05f, 4.580095628625713e-05f, 0.00013215171929914504f, 0.0001148937371908687f, 0.00017178010602947325f, 8.913009514799342e-05f, 0.00010060281783808023f, 0.000106435501947999f, 0.00011275176802882925f, 6.382641731761396e-05f, 0.0001493021409260109f, 7.826150977052748e-05f, 0.00011263459600741044f, 8.129865454975516e-05f, 8.81448868312873e-05f, 0.00017852197925094515f, 0.0001288283383473754f, 8.451561006950215e-05f, 0.00012410024646669626f, 0.00013135357585269958f, 3.3197015000041574e-05f, 0.00012131884432164952f, 9.171927376883104e-05f, 0.0001273412926821038f, 0.00010641885455697775f, 0.00013431852858047932f, 0.00014236950664781034f, 9.943899203790352e-05f, 5.1306207751622424e-05f, 0.0001298630377277732f, 7.416752487188205e-05f, 8.480506221530959e-05f, 7.316796836676076e-05f, 0.00015663713566027582f, 0.0001497454068157822f, 0.0001429808617103845f, 0.0001240415731444955f, 8.090578921837732e-05f, 9.761194814927876e-05f, 0.0001191439587273635f, 0.00011905060091521591f, 0.00018786930013448f, 0.00018205068772658706f, 5.752628203481436e-05f, 5.2747680456377566e-05f, 0.00016646151198074222f, 6.143072823761031e-05f, 0.00012030307698296383f, 0.00011158107372466475f, 8.77889251569286e-05f, 4.3329135223757476e-05f, 9.579143079463392e-05f, 0.00010551507875788957f, 0.00015557275037281215f, 0.00015043700113892555f, 0.0001461969077354297f, 6.346309965010732e-05f, 9.077051799977198e-05f, 0.00011992836516583338f, 7.32846456230618e-05f, 0.0001771188253769651f, 0.0002738963230513036f, 4.051761425216682e-05f, 8.20307686808519e-05f, 0.0002669956593308598f, 0.00014153138909023255f, 3.5214252420701087e-05f, 6.593084253836423e-05f, 0.00015874984092079103f, 5.312159191817045e-05f, 0.00011063746205763891f, 0.0001533801987534389f, 0.00011996249668300152f, 0.00014166595065034926f, 0.00011709144018823281f, 7.630930485902354e-05f, 6.976127770030871e-05f, 0.00011169834760949016f, 0.00011697418085532263f, 9.93964058579877e-05f, 5.725166556658223e-05f, 9.833730291575193e-05f, 0.00012644348316825926f, 0.00022806687047705054f, 0.00017032690811902285f, 0.0002064213331323117f, 0.0001012963693938218f, 0.00015911519585642964f, 0.00012733659241348505f, 0.00013803275942336768f, 9.690233127912506e-05f, 6.364430009853095e-05f, 0.0001382247282890603f, 8.877732034306973e-05f, 6.486399797722697e-05f, 9.994199353968725e-05f, 0.00015963461191859096f, 8.815313776722178e-05f, 0.00010241194831905887f, 0.00021200001356191933f, 7.092965097399428e-05f, 8.836001507006586e-05f, 0.0001056648907251656f, 0.0001362863404210657f, 0.00012622067879419774f, 7.810661190887913e-05f, 0.00013895765005145222f, 0.00012361662811599672f, 0.00011231051030335948f, 0.00013332617527339607f, 5.982992297504097e-05f, 0.00014035341155249625f, 4.617575905285776e-05f, 9.031443914864212e-05f, 8.608790813013911e-05f, 0.0001027881444315426f, 0.00010047335672425106f, 5.5274238548008725e-05f, 0.00010918166663032025f, 0.00017129239859059453f, 9.397983376402408e-05f, 0.00010229438339592889f, 8.539737609680742e-05f, 8.599569991929457e-05f, 5.7398417993681505e-05f, 0.00010181403922615573f, 9.454807877773419e-05f, 0.00014214856491889805f, 9.386274905409664e-05f, 7.690655183978379e-05f, 0.00012825119483750314f, 0.00010843658674275503f, 0.00024315182236023247f, 6.317321822280064e-05f, 0.00012247484119143337f, 9.9589851743076e-05f, 0.0001369179371977225f, 0.00017368962289765477f, 0.00012925952614750713f, 5.75023477722425e-05f, 0.00010585262498352677f, 0.00012120188330300152f, 5.671520557370968e-05f, 0.00010605690476950258f, 0.00010937618208117783f, 6.731394387315959e-05f, 0.00014436736819334328f, 6.320778629742563e-05f, 0.00014143632142804563f, 2.9079423256916925e-05f, 9.69549801084213e-05f, 0.00020191939256619662f, 0.00011760405323002487f, 8.716953743714839e-05f, 0.00014117774844635278f, 6.037208004272543e-05f, 9.82910132734105e-05f, 0.00019022087508346885f, 0.00014119574916549027f, 0.00023141880228649825f, 0.00021792019833810627f, 0.0002262326015625149f, 7.43379641789943e-05f, 0.00012367143062874675f, 8.269088721135631e-05f, 7.198510866146535e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #41 */
AI_STATIC ai_intq_info_list conv2d_44_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 288, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0007818694575689733f, 0.0008974933298304677f, 0.0007905456004664302f, 0.000771368679124862f, 0.000820089946500957f, 0.00027205291553400457f, 0.0009995620930567384f, 0.0011090058833360672f, 0.000719400413800031f, 0.0004942041705362499f, 0.0009261606028303504f, 0.0006868544733151793f, 0.0003015733091160655f, 0.0003824254381470382f, 0.00013859452155884355f, 0.00015102565521374345f, 0.000636841868981719f, 0.0005451014149002731f, 0.0006339901010505855f, 0.0003283635887783021f, 0.0013781831366941333f, 0.0006896576378494501f, 0.0010498821502551436f, 0.0010051033459603786f, 0.0006322392146103084f, 0.0008640343439765275f, 0.0006270285230129957f, 0.0005260552861727774f, 0.0009671677253209054f, 0.0011100426781922579f, 0.0008793339366093278f, 0.00104517734143883f, 0.0013945322716608644f, 0.0010501700453460217f, 0.0009444282623007894f, 0.0008937896927818656f, 0.0005756388418376446f, 0.0010193989146500826f, 0.0007026909152045846f, 0.0008276657899841666f, 0.00032738843583501875f, 0.0007229206967167556f, 0.0006946693174540997f, 0.0005419282824732363f, 0.0008880094392225146f, 0.0008033090271055698f, 0.00122881552670151f, 0.0004847062227781862f, 0.0006878376589156687f, 0.0010633704951032996f, 0.0002061154373222962f, 0.00033605453791096807f, 0.0011119791306555271f, 0.001158642815425992f, 0.0004026415990665555f, 0.0004895456950180233f, 0.0002901426632888615f, 0.0007859504548832774f, 0.0005869391607120633f, 0.00061312026809901f, 0.0005318706971593201f, 0.00047347796498797834f, 0.0005605255719274282f, 0.0002668627130333334f, 0.001331108040176332f, 0.0008090190822258592f, 0.0007292396039701998f, 0.0009269706788472831f, 0.0008235258283093572f, 0.0004889894044026732f, 0.0007441154448315501f, 0.0006351693300530314f, 0.0007153188344091177f, 0.0005095767555758357f, 0.0006578597240149975f, 0.0004722110752481967f, 0.0008533353102393448f, 0.0008101450512185693f, 0.0006673238822259009f, 0.0006473349640145898f, 0.0006339538376778364f, 0.0006991394329816103f, 0.0005878392257727683f, 0.0007505602552555501f, 0.0007746054325252771f, 0.0003754792851395905f, 0.0005160737200640142f, 0.0004987179418094456f, 0.0005129628116264939f, 0.0008622818859294057f, 0.0006224265671335161f, 0.0008916388615034521f, 0.0001776978897396475f, 0.0004942163359373808f, 0.0008664662600494921f, 0.0007761522429063916f, 0.0005786248948425055f, 0.0006127724191173911f, 0.00034049441455863416f, 0.0010246406309306622f, 0.0002827470889315009f, 0.0003940212482120842f, 0.000346920161973685f, 0.0005985304596833885f, 0.0005018403753638268f, 0.0010891141137108207f, 0.00037256666109897196f, 0.000941211124882102f, 0.0006838217959739268f, 0.0007225439767353237f, 0.000823187583591789f, 0.0006397721008397639f, 0.0010743691818788648f, 0.0006098813028074801f, 0.0013469124678522348f, 0.0008049131720326841f, 0.0004546006384771317f, 0.0006004625465720892f, 0.0006826931494288146f, 0.0007476008031517267f, 0.0009541483013890684f, 0.0010618098312988877f, 0.0006117338198237121f, 0.00029273430118337274f, 0.0008446404826827347f, 0.0007343370234593749f, 0.001097923144698143f, 0.0005696701118722558f, 0.0006429973873309791f, 0.000680276658385992f, 0.0007206467562355101f, 0.0004079430364072323f, 0.0009542564512230456f, 0.0005002041580155492f, 0.0007198978564701974f, 0.0005196158890612423f, 0.0005633732071146369f, 0.0011410134611651301f, 0.0008233992266468704f, 0.000540176872164011f, 0.000793179904576391f, 0.0008395391632802784f, 0.00021217689209152013f, 0.0007754027610644698f, 0.0005862187244929373f, 0.0008138948469422758f, 0.0006801702547818422f, 0.0008584895404055715f, 0.000909946858882904f, 0.0006355588557198644f, 0.0003279208031017333f, 0.000830012490041554f, 0.0004740376607514918f, 0.0005420268862508237f, 0.00046764907892793417f, 0.0010011376580223441f, 0.0009570895927026868f, 0.0009138543391600251f, 0.0007928048726171255f, 0.0005171049269847572f, 0.0006238814094103873f, 0.0007615020731464028f, 0.0007609053864143789f, 0.0012007562909275293f, 0.0011635669507086277f, 0.0003676760825328529f, 0.00033713391167111695f, 0.0010639296378940344f, 0.00039263113285414875f, 0.0007689105113968253f, 0.0007131643360480666f, 0.0005610981024801731f, 0.0002769357815850526f, 0.0006122456979937851f, 0.0006743938429281116f, 0.0009943346958607435f, 0.0009615098824724555f, 0.0009344095597043633f, 0.00040562092908658087f, 0.0005801547667942941f, 0.0007665155571885407f, 0.0004683947772718966f, 0.0011320452904328704f, 0.0017505933064967394f, 0.00025896611623466015f, 0.0005242951447144151f, 0.001706488081254065f, 0.0009045901242643595f, 0.0002250699617434293f, 0.0004213933425489813f, 0.001014640904031694f, 0.0003395237436052412f, 0.0007071332656778395f, 0.0009803210850805044f, 0.0007667337195016444f, 0.0009054502006620169f, 0.0007483835215680301f, 0.00048772673471830785f, 0.0004458753683138639f, 0.0007139138760976493f, 0.0007476340397261083f, 0.000635286676697433f, 0.0003659208887256682f, 0.0006285174749791622f, 0.0008081566193141043f, 0.0014576768735423684f, 0.0010886350646615028f, 0.0013193306513130665f, 0.0006474301917478442f, 0.0010169760789722204f, 0.0008138648117892444f, 0.0008822287782095373f, 0.0006193459266796708f, 0.0004067790578119457f, 0.0008834557957015932f, 0.0005674153799191117f, 0.0004145746643189341f, 0.000638773781247437f, 0.0010202958947047591f, 0.0005634259432554245f, 0.0006545603391714394f, 0.001354986452497542f, 0.0004533429746516049f, 0.0005647481884807348f, 0.0006753513589501381f, 0.000871066702529788f, 0.0008067325106821954f, 0.000499214103911072f, 0.0008881402318365872f, 0.0007900889031589031f, 0.0007178264786489308f, 0.0008521469426341355f, 0.000382399681257084f, 0.0008970611379481852f, 0.0002951298374682665f, 0.0005772397853434086f, 0.0005502261337824166f, 0.0006569647812284529f, 0.0006421699654310942f, 0.000353282259311527f, 0.0006978286546654999f, 0.001094806008040905f, 0.0006006669718772173f, 0.0006538089364767075f, 0.0005458126543089747f, 0.0005496367812156677f, 0.0003668588469736278f, 0.0006507388316094875f, 0.0006042988388799131f, 0.0009085347410291433f, 0.0005999185959808528f, 0.0004915440222248435f, 0.0008197104907594621f, 0.0006930665113031864f, 0.0015540915774181485f, 0.00040376815013587475f, 0.000782791234087199f, 0.000636523065622896f, 0.0008751035202294588f, 0.0011101276613771915f, 0.0008261551847681403f, 0.0003675231127999723f, 0.0006765512516722083f, 0.0007746552000753582f, 0.00036249213735572994f, 0.0006778569077141583f, 0.0006990718538872898f, 0.00043023336911574006f, 0.000922716106288135f, 0.0004039890773128718f, 0.0009039824944920838f, 0.00018585953512229025f, 0.0006196824251674116f, 0.0012905567418783903f, 0.0007516598561778665f, 0.0005571393412537873f, 0.0009023298625834286f, 0.00038586484151892364f, 0.0006282216054387391f, 0.0012157863238826394f, 0.0009024448809213936f, 0.0014791005523875356f, 0.0013928250409662724f, 0.001445953268557787f, 0.00047512701712548733f, 0.0007904391386546195f, 0.0005285142688080668f, 0.0004600888933055103f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #42 */
AI_STATIC ai_intq_info_list conv2d_43_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00013384218618739396f, 9.12997784325853e-05f, 0.00015286258712876588f, 0.00015412915672641248f, 0.0001554094924358651f, 0.0001355411222903058f, 0.00015528158110100776f, 0.00014409604773391038f, 0.00013764374307356775f, 0.0001508317654952407f, 0.00011283225467195734f, 0.00019442051416262984f, 0.0001298655552091077f, 0.00013526217662729323f, 0.00015465266187675297f, 0.000202687835553661f, 0.00016594499174971133f, 0.0001597395894350484f, 0.00012495856208261102f, 9.624572703614831e-05f, 0.0001496518962085247f, 0.00011173364327987656f, 0.00013112803571857512f, 0.0001615210494492203f, 0.00013142915850039572f, 0.00011579129204619676f, 0.0001428493415005505f, 0.00016270177729893476f, 0.0001466193061787635f, 0.00014189619105309248f, 0.0001139915402745828f, 0.00014167861081659794f, 0.00016051565762609243f, 0.0001689097989583388f, 0.00010429335088701919f, 0.0001555010094307363f, 0.00014381992514245212f, 0.0002051808260148391f, 0.0001540601224405691f, 8.213010733015835e-05f, 0.00025049378746189177f, 9.747964213602245e-05f, 0.00014760748308617622f, 0.0001234248047694564f, 0.00011984878074144945f, 0.00015323958359658718f, 0.00018413229554425925f, 0.00018579780589789152f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #43 */
AI_STATIC ai_intq_info_list conv2d_43_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.005688292905688286f, 0.0038802404887974262f, 0.006496659945696592f, 0.006550488993525505f, 0.006604903377592564f, 0.005760497879236937f, 0.006599466782063246f, 0.006124082021415234f, 0.005849858745932579f, 0.006410349626094103f, 0.004795370623469353f, 0.008262871764600277f, 0.005519285798072815f, 0.005748642608523369f, 0.006572738289833069f, 0.008614232763648033f, 0.007052662316709757f, 0.006788932718336582f, 0.005310738924890757f, 0.004090443253517151f, 0.006360205356031656f, 0.00474867969751358f, 0.005572941619902849f, 0.0068646445870399475f, 0.0055857389234006405f, 0.004921129904687405f, 0.0060710967518389225f, 0.006914825178682804f, 0.0062313200905919075f, 0.006030587945133448f, 0.004844640381634235f, 0.0060213408432900906f, 0.006821915041655302f, 0.007178666535764933f, 0.0044324672780931f, 0.006608792580664158f, 0.006112346891313791f, 0.008720184676349163f, 0.006547555327415466f, 0.003490529488772154f, 0.01064598560333252f, 0.0041428846307098866f, 0.006273317616432905f, 0.00524555379524827f, 0.005093573126941919f, 0.00651268195360899f, 0.007825622335076332f, 0.007896406576037407f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #44 */
AI_STATIC ai_intq_info_list conv2d_42_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00014770282723475248f, 0.00023435801267623901f, 0.00025145855033770204f, 8.916469960240647e-05f, 4.674625233747065e-05f, 0.00041347136721014977f, 9.189683623844758e-05f, 0.00020765062072314322f, 0.00017431368178222328f, 0.00016391267126891762f, 5.203817636356689e-05f, 0.00031743524596095085f, 0.00010448432294651866f, 0.0001388332893839106f, 0.0003271122695878148f, 8.130961214192212e-05f, 8.226433419622481e-05f, 7.446520612575114e-05f, 0.00011125187302241102f, 0.00013237948587629944f, 0.0005679787718690932f, 0.00012996258737985045f, 0.0001479299389757216f, 0.0002524893207009882f, 0.00016242946730926633f, 0.00014182347513269633f, 0.0002210463280789554f, 7.564478437416255e-05f, 6.260954978642985e-05f, 0.00012401668936945498f, 0.0002335976460017264f, 9.602959471521899e-05f, 0.0002860675158444792f, 0.0001082819071598351f, 0.00020661413145717233f, 0.00014482857659459114f, 0.00015422861906699836f, 0.00016141775995492935f, 8.465557766612619e-05f, 0.00018812603957485408f, 9.12082614377141e-05f, 5.515233351616189e-05f, 0.000175926586962305f, 0.0002199936716351658f, 3.3231888664886355e-05f, 8.136150427162647e-05f, 0.0002451914770063013f, 9.30694441194646e-05f, 6.924072658875957e-05f, 0.0002164913748856634f, 0.00011005951091647148f, 0.0001729561627143994f, 0.00022416102001443505f, 0.0001636976667214185f, 0.0002879220701288432f, 0.00013470232079271227f, 0.00010105693945661187f, 0.00015430377970915288f, 0.00014318461762741208f, 8.818997594062239e-05f, 0.00014238705625757575f, 0.00023936024808790535f, 3.493022086331621e-05f, 0.00013832762488164008f, 0.0001084032774087973f, 0.00018186915258411318f, 0.00029583246214315295f, 0.00010145115811610594f, 0.0003328458988107741f, 0.00029143516439944506f, 7.407058001263067e-05f, 0.00022502407955471426f, 0.00011989656195510179f, 0.0001022136930259876f, 0.0004975604242645204f, 0.0002552405057940632f, 6.570280675077811e-05f, 0.00018627273675519973f, 0.000256408384302631f, 0.0003600698837544769f, 0.00012007169425487518f, 6.28840207355097e-05f, 0.00015699096547905356f, 0.00011301153426757082f, 0.0001619277463760227f, 0.0001942925591720268f, 0.0002599808794911951f, 9.459086140850559e-05f, 0.00022287896717898548f, 0.00023982398852240294f, 6.263848626986146e-05f, 7.923033263068646e-05f, 0.0002660659374669194f, 0.00023713837435934693f, 0.00013672852946911007f, 0.0001302860036958009f, 9.800882980925962e-05f, 0.00022625102428719401f, 0.00011536214151419699f, 0.00011861248640343547f, 6.782927084714174e-05f, 0.0002836980565916747f, 0.00017280768952332437f, 9.052446694113314e-05f, 0.00014781428035348654f, 4.6681489038746804e-05f, 0.00013118068454787135f, 0.00032183952862396836f, 0.00010584277333691716f, 0.00020054297056049109f, 0.00014873895270284265f, 0.0002637268335092813f, 0.00010476979514351115f, 6.89032967784442e-05f, 9.137251618085429e-05f, 0.00015370140317827463f, 0.0001324590266449377f, 0.00010450636909808964f, 9.204702655551955e-05f, 0.00027313941973261535f, 0.0001465119275962934f, 0.00011585359607124701f, 0.0002071766066364944f, 9.523883636575192e-05f, 7.532551535405219e-05f, 0.00023095268988981843f, 0.00015344111307058483f, 0.00031235377537086606f, 0.00017004343681037426f, 0.00012980074097868055f, 0.0001382036425638944f, 0.00029173048096708953f, 0.0001350946258753538f, 0.0002598013961687684f, 8.351448195753619e-05f, 0.0001496940094511956f, 0.00012007954501314089f, 0.00011529090988915414f, 0.00023159792181104422f, 0.00010329313954571262f, 0.0001952079765032977f, 0.00025941632338799536f, 0.0001289105712203309f, 0.00014952932542655617f, 0.00011121795978397131f, 0.0001966369163710624f, 0.00016442869673483074f, 0.00013622836559079587f, 0.00022447295486927032f, 6.036816193955019e-05f, 3.7104404327692464e-05f, 9.760358079802245e-05f, 0.00014445523265749216f, 0.00011615406401688233f, 0.0002555939427111298f, 0.00010835572174983099f, 0.00019995641196146607f, 9.916436829371378e-05f, 7.06533101038076e-05f, 0.00019785361655522138f, 0.00019784261530730873f, 4.395952055347152e-05f, 0.00020723941270262003f, 0.00020973845676053315f, 0.000228245829930529f, 0.00011308546527288854f, 0.00012019622954539955f, 0.000156957030412741f, 0.0002535753883421421f, 0.0001888372062239796f, 0.00014550008927471936f, 0.0002652976254466921f, 0.0003690875309985131f, 0.00013946120452601463f, 0.000154552748426795f, 0.0002592950768303126f, 0.00018057346460409462f, 0.0001590131432749331f, 0.00012755363422911614f, 0.00028382125310599804f, 0.0002284978691022843f, 0.00014532690693158656f, 7.984587864484638e-05f, 0.0002193926920881495f, 0.00014794468006584793f, 0.00013082459918223321f, 0.00010463051876286045f, 7.031400309642777e-05f, 0.0001938073110068217f, 0.00037205807166174054f, 0.0002305131929460913f, 0.00019309527124278247f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #45 */
AI_STATIC ai_intq_info_list conv2d_42_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0062773702666163445f, 0.009960215538740158f, 0.01068698801100254f, 0.0037894996348768473f, 0.0019867157097905874f, 0.01757253333926201f, 0.003905615536496043f, 0.00882515124976635f, 0.007408331613987684f, 0.0069662886671721935f, 0.002211622428148985f, 0.013490998186171055f, 0.0044405837543308735f, 0.005900414660573006f, 0.013902271166443825f, 0.003455658443272114f, 0.0034962340723723173f, 0.003164771245792508f, 0.004728204570710659f, 0.005626128055155277f, 0.024139096960425377f, 0.005523409694433212f, 0.006287022493779659f, 0.010730796493589878f, 0.006903252098709345f, 0.006027497351169586f, 0.009394468739628792f, 0.003214903175830841f, 0.00266090570949018f, 0.005270709283649921f, 0.009927899576723576f, 0.004081257618963718f, 0.012157869525253773f, 0.004601981025189161f, 0.008781100623309612f, 0.00615521427243948f, 0.006554716266691685f, 0.0068602547980844975f, 0.003597862087190151f, 0.007995356805622578f, 0.0038763510528951883f, 0.0023439740762114525f, 0.007476879749447107f, 0.009349730797111988f, 0.0014123552246019244f, 0.0034578638151288033f, 0.010420638136565685f, 0.003955451305955648f, 0.0029427306726574898f, 0.009200883097946644f, 0.004677528981119394f, 0.007350636646151543f, 0.009526843205094337f, 0.006957150995731354f, 0.01223668735474348f, 0.005724848248064518f, 0.004294919781386852f, 0.0065579102374613285f, 0.006085345987230539f, 0.0037480739410966635f, 0.006051450036466122f, 0.010172810405492783f, 0.0014845343539491296f, 0.005878923926502466f, 0.004607139155268669f, 0.007729438599199057f, 0.0125728789716959f, 0.004311674274504185f, 0.01414595078676939f, 0.01238599419593811f, 0.0031479995232075453f, 0.009563523344695568f, 0.005095603875815868f, 0.004344081971794367f, 0.021146317943930626f, 0.010847721248865128f, 0.0027923693414777517f, 0.007916591130197048f, 0.01089735608547926f, 0.015302969142794609f, 0.005103047005832195f, 0.0026725707575678825f, 0.006672115996479988f, 0.004802990239113569f, 0.006881929002702236f, 0.008257433772087097f, 0.011049187742173672f, 0.004020111635327339f, 0.009472356177866459f, 0.010192519053816795f, 0.0026621355209499598f, 0.0033672889694571495f, 0.011307802051305771f, 0.010078380815684795f, 0.005810962524265051f, 0.005537155084311962f, 0.004165375139564276f, 0.009615668095648289f, 0.004902890883386135f, 0.005041030701249838f, 0.002882743952795863f, 0.01205716747790575f, 0.0073443264700472355f, 0.0038472898304462433f, 0.0062821065075695515f, 0.0019839631859213114f, 0.005575179122388363f, 0.013678180053830147f, 0.004498317837715149f, 0.008523075841367245f, 0.006321405526250601f, 0.01120838988572359f, 0.004452716093510389f, 0.0029283901676535606f, 0.0038833320140838623f, 0.006532309576869011f, 0.0056295087561011314f, 0.004441520664840937f, 0.003911998588591814f, 0.01160842552781105f, 0.006226756609976292f, 0.0049237776547670364f, 0.008805005811154842f, 0.0040476503781974316f, 0.003201334271579981f, 0.009815488941967487f, 0.006521247327327728f, 0.013275034725666046f, 0.007226846180856228f, 0.00551653141155839f, 0.005873654969036579f, 0.012398544698953629f, 0.005741521716117859f, 0.011041559278964996f, 0.0035493653267621994f, 0.006361995358020067f, 0.005103380419313908f, 0.0048998636193573475f, 0.00984291173517704f, 0.004389958456158638f, 0.008296338841319084f, 0.011025194078683853f, 0.00547869922593236f, 0.006354996468871832f, 0.004726763349026442f, 0.00835706852376461f, 0.006988219451159239f, 0.00578970555216074f, 0.009540100581943989f, 0.0025656467769294977f, 0.0015769371530041099f, 0.004148152191191912f, 0.006139347329735756f, 0.004936547484248877f, 0.0108627425506711f, 0.0046051181852817535f, 0.008498147130012512f, 0.004214485641568899f, 0.0030027655884623528f, 0.008408778347074986f, 0.008408310823142529f, 0.0018682796508073807f, 0.00880767498165369f, 0.008913883939385414f, 0.00970044732093811f, 0.004806132055819035f, 0.005108339712023735f, 0.006670673377811909f, 0.010776953771710396f, 0.008025581017136574f, 0.0061837537214159966f, 0.011275148950517178f, 0.015686219558119774f, 0.005927100777626038f, 0.006568491458892822f, 0.011020040139555931f, 0.00767437182366848f, 0.0067580584436655045f, 0.005421029403805733f, 0.012062402442097664f, 0.009711159393191338f, 0.00617639347910881f, 0.0033934498205780983f, 0.009324189275503159f, 0.006287648808211088f, 0.005560045130550861f, 0.004446797072887421f, 0.0029883452225476503f, 0.008236810564994812f, 0.0158124677836895f, 0.009796810336411f, 0.008206549100577831f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #46 */
AI_STATIC ai_intq_info_list conv2d_41_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0002862008986994624f, 0.00017659641162026674f, 9.439402492716908e-05f, 0.00025212581385858357f, 0.00017009522707667202f, 0.00010041975474450737f, 0.0003438263665884733f, 0.00011499998799990863f, 0.0001764818443916738f, 0.000286079099169001f, 0.0002292274875799194f, 0.00013765950279776007f, 0.00014923932030797005f, 0.00034184951800853014f, 0.00025188684230670333f, 0.0002268263924634084f, 0.00025535046006552875f, 0.00031550353742204607f, 0.0002842520480044186f, 0.000307151407469064f, 6.40439466224052e-05f, 0.0002067762688966468f, 0.000204637719434686f, 5.435413550003432e-05f, 0.00020074122585356236f, 9.769932512426749e-05f, 0.0001687770854914561f, 0.0002796553890220821f, 0.0002959629928227514f, 0.0002566548064351082f, 0.00014276512956712395f, 0.0002817557251546532f, 0.0001846068917075172f, 0.00018900273425970227f, 0.0002651806571520865f, 0.00034804907045327127f, 0.00022603852266911417f, 0.00012898631393909454f, 0.000194881358765997f, 0.0001859552867244929f, 0.00028159463545307517f, 0.00030510060605593026f, 0.00019458912720438093f, 0.00012805155711248517f, 0.00026370157138444483f, 0.00020155282982159406f, 0.00016264079022221267f, 0.000495599873829633f, 0.0003973740094806999f, 0.00018297669885214418f, 0.00021632730204146355f, 0.0003664210962597281f, 0.00014011276653036475f, 0.00023562840942759067f, 9.801843407331035e-05f, 0.00020930932078044862f, 0.0001599776151124388f, 0.00021517979621421546f, 0.00019915829761885107f, 0.00020376528846099973f, 0.00045345534454099834f, 0.0001754406257532537f, 0.00025438962620683014f, 0.0002893315104302019f, 9.919598960550502e-05f, 0.00024604343343526125f, 8.90539595275186e-05f, 0.00010723844752646983f, 0.00016549081192351878f, 0.0001133712285081856f, 0.00022922534844838083f, 8.180078293662518e-05f, 0.00037396280094981194f, 0.0002086907479679212f, 0.00015797802188899368f, 0.00014167196059133857f, 0.0004115627089049667f, 0.00020892717293463647f, 0.00014032417675480247f, 0.00013342265447136015f, 0.00014524944708682597f, 0.00038364893407560885f, 0.0002032013435382396f, 0.0001902970252558589f, 9.885670442599803e-05f, 0.00020740691979881376f, 0.0001355299900751561f, 0.00029404531233012676f, 0.00010267782636219636f, 0.00020275035058148205f, 0.00023017347848508507f, 0.00038385955849662423f, 0.00035188710899092257f, 0.00021792127517983317f, 0.00027582087204791605f, 0.0001650383201194927f, 0.00029025180265307426f, 0.00027606464573182166f, 0.0001803133200155571f, 0.00039362366078421474f, 0.00028860699967481196f, 0.0001850206172093749f, 0.00021338080114219338f, 0.00019078873447142541f, 0.00021235707390587777f, 0.0004056843463331461f, 0.00019091660215053707f, 0.00019132897432427853f, 0.00030143235926516354f, 0.00012219454220030457f, 0.00019223711569793522f, 0.00015903502935543656f, 0.00019399252778384835f, 0.0004097909259144217f, 0.00021175439178477973f, 0.00036233055288903415f, 0.00022486888337880373f, 0.0002762001531664282f, 0.00042470861808396876f, 0.00013743086310569197f, 0.00013037463941145688f, 0.0002374207106186077f, 0.00018476348486728966f, 0.0002551230136305094f, 0.00020813751325476915f, 0.00023371238785330206f, 0.00027180457254871726f, 0.00016206256987061352f, 0.00033069009077735245f, 0.00023042554676067084f, 0.0003897689748555422f, 0.0001729709911160171f, 0.00016806123312562704f, 0.00041417384636588395f, 0.0002178681897930801f, 0.0001718228595564142f, 0.0003935866989195347f, 0.0003585274098441005f, 0.0001210025220643729f, 0.00024494685931131244f, 9.668819984653965e-05f, 0.0002370478759985417f, 0.00015797966625541449f, 0.00015910201182123274f, 0.00025672820629552007f, 0.00016068245167843997f, 0.00021187507081776857f, 0.00028235718491487205f, 0.0001078043133020401f, 0.0002922526327893138f, 0.0006214690511114895f, 0.00015932209498714656f, 0.00024779545492492616f, 0.00023307566880248487f, 0.00016349650104530156f, 0.00023161487479228526f, 0.000235486586461775f, 0.00036799744702875614f, 0.00012995720317121595f, 0.00014514787471853197f, 0.00010905904491664842f, 0.00035838180338032544f, 0.00020065590797457844f, 0.0002938290126621723f, 0.00016272546781692654f, 0.0001914857275551185f, 0.00022118781635072082f, 0.0003973711864091456f, 0.0002547610201872885f, 0.00018899940187111497f, 0.0001835650618886575f, 0.00010907280375249684f, 0.00012890406651422381f, 0.0003226468979846686f, 0.0002017303486354649f, 0.00020660784502979368f, 0.00017954969371203333f, 0.00022362808522302657f, 0.0003066076315008104f, 0.00014059427485335618f, 0.00011648838699329644f, 0.00014965601440053433f, 0.0003016482514794916f, 0.00020129888434894383f, 0.00021490971266757697f, 0.0002482854179106653f, 0.00025263699353672564f, 0.00019039708422496915f, 0.00017326742818113416f, 9.537727601127699e-05f, 0.0002534211380407214f, 0.00026012916350737214f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #47 */
AI_STATIC ai_intq_info_list conv2d_41_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0008093458600342274f, 0.0004993959446437657f, 0.0002669363166205585f, 0.000712985114660114f, 0.0004810113168787211f, 0.00028397643472999334f, 0.0009723045513965189f, 0.0003252078022342175f, 0.0004990719608031213f, 0.0008090014453046024f, 0.0006482310709543526f, 0.00038928649155423045f, 0.0004220329865347594f, 0.0009667142294347286f, 0.0007123093237169087f, 0.0006414410308934748f, 0.0007221041014418006f, 0.000892210635356605f, 0.0008038347004912794f, 0.0008685916545800865f, 0.00018110951350536197f, 0.0005847414140589535f, 0.000578693812713027f, 0.0001537077478133142f, 0.0005676749278791249f, 0.000276283361017704f, 0.0004772837564814836f, 0.0007908358238637447f, 0.0008369520073756576f, 0.0007257926044985652f, 0.0004037246690131724f, 0.000796775333583355f, 0.0005220487364567816f, 0.0005344797391444445f, 0.0007499028579331934f, 0.0009842459112405777f, 0.00063921301625669f, 0.0003647596458904445f, 0.0005511038471013308f, 0.000525861862115562f, 0.0007963198586367071f, 0.0008627921924926341f, 0.0005502774729393423f, 0.0003621162613853812f, 0.0007457201136276126f, 0.0005699700559489429f, 0.0004599309468176216f, 0.001401504036039114f, 0.0011237316066399217f, 0.0005174387479200959f, 0.000611750700045377f, 0.0010362000903114676f, 0.0003962240880355239f, 0.0006663321983069181f, 0.0002771857543848455f, 0.0005919046234339476f, 0.00045239977771416306f, 0.0006085056811571121f, 0.0005631985841318965f, 0.0005762266810052097f, 0.0012823237339034677f, 0.0004961275262758136f, 0.000719386909622699f, 0.0008181988960132003f, 0.00028051575645804405f, 0.0006957848090678453f, 0.0002518351830076426f, 0.00030325897387228906f, 0.0004679904959630221f, 0.00032060183002613485f, 0.0006482250173576176f, 0.00023132395290303975f, 0.0010575272608548403f, 0.000590155366808176f, 0.00044674513628706336f, 0.00040063331834971905f, 0.0011638557771220803f, 0.0005908239400014281f, 0.0003968219389207661f, 0.00037730514304712415f, 0.0004107500717509538f, 0.0010849186219274998f, 0.0005746319075115025f, 0.0005381398368626833f, 0.0002795562904793769f, 0.0005865248385816813f, 0.0003832644724752754f, 0.0008315290324389935f, 0.0002903620188590139f, 0.0005733565194532275f, 0.0006509062368422747f, 0.0010855142027139664f, 0.0009950995445251465f, 0.0006162583013065159f, 0.000779992260504514f, 0.0004667108878493309f, 0.0008208014187403023f, 0.0007806816138327122f, 0.0005099069676361978f, 0.001113126054406166f, 0.0008161500445567071f, 0.0005232187104411423f, 0.0006034183315932751f, 0.0005395303596742451f, 0.0006005233153700829f, 0.001147232367657125f, 0.0005398919456638396f, 0.00054105807794258f, 0.0008524188306182623f, 0.00034555321326479316f, 0.0005436261999420822f, 0.0004497342451941222f, 0.0005485903238877654f, 0.0011588453780859709f, 0.0005988189950585365f, 0.0010246324818581343f, 0.0006359054241329432f, 0.0007810647948645055f, 0.001201031031087041f, 0.0003886399499606341f, 0.0003686856653075665f, 0.0006714006303809583f, 0.0005224915803410113f, 0.0007214608485810459f, 0.0005885908612981439f, 0.0006609138799831271f, 0.0007686345488764346f, 0.0004582958063110709f, 0.0009351565386168659f, 0.000651619047857821f, 0.0011022253893315792f, 0.0004891436547040939f, 0.00047525938134640455f, 0.0011712397681549191f, 0.0006161081837490201f, 0.0004858968604821712f, 0.0011130215134471655f, 0.0010138775687664747f, 0.0003421823203098029f, 0.0006926837959326804f, 0.0002734239969868213f, 0.0006703463150188327f, 0.00044674979289993644f, 0.0004499236529227346f, 0.0007260002312250435f, 0.00045439298264682293f, 0.0005991602665744722f, 0.0007984762778505683f, 0.00030485918978229165f, 0.0008264594944193959f, 0.001757448655553162f, 0.00045054603833705187f, 0.0007007393287494779f, 0.0006591133424080908f, 0.00046235081390477717f, 0.000654982344713062f, 0.0006659311475232244f, 0.0010406578658148646f, 0.0003675052139442414f, 0.0004104628460481763f, 0.00030840744148008525f, 0.0010134658077731729f, 0.0005674336571246386f, 0.0008309173281304538f, 0.0004601704131346196f, 0.000541501387488097f, 0.0006254957406781614f, 0.0011237236903980374f, 0.0007204372086562216f, 0.0005344703095033765f, 0.0005191025556996465f, 0.0003084463533014059f, 0.0003645270480774343f, 0.0009124112548306584f, 0.0005704720970243216f, 0.0005842651007696986f, 0.0005077475216239691f, 0.0006323965499177575f, 0.0008670539245940745f, 0.0003975857107434422f, 0.0003294168272987008f, 0.0004232113715261221f, 0.0008530293707735837f, 0.0005692519480362535f, 0.0006077419384382665f, 0.000702124903909862f, 0.0007144306437112391f, 0.0005384227843023837f, 0.0004899819614365697f, 0.0002697168383747339f, 0.0007166481809690595f, 0.000735617708414793f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #48 */
AI_STATIC ai_intq_info_list conv2d_39_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(6.818052497692406e-05f, 0.00011241664469707757f, 0.00015609870024491102f, 0.0003281942626927048f, 0.00012568285455927253f, 0.0001819686294766143f, 0.00032164936419576406f, 0.00010808898514369503f, 8.364568202523515e-05f, 0.00020019241492263973f, 9.40685422392562e-05f, 0.00014452759933192283f, 9.580919868312776e-05f, 9.990632679546252e-05f, 0.00013136649795342237f, 0.00014186915359459817f, 0.0002520350681152195f, 0.0001950755249708891f, 0.00012668877025134861f, 0.00023053234326653183f, 0.00011757091851904988f, 0.0001785445783752948f, 0.00028663838747888803f, 0.00013265389134176075f, 0.00028736639069393277f, 0.00011976302630500868f, 0.00021699347416870296f, 0.00016405836504418403f, 8.23375812615268e-05f, 6.443104211939499e-05f, 0.00016838138981256634f, 9.571723057888448e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #49 */
AI_STATIC ai_intq_info_list conv2d_39_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0028976721223443747f, 0.0047777071595191956f, 0.006634194403886795f, 0.013948256149888039f, 0.005341521464288235f, 0.007733666338026524f, 0.013670097105205059f, 0.004593781661242247f, 0.003554941387847066f, 0.008508177474141121f, 0.003997913096100092f, 0.006142422556877136f, 0.004071890842169523f, 0.004246018826961517f, 0.0055830758064985275f, 0.0060294391587376595f, 0.0107114901766181f, 0.008290709927678108f, 0.005384272430092096f, 0.009797624312341213f, 0.0049967640079557896f, 0.007588144391775131f, 0.01218213140964508f, 0.0056377905420959f, 0.012213071808218956f, 0.005089928396046162f, 0.009222222492098808f, 0.0069724805653095245f, 0.003499347250908613f, 0.002738319104537368f, 0.007156209088861942f, 0.004067982081323862f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #50 */
AI_STATIC ai_intq_info_list conv2d_38_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00012619960762094706f, 0.00021174925495870411f, 0.00017684193153399974f, 0.00013136756024323404f, 7.449159602401778e-05f, 0.00025517784524708986f, 0.00011706643999787048f, 0.00011409555008867756f, 0.00015798445383552462f, 0.00023872552264947444f, 0.0001758137805154547f, 0.00036315148463472724f, 0.0003597745089791715f, 0.00014211743837222457f, 0.00023064602282829583f, 0.0001361465110676363f, 0.00020071906328666955f, 0.0002814809267874807f, 0.000486684322822839f, 8.033048652578145e-05f, 0.0003154808364342898f, 0.00018118070147465914f, 0.0002722345816437155f, 0.00042726853280328214f, 0.00045312257134355605f, 0.0001815578289097175f, 0.00013545682304538786f, 9.658867202233523e-05f, 0.00020909217710141093f, 0.00022496316523756832f, 0.0001500954240327701f, 0.0001581078686285764f, 0.00028658355586230755f, 0.00043234333861619234f, 0.0005168570205569267f, 0.0001454849843867123f, 0.00021823326824232936f, 0.00019437653827480972f, 0.0004197469097562134f, 0.00034879843587987125f, 0.00020904006669297814f, 8.850782614899799e-05f, 0.0001230309862876311f, 0.00016361704911105335f, 8.466617146041244e-05f, 0.0002826815762091428f, 0.00019345631881151348f, 0.00017975088849198073f, 0.0005584939499385655f, 0.00024220597697421908f, 0.00023672603128943592f, 0.00020805129315704107f, 0.00023107713786885142f, 0.00022637266374658793f, 0.00028173092869110405f, 0.0005712080746889114f, 0.00018915883265435696f, 0.00018729554722085595f, 0.00013188229058869183f, 0.00025613061734475195f, 6.808478065067902e-05f, 0.000452668231446296f, 9.340205724583939e-05f, 0.0004286332114133984f, 0.0003157896571792662f, 0.00018013757653534412f, 0.0002596013655420393f, 0.00036991783417761326f, 0.00015428061306010932f, 0.0001620189577806741f, 0.0005244379863142967f, 0.0001434663572581485f, 0.0002993175876326859f, 0.00015065795741975307f, 0.0001608567254152149f, 0.0001719016581773758f, 6.0064838180551305e-05f, 0.000237503758398816f, 0.00014283608470577747f, 0.00018583198834676296f, 0.00013676541857421398f, 0.0003147621755488217f, 0.0002447766892146319f, 0.00026133531355299056f, 0.00016563459939789027f, 0.00014987631584517658f, 0.0002853128535207361f, 0.00023028400028124452f, 0.00028583683888427913f, 0.00018763182742986828f, 0.0004548255237750709f, 0.00010605925490381196f, 0.0001243370061274618f, 0.0005713324062526226f, 0.00037858643918298185f, 0.00030677783070132136f, 0.00036051051574759185f, 0.00018625249504111707f, 0.0001961665548151359f, 0.000500583671964705f, 0.00020458306244108826f, 0.0001909842249006033f, 0.00028300806297920644f, 0.00017100122931879014f, 0.00018010882195085287f, 0.0002473472850397229f, 9.800084080779925e-05f, 0.000134227258968167f, 9.160378976957873e-05f, 0.00030363083351403475f, 0.00035433247103355825f, 0.00017472816398367286f, 0.00039591125096194446f, 0.000227266427827999f, 0.00022913211432751268f, 0.00019003554189112037f, 0.00014071039913687855f, 0.00027808468439616263f, 0.0005570757784880698f, 0.00025205768179148436f, 0.0008565393509343266f, 0.0002938814868684858f, 0.00019281414279248565f, 0.000288576353341341f, 0.00033595177228562534f, 0.00024653220316395164f, 0.0002550949575379491f, 0.00015907423221506178f, 7.113184983609244e-05f, 0.000510983809363097f, 0.00024874883820302784f, 0.00023033260367810726f, 0.00030859452090226114f, 0.00017834300524555147f, 0.0003632493317127228f, 0.0001658519176999107f, 0.0004164281999692321f, 0.00014368190022651106f, 0.0002044950524577871f, 0.0001577924267621711f, 0.0003276992356404662f, 0.00018910912331193686f, 0.00019172653264831752f, 0.00021290344011504203f, 0.00011251190881012008f, 0.00031621771631762385f, 0.00010957467020489275f, 0.00022083662042859942f, 8.641271415399387e-05f, 0.00011112494394183159f, 0.0001937993074534461f, 8.188388164853677e-05f, 0.0001756353594828397f, 0.00041516381315886974f, 0.00014967120660003275f, 0.00015986479411367327f, 6.543361814692616e-05f, 0.0004890745040029287f, 0.0002489991602487862f, 0.000365530519047752f, 0.00020871745073236525f, 0.00019118734053336084f, 0.0001312467793468386f, 0.00013977345952298492f, 0.00030880823032930493f, 0.00025601134984754026f, 0.00014411902520805597f, 0.00032089263550005853f, 7.544957770733163e-05f, 0.00023291206161957234f, 0.0003225365071557462f, 0.00024129720986820757f, 0.00032548338640481234f, 0.0001708529598545283f, 0.0003698316286318004f, 0.000337069301167503f, 0.0003063413023483008f, 0.00035701689193956554f, 0.00019461553893052042f, 0.0006559256580658257f, 0.00024605431826785207f, 9.024036262417212e-05f, 0.0002397610223852098f, 0.00011111440107924864f, 0.00026772322598844767f, 9.757652878761292e-05f, 0.00024534607655368745f, 0.0001801106263883412f, 0.00012424899614416063f, 0.00018097170686814934f, 0.0002035004727076739f, 0.0001619270333321765f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #51 */
AI_STATIC ai_intq_info_list conv2d_38_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00536348344758153f, 0.008999343030154705f, 0.0075157820247113705f, 0.005583121441304684f, 0.003165892791002989f, 0.010845057666301727f, 0.004975323565304279f, 0.004849060904234648f, 0.00671433936804533f, 0.010145834647119045f, 0.007472085300832987f, 0.01543393824249506f, 0.015290415845811367f, 0.006039991043508053f, 0.009802456013858318f, 0.005786226596683264f, 0.008530559949576855f, 0.01196293905377388f, 0.02068408392369747f, 0.0034140455536544323f, 0.01340793538838625f, 0.0077001797035336494f, 0.011569969356060028f, 0.018158912658691406f, 0.01925770938396454f, 0.007716207765042782f, 0.005756915081292391f, 0.004105018451809883f, 0.008886417374014854f, 0.009560934267938137f, 0.0063790553249418736f, 0.006719584576785564f, 0.012179800309240818f, 0.018374592065811157f, 0.021966421976685524f, 0.006183111574500799f, 0.009274913929402828f, 0.008261002600193024f, 0.01783924363553524f, 0.014823933131992817f, 0.008884202688932419f, 0.0037615825422108173f, 0.005228816531598568f, 0.006953724194318056f, 0.003598312148824334f, 0.012013967148959637f, 0.008221893571317196f, 0.007639412768185139f, 0.02373599261045456f, 0.010293753817677498f, 0.010060856118798256f, 0.00884217955172062f, 0.009820777922868729f, 0.00962083786725998f, 0.011973564513027668f, 0.02427634224295616f, 0.008039250038564205f, 0.007960060611367226f, 0.005604997277259827f, 0.010885551571846008f, 0.0028936031740158796f, 0.01923839934170246f, 0.0039695873856544495f, 0.01821691170334816f, 0.01342105958610773f, 0.0076558468863368034f, 0.011033058166503906f, 0.015721507370471954f, 0.00655692582949996f, 0.006885805632919073f, 0.02228861302137375f, 0.0060973200015723705f, 0.01272099744528532f, 0.00640296284109354f, 0.006836410611867905f, 0.007305820472538471f, 0.002552755642682314f, 0.010093909688293934f, 0.0060705337673425674f, 0.007897859439253807f, 0.005812529940158129f, 0.013377392664551735f, 0.010403009131550789f, 0.011106750927865505f, 0.007039470132440329f, 0.006369743496179581f, 0.01212579570710659f, 0.009787069633603096f, 0.012148065492510796f, 0.007974352687597275f, 0.019330084323883057f, 0.004507518373429775f, 0.005284322891384363f, 0.02428162656724453f, 0.016089923679828644f, 0.013038057833909988f, 0.015321696177124977f, 0.007915730588138103f, 0.008337078616023064f, 0.021274805068969727f, 0.008694780059158802f, 0.008116829209029675f, 0.012027841992676258f, 0.007267551962286234f, 0.007654624991118908f, 0.010512258857488632f, 0.004165035672485828f, 0.005704658105969429f, 0.0038931609597057104f, 0.01290431059896946f, 0.01505912933498621f, 0.007425946649163961f, 0.01682622730731964f, 0.009658822789788246f, 0.009738114662468433f, 0.008076510392129421f, 0.0059801917523145676f, 0.011818598955869675f, 0.023675719276070595f, 0.010712451301515102f, 0.03640292212367058f, 0.012489963322877884f, 0.00819460116326809f, 0.01226449478417635f, 0.014277949929237366f, 0.01047761831432581f, 0.010841535404324532f, 0.006760654971003532f, 0.003023103578016162f, 0.021716810762882233f, 0.010571825318038464f, 0.009789135307073593f, 0.013115267269313335f, 0.007579577621072531f, 0.015438096597790718f, 0.00704870605841279f, 0.017698198556900024f, 0.006106480490416288f, 0.008691039867699146f, 0.006706178188323975f, 0.0139272166416049f, 0.008037137798964977f, 0.008148377761244774f, 0.009048395790159702f, 0.004781756084412336f, 0.013439252972602844f, 0.004656923469156027f, 0.009385555982589722f, 0.0036725401878356934f, 0.004722809884697199f, 0.008236470632255077f, 0.0034800649154931307f, 0.007464502938091755f, 0.01764446124434471f, 0.0063610258512198925f, 0.00679425336420536f, 0.0027809287421405315f, 0.02078566513955593f, 0.010582463815808296f, 0.015535046346485615f, 0.00887049175798893f, 0.008125461637973785f, 0.005577987991273403f, 0.005940372124314308f, 0.013124349527060986f, 0.010880482383072376f, 0.006125058513134718f, 0.013637936674058437f, 0.0032066069543361664f, 0.009898762218654156f, 0.013707800768315792f, 0.01025513093918562f, 0.013833043165504932f, 0.007261250633746386f, 0.015717843547463417f, 0.014325444586575031f, 0.013019504956901073f, 0.01517321728169918f, 0.008271160535514355f, 0.0278768390417099f, 0.010457308031618595f, 0.0038352152332663536f, 0.010189843364059925f, 0.004722361918538809f, 0.011378237046301365f, 0.004147002473473549f, 0.010427208617329597f, 0.007654701359570026f, 0.00528058223426342f, 0.007691297214478254f, 0.008648769930005074f, 0.006881898734718561f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #52 */
AI_STATIC ai_intq_info_list conv2d_37_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00013943407975602895f, 0.00012369673640932888f, 0.0002680801844689995f, 0.0002111766516463831f, 0.0001771768438629806f, 0.00016607258294243366f, 0.00015086140774656087f, 0.00016097462503239512f, 0.00018818031821865588f, 0.00014171755174174905f, 0.00021464511519297957f, 8.341434295289218e-05f, 8.103805157588795e-05f, 0.00014019478112459183f, 0.00017128467152360827f, 0.0001522792736068368f, 0.00014330407429952174f, 9.697615314507857e-05f, 0.00014165806351229548f, 0.00031467858934774995f, 0.00013624070561490953f, 9.401800343766809e-05f, 0.00012445481843315065f, 0.0001014604204101488f, 0.0001162771150120534f, 0.0001819244644138962f, 8.160720608429983e-05f, 0.0002190653030993417f, 0.00012042868911521509f, 0.00016537799092475325f, 0.00011818725033663213f, 0.00014313105202745646f, 0.0002041121042566374f, 0.000144576930324547f, 5.9582489484455436e-05f, 0.00018098411965183914f, 0.00019203849660698324f, 0.0002958608092740178f, 8.500568947056308e-05f, 0.00015946137136779726f, 8.80227962625213e-05f, 0.0001769600057741627f, 0.0001280750147998333f, 0.00013373233377933502f, 0.00017372527508996427f, 0.0001623092539375648f, 0.0003096818982157856f, 6.777898670407012e-05f, 0.00023034913465380669f, 0.00013435506843961775f, 0.00017353253497276455f, 0.00014690871466882527f, 0.00014935416402295232f, 0.00018311379244551063f, 0.0001624706492293626f, 0.00012723747931886464f, 0.00014341023052111268f, 9.871552902041003e-05f, 0.0001507832494098693f, 0.0001712642697384581f, 0.00021273807215038687f, 0.00010645373549778014f, 0.0001854730217019096f, 0.00016200781101360917f, 0.00024746518465690315f, 0.00014884471602272242f, 8.543468720745295e-05f, 0.0002406927669653669f, 0.00016294549277517945f, 0.00016877515008673072f, 9.230281284544617e-05f, 0.00013727514306083322f, 0.0001710287033347413f, 0.00020053068874403834f, 0.00013565750850830227f, 0.00016134839097503573f, 0.00023079640232026577f, 0.00031132137519307435f, 0.00022082355280872434f, 8.897078078007326e-05f, 0.00012216684990562499f, 0.00018189926049672067f, 0.0001628767349757254f, 0.00033820333192124963f, 0.0001362335024168715f, 0.00017918672529049218f, 0.00015825789887458086f, 0.00017180843860842288f, 0.0001038954796968028f, 0.00012325934949330986f, 0.00020042738469783217f, 0.00010733691306086257f, 9.573123679729179e-05f, 0.00023224882897920907f, 7.410796388285235e-05f, 0.00013206600851844996f, 4.975319097866304e-05f, 0.000194459586055018f, 0.0002749380946625024f, 9.226711699739099e-05f, 0.0002072773640975356f, 0.00023514899658039212f, 0.0002450720639899373f, 0.0002151333901565522f, 9.371840860694647e-05f, 0.00013477468746714294f, 0.0002664517378434539f, 9.559985483065248e-05f, 0.00017076781659852713f, 0.00012892161612398922f, 0.00010996153287123889f, 0.00016871138359420002f, 0.00020258364384062588f, 0.00010190628381678835f, 0.00017726221994962543f, 0.00011677738802973181f, 0.0002107450709445402f, 0.00010536834452068433f, 6.356186349876225e-05f, 0.0001465142413508147f, 0.0001659040863160044f, 0.00020476571808103472f, 0.00016451576084364206f, 0.0002246316580567509f, 8.417498611379415e-05f, 0.00015511448145844042f, 0.00010151454625884071f, 0.00014641121379099786f, 0.000176061803358607f, 7.583328988403082e-05f, 0.00016931277059484273f, 0.00012368688476271927f, 0.00021057919366285205f, 9.059029980562627e-05f, 0.00023306124785449356f, 0.00014979987463448197f, 0.00019359610450919718f, 0.00015662408259231597f, 0.00020886365382466465f, 0.0001592083426658064f, 0.00026012054877355695f, 0.00033713693846948445f, 0.00012959755258634686f, 0.00022924829681869596f, 0.00016800995217636228f, 9.873327508103102e-05f, 0.0002714572474360466f, 0.00020915466302540153f, 0.0001589748280821368f, 0.00023673626128584146f, 0.00016728376795072109f, 0.00031093612778931856f, 0.0001595353096490726f, 0.00016357662389054894f, 0.0001641491544432938f, 0.00017919280799105763f, 0.00023617582337465137f, 0.00013651752669829875f, 0.00023758246970828623f, 8.433635230176151e-05f, 0.00015566653746645898f, 0.00019156234338879585f, 0.00018690172873903066f, 0.0001966516865650192f, 7.458301843144e-05f, 6.350518378894776e-05f, 0.00021807276061736047f, 0.0001280774740735069f, 0.00015024554159026593f, 0.00011775275197578594f, 0.00012838146358262748f, 0.00018006592290475965f, 0.0001395788131048903f, 6.846930045867339e-05f, 0.00013965969264972955f, 0.00014950937475077808f, 1.200356928166002e-05f, 9.139713802142069e-05f, 9.43713093874976e-05f, 4.582058318192139e-05f, 8.040157263167202e-05f, 0.0003918060101568699f, 0.00011058989184675738f, 0.00017621162987779826f, 0.0001635398220969364f, 0.00010837479931069538f, 0.00020270828099455684f, 0.0001653400540817529f, 0.00017462106188759208f, 0.000171370615134947f, 0.0002281790948472917f, 0.00016034847067203373f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #53 */
AI_STATIC ai_intq_info_list conv2d_37_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0004508550919126719f, 0.00039996893610805273f, 0.0008668276132084429f, 0.0006828320911154151f, 0.0005728949327021837f, 0.0005369897116906941f, 0.0004878049367107451f, 0.0005205056513659656f, 0.0006084743072278798f, 0.000458238588180393f, 0.0006940472521819174f, 0.0002697172749321908f, 0.0002620336308609694f, 0.0004533147730398923f, 0.000553842808585614f, 0.0004923895467072725f, 0.00046336857485584915f, 0.00031356888939626515f, 0.0004580462700687349f, 0.0010175019269809127f, 0.00044052943121641874f, 0.00030400382820516825f, 0.00040242017712444067f, 0.0003280686214566231f, 0.0003759778628591448f, 0.0005882462137378752f, 0.00026387395337224007f, 0.0007083397940732539f, 0.0003894018300343305f, 0.000534743769094348f, 0.0003821542195510119f, 0.0004628090828191489f, 0.0006599891348741949f, 0.0004674843221437186f, 0.00019265784067101777f, 0.0005852056201547384f, 0.0006209495477378368f, 0.0009566552471369505f, 0.0002748628321569413f, 0.0005156125989742577f, 0.00028461855254136026f, 0.000572193821426481f, 0.0004141259705647826f, 0.0004324186884332448f, 0.0005617344286292791f, 0.0005248211091384292f, 0.0010013452265411615f, 0.0002191609237343073f, 0.0007448256365023553f, 0.0004344322660472244f, 0.0005611111992038786f, 0.00047502401866950095f, 0.0004829313256777823f, 0.0005920918774791062f, 0.0005253429990261793f, 0.00041141780093312263f, 0.00046371182543225586f, 0.00031919311732053757f, 0.0004875522281508893f, 0.000553776859305799f, 0.0006878809072077274f, 0.0003442143206484616f, 0.0005997203406877816f, 0.0005238464218564332f, 0.0008001697715371847f, 0.00048128404887393117f, 0.00027624997892417014f, 0.0007782714092172682f, 0.0005268784007057548f, 0.000545728369615972f, 0.00029845783137716353f, 0.0004438742180354893f, 0.0005530151538550854f, 0.0006484087789431214f, 0.00043864367762580514f, 0.0005217142170295119f, 0.0007462718640454113f, 0.0010066464310511947f, 0.0007140249945223331f, 0.00028768382617272437f, 0.00039502212894149125f, 0.0005881647230125964f, 0.0005266560474410653f, 0.001093568280339241f, 0.0004405061190482229f, 0.0005793938180431724f, 0.0005117212422192097f, 0.0005555364186875522f, 0.00033594228443689644f, 0.0003985546645708382f, 0.000648074725177139f, 0.0003470700467005372f, 0.00030954350950196385f, 0.0007509682327508926f, 0.0002396254421910271f, 0.00042703066719695926f, 0.000160875148139894f, 0.0006287780706770718f, 0.0008890024619176984f, 0.0002983424055855721f, 0.0006702239043079317f, 0.0007603458361700177f, 0.0007924317033030093f, 0.0006956260767765343f, 0.00030303510720841587f, 0.00043578908662311733f, 0.0008615621482022107f, 0.0003091186808887869f, 0.0005521716084331274f, 0.0004168633895460516f, 0.00035555666545405984f, 0.0005455221980810165f, 0.0006550468970090151f, 0.00032951030880212784f, 0.0005731710116378963f, 0.0003775954828597605f, 0.0006814366206526756f, 0.000340704747941345f, 0.00020552500791382045f, 0.00047374851419590414f, 0.000536444887984544f, 0.000662102596834302f, 0.0005319557967595756f, 0.0007263383595272899f, 0.00027217678143642843f, 0.0005015570786781609f, 0.0003282436227891594f, 0.00047341539175249636f, 0.0005692894919775426f, 0.00024520422448404133f, 0.0005474667414091527f, 0.00039993709651753306f, 0.000680900237057358f, 0.00029292047838680446f, 0.0007535951444879174f, 0.0004843725182581693f, 0.0006259860238060355f, 0.0005064383149147034f, 0.0006753531051799655f, 0.000514794432092458f, 0.0008410904556512833f, 0.001090120174922049f, 0.0004190490290056914f, 0.000741266121622175f, 0.0005432541365735233f, 0.0003192504809703678f, 0.0008777471957728267f, 0.0006762940902262926f, 0.0005140393623150885f, 0.0007654781802557409f, 0.000540906039532274f, 0.0010054007871076465f, 0.0005158516578376293f, 0.00052891910308972f, 0.0005307703977450728f, 0.0005794134922325611f, 0.0007636660011485219f, 0.00044142449041828513f, 0.0007682143477723002f, 0.00027269855490885675f, 0.0005033421330153942f, 0.0006194099551066756f, 0.0006043399916961789f, 0.0006358661339618266f, 0.00024116149870678782f, 0.00020534172654151917f, 0.000705130398273468f, 0.0004141339159104973f, 0.000485813565319404f, 0.000380749290343374f, 0.0004151168395765126f, 0.0005822366802021861f, 0.0004513230815064162f, 0.00022139301290735602f, 0.0004515846085269004f, 0.00048343316302634776f, 3.8813108403701335e-05f, 0.00029552934574894607f, 0.0003051462408620864f, 0.00014815920440014452f, 0.00025997558259405196f, 0.0012668906711041927f, 0.0003575884329620749f, 0.0005697739543393254f, 0.0005288001266308129f, 0.00035042600939050317f, 0.0006554499268531799f, 0.0005346211255528033f, 0.0005646309000439942f, 0.0005541206919588149f, 0.0007378088776022196f, 0.0005184810142964125f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #54 */
AI_STATIC ai_intq_info_list conv2d_35_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(8.135543612297624e-05f, 8.776348840910941e-05f, 0.00014290180115494877f, 0.0004227952449582517f, 0.00013373071851674467f, 0.0001465610257582739f, 0.00030168757075443864f, 6.807275349274278e-05f, 7.87207463872619e-05f, 0.00019925164815504104f, 0.00019004671776201576f, 0.0001159907624241896f, 0.00013114330067764968f, 6.35339310974814e-05f, 0.0001774132833816111f, 5.11833859491162e-05f, 0.00039383949479088187f, 0.0001653028593864292f, 9.599516488378868e-05f, 0.0002835346676874906f, 0.00010464055958436802f, 0.0002486480516381562f, 0.00018119660671800375f, 0.00014708074741065502f, 0.0002913872594945133f, 0.00012870579666923732f, 5.5380642152158543e-05f, 0.0002695272269193083f, 0.00010894078150158748f, 6.626120739383623e-05f, 0.00015304387488868088f, 7.745800394332036e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #55 */
AI_STATIC ai_intq_info_list conv2d_35_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0034576060716062784f, 0.0037299480754882097f, 0.006073326338082552f, 0.01796879805624485f, 0.005683555733412504f, 0.006228843703866005f, 0.012821720913052559f, 0.0028930921107530594f, 0.003345631528645754f, 0.008468194864690304f, 0.008076985366642475f, 0.004929607268422842f, 0.005573590286076069f, 0.002700192155316472f, 0.00754006439819932f, 0.0021752938628196716f, 0.016738178208470345f, 0.007025371305644512f, 0.0040797945111989975f, 0.012050222605466843f, 0.0044472236186265945f, 0.010567542165517807f, 0.00770085584372282f, 0.00625093188136816f, 0.012383958324790001f, 0.005469996016472578f, 0.0023536772932857275f, 0.01145490724593401f, 0.004629983101040125f, 0.0028161013033241034f, 0.006504364311695099f, 0.0032919649966061115f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #56 */
AI_STATIC ai_intq_info_list conv2d_34_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(8.986233297036961e-05f, 0.00012167026579845697f, 0.00025536524481140077f, 0.000133597815874964f, 8.126611646730453e-05f, 0.00021348051086533815f, 0.0002962957660201937f, 0.00033162839827127755f, 0.00014191372611094266f, 0.0002791377482935786f, 0.00020724255591630936f, 0.0002937236858997494f, 0.0001552644098410383f, 0.0004400713660288602f, 0.00011434000043664128f, 9.847613546298817e-05f, 0.00023732840782031417f, 0.00014324294170364738f, 0.00019412933033891022f, 0.00034205132396891713f, 0.00012311679893173277f, 0.0001758391736075282f, 0.0003308399172965437f, 0.0001523678220110014f, 0.00027381378458812833f, 0.00044665485620498657f, 0.0003924680931959301f, 0.00021067901980131865f, 8.732288551982492e-05f, 0.0002552238875068724f, 0.0002471765910740942f, 0.00017630806542001665f, 0.000358918565325439f, 0.0002227488876087591f, 0.0001014165609376505f, 0.0002527788165025413f, 0.0003077296132687479f, 0.0003374159277882427f, 0.00019126773986499757f, 7.563518738606945e-05f, 0.0006170445121824741f, 0.00012376227823551744f, 0.00011426656419644132f, 0.00019872431585099548f, 0.00032251948141492903f, 0.00033744933898560703f, 0.00017152419604826719f, 0.00022635897039435804f, 0.00016187196888495237f, 0.0001494186435593292f, 0.00014744779036846012f, 0.00035430293064564466f, 0.00018154850113205612f, 0.00021668201952707022f, 0.00020070619939360768f, 0.00011549787450348958f, 3.7709014577558264e-05f, 5.0238410040037706e-05f, 0.00013566255802288651f, 6.368810136336833e-05f, 0.00010394882701802999f, 0.0001398917374899611f, 0.00011449059820733964f, 0.00017972421483136714f, 0.00024604855570942163f, 0.0004135296621825546f, 0.00037688229349441826f, 0.00014750906848348677f, 0.0002599329163786024f, 9.384384611621499e-05f, 0.00019727139442693442f, 0.0003574094153009355f, 0.0002992318768519908f, 0.00010188730811933056f, 0.0001459845807403326f, 0.00022379170695785433f, 0.00023166027676779777f, 0.00010421720071462914f, 0.0002483577118255198f, 0.00018888602789957076f, 0.0001249768683919683f, 0.0001709179050521925f, 0.00033589144004508853f, 0.00016564218094572425f, 0.00011402944073779508f, 0.00012654491001740098f, 0.0001623909774934873f, 0.0005360183422453701f, 0.00010912877769442275f, 0.0002319088380318135f, 0.00037026384961791337f, 0.000165597113664262f, 0.0005884945276193321f, 0.00038966492866165936f, 0.00021686963737010956f, 0.00017672560352366418f, 0.00023292787955142558f, 0.00022041393094696105f, 0.00032510346500203013f, 0.00012435913959052414f, 0.00037029225495643914f, 0.00018179284234065562f, 0.00035643327282741666f, 0.00011532016651472077f, 0.001245002611540258f, 0.00021639466285705566f, 0.00023633037926629186f, 0.00024243710504379123f, 0.00045244439388625324f, 0.0003798589750658721f, 8.332435390911996e-05f, 0.00020196460536681116f, 0.000692881119903177f, 0.0001583028060849756f, 0.00013400793250184506f, 0.00024816684890538454f, 0.0002925502776633948f, 9.885198232950643e-05f, 0.00017095716611947864f, 0.00013981011579744518f, 0.00020200722792651504f, 0.0002793424064293504f, 0.002550046890974045f, 8.879256347427145e-05f, 0.00022410822566598654f, 0.00043632244342006743f, 0.00023771995620336384f, 0.00021566198847722262f, 0.00013779914297629148f, 0.0001852746936492622f, 0.0001705819449853152f, 0.00019882067863363773f, 0.00048289637197740376f, 0.00042915326775982976f, 0.00012090212112525478f, 0.0001482111110817641f, 0.00021370730246417224f, 0.00019580511434469372f, 0.0001923213858390227f, 0.0002470471372362226f, 0.0002155997062800452f, 0.00027096987469121814f, 0.00028733970248140395f, 0.00012558125308714807f, 0.0001156902508228086f, 0.000185116907232441f, 0.0003024170291610062f, 0.00029703439213335514f, 0.0001088057179003954f, 0.0006901291199028492f, 0.000127978521049954f, 0.0001862330682342872f, 0.00024053006200119853f, 0.0002924883156083524f, 0.00019121670629829168f, 0.00012320384848862886f, 0.00018684077076613903f, 0.00011638456635409966f, 0.0002758591726887971f, 0.00031396790291182697f, 0.00036178083973936737f, 0.00017369844135828316f, 0.00011104568693554029f, 8.97321806405671e-05f, 5.695084837498143e-05f, 0.0001768410875229165f, 0.00012215389870107174f, 0.00024230231065303087f, 0.0003229583380743861f, 0.00024245523673016578f, 0.00017164759628940374f, 0.00024469627533107996f, 0.00011996652028756216f, 8.822522795526311e-05f, 0.00011807441478595138f, 0.00021447851031553f, 0.00023496999347116798f, 0.00010868837125599384f, 0.00013337752898223698f, 0.0002025594876613468f, 0.00012841551506426185f, 0.00025393851683475077f, 0.00017352144641336054f, 0.00018519166042096913f, 0.0002599268627818674f, 0.00022929957776796073f, 0.0002563308225944638f, 0.0005095208762213588f, 0.00012342981062829494f, 0.0002371325681451708f, 0.0001297978451475501f, 0.00021901229047216475f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #57 */
AI_STATIC ai_intq_info_list conv2d_34_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0038191492203623056f, 0.00517098605632782f, 0.010853022336959839f, 0.005677907261997461f, 0.003453809767961502f, 0.009072921238839626f, 0.012592569924890995f, 0.014094206504523754f, 0.006031333468854427f, 0.011863353662192822f, 0.00880780816078186f, 0.012483255937695503f, 0.006598737556487322f, 0.018703032284975052f, 0.004859449807554483f, 0.004185235593467951f, 0.01008645724505186f, 0.006087824702262878f, 0.008250496350228786f, 0.01453718077391386f, 0.0052324640564620495f, 0.007473164703696966f, 0.014060696586966515f, 0.006475632078945637f, 0.011637086048722267f, 0.01898283138871193f, 0.01667989417910576f, 0.00895385816693306f, 0.0037112224381417036f, 0.010847015306353569f, 0.010505004785954952f, 0.007493092678487301f, 0.015254039317369461f, 0.009466827847063541f, 0.004310203716158867f, 0.010743099264800549f, 0.013078508898615837f, 0.014340177178382874f, 0.00812887866050005f, 0.003214495489373803f, 0.026224391534924507f, 0.005259896628558636f, 0.004856328945606947f, 0.008445783518254757f, 0.01370707806199789f, 0.014341596513986588f, 0.00728977844119072f, 0.009620255790650845f, 0.00687955878674984f, 0.006350292358547449f, 0.006266531068831682f, 0.015057873912155628f, 0.007715811021625996f, 0.009208985604345798f, 0.008530013263225555f, 0.004908659495413303f, 0.0016026330413296819f, 0.0021351324394345284f, 0.005765658803284168f, 0.0027067442424595356f, 0.004417825024574995f, 0.005945398472249508f, 0.004865850321948528f, 0.007638278882950544f, 0.010457064025104046f, 0.017575010657310486f, 0.016017496585845947f, 0.006269135512411594f, 0.011047149077057838f, 0.003988363314419985f, 0.008384034037590027f, 0.015189900062978268f, 0.012717355042696f, 0.004330210387706757f, 0.00620434433221817f, 0.009511147625744343f, 0.009845561347901821f, 0.004429230932146311f, 0.010555203072726727f, 0.008027656003832817f, 0.0053115165792405605f, 0.007264011073857546f, 0.01427538599818945f, 0.0070397923700511456f, 0.004846251104027033f, 0.005378158297389746f, 0.00690161669626832f, 0.022780777886509895f, 0.004637972917407751f, 0.009856125339865685f, 0.015736212953925133f, 0.007037877105176449f, 0.02501101605594158f, 0.016560759395360947f, 0.009216959588229656f, 0.007510838098824024f, 0.009899434633553028f, 0.009367591701447964f, 0.013816896826028824f, 0.0052852630615234375f, 0.015737419947981834f, 0.0077261957339942455f, 0.015148414298892021f, 0.004901106934994459f, 0.05291260778903961f, 0.009196773171424866f, 0.010044041089713573f, 0.010303576476871967f, 0.01922888681292534f, 0.016144005581736565f, 0.003541284939274192f, 0.008583495393395424f, 0.029447447508573532f, 0.006727869156748056f, 0.00569533696398139f, 0.010547090321779251f, 0.012433386407792568f, 0.004201209172606468f, 0.0072656795382499695f, 0.0059419297613203526f, 0.008585306815803051f, 0.01187205221503973f, 0.10837699472904205f, 0.003773683914914727f, 0.009524599649012089f, 0.018543703481554985f, 0.01010309811681509f, 0.009165634401142597f, 0.005856463685631752f, 0.007874174043536186f, 0.007249732501804829f, 0.008449878543615341f, 0.02052309550344944f, 0.018239013850688934f, 0.005138339940458536f, 0.0062989722937345505f, 0.009082560427486897f, 0.008321717381477356f, 0.008173658512532711f, 0.010499502532184124f, 0.00916298758238554f, 0.01151621900498867f, 0.012211937457323074f, 0.005337202921509743f, 0.004916835576295853f, 0.007867468520998955f, 0.012852723710238934f, 0.012623961083590984f, 0.004624242894351482f, 0.029330486431717873f, 0.0054390872828662395f, 0.007914905436336994f, 0.01022252719849348f, 0.012430752627551556f, 0.00812670961022377f, 0.005236163269728422f, 0.00794073287397623f, 0.004946344066411257f, 0.011724014766514301f, 0.013343635946512222f, 0.015375684946775436f, 0.00738218380138278f, 0.004719441756606102f, 0.003813617629930377f, 0.002420410979539156f, 0.007515746168792248f, 0.0051915403455495834f, 0.010297847911715508f, 0.013725729659199715f, 0.010304347611963749f, 0.007295022718608379f, 0.01039959117770195f, 0.005098577123135328f, 0.0037495719734579325f, 0.005018162541091442f, 0.009115336462855339f, 0.009986224584281445f, 0.0046192556619644165f, 0.005668545141816139f, 0.00860877800732851f, 0.005457659251987934f, 0.010792386718094349f, 0.007374661508947611f, 0.007870645262300968f, 0.01104689110070467f, 0.009745231829583645f, 0.010894060134887695f, 0.021654637530446053f, 0.005245767068117857f, 0.010078134015202522f, 0.005516408011317253f, 0.009308022446930408f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #58 */
AI_STATIC ai_intq_info_list conv2d_33_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00017641470185481012f, 0.0002408787840977311f, 0.00016961594519671053f, 0.0002143636957043782f, 0.00019856345898006111f, 0.00011132664803881198f, 5.197625432629138e-05f, 0.00023234027321450412f, 0.000123922509374097f, 0.00021366246801335365f, 9.348551975563169e-05f, 0.00013424902863334864f, 0.00013268369366414845f, 8.607022027717903e-05f, 0.00017774170555640012f, 0.00017381046200171113f, 0.00026280098245479167f, 0.0001243463921127841f, 0.00018271933367941529f, 0.0001970709126908332f, 0.00010673215001588687f, 0.00011734710278687999f, 0.00023724338097963482f, 0.00013487406249623746f, 0.00010736280819401145f, 5.150671495357528e-05f, 0.0001634088985156268f, 0.00013703078730031848f, 0.00017393205780535936f, 7.958855712786317e-05f, 0.0001402542256982997f, 0.00012426069588400424f, 8.925363363232464e-05f, 0.00012071556557202712f, 0.00015496500418521464f, 0.00019839566084556282f, 8.120736310957e-05f, 9.009966015582904e-05f, 0.00013068104453850538f, 0.00023293434060178697f, 7.628826278960332e-05f, 0.00014264369383454323f, 0.00017683830810710788f, 0.00016853856504894793f, 0.00024281017249450088f, 0.00018927804194390774f, 0.00012375894584693015f, 9.721140668261796e-05f, 0.00010513110464671627f, 0.0001083796305465512f, 0.0002069520705845207f, 0.00014677939179819077f, 0.00018416075909044594f, 0.0002614792319945991f, 0.0003207786357961595f, 0.00010844752978300676f, 0.000273029349045828f, 0.0002480196417309344f, 0.00022221807739697397f, 0.0001826734805945307f, 0.000104308950540144f, 0.0002002249821089208f, 0.00011542363790795207f, 0.00020581802527885884f, 8.518080721842125e-05f, 0.0001774798583937809f, 0.00013101173681207f, 0.00018844744772650301f, 0.0002475941146258265f, 0.0003726688155438751f, 0.00015970251115504652f, 0.000274718418950215f, 0.00015820508997421712f, 0.00016307008627336472f, 9.526661597192287e-05f, 0.00017554144142195582f, 0.000183512267540209f, 0.00019641008111648262f, 0.0001580321550136432f, 0.0001407552044838667f, 0.0002526705211494118f, 0.00020440791558939964f, 0.00013242263230495155f, 9.494875121163204e-05f, 0.00015134806744754314f, 0.00020523741841316223f, 0.0001186878071166575f, 2.4203995053539984e-05f, 0.00033244580845348537f, 0.00010057487088488415f, 6.145702354842797e-05f, 0.00015152875857893378f, 6.610771379200742e-05f, 8.21068897494115e-05f, 0.00015525305934716016f, 0.0001528306893305853f, 9.247392154065892e-05f, 0.00022337143309414387f, 0.0001664543669903651f, 9.388916805619374e-05f, 4.6061191824264824e-05f, 0.00026609288761392236f, 0.00014066237781662494f, 0.00017317586753051728f, 0.00014673147234134376f, 0.00013542029773816466f, 7.967063720570877e-05f, 0.00016357508138753474f, 8.408274152316153e-05f, 7.586007268400863e-05f, 0.0002544534800108522f, 0.00013808728544972837f, 8.708343375474215e-05f, 0.00016190970200113952f, 0.00015934905968606472f, 0.0003289411251898855f, 0.0001656987442402169f, 0.00012669687566813082f, 0.00012278009671717882f, 0.00011606309999478981f, 0.00035349224344827235f, 0.0002716229937504977f, 1.0191137933546202e-09f, 0.00031230924651026726f, 3.856592229567468e-05f, 0.0001479837083024904f, 2.386171036050655e-05f, 0.00011281423212494701f, 0.00022356565750669688f, 0.00020038886577822268f, 0.00011448968871263787f, 0.0001062841693055816f, 0.00011056212679250166f, 6.0389385907910764e-05f, 7.435850420733914e-05f, 0.00012195035378681496f, 0.00011030906171072274f, 0.00019115004397463053f, 0.00011522640124894679f, 0.00019221450202167034f, 0.00017249448865186423f, 0.00017201079754158854f, 0.00022910608095116913f, 0.00010179533273912966f, 0.00010166629363084212f, 8.59257488627918e-05f, 0.00010115477198269218f, 0.00014268197992350906f, 0.00022412886028178036f, 0.00011261814506724477f, 0.00022578438802156597f, 0.00021986858337186277f, 0.00011143842857563868f, 0.00012059491564286873f, 0.00012307801807764918f, 9.629170381231233e-05f, 0.00011733044811990112f, 0.00017595654935576022f, 0.00024782109539955854f, 0.00016733755182940513f, 9.316854266216978e-05f, 0.00025869428645819426f, 0.00013658168609254062f, 6.260613008635119e-05f, 0.00019326608162373304f, 0.00014381260552909225f, 0.00017287835362367332f, 0.00016606657300144434f, 0.00017362686048727483f, 0.00019847694784402847f, 8.827468263916671e-05f, 0.0001692243677098304f, 0.00011654750414891168f, 0.00014019843365531415f, 0.00020134550868533552f, 0.0001341432216577232f, 0.00025034192367456853f, 0.00020768742251675576f, 0.00012433028314262629f, 0.0001782585313776508f, 0.00012852308282162994f, 0.00011022022954421118f, 0.00012354724458418787f, 0.0001392803096678108f, 9.762441914062947e-05f, 0.00011043455742765218f, 0.00020027784921694547f, 9.221966320183128e-05f, 9.677714115241542e-05f, 0.00010839221795322374f, 0.0003009504871442914f, 0.0002661779581103474f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #59 */
AI_STATIC ai_intq_info_list conv2d_33_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0006287097348831594f, 0.0008584479219280183f, 0.0006044802139513195f, 0.0007639530231244862f, 0.0007076438632793725f, 0.0003967478114645928f, 0.0001852338609751314f, 0.0008280182373709977f, 0.00044163718121126294f, 0.0007614539354108274f, 0.0003331652842462063f, 0.00047843900392763317f, 0.00047286043991334736f, 0.0003067385114263743f, 0.0006334389327093959f, 0.0006194286979734898f, 0.0009365746518597007f, 0.0004431478155311197f, 0.0006511782994493842f, 0.0007023246726021171f, 0.00038037385093048215f, 0.0004182036209385842f, 0.0008454920025542378f, 0.00048066649469546974f, 0.00038262142334133387f, 0.00018356050713919103f, 0.0005823594401590526f, 0.0004883526707999408f, 0.0006198620540089905f, 0.0002836390631273389f, 0.0004998404183425009f, 0.00044284239993430674f, 0.00031808362109586596f, 0.000430208194302395f, 0.0005522669525817037f, 0.0007070458377711475f, 0.0002894081990234554f, 0.00032109871972352266f, 0.00046572336577810347f, 0.0008301353664137423f, 0.00027187741943635046f, 0.0005083560245111585f, 0.0006302194087766111f, 0.0006006406038068235f, 0.0008653310360386968f, 0.0006745523423887789f, 0.000441054260591045f, 0.0003464436740614474f, 0.00037466801586560905f, 0.00038624517037533224f, 0.0007375393179245293f, 0.0005230949027463794f, 0.0006563152419403195f, 0.0009318641969002783f, 0.001143196364864707f, 0.00038648716872558f, 0.0009730266174301505f, 0.0008838966605253518f, 0.0007919445633888245f, 0.0006510148523375392f, 0.00037173801683820784f, 0.0007135652122087777f, 0.0004113487375434488f, 0.0007334977854043245f, 0.00030356881325133145f, 0.0006325057474896312f, 0.000466901867184788f, 0.0006715922500006855f, 0.0008823801181279123f, 0.0013281235005706549f, 0.0005691505502909422f, 0.0009790462208911777f, 0.0005638140137307346f, 0.0005811519804410636f, 0.00033951280056498945f, 0.00062559760408476f, 0.0006540041649714112f, 0.0006999695906415582f, 0.0005631977110169828f, 0.0005016258219256997f, 0.0009004715248011053f, 0.0007284724269993603f, 0.00047193001955747604f, 0.000338379992172122f, 0.0005393768078647554f, 0.0007314286194741726f, 0.00042298162588849664f, 8.625860937172547e-05f, 0.0011847760761156678f, 0.00035843043588101864f, 0.0002190216036979109f, 0.0005400207592174411f, 0.00023559581313747913f, 0.0002926139277406037f, 0.0005532935028895736f, 0.0005446606082841754f, 0.00032956013455986977f, 0.0007960549555718899f, 0.0005932128988206387f, 0.0003346038283780217f, 0.0001641536655370146f, 0.00094830640591681f, 0.0005012949695810676f, 0.0006171670975163579f, 0.0005229241214692593f, 0.00048261319170705974f, 0.0002839315857272595f, 0.0005829516449011862f, 0.00029965551220811903f, 0.00027035141829401255f, 0.000906825705897063f, 0.0004921178333461285f, 0.00031034942367114127f, 0.0005770165589638054f, 0.0005678908783011138f, 0.0011722859926521778f, 0.0005905200378037989f, 0.0004515244800131768f, 0.00043756581726484f, 0.0004136276547797024f, 0.0012597816530615091f, 0.0009680146467871964f, 3.6319349749902585e-09f, 0.0011130130151286721f, 0.00013744189345743507f, 0.0005273868446238339f, 8.503877324983478e-05f, 0.00040204927790910006f, 0.0007967471028678119f, 0.0007141492678783834f, 0.0004080203070770949f, 0.00037877733120694757f, 0.0003940231981687248f, 0.00021521672897506505f, 0.0002650000969879329f, 0.00043460875167511404f, 0.0003931213286705315f, 0.000681223813444376f, 0.0004106458218302578f, 0.0006850173231214285f, 0.0006147387903183699f, 0.0006130150286480784f, 0.0008164921891875565f, 0.0003627799451351166f, 0.00036232007551006973f, 0.0003062236646655947f, 0.0003604970988817513f, 0.0005084924632683396f, 0.000798754277639091f, 0.0004013504658360034f, 0.000804654264356941f, 0.0007835713913664222f, 0.0003971461846958846f, 0.0004297782143112272f, 0.00043862752499990165f, 0.0003431660297792405f, 0.00041814424912445247f, 0.0006270769517868757f, 0.0008831890881992877f, 0.0005963604198768735f, 0.0003320356481708586f, 0.0009219390922226012f, 0.0004867521347478032f, 0.00022311678912956268f, 0.0006887649651616812f, 0.0005125218303874135f, 0.0006161068449728191f, 0.0005918308743275702f, 0.0006187743856571615f, 0.0007073355372995138f, 0.00031459482852369547f, 0.0006030846852809191f, 0.00041535397758707404f, 0.0004996415809728205f, 0.0007175585487857461f, 0.00047806190559640527f, 0.0008921728003770113f, 0.0007401600014418364f, 0.00044309039367362857f, 0.0006352807977236807f, 0.0004580327367875725f, 0.00039280473720282316f, 0.0004402998019941151f, 0.0004963695537298918f, 0.00034791557118296623f, 0.00039356856723316014f, 0.0007137536304071546f, 0.0003286540159024298f, 0.00034489601966924965f, 0.0003862900484818965f, 0.0010725324973464012f, 0.0009486095514148474f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #60 */
AI_STATIC ai_intq_info_list conv2d_31_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(4.674110823543742e-05f, 9.650745778344572e-05f, 4.077962148585357e-05f, 0.00020214727555867285f, 7.39593815524131e-05f, 0.00010302272130502388f, 0.00023275612329598516f, 3.91096509702038e-05f, 3.733965786523186e-05f, 0.0001356457796646282f, 6.357083475450054e-05f, 0.00011017073120456189f, 6.641150685027242e-05f, 3.9378752262564376e-05f, 8.47415067255497e-05f, 2.5711482521728612e-05f, 0.00045713825966231525f, 0.0001360341557301581f, 0.000162411030032672f, 0.00021386267326306552f, 6.341264088405296e-05f, 0.00010338540596421808f, 0.00011841842206194997f, 8.463102858513594e-05f, 0.00023717194562777877f, 8.807222911855206e-05f, 0.00020294662681408226f, 0.00013436989684123546f, 4.341265594121069e-05f, 5.488477836479433e-05f, 0.00011821922817034647f, 6.272533209994435e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #61 */
AI_STATIC ai_intq_info_list conv2d_31_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0019864970818161964f, 0.004101566970348358f, 0.001733133802190423f, 0.008591258898377419f, 0.003143273526802659f, 0.004378465469926596f, 0.009892134927213192f, 0.0016621601535007358f, 0.0015869354829192162f, 0.005764945410192013f, 0.002701760269701481f, 0.004682255908846855f, 0.0028224890120327473f, 0.0016735969111323357f, 0.003601514035835862f, 0.0010927380062639713f, 0.019428376108407974f, 0.00578145170584321f, 0.006902468390762806f, 0.00908916350454092f, 0.0026950370520353317f, 0.0043938797898590565f, 0.005032782908529043f, 0.0035968185402452946f, 0.010079807601869106f, 0.003743069712072611f, 0.008625231683254242f, 0.0057107205502688885f, 0.0018450379138812423f, 0.002332603093236685f, 0.0050243171863257885f, 0.0026658265851438046f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #62 */
AI_STATIC ai_intq_info_list conv2d_30_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0002701350022107363f, 0.00023878396314103156f, 0.0009491299279034138f, 0.00027368287555873394f, 0.00046276068314909935f, 0.00023258489090949297f, 0.0003757200902327895f, 0.00032809580443426967f, 0.0010491502471268177f, 0.0004422710626386106f, 0.0003297660150565207f, 0.00021691770234610885f, 0.0002829399600159377f, 0.0002754255838226527f, 0.00014272326370701194f, 0.0004842519119847566f, 0.0002242685732198879f, 0.0006900042644701898f, 0.000322283071000129f, 0.000289007555693388f, 0.00021492282394319773f, 0.00013577501522377133f, 0.0004210680490359664f, 0.0003863153397105634f, 0.00022065585653763264f, 0.00037558801705017686f, 0.0002832999743986875f, 0.0006211580475792289f, 0.0004426108789630234f, 0.0003036294365301728f, 0.0002831665624398738f, 0.0001844684884417802f, 0.00023339089239016175f, 0.00022964667004998773f, 0.0005147631163708866f, 0.0003215803299099207f, 0.0003586646053008735f, 0.00029859732603654265f, 0.00037655647611245513f, 0.0005302284844219685f, 0.0004637195961549878f, 0.00024054748064372689f, 0.00015978880401235074f, 0.0004556899075396359f, 0.00024598705931566656f, 0.0004764260374940932f, 0.000332049181452021f, 0.0003140284097753465f, 0.00017239904263988137f, 0.00015026734035927802f, 0.000308910442981869f, 0.00017008052964229137f, 0.00031794834649190307f, 0.0003040578740183264f, 0.00025232176994904876f, 0.00033911000355146825f, 0.00040130395791493356f, 0.0003579535405151546f, 0.0004585704591590911f, 0.00022809041547589004f, 0.0001773391995811835f, 0.00021405474399216473f, 0.0001649060723138973f, 0.00048589360085316f, 0.0004509169375523925f, 0.0003129493270535022f, 0.0002768498961813748f, 0.0007753517129458487f, 0.00013194653729442507f, 0.000303495122352615f, 0.0006899635191075504f, 0.00025368118076585233f, 0.0004774190892931074f, 0.00011381106014596298f, 0.00015086369239725173f, 0.00037184133543632925f, 0.000310632458422333f, 0.0003009567444678396f, 0.00020694895647466183f, 0.0004817146109417081f, 0.0003037062706425786f, 0.0003913889522664249f, 0.00022828503279015422f, 0.00011825983528979123f, 0.0002231569669675082f, 0.00013065834355074912f, 0.0003406729083508253f, 0.0002151480148313567f, 0.0002718237810768187f, 0.0002167552156606689f, 0.00046785493032075465f, 0.00028669199673458934f, 0.00017569444025866687f, 0.00047545123379677534f, 0.00030929085914976895f, 0.0005774818710051477f, 0.00026079273084178567f, 0.0002892435877583921f, 0.00023682108439970762f, 0.00013737974222749472f, 0.00039721722714602947f, 0.0005984447197988629f, 0.00016344543837476522f, 0.0004107355198357254f, 0.0002723300422076136f, 0.000145992380566895f, 0.0004058346676174551f, 0.0003461480373516679f, 0.00017674655828159302f, 0.00019032218551728874f, 0.0002830061421263963f, 0.0003156489983666688f, 0.00020620709983631968f, 0.00016957895422820002f, 0.00016803097969386727f, 0.0003475818957667798f, 0.00019087684631813318f, 0.00019180872186552733f, 0.0003876696282532066f, 0.00022622804681304842f, 0.00020003979443572462f, 0.00042870090692304075f, 0.00019184351549483836f, 0.00038293233956210315f, 0.00035441387444734573f, 0.00027763156685978174f, 0.0002641783212311566f, 0.0001603030104888603f, 0.00023087750014383346f, 0.00023281222092919052f, 0.0002825268020387739f, 0.00017984652367886156f, 0.0003031970118172467f, 0.0002230333921033889f, 0.00032547529553994536f, 0.0004813539853785187f, 0.00034444680204614997f, 0.0004153371846769005f, 0.00029267428908497095f, 0.00018049516074825078f, 0.000179587586899288f, 0.0006113229319453239f, 0.0002901531697716564f, 0.0004049587878398597f, 0.00071813102113083f, 0.0001369264064123854f, 0.00048581406008452177f, 0.00017792268772609532f, 0.00035733942058868706f, 0.0001965723349712789f, 0.00021711627778131515f, 0.00026793949655257165f, 0.0005516815581358969f, 0.00026707336655817926f, 0.0003495930868666619f, 0.00017436625785194337f, 0.00014799099881201982f, 0.00024302472593262792f, 0.000410946027841419f, 0.00026644294848665595f, 0.00012439546117093414f, 0.0004341966996435076f, 0.00010431037662783638f, 0.0002234952407889068f, 0.00016132245946209878f, 0.00015686637198086828f, 0.0001943908428074792f, 0.00017515200306661427f, 0.00014126795576885343f, 0.00039416211075149477f, 0.0005388052668422461f, 0.00018597613961901516f, 0.00035053968895226717f, 0.00014724861830472946f, 0.00033086459734477103f, 0.0001970292069017887f, 0.0005001453682780266f, 0.0004452913999557495f, 0.00020222165039740503f, 0.0002601147280074656f, 0.0001842366182245314f, 0.0002183222386520356f, 0.0003606613026931882f, 0.00021498475689440966f, 0.00022002641344442964f, 0.0004219137190375477f, 0.00022926132078282535f, 0.00027755319024436176f, 0.0002996245166286826f, 0.0003412590886000544f, 0.00048821879317983985f, 0.00014611188089475036f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #63 */
AI_STATIC ai_intq_info_list conv2d_30_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.01148073747754097f, 0.010148318484425545f, 0.04033802077174187f, 0.01163152139633894f, 0.019667329266667366f, 0.009884857572615147f, 0.01596810296177864f, 0.013944071717560291f, 0.044588882476091385f, 0.018796520307660103f, 0.014015055261552334f, 0.009219001978635788f, 0.012024948373436928f, 0.01170558761805296f, 0.006065738387405872f, 0.020580705255270004f, 0.00953141413629055f, 0.029325181618332863f, 0.013697030022740364f, 0.012282821349799633f, 0.009134219959378242f, 0.005770437885075808f, 0.017895391210913658f, 0.016418401151895523f, 0.009377873502671719f, 0.015962490811944008f, 0.012040249072015285f, 0.026399215683341026f, 0.01881096139550209f, 0.012904250994324684f, 0.012034579180181026f, 0.007839910686016083f, 0.009919112548232079f, 0.00975998304784298f, 0.021877432242035866f, 0.01366716343909502f, 0.015243245288729668f, 0.012690385803580284f, 0.016003649681806564f, 0.02253471128642559f, 0.01970808207988739f, 0.0102232675999403f, 0.006791024003177881f, 0.01936682127416134f, 0.0104544498026371f, 0.020248105749487877f, 0.01411208976060152f, 0.013346207328140736f, 0.007326959166675806f, 0.006386362016201019f, 0.013128693215548992f, 0.007228422444313765f, 0.013512804172933102f, 0.012922459281980991f, 0.010723675601184368f, 0.014412174932658672f, 0.017055418342351913f, 0.015213025733828545f, 0.019489243626594543f, 0.009693842381238937f, 0.007536915596574545f, 0.009097326546907425f, 0.00700850784778595f, 0.020650478079915047f, 0.01916396990418434f, 0.013300346210598946f, 0.011766120791435242f, 0.0329524464905262f, 0.005607727915048599f, 0.012898542918264866f, 0.029323449358344078f, 0.01078145019710064f, 0.0202903114259243f, 0.004836970008909702f, 0.006411706563085318f, 0.015803257003426552f, 0.01320187933743f, 0.012790661305189133f, 0.008795330300927162f, 0.020472871139645576f, 0.012907516211271286f, 0.016634030267596245f, 0.009702113457024097f, 0.005026042927056551f, 0.00948417093604803f, 0.005552979186177254f, 0.014478598721325397f, 0.009143790230154991f, 0.011552509851753712f, 0.009212096221745014f, 0.019883833825588226f, 0.01218440942466259f, 0.007467013318091631f, 0.020206676796078682f, 0.013144860975444317f, 0.024542979896068573f, 0.011083691380918026f, 0.012292852625250816f, 0.010064896196126938f, 0.005838639102876186f, 0.016881732270121574f, 0.025433899834752083f, 0.006946431007236242f, 0.017456259578466415f, 0.011574026197195053f, 0.006204676348716021f, 0.017247973009943962f, 0.014711291529238224f, 0.007511728443205357f, 0.008088693022727966f, 0.012027760967612267f, 0.013415082357823849f, 0.00876380130648613f, 0.007207105401903391f, 0.0071413167752325535f, 0.014772230759263039f, 0.008112265728414059f, 0.008151870220899582f, 0.01647595874965191f, 0.009614692069590092f, 0.008501690812408924f, 0.018219787627458572f, 0.008153349161148071f, 0.016274623572826385f, 0.015062589198350906f, 0.011799341067671776f, 0.011227577924728394f, 0.006812877953052521f, 0.009812293574213982f, 0.009894519113004208f, 0.012007388286292553f, 0.007643477059900761f, 0.012885873205959797f, 0.009478919208049774f, 0.0138326995074749f, 0.020457543432712555f, 0.014638989232480526f, 0.01765182986855507f, 0.012438656762242317f, 0.007671044208109379f, 0.007632472552359104f, 0.025981223210692406f, 0.012331509031355381f, 0.01721074804663658f, 0.030520567670464516f, 0.005819372367113829f, 0.020647097378969193f, 0.007561713922768831f, 0.015186924487352371f, 0.008354323916137218f, 0.009227441623806953f, 0.011387428268790245f, 0.023446466773748398f, 0.011350617744028568f, 0.014857705682516098f, 0.007410565856844187f, 0.006289617624133825f, 0.010328550823032856f, 0.01746520586311817f, 0.011323824524879456f, 0.00528680719435215f, 0.018453359603881836f, 0.004433190915733576f, 0.00949854776263237f, 0.006856204476207495f, 0.006666820961982012f, 0.008261610753834248f, 0.007443959824740887f, 0.0060038878582417965f, 0.016751889139413834f, 0.022899223491549492f, 0.007903985679149628f, 0.014897936023771763f, 0.006258066277951002f, 0.014061745256185532f, 0.008373741060495377f, 0.021256178617477417f, 0.018924884498119354f, 0.008594419807195663f, 0.011054876260459423f, 0.007830056361854076f, 0.009278695099055767f, 0.01532810553908348f, 0.009136851876974106f, 0.009351122193038464f, 0.01793133281171322f, 0.009743605740368366f, 0.01179601065814495f, 0.012734041549265385f, 0.014503510668873787f, 0.020749298855662346f, 0.006209754850715399f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #64 */
AI_STATIC ai_intq_info_list conv2d_29_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00025335620739497244f, 0.00015901308506727219f, 0.00019447895465418696f, 0.0001198259778902866f, 8.714882278582081e-05f, 0.0002476797963026911f, 0.00019262357091065496f, 0.00015438564878422767f, 7.405025826301426e-05f, 0.00017963736900128424f, 0.0001884692901512608f, 0.00022412664839066565f, 0.0001391715632053092f, 0.00017077320080716163f, 0.00017522380221635103f, 0.00015520422311965376f, 0.0001533026370452717f, 0.0002341875951969996f, 0.0001059326677932404f, 9.318401134805754e-05f, 0.000177544352482073f, 0.0001892619620775804f, 0.0004467002581804991f, 0.00013766868505626917f, 0.00023736970615573227f, 0.0003051751118618995f, 0.00021427648607641459f, 0.00011885087587870657f, 0.00017960260447580367f, 0.00012909836368635297f, 0.00014600390568375587f, 0.00014464830746874213f, 0.00021754992485512048f, 0.0002311653079232201f, 8.813104795990512e-05f, 0.00017565221060067415f, 0.00011798867490142584f, 0.00018690976139623672f, 0.0002178766008000821f, 0.00012091641838196665f, 0.00015280378283932805f, 0.00031380957807414234f, 0.0001340197050012648f, 8.405203698202968e-05f, 0.00020933637279085815f, 0.0002513502258807421f, 0.00012460965081118047f, 0.0002086698223138228f, 0.00017167309124488384f, 0.00014998303959146142f, 0.00018544163322076201f, 0.00021544253104366362f, 0.00015538114530500025f, 0.00012086756032658741f, 0.00018113346595782787f, 0.0001350814418401569f, 0.0001837748131947592f, 0.00010017846943810582f, 0.00013881997438147664f, 0.00018901015573646873f, 0.00018457317491993308f, 0.0004365791392046958f, 0.00015968835214152932f, 0.00020340712217148393f, 0.00011232117685722187f, 0.00020282497280277312f, 0.00015459689893759787f, 8.399233047384769e-05f, 0.00024545195628888905f, 0.00014170384383760393f, 6.360069528454915e-05f, 0.00010240491974400356f, 0.00014632126840297133f, 0.00020166086324024945f, 0.00018172812997363508f, 7.247667235787958e-05f, 0.00016814858827274293f, 0.0002459607203491032f, 0.00013099830539431423f, 8.031615288928151e-05f, 0.00016195300850085914f, 0.0001875979796750471f, 0.00016523110389243811f, 0.0004969105939380825f, 0.0001094945619115606f, 0.00021550504607148468f, 8.527646423317492e-05f, 0.00013848936941940337f, 0.00012077488645445555f, 0.00028991594444960356f, 0.00014555137022398412f, 0.00017592583026271313f, 0.000355531316017732f, 8.761346543906257e-05f, 0.00024458239204250276f, 9.192941070068628e-05f, 0.00014305512013379484f, 0.0003001904988195747f, 0.00015203312796074897f, 0.0002657448058016598f, 0.0001350313104921952f, 0.00010609292075969279f, 0.0002147870691260323f, 0.00013055512681603432f, 7.913651643320918e-05f, 0.00027311017038300633f, 0.00010526544792810455f, 0.0002430839231237769f, 9.572735871188343e-05f, 0.0001708266936475411f, 0.00012298939691390842f, 0.000155837886268273f, 0.00020303981727920473f, 0.0002066336019197479f, 0.0001900129282148555f, 0.000230184115935117f, 0.00011813692981377244f, 0.0002740562194958329f, 0.00024652143474668264f, 0.00011133675434393808f, 0.00012620195047929883f, 0.00014495178766082972f, 0.00017633392417337745f, 0.0002817235072143376f, 0.00038690902874805033f, 0.0002144028403563425f, 0.00032261814340017736f, 0.00019192707259207964f, 0.00016155246703419834f, 0.0002051276678685099f, 0.00012125475041102618f, 0.0001431227137800306f, 0.0002809461147990078f, 0.00024783742264844477f, 0.00017166374891530722f, 4.514988177106716e-05f, 0.00027160634635947645f, 0.0002040514664258808f, 0.00039547192864120007f, 0.00025311295758001506f, 0.00010277983528794721f, 0.00017569627379998565f, 0.000120459109894f, 0.00023730522661935538f, 8.831800369080156e-05f, 0.0003470117226243019f, 0.00013090670108795166f, 0.0001894749148050323f, 0.00019769462232943624f, 0.00018367511802352965f, 0.00010540371295064688f, 0.0001632702478673309f, 0.0002867051225621253f, 0.00013561257219407707f, 8.617975981906056e-05f, 0.0002121267025358975f, 0.00032598894904367626f, 0.00022641598479822278f, 9.553886775393039e-05f, 0.00021151237888261676f, 0.00025167048443108797f, 0.00011671644460875541f, 0.00023005613184068352f, 0.0003089687670581043f, 0.00021814739739056677f, 0.0001166124056908302f, 0.00016744414460845292f, 9.96611051959917e-05f, 0.00030696357134729624f, 0.00012324702402111143f, 9.48137603700161e-05f, 0.00015058177814353257f, 0.000156893307575956f, 0.00015936879208311439f, 0.00013562670210376382f, 0.00022323493612930179f, 0.00010737289267126471f, 0.00012274491018615663f, 0.0001473019947297871f, 6.924237823113799e-05f, 0.00026767022791318595f, 0.0001488002308178693f, 0.0001274899987038225f, 0.00012043477909173816f, 0.00016048458928707987f, 0.00015475887630600482f, 0.00013599192607216537f, 0.00021113650291226804f, 9.238125494448468e-05f, 9.832080104388297e-05f, 0.00012142377818236127f, 0.00021417911921162158f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #65 */
AI_STATIC ai_intq_info_list conv2d_29_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 192, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0009595530573278666f, 0.0006022409652359784f, 0.0007365632918663323f, 0.00045382502139545977f, 0.0003300646203570068f, 0.0009380543488077819f, 0.0007295362302102149f, 0.0005847151624038815f, 0.00028045554063282907f, 0.000680352735798806f, 0.000713802466634661f, 0.0008488500025123358f, 0.0005270938272587955f, 0.0006467808852903545f, 0.000663636950775981f, 0.0005878154188394547f, 0.0005806134431622922f, 0.000886954483576119f, 0.0004012059362139553f, 0.00035292209940962493f, 0.0006724257254973054f, 0.000716804584953934f, 0.0016918180044740438f, 0.0005214018747210503f, 0.0008990063215605915f, 0.0011558103142306209f, 0.0008115437813103199f, 0.00045013194903731346f, 0.0006802210700698197f, 0.0004889429546892643f, 0.0005529704503715038f, 0.0005478363018482924f, 0.0008239414892159402f, 0.0008755079470574856f, 0.0003337846719659865f, 0.0006652594893239439f, 0.0004468664701562375f, 0.0007078959606587887f, 0.0008251787512563169f, 0.0004579549131449312f, 0.000578724080696702f, 0.0011885121930390596f, 0.0005075818626210093f, 0.00031833595130592585f, 0.0007928337436169386f, 0.000951955618802458f, 0.00047194253420457244f, 0.0007903092773631215f, 0.0006501890602521598f, 0.0005680408794432878f, 0.0007023356156423688f, 0.0008159600547514856f, 0.0005884855054318905f, 0.0004577698709908873f, 0.0006860190187580884f, 0.0005116030806675553f, 0.0006960227619856596f, 0.00037941266782581806f, 0.000525762268807739f, 0.0007158509106375277f, 0.0006990464171394706f, 0.0016534855822101235f, 0.0006047984934411943f, 0.0007703775190748274f, 0.0004254015802871436f, 0.0007681727292947471f, 0.0005855152849107981f, 0.000318109814543277f, 0.0009296167991124094f, 0.0005366845289245248f, 0.00024087920610327274f, 0.0003878450661431998f, 0.0005541724385693669f, 0.0007637637900188565f, 0.0006882711895741522f, 0.0002744958037510514f, 0.0006368404719978571f, 0.000931543589103967f, 0.000496138702146709f, 0.0003041867748834193f, 0.0006133755668997765f, 0.0007105024997144938f, 0.0006257909117266536f, 0.001881983014754951f, 0.0004146961437072605f, 0.0008161967853084207f, 0.0003229732974432409f, 0.0005245101056061685f, 0.0004574188787955791f, 0.0010980182560160756f, 0.0005512565257959068f, 0.0006662957603111863f, 0.0013465277152135968f, 0.0003318243834655732f, 0.0009263234096579254f, 0.0003481704625301063f, 0.0005418023210950196f, 0.0011369317071512341f, 0.0005758053157478571f, 0.0010064732050523162f, 0.0005114132072776556f, 0.0004018128674943f, 0.0008134775562211871f, 0.0004944602260366082f, 0.00029971907497383654f, 0.0010343685280531645f, 0.00039867893792688847f, 0.0009206481627188623f, 0.00036255468148738146f, 0.0006469834479503334f, 0.0004658060206566006f, 0.0005902153206989169f, 0.0007689864141866565f, 0.0007825974025763571f, 0.0007196487858891487f, 0.000871791853569448f, 0.00044742797035723925f, 0.0010379515588283539f, 0.0009336672956123948f, 0.00042167320498265326f, 0.0004779731680173427f, 0.0005489856703206897f, 0.0006678413483314216f, 0.0010669904295355082f, 0.0014653666876256466f, 0.0008120223646983504f, 0.0012218734482303262f, 0.0007268983754329383f, 0.0006118585588410497f, 0.0007768938667140901f, 0.00045923629659228027f, 0.000542058318387717f, 0.0010640461696311831f, 0.0009386513847857714f, 0.0006501536699943244f, 0.00017099919205065817f, 0.0010286730248481035f, 0.0007728178752586246f, 0.0014977975515648723f, 0.0009586317464709282f, 0.0003892650129273534f, 0.0006654263706877828f, 0.0004562229150906205f, 0.0008987620822153986f, 0.00033449273905716836f, 0.0013142608804628253f, 0.0004957917844876647f, 0.000717611110303551f, 0.0007487421971745789f, 0.0006956451688893139f, 0.0003992026031482965f, 0.0006183644291013479f, 0.0010858576279133558f, 0.0005136146210134029f, 0.00032639442360959947f, 0.000803401751909405f, 0.0012346399016678333f, 0.000857520557474345f, 0.00036184079363010824f, 0.0008010750752873719f, 0.0009531685500405729f, 0.0004420480690896511f, 0.0008713071001693606f, 0.0011701782932505012f, 0.0008262043702416122f, 0.0004416540323290974f, 0.0006341725238598883f, 0.000377453223336488f, 0.0011625838233157992f, 0.0004667817847803235f, 0.00035909455618821084f, 0.0005703085334971547f, 0.0005942126153968275f, 0.0006035881815478206f, 0.0005136681720614433f, 0.0008454727358184755f, 0.00040666060522198677f, 0.00046488005318678916f, 0.0005578867858275771f, 0.00026224632165394723f, 0.0010137654608115554f, 0.0005635611596517265f, 0.0004828514938708395f, 0.0004561307723633945f, 0.0006078141159377992f, 0.0005861287354491651f, 0.000515051418915391f, 0.0007996515487320721f, 0.00034988176776096225f, 0.00037237699143588543f, 0.0004598764644470066f, 0.0008111750357784331f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #66 */
AI_STATIC ai_intq_info_list conv2d_28_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00034538484760560095f, 0.00043056122376583517f, 0.0002974056114908308f, 0.00027710036374628544f, 0.000285167305264622f, 0.00025581466616131365f, 0.00029145643929950893f, 0.0004094596370123327f, 0.00039985377225093544f, 0.00029197873664088547f, 0.0004606662841979414f, 0.0002857506333384663f, 0.0006051353993825614f, 0.00030309605062939227f, 0.00028186754207126796f, 0.000375953852199018f, 0.00016122830857057124f, 0.0004402488411869854f, 0.00033365297713316977f, 0.00023214361863210797f, 0.0003615977184381336f, 0.0002818519715219736f, 0.0003638691850937903f, 0.00025046849623322487f, 0.00032778148306533694f, 0.00023064229753799736f, 0.00034063649945892394f, 0.00038506166310980916f, 0.0003304117708466947f, 0.000341536826454103f, 0.0002019536041188985f, 0.0003067860670853406f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #67 */
AI_STATIC ai_intq_info_list conv2d_28_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 32, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.014678855426609516f, 0.01829885132610798f, 0.012639738619327545f, 0.011776764877140522f, 0.012119609862565994f, 0.010872122831642628f, 0.012386898510158062f, 0.017402034252882004f, 0.01699378527700901f, 0.012409095652401447f, 0.019578317180275917f, 0.012144401669502258f, 0.02571825310587883f, 0.012881581671535969f, 0.011979370377957821f, 0.01597803831100464f, 0.006852203048765659f, 0.018710575997829437f, 0.014180251397192478f, 0.009866103529930115f, 0.01536790281534195f, 0.01197870820760727f, 0.015464439988136292f, 0.010644910857081413f, 0.013930712826550007f, 0.009802297689020634f, 0.014477050863206387f, 0.01636512018740177f, 0.014042499475181103f, 0.014515314251184464f, 0.008583027869462967f, 0.013038408011198044f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #68 */
AI_STATIC ai_intq_info_list conv2d_27_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(5.2046063501620665e-05f, 0.00011575860844459385f, 7.822888437658548e-05f, 5.725452501792461e-05f, 8.864932897267863e-05f, 3.6494137020781636e-05f, 4.5620538003277034e-05f, 0.0001602473930688575f, 6.912896787980571e-05f, 0.00013398852024693042f, 8.66001209942624e-05f, 9.74458598648198e-05f, 0.0001399325847160071f, 4.091285518370569e-05f, 7.503123197238892e-05f, 5.548268381971866e-05f, 7.843258208595216e-05f, 8.002729009604082e-05f, 3.5098819353152066e-05f, 5.791317380499095e-05f, 9.603247599443421e-05f, 8.318069012602791e-05f, 6.55673720757477e-05f, 0.00011280620674369857f, 7.633880159119144e-05f, 2.831560959748458e-05f, 0.00019769489881582558f, 0.00011597189586609602f, 8.098157559288666e-05f, 4.1187682654708624e-05f, 6.674813630525023e-05f, 0.0002151467342628166f, 0.00011087180610047653f, 0.00011590803478611633f, 7.302705489564687e-05f, 9.335850336356089e-05f, 0.00013960849901195616f, 7.448353426298127e-05f, 6.965720240259543e-05f, 6.419612327590585e-05f, 7.90720441727899e-05f, 7.008657848928124e-05f, 5.480973777594045e-05f, 2.793648855003994e-05f, 8.36328326840885e-05f, 5.911482003284618e-05f, 0.00018378376262262464f, 5.768675691797398e-05f, 7.223069405881688e-05f, 5.241414692136459e-05f, 4.481783616938628e-05f, 0.0001427773095201701f, 0.00011607848136918619f, 5.361565126804635e-05f, 9.004891762742773e-05f, 4.17483170167543e-05f, 7.857628224883229e-05f, 8.92764946911484e-05f, 6.885510811116546e-05f, 0.00014727846428286284f, 0.00010333208047086373f, 9.025468898471445e-05f, 0.00013218862295616418f, 8.45207177917473e-05f, 7.885228842496872e-05f, 6.727061554556713e-05f, 4.507076300797053e-05f, 8.611394150648266e-05f, 9.066338679986075e-05f, 7.017914322204888e-05f, 0.0003266175335738808f, 0.00010183143604081124f, 7.36258807592094e-05f, 5.516067176358774e-05f, 6.138126627774909e-05f, 9.469098586123437e-05f, 0.0001340680755674839f, 7.825835928088054e-05f, 7.632742926944047e-05f, 6.800924165872857e-05f, 0.00010459300392540172f, 5.360729119274765e-05f, 3.235050826333463e-05f, 0.0002399483200861141f, 0.00011652037937892601f, 3.760487015824765e-05f, 5.718036845792085e-05f, 0.00017439151997677982f, 7.074691529851407e-05f, 5.008696098229848e-05f, 0.00010694071534089744f, 7.362713949987665e-05f, 6.122163904365152e-05f, 0.0001620414259377867f, 0.00018774668569676578f, 0.00018094763800036162f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #69 */
AI_STATIC ai_intq_info_list conv2d_27_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0022119577042758465f, 0.004919740837067366f, 0.003324727527797222f, 0.0024333172477781773f, 0.003767596557736397f, 0.0015510007506236434f, 0.0019388728542253375f, 0.006810513790696859f, 0.002937980927526951f, 0.005694512277841568f, 0.0036805050913244486f, 0.004141448996961117f, 0.005947134457528591f, 0.001738796359859407f, 0.003188827307894826f, 0.0023580139968544245f, 0.0033333846367895603f, 0.0034011597745120525f, 0.0014916998334228992f, 0.002461309777572751f, 0.00408138008788228f, 0.0035351791884750128f, 0.0027866133023053408f, 0.004794263746589422f, 0.0032443988602608442f, 0.0012034133542329073f, 0.008402032777667046f, 0.004928805399686098f, 0.0034417167771607637f, 0.001750476541928947f, 0.0028367957565933466f, 0.009143736213445663f, 0.004712051711976528f, 0.004926091525703669f, 0.0031036497093737125f, 0.003967736382037401f, 0.005933361127972603f, 0.003165550297126174f, 0.0029604309238493443f, 0.00272833532653749f, 0.0033605617936700583f, 0.0029786794912070036f, 0.002329413779079914f, 0.0011873007752001286f, 0.0035543953999876976f, 0.0025123797822743654f, 0.007810809649527073f, 0.0024516871199011803f, 0.0030698045156896114f, 0.0022276011295616627f, 0.0019047580426558852f, 0.006068035494536161f, 0.004933335352689028f, 0.002278665080666542f, 0.003827078966423869f, 0.0017743033822625875f, 0.0033394917845726013f, 0.003794251009821892f, 0.002926342189311981f, 0.006259334739297628f, 0.00439161341637373f, 0.0038358240853995085f, 0.0056180162355303764f, 0.003592130495235324f, 0.0033512222580611706f, 0.00285900104790926f, 0.001915507367812097f, 0.0036598423030227423f, 0.0038531937170773745f, 0.002982613630592823f, 0.013881244696676731f, 0.004327835980802774f, 0.003129099728539586f, 0.0023443284444510937f, 0.0026087036821991205f, 0.004024366848170757f, 0.005697892978787422f, 0.00332598015666008f, 0.0032439157366752625f, 0.0028903926722705364f, 0.004445202648639679f, 0.0022783097811043262f, 0.001374896615743637f, 0.010197803378105164f, 0.004952115938067436f, 0.0015982069307938218f, 0.0024301656521856785f, 0.007411639671772718f, 0.0030067439656704664f, 0.002128695836290717f, 0.0045449803583323956f, 0.0031291532795876265f, 0.002601919462904334f, 0.006886760704219341f, 0.007979233749210835f, 0.0076902746222913265f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #70 */
AI_STATIC ai_intq_info_list conv2d_25_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00045352254528552294f, 0.00017524023132864386f, 0.0003346988232806325f, 0.0005607173079624772f, 0.0002673508715815842f, 0.0006431983783841133f, 0.0004577857325784862f, 0.0004129033477511257f, 0.00036721472861245275f, 0.00021593055862467736f, 0.00015239832282532007f, 0.0003167684481013566f, 0.00038734317058697343f, 0.0006573525606654584f, 0.00031169765861704946f, 0.0002880289393942803f, 0.00027038660482503474f, 0.0005092687206342816f, 0.001130639691837132f, 0.000636483367998153f, 0.0006826173630543053f, 0.00026307450025342405f, 0.00022084156807977706f, 0.00044339997111819685f, 0.00028769217897206545f, 0.0008177700219675899f, 0.00014482751430477947f, 0.00017949665198102593f, 0.0004267341282684356f, 0.0005223223706707358f, 0.0007870318368077278f, 0.00023586073075421154f, 0.0007278453558683395f, 0.0003471057571005076f, 0.0003210458380635828f, 0.00048427379806526005f, 0.000383227423299104f, 0.00022853133850730956f, 0.00035513268085196614f, 0.0006847712793387473f, 0.0005914134089834988f, 0.0006035685073584318f, 0.0004976946511305869f, 0.0006734435446560383f, 0.00033936547697521746f, 0.0019783778116106987f, 0.00012742314720526338f, 0.00042578772990964353f, 0.0006718133809044957f, 0.00033668664400465786f, 0.00046366348396986723f, 0.00015789704048074782f, 0.00020703565678559244f, 0.0005181207088753581f, 0.0003017116105183959f, 0.00042614396079443395f, 0.0002141685545211658f, 0.0004320528532844037f, 0.0001553545444039628f, 0.00019866567163262516f, 0.0005039202515035868f, 0.0002273318823426962f, 0.00017339982150588185f, 0.0002777400950435549f, 0.00020969577599316835f, 0.0007066447287797928f, 0.0003554940049070865f, 0.00033164312480948865f, 0.00030195480212569237f, 0.0003721466346178204f, 7.019539043540135e-05f, 0.00024034619855228812f, 0.0006087905494496226f, 0.00036652805283665657f, 0.00037502043414860964f, 0.00026614335365593433f, 0.00012509337102528661f, 0.00032130349427461624f, 0.0010730426292866468f, 0.000302670756354928f, 0.0003709840530063957f, 0.00031772421789355576f, 0.00041880953358486295f, 0.0002557547122705728f, 0.00020453627803362906f, 0.0009778453968465328f, 0.00044015145977027714f, 0.000250239361776039f, 0.0005126175237819552f, 0.0007333082612603903f, 0.0003128539538010955f, 0.00038672235677950084f, 0.0007278209668584168f, 0.0003259437216911465f, 0.00014032574836164713f, 0.0001984969712793827f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #71 */
AI_STATIC ai_intq_info_list conv2d_25_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0008254118729382753f, 0.000318937556585297f, 0.0006091524846851826f, 0.0010205066064372659f, 0.00048657908337190747f, 0.001170622301287949f, 0.0008331708959303796f, 0.0007514848839491606f, 0.0006683315150439739f, 0.00039299402851611376f, 0.00027736523770727217f, 0.0005765191745012999f, 0.0007049653213471174f, 0.0011963829165324569f, 0.0005672903498634696f, 0.0005242131883278489f, 0.0004921041545458138f, 0.0009268699795939028f, 0.0020577663090080023f, 0.0011584009043872356f, 0.0012423648731783032f, 0.0004787961079273373f, 0.0004019320767838508f, 0.0008069887990131974f, 0.000523600319866091f, 0.0014883429976180196f, 0.0002635863493196666f, 0.0003266842395532876f, 0.0007766569033265114f, 0.0009506277274340391f, 0.0014323993818834424f, 0.0004292669764254242f, 0.0013246799353510141f, 0.0006317331572063267f, 0.0005843040416948497f, 0.0008813792373985052f, 0.0006974746356718242f, 0.000415927468566224f, 0.0006463421741500497f, 0.0012462850427255034f, 0.001076373504474759f, 0.0010984957916662097f, 0.0009058052091859281f, 0.0012256684713065624f, 0.0006176457973197103f, 0.0036006513983011246f, 0.00023191036598291248f, 0.000774934480432421f, 0.001222701626829803f, 0.0006127703236415982f, 0.0008438684162683785f, 0.0002873729099519551f, 0.00037680528475902975f, 0.0009429806959815323f, 0.0005491157062351704f, 0.0007755827973596752f, 0.00038978716474957764f, 0.0007863370119594038f, 0.00028274557553231716f, 0.0003615719033405185f, 0.0009171357960440218f, 0.0004137444484513253f, 0.0003155879967380315f, 0.0005054874927736819f, 0.0003816467069555074f, 0.0012860947754234076f, 0.0006469997460953891f, 0.0006035910919308662f, 0.0005495583172887564f, 0.0006773076020181179f, 0.00012775574577972293f, 0.0004374305426608771f, 0.0011079999385401607f, 0.0006670817383565009f, 0.0006825379095971584f, 0.0004843813949264586f, 0.00022767017071601003f, 0.0005847729626111686f, 0.0019529395503923297f, 0.0005508613539859653f, 0.0006751916953362525f, 0.0005782586522400379f, 0.0007622341508977115f, 0.0004654740623664111f, 0.0003722564142663032f, 0.00177968037314713f, 0.0008010764722712338f, 0.0004554360930342227f, 0.000932964903768152f, 0.0013346223859116435f, 0.0005693947896361351f, 0.0007038353942334652f, 0.001324635581113398f, 0.0005932181957177818f, 0.0002553931262809783f, 0.00036126485792919993f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #72 */
AI_STATIC ai_intq_info_list conv2d_23_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00030602628248743713f, 0.00022220172104425728f, 0.0002095819218084216f, 0.0004084424872417003f, 0.00033873869688250124f, 0.00020323377975728363f, 0.00023596528626512736f, 0.00023927801521494985f, 0.00020596272952388972f, 0.00023532008344773203f, 0.00023858534405007958f, 0.00018410501070320606f, 0.0007241159910336137f, 0.0002221697650384158f, 0.0003170275886077434f, 0.00021190413099247962f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #73 */
AI_STATIC ai_intq_info_list conv2d_23_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.013006116263568401f, 0.009443572722375393f, 0.008907231502234936f, 0.01735880598425865f, 0.014396394602954388f, 0.008637435734272003f, 0.010028524324297905f, 0.01016931515187025f, 0.008753416128456593f, 0.010001103393733501f, 0.01013987697660923f, 0.007824462838470936f, 0.030774928629398346f, 0.009442214854061604f, 0.013473672792315483f, 0.009005925618112087f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #74 */
AI_STATIC ai_intq_info_list conv2d_22_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0002559213899075985f, 0.00039032791391946375f, 0.00022546244144905359f, 0.001543110585771501f, 0.00012652135046664625f, 0.00016232144844252616f, 0.00012470722140278667f, 7.542216189904138e-05f, 0.00019533447630237788f, 0.00020548207976389676f, 0.0004258316184859723f, 0.00016516519826836884f, 8.700713806319982e-05f, 0.00024037438561208546f, 0.00012425181921571493f, 5.4903335694689304e-05f, 0.00024242881045211107f, 0.0001068319397745654f, 0.00019426313519943506f, 0.00014572939835488796f, 0.00010357036080677062f, 0.00018350886239204556f, 0.0002581044682301581f, 5.256493022898212e-05f, 9.242841770173982e-05f, 0.0003539248718880117f, 0.0001409619435435161f, 0.00016149158182088286f, 6.805684824939817e-05f, 0.00031044144998304546f, 0.0002598573628347367f, 0.000642747909296304f, 0.00035949787707068026f, 0.00040486035868525505f, 0.0002455218054819852f, 0.00017623881285544485f, 7.493683369830251e-05f, 0.00012017264089081436f, 0.0004978586221113801f, 0.00014088851457927376f, 0.00015542648907285184f, 0.0001228928449563682f, 0.00011640077718766406f, 0.0002165350888390094f, 0.00013166185817681253f, 0.00012785608123522252f, 0.0001665994932409376f, 9.78528696577996e-05f, 0.00016898245667107403f, 0.00017178997222799808f, 0.0001520369842182845f, 0.0001513901079306379f, 0.0003092486585956067f, 0.00011329199332976714f, 0.00019843970949295908f, 0.0002662639308255166f, 0.0001808476954465732f, 0.0004185440484434366f, 0.0001294881949434057f, 9.35200514504686e-05f, 0.00022856416762806475f, 9.196760220220312e-05f, 0.00019633612828329206f, 0.00011071092740166932f, 0.00012313503248151392f, 0.00029562096460722387f, 0.0002858522639144212f, 0.00011717324377968907f, 0.00015174751752056181f, 4.673783405451104e-05f, 0.00021964684128761292f, 0.00010454680159455165f, 0.0001060899012372829f, 0.0002046413574134931f, 0.00010357515566283837f, 0.00043090732651762664f, 0.000185011129360646f, 0.000286137277726084f, 0.00020709368982352316f, 0.0002917922683991492f, 0.00018291575543116778f, 0.0002845378767233342f, 8.855092892190441e-05f, 0.00014813040615990758f, 8.683729538461193e-05f, 0.00013241928536444902f, 0.00010973812459269539f, 0.0004519714566413313f, 0.00030054026865400374f, 0.00010715037205955014f, 0.000199980684556067f, 8.708821405889466e-05f, 0.0004935096949338913f, 0.0001910627179313451f, 8.07744509074837e-05f, 9.105534991249442e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #75 */
AI_STATIC ai_intq_info_list conv2d_22_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.010876658372581005f, 0.016588935628533363f, 0.009582153521478176f, 0.06558220088481903f, 0.005377157591283321f, 0.006898661144077778f, 0.005300056654959917f, 0.0032054418697953224f, 0.00830171536654234f, 0.008732988499104977f, 0.01809784397482872f, 0.007019520737230778f, 0.003697803243994713f, 0.010215911082923412f, 0.005280701909214258f, 0.0023333916906267405f, 0.010303224436938763f, 0.004540357273072004f, 0.008256183005869389f, 0.00619349954649806f, 0.004401740152388811f, 0.007799126207828522f, 0.010969439521431923f, 0.0022340095601975918f, 0.003928207792341709f, 0.015041806735098362f, 0.005990882404148579f, 0.00686339195817709f, 0.0028924159705638885f, 0.013193761929869652f, 0.011043937876820564f, 0.027316786348819733f, 0.015278659760951996f, 0.01720656454563141f, 0.010434676893055439f, 0.007490149699151516f, 0.003184815403074026f, 0.0051073371432721615f, 0.021158991381525993f, 0.005987761542201042f, 0.006605625618249178f, 0.005222945939749479f, 0.004947032779455185f, 0.009202741086483002f, 0.005595628637820482f, 0.0054338835179805756f, 0.007080478593707085f, 0.004158746916800737f, 0.00718175433576107f, 0.007301073521375656f, 0.0064615714363753796f, 0.006434079725295305f, 0.013143068179488182f, 0.0048149097710847855f, 0.008433687500655651f, 0.011316216550767422f, 0.007686026860028505f, 0.017788121476769447f, 0.005503248423337936f, 0.0039746020920574665f, 0.00971397664397955f, 0.00390862300992012f, 0.008344285190105438f, 0.004705214407294989f, 0.0052332389168441296f, 0.012563890777528286f, 0.012148721143603325f, 0.004979862831532955f, 0.006449269130825996f, 0.0019863578490912914f, 0.009334990754723549f, 0.004443238954991102f, 0.004508820828050375f, 0.00869725737720728f, 0.004401944112032652f, 0.018313560634851456f, 0.007862973026931286f, 0.012160833925008774f, 0.008801481686532497f, 0.012401171028614044f, 0.0077739194966852665f, 0.012092859484255314f, 0.003763414453715086f, 0.006295542232692242f, 0.0036905850283801556f, 0.005627819802612066f, 0.004663870204240084f, 0.019208787009119987f, 0.012772960588335991f, 0.004553890787065029f, 0.008499179035425186f, 0.0037012489046901464f, 0.020974161103367805f, 0.008120165206491947f, 0.003432914149016142f, 0.0038698522839695215f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #76 */
AI_STATIC ai_intq_info_list conv2d_21_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0004320672305766493f, 7.803764310665429e-05f, 0.00013441481860354543f, 1.735904697852675e-05f, 0.00011167957563884556f, 0.00015989424719009548f, 0.0002526021853554994f, 0.0004234038351569325f, 0.00010700176062528044f, 0.00016127340495586395f, 8.955820521805435e-05f, 0.0001231765781994909f, 0.000338587851729244f, 0.00013860319450031966f, 0.00018144834029953927f, 0.00043962307972833514f, 0.00016330013750120997f, 0.00025928832474164665f, 0.00025761869619600475f, 0.00019682483980432153f, 0.00030593061819672585f, 0.00021967380598653108f, 0.00010257398389512673f, 0.0003105222713202238f, 0.00021669008128810674f, 0.00022719021944794804f, 0.00011609331704676151f, 0.0001586648286320269f, 0.00041196937672793865f, 7.155416096793488e-05f, 0.0001239965349668637f, 4.312838427722454e-05f, 0.0002067314344458282f, 4.680524580180645e-05f, 0.00013201820547692478f, 0.00019370049994904548f, 0.00023495752247981727f, 0.00021690517314709723f, 0.00011176918633282185f, 0.0002618932630866766f, 0.00016140204388648272f, 0.00019049552793148905f, 0.00012567949306685477f, 0.00015054708637762815f, 0.00017383843078278005f, 0.00024024939921218902f, 0.00017386578838340938f, 0.0003606773680076003f, 0.00022853574773762375f, 0.0001788991066860035f, 0.00020441629749257118f, 0.00020938289526384324f, 0.00016573633183725178f, 0.0002498493413440883f, 0.00015546979557257146f, 0.00018362970149610192f, 0.0001907188561744988f, 0.00012731146125588566f, 0.0002453083579894155f, 0.00039536567055620253f, 0.00017312885029241443f, 0.0002721173514146358f, 0.0001877031463664025f, 0.00027005592710338533f, 0.0002494684304110706f, 0.000293268880341202f, 0.00011725765943992883f, 0.00028563314117491245f, 0.00021663551160600036f, 0.00043479574378579855f, 0.000302294414723292f, 0.00014757523604203016f, 0.00012400111882016063f, 0.00017600310093257576f, 0.00014706449292134494f, 0.0001911102735903114f, 0.00014890581951476634f, 0.0002154462126782164f, 0.0002717912429943681f, 0.00022679840913042426f, 0.00027143204351887107f, 8.646367496112362e-05f, 0.0002809985599014908f, 0.00014852509775664657f, 0.0002153366367565468f, 0.00018962530884891748f, 0.0001291406952077523f, 7.906965765869245e-05f, 8.036377403186634e-05f, 0.00027630478143692017f, 0.00019124129903502762f, 0.00027771611348725855f, 6.0132762882858515e-05f, 0.00027987814974039793f, 0.0003925119526684284f, 0.00021848743199370801f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #77 */
AI_STATIC ai_intq_info_list conv2d_21_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0012916241539642215f, 0.00023328614770434797f, 0.00040182037628255785f, 5.189323201193474e-05f, 0.00033385553979314864f, 0.00047798873856663704f, 0.0007551303133368492f, 0.0012657257029786706f, 0.00031987164402380586f, 0.0004821116162929684f, 0.00026772578712552786f, 0.0003682247188407928f, 0.0010121763916686177f, 0.00041434113518334925f, 0.0005424226401373744f, 0.0013142116367816925f, 0.0004881703353021294f, 0.0007751178927719593f, 0.0007701267022639513f, 0.0005883892299607396f, 0.0009145506774075329f, 0.0006566940573975444f, 0.00030663522193208337f, 0.0009282769751735032f, 0.0006477744900621474f, 0.0006791636697016656f, 0.0003470499941613525f, 0.0004743135068565607f, 0.0012315434869378805f, 0.00021390439360402524f, 0.0003706759016495198f, 0.0001289282226935029f, 0.0006180040654726326f, 0.0001399198517901823f, 0.0003946559445466846f, 0.0005790493451058865f, 0.0007023832877166569f, 0.0006484175100922585f, 0.0003341234114486724f, 0.0007829051464796066f, 0.00048249613610096276f, 0.0005694683641195297f, 0.00037570696440525353f, 0.0004500462964642793f, 0.0005196735728532076f, 0.0007182028493843973f, 0.000519755354616791f, 0.0010782108874991536f, 0.000683185993693769f, 0.0005348019767552614f, 0.0006110831745900214f, 0.0006259303772822022f, 0.0004954530741088092f, 0.0007469009724445641f, 0.0004647622408811003f, 0.0005489436443895102f, 0.0005701360059902072f, 0.0003805855812970549f, 0.0007333261310122907f, 0.001181908301077783f, 0.0005175523110665381f, 0.0008134691161103547f, 0.0005611208034679294f, 0.0008073066710494459f, 0.0007457622559741139f, 0.0008766995742917061f, 0.00035053069586865604f, 0.0008538732654415071f, 0.0006476113921962678f, 0.0012997807934880257f, 0.0009036805713549256f, 0.0004411622358020395f, 0.0003706896095536649f, 0.0005261446349322796f, 0.00043963539064861834f, 0.0005713060963898897f, 0.0004451398563105613f, 0.0006440560682676733f, 0.0008124942542053759f, 0.0006779924151487648f, 0.0008114204392768443f, 0.00025847498909570277f, 0.0008400186197832227f, 0.00044400175102055073f, 0.0006437285337597132f, 0.000566866947337985f, 0.0003860539000015706f, 0.00023637125559616834f, 0.0002402398968115449f, 0.0008259870228357613f, 0.0005716977757401764f, 0.0008302060887217522f, 0.00017976120579987764f, 0.0008366692345589399f, 0.0011733773862943053f, 0.0006531475228257477f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #78 */
AI_STATIC ai_intq_info_list conv2d_19_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00018780439859256148f, 0.00028094564913772047f, 0.0002696265291888267f, 0.00028614839538931847f, 0.0002875376376323402f, 0.0003382411669008434f, 0.00021416974777821451f, 0.00016301347932312638f, 0.0001917881891131401f, 0.00021278360509313643f, 0.0003330846375320107f, 0.00016317135305143893f, 0.00030623533530160785f, 0.00044823679490946233f, 0.0003817107935901731f, 0.000315824116114527f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #79 */
AI_STATIC ai_intq_info_list conv2d_19_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.007981686852872372f, 0.011940189637243748f, 0.011459127068519592f, 0.012161307036876678f, 0.012220349162817001f, 0.014375248923897743f, 0.009102214127779007f, 0.006928072776645422f, 0.008150997571647167f, 0.009043303318321705f, 0.014156096614897251f, 0.006934782490134239f, 0.013015001080930233f, 0.01905006356537342f, 0.01622270792722702f, 0.013422524556517601f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #80 */
AI_STATIC ai_intq_info_list conv2d_18_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0002490010519977659f, 0.00017483778356108814f, 0.00040622774395160377f, 0.000142398988828063f, 0.00011345568054821342f, 0.00019262880960013717f, 0.00015296335914172232f, 8.467221050523221e-05f, 8.728536340640858e-05f, 0.00024033327645156533f, 0.00012129460810683668f, 0.0001589906751178205f, 0.00022661166440229863f, 4.419236211106181e-05f, 0.0001933013991219923f, 0.00017376762116327882f, 0.00010662960266927257f, 0.00014627799100708216f, 9.261686500394717e-05f, 0.00019848007650580257f, 0.00012032141239615157f, 0.0002119256096193567f, 0.00011990423081442714f, 0.00036198311136104167f, 7.450893463101238e-05f, 0.00016315735410898924f, 0.00011069119500461966f, 0.00014460546663030982f, 0.00033635800355114043f, 0.0010379180312156677f, 0.0002707296225707978f, 0.00040618196362629533f, 0.00025385580374859273f, 0.000124029626022093f, 0.00034212670288980007f, 6.135158037068322e-05f, 0.00012000949209323153f, 0.00016271245840471238f, 0.00016798912838567048f, 0.00012809042527806014f, 0.0001813246781239286f, 9.139117173617706e-05f, 0.0001435511076124385f, 0.0002402287209406495f, 0.00047381792683154345f, 0.0001388962846249342f, 0.00013602280523627996f, 9.420814603799954e-05f, 9.089534432860091e-05f, 0.00030671272543258965f, 0.00020999873231630772f, 0.000169119710335508f, 0.0001434387086192146f, 0.00018046534387394786f, 0.00021797271620016545f, 0.00022089471167419106f, 0.00022922863718122244f, 0.00014227336214389652f, 0.00022011423425283283f, 0.00010939117782982066f, 0.0003201707440894097f, 0.00019592120952438563f, 0.0001465520035708323f, 0.00010827222286025062f, 0.0004241337301209569f, 9.247225534636527e-05f, 0.00014336852473206818f, 0.00017108864267356694f, 0.00032063506660051644f, 0.00010368651419412345f, 0.00048012463958002627f, 0.00013830627722200006f, 0.00013790925731882453f, 9.682349627837539e-05f, 0.0001198086902149953f, 0.0005389089346863329f, 0.0002065009030047804f, 5.7435507187619805e-05f, 0.0005554333911277354f, 0.0001271628716494888f, 0.00011721079499693587f, 0.00017560497508384287f, 0.00030394503846764565f, 0.00017530187324155122f, 0.00012538998271338642f, 6.855891842860729e-05f, 0.00024198851315304637f, 0.00028415501583367586f, 0.00017897336510941386f, 0.00011203978647245094f, 0.00037945155054330826f, 0.00017841317458078265f, 0.00017849952564574778f, 0.00011312557762721553f, 0.00010808237857418135f, 9.726258576847613e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #81 */
AI_STATIC ai_intq_info_list conv2d_18_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.010582543909549713f, 0.007430605590343475f, 0.017264679074287415f, 0.006051957141608f, 0.004821866285055876f, 0.008186724036931992f, 0.006500942632555962f, 0.0035985689610242844f, 0.003709628013893962f, 0.010214163921773434f, 0.005155020859092474f, 0.006757103838026524f, 0.00963099580258131f, 0.0018781754188239574f, 0.008215309120714664f, 0.007385123521089554f, 0.004531757906079292f, 0.006216814741492271f, 0.003936216700822115f, 0.008435402996838093f, 0.005113659892231226f, 0.00900683831423521f, 0.005095929838716984f, 0.015384281985461712f, 0.0031666296999901533f, 0.00693418737500906f, 0.004704375751316547f, 0.00614573247730732f, 0.014295214787125587f, 0.04411151632666588f, 0.01150600891560316f, 0.017262732610106468f, 0.010788870975375175f, 0.005271259229630232f, 0.014540384523570538f, 0.002607442205771804f, 0.005100403446704149f, 0.006915279198437929f, 0.007139537949115038f, 0.0054438430815935135f, 0.007706298492848873f, 0.0038841248024255037f, 0.006100921891629696f, 0.010209720581769943f, 0.020137261599302292f, 0.005903091747313738f, 0.005780969280749559f, 0.00400384608656168f, 0.003863051999360323f, 0.013035290874540806f, 0.008924946188926697f, 0.007187587674707174f, 0.006096145138144493f, 0.0076697771437466145f, 0.00926384050399065f, 0.009388024918735027f, 0.009742217138409615f, 0.006046617869287729f, 0.009354854933917522f, 0.004649125039577484f, 0.01360725611448288f, 0.008326651528477669f, 0.006228459998965263f, 0.004601569380611181f, 0.018025683239102364f, 0.003930070903152227f, 0.006093162111938f, 0.007271267473697662f, 0.013626989908516407f, 0.0044066766276955605f, 0.020405296236276627f, 0.005878016818314791f, 0.005861143581569195f, 0.004114998504519463f, 0.00509186927229166f, 0.022903628647327423f, 0.008776288479566574f, 0.0024410090409219265f, 0.023605918511748314f, 0.0054044220596551895f, 0.004981458652764559f, 0.007463211193680763f, 0.012917663902044296f, 0.007450329605489969f, 0.005329074338078499f, 0.002913753967732191f, 0.010284511372447014f, 0.012076588347554207f, 0.007606367580592632f, 0.004761690739542246f, 0.016126690432429314f, 0.0075825597159564495f, 0.007586229592561722f, 0.004807836841791868f, 0.004593500867486f, 0.00413365988060832f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #82 */
AI_STATIC ai_intq_info_list conv2d_17_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00012723625695798546f, 0.0001619884860701859f, 0.0003412173828110099f, 0.00026015081675723195f, 0.00025893861311487854f, 0.00012387827155180275f, 0.00020068290177732706f, 0.00024217447207774967f, 0.00030263073858805f, 0.00013818391016684473f, 0.0002519619301892817f, 0.00011628964421106502f, 0.0002280992193846032f, 0.0002701795310713351f, 0.00046491986722685397f, 0.00018109749362338334f, 0.0002581059525255114f, 0.0002089580666506663f, 0.00013174219930078834f, 0.00015173100109677762f, 0.0004448805993888527f, 8.54780082590878e-05f, 0.00014163444575387985f, 0.00012855541717726737f, 0.00035333132836967707f, 0.00010785739868879318f, 0.00028291388298384845f, 0.00011627686762949452f, 0.0002297128812642768f, 1.7867638234747574e-05f, 8.805652032606304e-05f, 0.0004355500568635762f, 0.00012744478590320796f, 0.0003190066199749708f, 0.00018166049267165363f, 0.0004480040224734694f, 0.000162096373969689f, 0.00016626546857878566f, 0.0001328358775936067f, 0.00035553841735236347f, 0.0001807263761293143f, 0.0002622034808155149f, 0.00014684177585877478f, 0.00015691232692915946f, 0.00025165820261463523f, 0.00039248086977750063f, 0.00039044118602760136f, 0.00021855861996300519f, 0.000384708953788504f, 0.00021991973335389048f, 0.0005651148385368288f, 0.0003699810476973653f, 0.0001401628542225808f, 0.00018744243425317109f, 0.00012603274080902338f, 0.00037837549461983144f, 9.233160380972549e-05f, 0.0002039036335190758f, 0.00023379024059977382f, 0.0002267709787702188f, 0.00014325643132906407f, 0.00023809884442016482f, 0.00017537656822241843f, 0.0001767825015122071f, 0.0004292811790946871f, 0.00029120559338480234f, 0.00032663808087818325f, 0.00013687957834918052f, 0.000279321480775252f, 0.00019876078295055777f, 0.0005344296805560589f, 0.0002686216030269861f, 0.0002537976251915097f, 0.0001712710945867002f, 0.00020823589875362813f, 0.0001793623814592138f, 9.974421845981851e-05f, 0.0004063941305503249f, 0.00018514115072321147f, 0.00022650124446954578f, 0.0002627227222546935f, 0.00029885038384236395f, 9.560830221744254e-05f, 0.000144184596138075f, 0.00026260685990564525f, 0.0007525179535150528f, 0.00026556753437034786f, 9.298288932768628e-05f, 0.00032736980938352644f, 0.00017371556896250695f, 5.605402839137241e-05f, 0.00026375235756859183f, 0.0001607653102837503f, 0.00012047790369251743f, 0.00024319316435139626f, 0.0001354580163024366f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #83 */
AI_STATIC ai_intq_info_list conv2d_17_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00040458852890878916f, 0.0005150943761691451f, 0.0010850102407857776f, 0.0008272330742329359f, 0.0008233784465119243f, 0.00039391074096783996f, 0.0006381357088685036f, 0.0007700714631937444f, 0.0009623115765862167f, 0.00043940008617937565f, 0.0008011938771232963f, 0.0003697802603710443f, 0.0007253147196024656f, 0.0008591225487180054f, 0.0014783620135858655f, 0.0005758576444350183f, 0.0008207306964322925f, 0.0006644492386840284f, 0.00041891660657711327f, 0.0004824774223379791f, 0.0014146406902000308f, 0.00027180477627553046f, 0.0004503722011577338f, 0.00040878323488868773f, 0.0011235304409638047f, 0.00034296722151339054f, 0.0008996155229397118f, 0.0003697396314237267f, 0.0007304458413273096f, 5.681589391315356e-05f, 0.0002800039655994624f, 0.00138497119769454f, 0.0004052516014780849f, 0.0010143839754164219f, 0.0005776478792540729f, 0.0014245726633816957f, 0.0005154374521225691f, 0.000528694421518594f, 0.0004223943396937102f, 0.0011305485386401415f, 0.0005746775423176587f, 0.0008337601320818067f, 0.0004669305926654488f, 0.000498953100759536f, 0.0008002280374057591f, 0.0012480189325287938f, 0.0012415330857038498f, 0.0006949772941879928f, 0.0012233055895194411f, 0.0006993053830228746f, 0.0017969639739021659f, 0.0011764735681936145f, 0.0004456927999854088f, 0.0005960334092378616f, 0.0004007615789305419f, 0.0012031663209199905f, 0.00029359798645600677f, 0.0006483770557679236f, 0.0007434111321344972f, 0.0007210911135189235f, 0.0004555298073682934f, 0.0007571117021143436f, 0.0005576661205850542f, 0.0005621367017738521f, 0.0013650372857227921f, 0.0009259816724807024f, 0.0010386506328359246f, 0.0004352525866124779f, 0.0008881923276931047f, 0.0006320237298496068f, 0.0016993907047435641f, 0.000854168611112982f, 0.0008070310577750206f, 0.0005446114228107035f, 0.0006621528882533312f, 0.0005703402566723526f, 0.000317168771289289f, 0.0012922605965286493f, 0.0005887157167308033f, 0.0007202334236353636f, 0.0008354112505912781f, 0.0009502907050773501f, 0.0003040173032786697f, 0.0004584811977110803f, 0.0008350428543053567f, 0.0023928724694997072f, 0.0008444572449661791f, 0.000295668956823647f, 0.0010409774258732796f, 0.000552384415641427f, 0.00017824178212322295f, 0.0008386853151023388f, 0.0005112049402669072f, 0.0003830981731880456f, 0.0007733107195235789f, 0.0004307322669774294f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #84 */
AI_STATIC ai_intq_info_list conv2d_16_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0003128568932879716f, 0.0003082420735154301f, 0.000512828934006393f, 0.00027920177672058344f, 0.0004323810862842947f, 0.00037618272472172976f, 0.0002628315123729408f, 0.0004289198841433972f, 0.0004927598056383431f, 0.000394982926081866f, 0.0006116553558968008f, 0.0004405951185617596f, 0.00042681253398768604f, 0.0007194007048383355f, 0.00047670103958807886f, 0.00038762035546824336f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #85 */
AI_STATIC ai_intq_info_list conv2d_16_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.013296417891979218f, 0.01310028787702322f, 0.021795228123664856f, 0.011866074986755848f, 0.018376195803284645f, 0.015987765043973923f, 0.011170338839292526f, 0.01822909526526928f, 0.020942291244864464f, 0.01678677462041378f, 0.025995353236794472f, 0.01872529275715351f, 0.018139531835913658f, 0.030574528500437737f, 0.0202597938477993f, 0.016473865136504173f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #86 */
AI_STATIC ai_intq_info_list conv2d_15_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(6.603148358408362e-05f, 4.501078728935681e-05f, 0.00012918772699777037f, 4.417380478116684e-05f, 0.00010186943836743012f, 7.377713336609304e-05f, 0.00020522749400697649f, 7.876812742324546e-05f, 0.00015911369700916111f, 0.00014997513790149242f, 8.543386502424255e-05f, 6.0708152886945754e-05f, 4.253336737747304e-05f, 0.00012351117038633674f, 0.00014410572475753725f, 5.136201434652321e-05f, 8.863418770488352e-05f, 0.00012869304919149727f, 4.016626553493552e-05f, 9.223891538567841e-05f, 5.298335963743739e-05f, 8.765742677496746e-05f, 8.995341340778396e-05f, 4.6104036300675943e-05f, 7.179559906944633e-05f, 5.604563921224326e-05f, 3.001836194016505e-05f, 0.00010501919314265251f, 8.138447446981445e-05f, 7.189590542111546e-05f, 6.387492612702772e-05f, 5.994462117087096e-05f, 5.853132461197674e-05f, 7.993677718332037e-05f, 9.948656224878505e-05f, 9.245320688933134e-05f, 4.905214154860005e-05f, 2.778735688480083e-05f, 0.00022182412794791162f, 0.00013754759856965393f, 4.4650998461293057e-05f, 6.815587403252721e-05f, 3.496471981634386e-05f, 8.135571260936558e-05f, 0.00017272619879804552f, 8.023619011510164e-05f, 4.243158036842942e-05f, 0.00010259483678964898f, 6.254152685869485e-05f, 0.00011389845167286694f, 9.176299499813467e-05f, 3.4075270377798006e-05f, 7.986055425135419e-05f, 0.00012446810433175415f, 0.0004892874276265502f, 6.378644320648164e-05f, 9.400711860507727e-05f, 7.447873940691352e-05f, 7.025160448392853e-05f, 6.87386782374233e-05f, 0.00015527613868471235f, 7.638741953996941e-05f, 8.5597021097783e-05f, 4.625451038009487e-05f, 0.00020251625392120332f, 0.00016618047084193677f, 5.6487551773898304e-05f, 9.906083869282156e-05f, 0.00029832610744051635f, 0.0005219418089836836f, 5.239728852757253e-05f, 8.495964721078053e-05f, 8.0819234426599e-05f, 3.3407490263925865e-05f, 0.00010476855095475912f, 9.304424020228907e-05f, 5.8254958275938407e-05f, 4.119488949072547e-05f, 0.00016153661999851465f, 6.160304474178702e-05f, 6.269800360314548e-05f, 0.0001148036535596475f, 0.00014072679914534092f, 0.00010001145710702986f, 0.00011663643090287223f, 0.0001713419333100319f, 7.798844308126718e-05f, 9.426606266060844e-05f, 0.00016558349307160825f, 5.456802682601847e-05f, 0.0001572120818309486f, 0.0001367090444546193f, 5.10303107148502e-05f, 0.00015064136823639274f, 6.505659985123202e-05f, 0.0001425040973117575f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #87 */
AI_STATIC ai_intq_info_list conv2d_15_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0028063380159437656f, 0.0019129584543406963f, 0.005490478128194809f, 0.00187738670501858f, 0.004329450894147158f, 0.0031355281826108694f, 0.008722168393433094f, 0.003347645280882716f, 0.006762332282960415f, 0.006373943295329809f, 0.003630939172580838f, 0.0025800964795053005f, 0.001807668013498187f, 0.005249224603176117f, 0.006124493200331926f, 0.0021828855387866497f, 0.003766952781006694f, 0.005469454452395439f, 0.0017070661997422576f, 0.0039201537147164345f, 0.002251792699098587f, 0.003725440474227071f, 0.0038230200298130512f, 0.0019594214390963316f, 0.0030513128731399775f, 0.002381939673796296f, 0.0012757803779095411f, 0.004463315475732088f, 0.003458840074017644f, 0.003055575769394636f, 0.00271468423306942f, 0.0025476464070379734f, 0.0024875812232494354f, 0.0033973129466176033f, 0.0042281788773834705f, 0.003929261118173599f, 0.0020847159903496504f, 0.001180962659418583f, 0.009427525103092194f, 0.005845772568136454f, 0.0018976674182340503f, 0.002896624617278576f, 0.0014860006049275398f, 0.003457617713138461f, 0.007340863347053528f, 0.003410038072615862f, 0.00180334213655442f, 0.004360280465334654f, 0.0026580148842185736f, 0.004840684123337269f, 0.0038999272510409355f, 0.0014481989201158285f, 0.003394073573872447f, 0.005289894063025713f, 0.020794715732336044f, 0.002710923785343766f, 0.003995302598923445f, 0.003165346337482333f, 0.0029856932815164328f, 0.0029213938396424055f, 0.006599235814064741f, 0.0032464652322232723f, 0.0036378733348101377f, 0.0019658165983855724f, 0.008606940507888794f, 0.0070626698434352875f, 0.0024007209576666355f, 0.004210085608065128f, 0.012678858824074268f, 0.0221825260668993f, 0.0022268847096711397f, 0.003610784886404872f, 0.0034348175395280123f, 0.0014198183780536056f, 0.0044526634737849236f, 0.003954380284994841f, 0.0024758356157690287f, 0.0017507828306406736f, 0.006865306291729212f, 0.00261812936514616f, 0.0026646649930626154f, 0.004879155196249485f, 0.005980888847261667f, 0.004250486847013235f, 0.0049570482224226f, 0.007282032165676355f, 0.0033145088236778975f, 0.0040063075721263885f, 0.007037298288196325f, 0.0023191410582512617f, 0.006681513506919146f, 0.00581013411283493f, 0.002168788108974695f, 0.006402258295565844f, 0.002764905570074916f, 0.006056423764675856f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #88 */
AI_STATIC ai_intq_info_list conv2d_13_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0007817071746103466f, 0.0010151759488508105f, 0.00025956329773180187f, 0.0007408619276247919f, 0.0003926026984117925f, 0.0007473446312360466f, 0.0002173724351450801f, 0.0004901686334051192f, 0.00037346710450947285f, 0.00029332167468965054f, 0.00027684459928423166f, 0.0003486390342004597f, 0.0009533751290291548f, 0.00035241746809333563f, 0.00022666736913379282f, 0.0005600182339549065f, 0.0008096476085484028f, 0.0002543304581195116f, 0.0006502372561953962f, 0.0003701690293382853f, 0.0006912372191436589f, 0.000340777391102165f, 0.00046032731188461185f, 0.0007664526347070932f, 0.0006293766782619059f, 0.0005078520043753088f, 0.0007753217360004783f, 0.00020387108088470995f, 0.0005036276997998357f, 0.000374261086108163f, 0.0005409232689999044f, 0.00032088422449305654f, 0.00035810814006254077f, 0.00110666046384722f, 0.000349596404703334f, 0.0004239776753820479f, 0.0005632811225950718f, 0.0010213963687419891f, 0.00016539996431674808f, 0.0002515230735298246f, 0.000360172416549176f, 0.0016102176159620285f, 0.0006481129676103592f, 0.00017992028733715415f, 9.991002298193052e-05f, 0.00105714937672019f, 0.0018961677560582757f, 0.00026692202663980424f, 0.00037332074134610593f, 0.00020658985886257142f, 0.0002692576090339571f, 0.0009723939001560211f, 0.00022594146139454097f, 0.00031450731330551207f, 0.00011405222176108509f, 0.0005058504757471383f, 0.00016337574925273657f, 0.00039361545350402594f, 0.0005360599607229233f, 0.0004735840193461627f, 0.00015400008123833686f, 0.00031324345036409795f, 0.00037630039150826633f, 0.0009229121496900916f, 0.0002573455567471683f, 0.00015924530453048646f, 0.00043317925883457065f, 0.0006431816727854311f, 0.00012775960203725845f, 9.588467219145969e-05f, 0.0006855932297185063f, 0.0005742379580624402f, 0.0003534825809765607f, 0.0008165570325218141f, 0.000192731517017819f, 0.0004560781526379287f, 0.0005382805247791111f, 0.0012123846681788564f, 0.00041139667155221105f, 0.00043930180254392326f, 0.000742438540328294f, 0.00025808048667386174f, 0.0001982257963391021f, 0.0005299701006151736f, 0.0007207746966741979f, 0.00024151983961928636f, 0.000604050001129508f, 0.0003022848686669022f, 0.0002632548857945949f, 0.0011715800501406193f, 0.00014076197112444788f, 0.00013432346167974174f, 0.0006834909436292946f, 0.00017452577594667673f, 0.0005719334003515542f, 0.0003972992126364261f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #89 */
AI_STATIC ai_intq_info_list conv2d_13_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.001562201650813222f, 0.0020287770312279463f, 0.0005187239148654044f, 0.0014805744867771864f, 0.0007845963118597865f, 0.0014935298822820187f, 0.000434407644206658f, 0.0009795768419280648f, 0.000746354809962213f, 0.0005861882818862796f, 0.0005532596842385828f, 0.0006967372028157115f, 0.0019052712013944983f, 0.0007042881916277111f, 0.0004529831057880074f, 0.0011191676603630185f, 0.001618039095774293f, 0.0005082663265056908f, 0.001299465773627162f, 0.0007397637818939984f, 0.001381402020342648f, 0.0006810260820202529f, 0.0009199404739774764f, 0.0015317162033170462f, 0.0012577769812196493f, 0.0010149161098524928f, 0.0015494406688958406f, 0.0004074258904438466f, 0.0010064741363748908f, 0.0007479415507987142f, 0.0010810074163600802f, 0.0006412706570699811f, 0.0007156607462093234f, 0.0022116040345281363f, 0.0006986504304222763f, 0.0008472976041957736f, 0.0011256884317845106f, 0.00204120809212327f, 0.00033054332016035914f, 0.0005026559229008853f, 0.0007197860977612436f, 0.0032179371919482946f, 0.0012952204560860991f, 0.00035956143983639777f, 0.0001996650535147637f, 0.0021126586943864822f, 0.0037893939297646284f, 0.0005334299639798701f, 0.0007460623164661229f, 0.0004128592263441533f, 0.0005380975198931992f, 0.0019432792905718088f, 0.00045153239625506103f, 0.0006285267299972475f, 0.00022792750678490847f, 0.0010109161958098412f, 0.00032649803324602544f, 0.0007866202504374087f, 0.001071288250386715f, 0.0009464333415962756f, 0.0003077612491324544f, 0.0006260009249672294f, 0.0007520170183852315f, 0.0018443925073370337f, 0.0005142918671481311f, 0.0003182435466442257f, 0.0008656865102238953f, 0.0012853655498474836f, 0.00025532100698910654f, 0.000191620594705455f, 0.0013701228890568018f, 0.0011475851060822606f, 0.0007064167875796556f, 0.0016318472335115075f, 0.0003851640212815255f, 0.0009114487329497933f, 0.0010757260024547577f, 0.002422888530418277f, 0.0008221550961025059f, 0.0008779221097938716f, 0.0014837252674624324f, 0.0005157606210559607f, 0.00039614408160559833f, 0.0010591179598122835f, 0.0014404312241822481f, 0.0004826649965252727f, 0.0012071628589183092f, 0.0006041007582098246f, 0.0005261013866402209f, 0.0023413426242768764f, 0.0002813055762089789f, 0.00026843853993341327f, 0.0013659215765073895f, 0.00034878079895861447f, 0.0011429794831201434f, 0.0007939820643514395f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #90 */
AI_STATIC ai_intq_info_list conv2d_11_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0006571530248038471f, 0.0003060840826947242f, 0.00023306226648855954f, 0.00041488982969895005f, 0.000386729632737115f, 0.00021686179388780147f, 0.00031443199259229004f, 0.0003648430574685335f, 0.0003446140908636153f, 0.00028337270487099886f, 0.0005907215527258813f, 0.00026387753314338624f, 0.0001452461292501539f, 0.00039735910831950605f, 0.00043644141987897456f, 0.0005784355453215539f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #91 */
AI_STATIC ai_intq_info_list conv2d_11_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.027929002419114113f, 0.013008573092520237f, 0.009905146434903145f, 0.01763281784951687f, 0.016436008736491203f, 0.009216626174747944f, 0.013363359495997429f, 0.015505829825997353f, 0.014646098017692566f, 0.01204333920031786f, 0.025105666369199753f, 0.011214794591069221f, 0.0061729601584374905f, 0.016887761652469635f, 0.018548760563135147f, 0.02458351105451584f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #92 */
AI_STATIC ai_intq_info_list conv2d_10_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0002813291794154793f, 0.00014645639748778194f, 0.00021299306536093354f, 0.00017132595530711114f, 0.0002571707300376147f, 0.00011150126374559477f, 0.00012878302368335426f, 0.0001701448782114312f, 9.553034033160657e-05f, 0.00031579966889694333f, 0.00012361594417598099f, 0.00012166046508355066f, 0.00018303653632756323f, 0.00014270900283008814f, 0.00025885505601763725f, 0.00027794542256742716f, 0.00010142054816242307f, 0.0002121870347764343f, 9.824743028730154e-05f, 9.867727931123227e-05f, 0.0001922181691043079f, 0.00011200635344721377f, 0.0003623344236984849f, 7.753851969027892e-05f, 0.0002006032009376213f, 0.00032593266223557293f, 0.00014066063158679754f, 0.00013161820243112743f, 9.478488937020302e-05f, 0.0005152348894625902f, 0.00010428220411995426f, 0.0001189048562082462f, 9.415003296453506e-05f, 0.00015669451386202127f, 9.733786282595247e-05f, 0.00012293517647776753f, 9.862471779342741e-05f, 0.00013796154235024005f, 0.0002695778093766421f, 0.0002043926651822403f, 0.00017538052634336054f, 0.0006011839141137898f, 0.0001755664125084877f, 0.00022230096510611475f, 0.00017496921645943075f, 0.00013105141988489777f, 0.0001773362746462226f, 0.00012933665129821748f, 9.888462227536365e-05f, 0.00031822704477235675f, 0.0002517985412850976f, 0.0004225544398650527f, 0.00016832054825499654f, 0.0003459172148723155f, 0.0016890455735847354f, 0.0004202332638669759f, 9.479228901909664e-05f, 6.357920210575685e-05f, 4.906692265649326e-05f, 0.00015874476230237633f, 0.00010581780225038528f, 0.00020202882296871394f, 0.00015363513375632465f, 0.00022270582849159837f, 0.0005853780312463641f, 0.00014265190111473203f, 0.0002145941834896803f, 6.399756239261478e-05f, 0.00016147429414559156f, 0.0002481633855495602f, 0.0001275399117730558f, 7.89351252024062e-05f, 9.045337355928496e-05f, 0.0001922259252751246f, 0.0002072057395707816f, 0.0001563038385938853f, 5.207639333093539e-05f, 0.00014659747830592096f, 0.00021923173335380852f, 0.00036003589048050344f, 0.0002161776355933398f, 0.00017761241178959608f, 5.4565291065955535e-05f, 0.00020355734159238636f, 0.00013890504487790167f, 0.000526672403793782f, 0.0002985126047860831f, 0.00019885681103914976f, 0.00020207947818562388f, 4.5716380554949865e-05f, 0.0002165126643376425f, 0.00017385680985171348f, 0.000203826988581568f, 0.00014435657067224383f, 0.00045003314153291285f, 0.0003855224058497697f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #93 */
AI_STATIC ai_intq_info_list conv2d_10_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.011956489644944668f, 0.006224396638572216f, 0.00905220489948988f, 0.007281353231519461f, 0.010929755866527557f, 0.0047388034872710705f, 0.00547327846288681f, 0.007231157273054123f, 0.004060039296746254f, 0.013421486131846905f, 0.0052536772564053535f, 0.005170569755136967f, 0.00777905248105526f, 0.006065132562071085f, 0.011001339182257652f, 0.011812680400907993f, 0.004310373216867447f, 0.009017948992550373f, 0.004175515845417976f, 0.004193784203380346f, 0.008169271983206272f, 0.00476027000695467f, 0.015399212948977947f, 0.0032953869085758924f, 0.008525636047124863f, 0.013852138072252274f, 0.005978076718747616f, 0.005593773443251848f, 0.004028357565402985f, 0.021897481754422188f, 0.004431993700563908f, 0.005053456407040358f, 0.0040013762190938f, 0.006659516599029303f, 0.004136858973652124f, 0.005224744789302349f, 0.004191550426185131f, 0.00586336525157094f, 0.011457056738436222f, 0.008686687797307968f, 0.007453672122210264f, 0.025550315156579018f, 0.007461572531610727f, 0.009447790682315826f, 0.007436191663146019f, 0.005569685250520706f, 0.007536791730672121f, 0.005496807862073183f, 0.004202596377581358f, 0.013524648733437061f, 0.0107014374807477f, 0.017958562821149826f, 0.007153623271733522f, 0.014701480977237225f, 0.07178443670272827f, 0.017859913408756256f, 0.004028672352433205f, 0.002702116034924984f, 0.0020853441674262285f, 0.006746652536094189f, 0.004497256595641375f, 0.008586225099861622f, 0.006529493257403374f, 0.009464997798204422f, 0.024878565222024918f, 0.006062705535441637f, 0.009120252914726734f, 0.0027198963798582554f, 0.006862657610327005f, 0.010546944104135036f, 0.005420445930212736f, 0.003354742657393217f, 0.003844268387183547f, 0.008169601671397686f, 0.008806243538856506f, 0.006642912980169058f, 0.0022132466547191143f, 0.0062303924933075905f, 0.009317348711192608f, 0.01530152466148138f, 0.009187549352645874f, 0.007548527326434851f, 0.0023190248757600784f, 0.0086511867120862f, 0.005903464276343584f, 0.02238357625901699f, 0.01268678531050682f, 0.008451414294540882f, 0.008588377386331558f, 0.0019429461099207401f, 0.00920178834348917f, 0.007388914469629526f, 0.008662646636366844f, 0.00613515404984355f, 0.01912640780210495f, 0.016384702175855637f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #94 */
AI_STATIC ai_intq_info_list conv2d_9_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00039681512862443924f, 0.000537202344276011f, 0.0005268180975690484f, 0.0005433923797681928f, 0.0003398408298380673f, 0.0009075195412151515f, 0.0011581986909732223f, 0.0007532205199822783f, 0.00043599173659458756f, 0.0003522349870763719f, 0.0005304914666339755f, 0.00044032844016328454f, 0.0003425948671065271f, 0.0004365273052826524f, 0.0002671722322702408f, 0.00035723732435144484f, 0.0010517742484807968f, 0.00041178782703354955f, 0.0004286873445380479f, 0.00045997370034456253f, 0.0004905040841549635f, 0.0007464844384230673f, 0.00028723166906274855f, 0.0004849772376473993f, 0.00019697571406140924f, 0.00015711762534920126f, 0.00065855891443789f, 0.0006718074437230825f, 0.0007970142760314047f, 0.0005074380896985531f, 0.00030285611865110695f, 0.0005706885713152587f, 0.000578877457883209f, 0.0003091708058491349f, 0.00029385779635049403f, 0.0003619760391302407f, 0.0005110012134537101f, 0.0005575896939262748f, 7.537180499639362e-05f, 0.000104551188996993f, 0.00044425472151488066f, 0.0001790574169717729f, 0.0004748700594063848f, 0.0005677241133525968f, 0.00042937291436828673f, 0.0007624709978699684f, 0.00044340419117361307f, 0.0008297825697809458f, 0.0013121430529281497f, 0.0004384692874737084f, 0.0002037880476564169f, 0.00010502860095584765f, 0.00043093046406283975f, 0.00024288694839924574f, 3.6021359846927226e-05f, 8.672248077346012e-05f, 0.001320604351349175f, 0.0003451878728810698f, 0.0016643094131723046f, 0.00024627073435112834f, 0.000370759516954422f, 0.0003968699893448502f, 0.00024746143026277423f, 0.0003139136824756861f, 0.00026676408015191555f, 0.000271959463134408f, 0.00038602264248766005f, 0.0008257454028353095f, 0.0005760598578490317f, 0.0008486605947837234f, 0.0005289090913720429f, 0.00030961702577769756f, 0.00017578582628630102f, 0.00040871594683267176f, 0.00038698891876265407f, 0.0007883925572969019f, 0.0004079576174262911f, 0.0007761177839711308f, 0.0005964647862128913f, 0.0002458134840708226f, 0.00029236346017569304f, 0.0003752151387743652f, 0.001490039168857038f, 0.0003691291785798967f, 0.00027350446907803416f, 0.0005420578527264297f, 0.00031690375180915f, 0.0006101299077272415f, 0.00034149139537476003f, 0.0016688057221472263f, 0.00038609051262028515f, 0.0005550016649067402f, 0.0001851686683949083f, 0.0007906332612037659f, 0.00034335136297158897f, 0.0004998976364731789f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #95 */
AI_STATIC ai_intq_info_list conv2d_9_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 96, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0013696291716769338f, 0.0018541832687333226f, 0.0018183415522798896f, 0.0018755485070869327f, 0.0011729792458936572f, 0.003132353536784649f, 0.003997586201876402f, 0.002599781844764948f, 0.0015048494096845388f, 0.001215758384205401f, 0.0018310204613953829f, 0.0015198177425190806f, 0.0011824850225821137f, 0.0015066979685798287f, 0.0009221596410498023f, 0.0012330241734161973f, 0.003630256513133645f, 0.0014213082613423467f, 0.0014796379255130887f, 0.001587624428793788f, 0.0016930017154663801f, 0.0025765320751816034f, 0.0009913959074765444f, 0.001673925551585853f, 0.0006798724643886089f, 0.0005423001130111516f, 0.0022730522323399782f, 0.002318780170753598f, 0.0027509385254234076f, 0.0017514504725113511f, 0.0010453244904056191f, 0.001969762844964862f, 0.0019980273209512234f, 0.0010671199997887015f, 0.0010142662795260549f, 0.0012493801768869162f, 0.0017637487035244703f, 0.001924551441334188f, 0.0002601499145384878f, 0.0003608641563914716f, 0.0015333695337176323f, 0.0006180264754220843f, 0.0016390400705859065f, 0.001959530869498849f, 0.001482004183344543f, 0.0026317103765904903f, 0.0015304338885471225f, 0.002864039968699217f, 0.004528933670371771f, 0.0015134008135646582f, 0.0007033856236375868f, 0.00036251198616810143f, 0.001487380126491189f, 0.0008383376407437027f, 0.00012432970106601715f, 0.0002993273956235498f, 0.00455813854932785f, 0.0011914349161088467f, 0.005744455382227898f, 0.000850016949698329f, 0.0012796968221664429f, 0.0013698185794055462f, 0.0008541267015971243f, 0.001083490322344005f, 0.0009207508992403746f, 0.0009386830497533083f, 0.0013323783641681075f, 0.00285010552033782f, 0.001988302217796445f, 0.002929198555648327f, 0.0018255588365718722f, 0.0010686601744964719f, 0.0006067344220355153f, 0.001410705503076315f, 0.0013357134303078055f, 0.002721180208027363f, 0.00140808813739568f, 0.0026788131799548864f, 0.0020587309263646603f, 0.0008484387653879821f, 0.0010091084986925125f, 0.0012950756354257464f, 0.005142951849848032f, 0.0012740696547552943f, 0.0009440156864002347f, 0.0018709424184635282f, 0.0010938106570392847f, 0.0021058968268334866f, 0.001178676262497902f, 0.005759974475950003f, 0.0013326125917956233f, 0.0019156187772750854f, 0.0006391198257915676f, 0.0027289141435176134f, 0.00118509610183537f, 0.0017254241975024343f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #96 */
AI_STATIC ai_intq_info_list conv2d_8_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0005158245912753046f, 0.00036397730582393706f, 0.0004639127873815596f, 0.0006125342915765941f, 0.0004604096757248044f, 0.0007252850919030607f, 0.0008323906222358346f, 0.0002607602800708264f, 0.0007729681092314422f, 0.0005554409581236541f, 0.0006911096861585975f, 0.0010366729693487287f, 0.0009619912598282099f, 0.0007131760357879102f, 0.0003587171377148479f, 0.0002649890084285289f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #97 */
AI_STATIC ai_intq_info_list conv2d_8_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.02192254550755024f, 0.015469035133719444f, 0.0197162926197052f, 0.02603270672261715f, 0.01956741139292717f, 0.03082461468875408f, 0.03537660092115402f, 0.01108231209218502f, 0.032851144671440125f, 0.02360624074935913f, 0.029372161254286766f, 0.044058602303266525f, 0.040884628891944885f, 0.030309980735182762f, 0.015245478600263596f, 0.011262033134698868f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #98 */
AI_STATIC ai_intq_info_list conv2d_7_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.00018973664555232972f, 6.658725760644302e-05f, 2.2229156456887722e-05f, 3.0389015591936186e-05f, 4.3030759115936235e-05f, 0.0001850907865446061f, 5.6010481785051525e-05f, 5.719985711039044e-05f, 0.00015573704149574041f, 9.095954010263085e-05f, 5.041874828748405e-05f, 5.7413079048274085e-05f, 9.968494850909337e-05f, 2.8393902539392002e-05f, 7.277181430254132e-05f, 3.6167777579976246e-05f, 9.092506661545485e-05f, 0.00012248294660821557f, 7.384613127214834e-05f, 6.075630153645761e-05f, 8.624682959634811e-05f, 3.6970730434404686e-05f, 3.520281461533159e-05f, 0.00011129012273158878f, 9.283340477850288e-05f, 7.661362906219438e-05f, 6.807361205574125e-05f, 7.261538848979399e-05f, 7.713177910773084e-05f, 6.274251063587144e-05f, 3.0278064514277503e-05f, 4.3097355955978855e-05f, 0.00010180275421589613f, 0.00016519469500053674f, 3.145827213302255e-05f, 0.00010411070979898795f, 6.308879528660327e-05f, 4.892147262580693e-05f, 3.216112600057386e-05f, 3.110357647528872e-05f, 6.351594493025914e-05f, 0.0001924338866956532f, 0.00012045307084918022f, 3.515507705742493e-05f, 8.548955520382151e-05f, 8.759170304983854e-05f, 0.0003029976214747876f, 3.945120261050761e-05f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #99 */
AI_STATIC ai_intq_info_list conv2d_7_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.008063807152211666f, 0.002829958451911807f, 0.0009447391494177282f, 0.0012915331171825528f, 0.001828807289712131f, 0.007866358384490013f, 0.0023804453667253256f, 0.0024309938307851553f, 0.0066188243217766285f, 0.003865780308842659f, 0.0021427967585623264f, 0.0024400558322668076f, 0.0042366101406514645f, 0.0012067408533766866f, 0.0030928021296858788f, 0.0015371304471045732f, 0.003864315338432789f, 0.0052055250853300095f, 0.0031384604517370462f, 0.0025821428280323744f, 0.003665490308776498f, 0.0015712559688836336f, 0.0014961196575313807f, 0.00472983019426465f, 0.003945419564843178f, 0.0032560790423303843f, 0.0028931284323334694f, 0.003086153883486986f, 0.003278100397437811f, 0.002666556742042303f, 0.0012868177145719528f, 0.0018316375790163875f, 0.004326616879552603f, 0.00702077429741621f, 0.0013369765365496278f, 0.004424705170094967f, 0.0026812737341970205f, 0.0020791625138372183f, 0.0013668477768078446f, 0.0013219020329415798f, 0.0026994275394827127f, 0.008178439922630787f, 0.005119255278259516f, 0.0014940907713025808f, 0.00363330589607358f, 0.003722647437825799f, 0.01287739910185337f, 0.001676676096394658f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #100 */
AI_STATIC ai_intq_info_list conv2d_5_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0003943640913348645f, 0.000888880284037441f, 0.001406993716955185f, 0.003359951777383685f, 0.001133755431510508f, 0.000229839381063357f, 0.0009101821924559772f, 0.0020614624954760075f, 0.00023123588471207768f, 0.0004688584594987333f, 0.0010506740072742105f, 0.0003642851661425084f, 0.0006071488605812192f, 0.0014046740252524614f, 0.0010180490789934993f, 0.0019944547675549984f, 0.0012859624112024903f, 0.00032183300936594605f, 0.0004137153155170381f, 0.0006783827557228506f, 0.002541993046179414f, 0.000840773107483983f, 0.0021328830625861883f, 0.00028630997985601425f, 0.0004472954315133393f, 0.000899693404790014f, 0.0006839022389613092f, 0.0006284626433625817f, 0.0003926511271856725f, 0.0006595151498913765f, 0.0012263365788385272f, 0.0017729451647028327f, 0.00037333928048610687f, 0.0003661382361315191f, 0.0033428892493247986f, 0.00034180309739895165f, 0.00044034598977304995f, 0.0019176877103745937f, 0.0020169876515865326f, 0.003039832692593336f, 0.0009543376509100199f, 0.0002999971329700202f, 0.0003231230366509408f, 0.0020808037370443344f, 0.00032812863355502486f, 0.0002503305731806904f, 0.0004840669280383736f, 0.0008951461059041321f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #101 */
AI_STATIC ai_intq_info_list conv2d_5_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 48, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0009503824985586107f, 0.0021421227138489485f, 0.0033907301258295774f, 0.008097185753285885f, 0.002732250140979886f, 0.0005538925179280341f, 0.002193458378314972f, 0.004967941902577877f, 0.0005572579684667289f, 0.0011299073230475187f, 0.0025320311542600393f, 0.000877894985023886f, 0.0014631749363616109f, 0.003385139862075448f, 0.002453407971188426f, 0.004806459415704012f, 0.0030990554951131344f, 0.0007755890255793929f, 0.0009970172541216016f, 0.0016348423669114709f, 0.006125977728515863f, 0.002026188652962446f, 0.005140059161931276f, 0.0006899816798977554f, 0.0010779423173516989f, 0.0021681813523173332f, 0.0016481437487527728f, 0.0015145392389968038f, 0.0009462544112466276f, 0.0015893731033429503f, 0.002955362433567643f, 0.004272640682756901f, 0.0008997145341709256f, 0.0008823606767691672f, 0.008056066930294037f, 0.0008237151778303087f, 0.001061194809153676f, 0.0046214573085308075f, 0.004860761575400829f, 0.00732572702690959f, 0.0022998692002147436f, 0.0007229665061458945f, 0.0007786978967487812f, 0.0050145527347922325f, 0.0007907609106041491f, 0.0006032745004631579f, 0.0011665583588182926f, 0.0021572227124124765f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #102 */
AI_STATIC ai_intq_info_list conv2d_4_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 8, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0011270781978964806f, 0.0009048028150573373f, 0.0012659929925575852f, 0.0014943847199901938f, 0.001526329666376114f, 0.0010632099583745003f, 0.0009708362049423158f, 0.0018302800599485636f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #103 */
AI_STATIC ai_intq_info_list conv2d_4_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 8, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.04790082201361656f, 0.038454119116067886f, 0.05380469933152199f, 0.06351134926080704f, 0.06486900895833969f, 0.045186422765254974f, 0.04126053676009178f, 0.0777869001030922f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #104 */
AI_STATIC ai_intq_info_list conv2d_3_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0005677524022758007f, 0.003429790260270238f, 0.00031433856929652393f, 0.001183888642117381f, 0.00011916697985725477f, 0.0027266463730484247f, 0.0001740642182994634f, 0.00026362534845247865f, 0.00018927363271359354f, 0.001269531319849193f, 0.00011374408495612442f, 0.00025842408649623394f, 0.00013886182568967342f, 0.00014277648006100208f, 7.886782987043262e-05f, 0.00021106800704728812f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #105 */
AI_STATIC ai_intq_info_list conv2d_3_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0241294763982296f, 0.14576607942581177f, 0.013359389267861843f, 0.05031526833772659f, 0.005064596422016621f, 0.11588247120380402f, 0.007397729437798262f, 0.011204076930880547f, 0.008044129237532616f, 0.053955078125f, 0.00483412342146039f, 0.010983023792505264f, 0.005901627708226442f, 0.006068000104278326f, 0.003351882565766573f, 0.008970390073955059f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #106 */
AI_STATIC ai_intq_info_list conv2d_2_bias_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(9.439161658519879e-05f, 1.4137138681924455e-10f, 0.00020956055959686637f, 6.562835691559243e-11f, 2.7903932277695276e-05f, 1.2999387122292205e-09f, 6.03291800871375e-06f, 4.936820187140256e-05f, 7.867333624744788e-05f, 2.443806268459525e-10f, 0.00010974015458486974f, 4.131493915338069e-05f, 0.00015532142424490303f, 6.568847311427817e-05f, 5.799985956400633e-05f, 0.00011213293328182772f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #107 */
AI_STATIC ai_intq_info_list conv2d_2_weights_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 16, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.024069860577583313f, 3.6049701890306096e-08f, 0.053437940776348114f, 1.673523009060318e-08f, 0.007115502376109362f, 3.3148435818475264e-07f, 0.0015383940190076828f, 0.01258889026939869f, 0.02006169967353344f, 6.231705640402652e-08f, 0.027983738109469414f, 0.010535309091210365f, 0.039606962352991104f, 0.016750559210777283f, 0.014789963141083717f, 0.028593895956873894f), AI_PACK_INTQ_ZP(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)));   /* Int quant #108 */
AI_STATIC ai_intq_info_list conversion_0_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.003921568859368563f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #109 */
AI_STATIC ai_intq_info_list conv2d_2_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #110 */
AI_STATIC ai_intq_info_list conv2d_3_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #111 */
AI_STATIC ai_intq_info_list conv2d_4_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.4149530231952667f), AI_PACK_INTQ_ZP(9)));   /* Int quant #112 */
AI_STATIC ai_intq_info_list conv2d_5_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #113 */
AI_STATIC ai_intq_info_list conv2d_7_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #114 */
AI_STATIC ai_intq_info_list conv2d_8_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.2897244989871979f), AI_PACK_INTQ_ZP(-1)));   /* Int quant #115 */
AI_STATIC ai_intq_info_list conv2d_9_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #116 */
AI_STATIC ai_intq_info_list conv2d_10_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #117 */
AI_STATIC ai_intq_info_list conv2d_11_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.4029577374458313f), AI_PACK_INTQ_ZP(-19)));   /* Int quant #118 */
AI_STATIC ai_intq_info_list eltwise_12_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.5003881454467773f), AI_PACK_INTQ_ZP(-18)));   /* Int quant #119 */
AI_STATIC ai_intq_info_list conv2d_13_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #120 */
AI_STATIC ai_intq_info_list conv2d_15_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #121 */
AI_STATIC ai_intq_info_list conv2d_16_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.314483106136322f), AI_PACK_INTQ_ZP(-4)));   /* Int quant #122 */
AI_STATIC ai_intq_info_list conv2d_17_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #123 */
AI_STATIC ai_intq_info_list conv2d_18_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #124 */
AI_STATIC ai_intq_info_list conv2d_19_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.16825948655605316f), AI_PACK_INTQ_ZP(17)));   /* Int quant #125 */
AI_STATIC ai_intq_info_list eltwise_20_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.33451467752456665f), AI_PACK_INTQ_ZP(4)));   /* Int quant #126 */
AI_STATIC ai_intq_info_list conv2d_21_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #127 */
AI_STATIC ai_intq_info_list conv2d_22_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #128 */
AI_STATIC ai_intq_info_list conv2d_23_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.3371323049068451f), AI_PACK_INTQ_ZP(-27)));   /* Int quant #129 */
AI_STATIC ai_intq_info_list eltwise_24_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.5494499802589417f), AI_PACK_INTQ_ZP(-12)));   /* Int quant #130 */
AI_STATIC ai_intq_info_list conv2d_25_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #131 */
AI_STATIC ai_intq_info_list conv2d_27_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #132 */
AI_STATIC ai_intq_info_list conv2d_28_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.2640356421470642f), AI_PACK_INTQ_ZP(-1)));   /* Int quant #133 */
AI_STATIC ai_intq_info_list conv2d_29_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #134 */
AI_STATIC ai_intq_info_list conv2d_30_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #135 */
AI_STATIC ai_intq_info_list conv2d_31_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.25265058875083923f), AI_PACK_INTQ_ZP(16)));   /* Int quant #136 */
AI_STATIC ai_intq_info_list eltwise_32_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.2805980145931244f), AI_PACK_INTQ_ZP(7)));   /* Int quant #137 */
AI_STATIC ai_intq_info_list conv2d_33_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #138 */
AI_STATIC ai_intq_info_list conv2d_34_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #139 */
AI_STATIC ai_intq_info_list conv2d_35_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.18303033709526062f), AI_PACK_INTQ_ZP(3)));   /* Int quant #140 */
AI_STATIC ai_intq_info_list eltwise_36_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.3092658519744873f), AI_PACK_INTQ_ZP(13)));   /* Int quant #141 */
AI_STATIC ai_intq_info_list conv2d_37_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #142 */
AI_STATIC ai_intq_info_list conv2d_38_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #143 */
AI_STATIC ai_intq_info_list conv2d_39_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.1807674914598465f), AI_PACK_INTQ_ZP(-23)));   /* Int quant #144 */
AI_STATIC ai_intq_info_list eltwise_40_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.3536200225353241f), AI_PACK_INTQ_ZP(24)));   /* Int quant #145 */
AI_STATIC ai_intq_info_list conv2d_41_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #146 */
AI_STATIC ai_intq_info_list conv2d_42_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #147 */
AI_STATIC ai_intq_info_list conv2d_43_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.15645913779735565f), AI_PACK_INTQ_ZP(1)));   /* Int quant #148 */
AI_STATIC ai_intq_info_list conv2d_44_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #149 */
AI_STATIC ai_intq_info_list conv2d_45_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #150 */
AI_STATIC ai_intq_info_list conv2d_46_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.1919996440410614f), AI_PACK_INTQ_ZP(-11)));   /* Int quant #151 */
AI_STATIC ai_intq_info_list eltwise_47_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.2395937144756317f), AI_PACK_INTQ_ZP(-16)));   /* Int quant #152 */
AI_STATIC ai_intq_info_list conv2d_48_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #153 */
AI_STATIC ai_intq_info_list conv2d_49_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #154 */
AI_STATIC ai_intq_info_list conv2d_50_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.3045297861099243f), AI_PACK_INTQ_ZP(-28)));   /* Int quant #155 */
AI_STATIC ai_intq_info_list eltwise_51_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.4657488763332367f), AI_PACK_INTQ_ZP(-16)));   /* Int quant #156 */
AI_STATIC ai_intq_info_list conv2d_52_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.0235294122248888f), AI_PACK_INTQ_ZP(-128)));   /* Int quant #157 */
AI_STATIC ai_intq_info_list conv2d_53_output_intq = AI_INTQ_INFO_LIST_OBJ_INIT(
  AI_BUFFER_META_FLAG_SCALE_FLOAT|AI_BUFFER_META_FLAG_ZEROPOINT_S8, 1, AI_PACK_INTQ_INFO(
    AI_PACK_INTQ_SCALE(0.11715689301490784f), AI_PACK_INTQ_ZP(20)));   /* Int quant #158 */


/**  Tensor declarations section  *********************************************/
AI_TENSOR_OBJ_DECLARE(
  conv2d_53_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1502, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1502, 1502),
  1, &conv2d_53_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_52_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_52_scratch1_array, &conv2d_52_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_52_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3072, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3072, 3072),
  1, &conv2d_52_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_50_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1632, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1632, 1632),
  1, &conv2d_50_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_49_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_49_scratch1_array, &conv2d_49_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_49_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 10657, 1, 1), AI_STRIDE_INIT(4, 1, 1, 10657, 10657),
  1, &conv2d_49_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_48_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_48_scratch1_array, &conv2d_48_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_48_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3072, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3072, 3072),
  1, &conv2d_48_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_46_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1632, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1632, 1632),
  1, &conv2d_46_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_45_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_45_scratch1_array, &conv2d_45_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_45_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 10657, 1, 1), AI_STRIDE_INIT(4, 1, 1, 10657, 10657),
  1, &conv2d_45_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_44_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_44_scratch1_array, &conv2d_44_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_44_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3072, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3072, 3072),
  1, &conv2d_44_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_43_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1248, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1248, 1248),
  1, &conv2d_43_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_42_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_42_scratch1_array, &conv2d_42_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_42_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 7105, 1, 1), AI_STRIDE_INIT(4, 1, 1, 7105, 7105),
  1, &conv2d_42_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_41_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_41_scratch1_array, &conv2d_41_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_41_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 2048, 1, 1), AI_STRIDE_INIT(4, 1, 1, 2048, 2048),
  1, &conv2d_41_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_39_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1088, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1088, 1088),
  1, &conv2d_39_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_38_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_38_scratch1_array, &conv2d_38_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_38_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 7105, 1, 1), AI_STRIDE_INIT(4, 1, 1, 7105, 7105),
  1, &conv2d_38_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_37_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_37_scratch1_array, &conv2d_37_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_37_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 2048, 1, 1), AI_STRIDE_INIT(4, 1, 1, 2048, 2048),
  1, &conv2d_37_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_35_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1088, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1088, 1088),
  1, &conv2d_35_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_34_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_34_scratch1_array, &conv2d_34_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_34_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 7105, 1, 1), AI_STRIDE_INIT(4, 1, 1, 7105, 7105),
  1, &conv2d_34_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_33_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_33_scratch1_array, &conv2d_33_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_33_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 2048, 1, 1), AI_STRIDE_INIT(4, 1, 1, 2048, 2048),
  1, &conv2d_33_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_31_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1088, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1088, 1088),
  1, &conv2d_31_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_30_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_30_scratch1_array, &conv2d_30_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_30_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 7105, 1, 1), AI_STRIDE_INIT(4, 1, 1, 7105, 7105),
  1, &conv2d_30_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_29_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_29_scratch1_array, &conv2d_29_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_29_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 2048, 1, 1), AI_STRIDE_INIT(4, 1, 1, 2048, 2048),
  1, &conv2d_29_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_28_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 704, 1, 1), AI_STRIDE_INIT(4, 1, 1, 704, 704),
  1, &conv2d_28_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_27_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 4, 4), AI_STRIDE_INIT(4, 1, 1, 96, 384),
  1, &conv2d_27_scratch1_array, &conv2d_27_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_27_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3553, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3553, 3553),
  1, &conv2d_27_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_25_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_25_scratch1_array, &conv2d_25_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_25_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1024, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1024, 1024),
  1, &conv2d_25_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_23_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 544, 1, 1), AI_STRIDE_INIT(4, 1, 1, 544, 544),
  1, &conv2d_23_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_22_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_22_scratch1_array, &conv2d_22_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_22_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3553, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3553, 3553),
  1, &conv2d_22_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_21_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_21_scratch1_array, &conv2d_21_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_21_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1024, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1024, 1024),
  1, &conv2d_21_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_19_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 544, 1, 1), AI_STRIDE_INIT(4, 1, 1, 544, 544),
  1, &conv2d_19_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_18_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_18_scratch1_array, &conv2d_18_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_18_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3553, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3553, 3553),
  1, &conv2d_18_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_17_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_17_scratch1_array, &conv2d_17_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_17_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1024, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1024, 1024),
  1, &conv2d_17_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_16_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 544, 1, 1), AI_STRIDE_INIT(4, 1, 1, 544, 544),
  1, &conv2d_16_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_15_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_15_scratch1_array, &conv2d_15_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_15_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3553, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3553, 3553),
  1, &conv2d_15_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_13_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 16, 16), AI_STRIDE_INIT(4, 1, 1, 96, 1536),
  1, &conv2d_13_scratch1_array, &conv2d_13_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_13_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1024, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1024, 1024),
  1, &conv2d_13_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_11_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 544, 1, 1), AI_STRIDE_INIT(4, 1, 1, 544, 544),
  1, &conv2d_11_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_10_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 16, 16), AI_STRIDE_INIT(4, 1, 1, 96, 1536),
  1, &conv2d_10_scratch1_array, &conv2d_10_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_10_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3553, 1, 1), AI_STRIDE_INIT(4, 1, 1, 3553, 3553),
  1, &conv2d_10_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_9_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 16, 16), AI_STRIDE_INIT(4, 1, 1, 96, 1536),
  1, &conv2d_9_scratch1_array, &conv2d_9_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_9_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1024, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1024, 1024),
  1, &conv2d_9_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_8_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 352, 1, 1), AI_STRIDE_INIT(4, 1, 1, 352, 352),
  1, &conv2d_8_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 16, 16), AI_STRIDE_INIT(4, 1, 1, 48, 768),
  1, &conv2d_7_scratch1_array, &conv2d_7_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 1777, 1, 1), AI_STRIDE_INIT(4, 1, 1, 1777, 1777),
  1, &conv2d_7_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 32, 32), AI_STRIDE_INIT(4, 1, 1, 48, 1536),
  1, &conv2d_5_scratch1_array, &conv2d_5_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 512, 1, 1), AI_STRIDE_INIT(4, 1, 1, 512, 512),
  1, &conv2d_5_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 144, 1, 1), AI_STRIDE_INIT(4, 1, 1, 144, 144),
  1, &conv2d_4_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 32, 32), AI_STRIDE_INIT(4, 1, 1, 16, 512),
  1, &conv2d_3_scratch1_array, &conv2d_3_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 593, 1, 1), AI_STRIDE_INIT(4, 1, 1, 593, 593),
  1, &conv2d_3_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_scratch1, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 32, 32), AI_STRIDE_INIT(4, 1, 1, 16, 512),
  1, &conv2d_2_scratch1_array, &conv2d_2_scratch1_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_scratch0, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 332, 1, 1), AI_STRIDE_INIT(4, 1, 1, 332, 332),
  1, &conv2d_2_scratch0_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_53_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 35, 1, 1), AI_STRIDE_INIT(4, 4, 4, 140, 140),
  1, &conv2d_53_bias_array, &conv2d_53_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_53_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 288, 1, 1, 35), AI_STRIDE_INIT(4, 1, 288, 288, 288),
  1, &conv2d_53_weights_array, &conv2d_53_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_52_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 1, 1), AI_STRIDE_INIT(4, 4, 4, 1152, 1152),
  1, &conv2d_52_bias_array, &conv2d_52_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_52_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 48, 1, 1, 288), AI_STRIDE_INIT(4, 1, 48, 48, 48),
  1, &conv2d_52_weights_array, &conv2d_52_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_50_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 1, 1), AI_STRIDE_INIT(4, 4, 4, 192, 192),
  1, &conv2d_50_bias_array, &conv2d_50_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_50_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 288, 1, 1, 48), AI_STRIDE_INIT(4, 1, 288, 288, 288),
  1, &conv2d_50_weights_array, &conv2d_50_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_49_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 1, 1), AI_STRIDE_INIT(4, 4, 4, 1152, 1152),
  1, &conv2d_49_bias_array, &conv2d_49_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_49_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 288, 3, 3, 1), AI_STRIDE_INIT(4, 1, 288, 864, 2592),
  1, &conv2d_49_weights_array, &conv2d_49_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_48_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 1, 1), AI_STRIDE_INIT(4, 4, 4, 1152, 1152),
  1, &conv2d_48_bias_array, &conv2d_48_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_48_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 48, 1, 1, 288), AI_STRIDE_INIT(4, 1, 48, 48, 48),
  1, &conv2d_48_weights_array, &conv2d_48_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_46_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 1, 1), AI_STRIDE_INIT(4, 4, 4, 192, 192),
  1, &conv2d_46_bias_array, &conv2d_46_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_46_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 288, 1, 1, 48), AI_STRIDE_INIT(4, 1, 288, 288, 288),
  1, &conv2d_46_weights_array, &conv2d_46_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_45_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 1, 1), AI_STRIDE_INIT(4, 4, 4, 1152, 1152),
  1, &conv2d_45_bias_array, &conv2d_45_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_45_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 288, 3, 3, 1), AI_STRIDE_INIT(4, 1, 288, 864, 2592),
  1, &conv2d_45_weights_array, &conv2d_45_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_44_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 1, 1), AI_STRIDE_INIT(4, 4, 4, 1152, 1152),
  1, &conv2d_44_bias_array, &conv2d_44_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_44_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 48, 1, 1, 288), AI_STRIDE_INIT(4, 1, 48, 48, 48),
  1, &conv2d_44_weights_array, &conv2d_44_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_43_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 1, 1), AI_STRIDE_INIT(4, 4, 4, 192, 192),
  1, &conv2d_43_bias_array, &conv2d_43_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_43_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 1, 1, 48), AI_STRIDE_INIT(4, 1, 192, 192, 192),
  1, &conv2d_43_weights_array, &conv2d_43_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_42_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_42_bias_array, &conv2d_42_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_42_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 3, 3, 1), AI_STRIDE_INIT(4, 1, 192, 576, 1728),
  1, &conv2d_42_weights_array, &conv2d_42_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_41_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_41_bias_array, &conv2d_41_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_41_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 32, 1, 1, 192), AI_STRIDE_INIT(4, 1, 32, 32, 32),
  1, &conv2d_41_weights_array, &conv2d_41_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_39_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 1, 1), AI_STRIDE_INIT(4, 4, 4, 128, 128),
  1, &conv2d_39_bias_array, &conv2d_39_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_39_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 1, 1, 32), AI_STRIDE_INIT(4, 1, 192, 192, 192),
  1, &conv2d_39_weights_array, &conv2d_39_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_38_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_38_bias_array, &conv2d_38_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_38_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 3, 3, 1), AI_STRIDE_INIT(4, 1, 192, 576, 1728),
  1, &conv2d_38_weights_array, &conv2d_38_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_37_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_37_bias_array, &conv2d_37_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_37_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 32, 1, 1, 192), AI_STRIDE_INIT(4, 1, 32, 32, 32),
  1, &conv2d_37_weights_array, &conv2d_37_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_35_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 1, 1), AI_STRIDE_INIT(4, 4, 4, 128, 128),
  1, &conv2d_35_bias_array, &conv2d_35_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_35_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 1, 1, 32), AI_STRIDE_INIT(4, 1, 192, 192, 192),
  1, &conv2d_35_weights_array, &conv2d_35_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_34_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_34_bias_array, &conv2d_34_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_34_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 3, 3, 1), AI_STRIDE_INIT(4, 1, 192, 576, 1728),
  1, &conv2d_34_weights_array, &conv2d_34_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_33_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_33_bias_array, &conv2d_33_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_33_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 32, 1, 1, 192), AI_STRIDE_INIT(4, 1, 32, 32, 32),
  1, &conv2d_33_weights_array, &conv2d_33_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_31_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 1, 1), AI_STRIDE_INIT(4, 4, 4, 128, 128),
  1, &conv2d_31_bias_array, &conv2d_31_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_31_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 1, 1, 32), AI_STRIDE_INIT(4, 1, 192, 192, 192),
  1, &conv2d_31_weights_array, &conv2d_31_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_30_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_30_bias_array, &conv2d_30_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_30_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 192, 3, 3, 1), AI_STRIDE_INIT(4, 1, 192, 576, 1728),
  1, &conv2d_30_weights_array, &conv2d_30_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_29_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 1, 1), AI_STRIDE_INIT(4, 4, 4, 768, 768),
  1, &conv2d_29_bias_array, &conv2d_29_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_29_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 32, 1, 1, 192), AI_STRIDE_INIT(4, 1, 32, 32, 32),
  1, &conv2d_29_weights_array, &conv2d_29_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_28_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 1, 1), AI_STRIDE_INIT(4, 4, 4, 128, 128),
  1, &conv2d_28_bias_array, &conv2d_28_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_28_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 1, 1, 32), AI_STRIDE_INIT(4, 1, 96, 96, 96),
  1, &conv2d_28_weights_array, &conv2d_28_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_27_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_27_bias_array, &conv2d_27_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_27_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 3, 3, 1), AI_STRIDE_INIT(4, 1, 96, 288, 864),
  1, &conv2d_27_weights_array, &conv2d_27_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_25_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_25_bias_array, &conv2d_25_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_25_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 1, 1, 96), AI_STRIDE_INIT(4, 1, 16, 16, 16),
  1, &conv2d_25_weights_array, &conv2d_25_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_23_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_23_bias_array, &conv2d_23_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_23_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 1, 1, 16), AI_STRIDE_INIT(4, 1, 96, 96, 96),
  1, &conv2d_23_weights_array, &conv2d_23_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_22_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_22_bias_array, &conv2d_22_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_22_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 3, 3, 1), AI_STRIDE_INIT(4, 1, 96, 288, 864),
  1, &conv2d_22_weights_array, &conv2d_22_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_21_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_21_bias_array, &conv2d_21_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_21_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 1, 1, 96), AI_STRIDE_INIT(4, 1, 16, 16, 16),
  1, &conv2d_21_weights_array, &conv2d_21_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_19_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_19_bias_array, &conv2d_19_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_19_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 1, 1, 16), AI_STRIDE_INIT(4, 1, 96, 96, 96),
  1, &conv2d_19_weights_array, &conv2d_19_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_18_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_18_bias_array, &conv2d_18_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_18_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 3, 3, 1), AI_STRIDE_INIT(4, 1, 96, 288, 864),
  1, &conv2d_18_weights_array, &conv2d_18_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_17_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_17_bias_array, &conv2d_17_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_17_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 1, 1, 96), AI_STRIDE_INIT(4, 1, 16, 16, 16),
  1, &conv2d_17_weights_array, &conv2d_17_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_16_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_16_bias_array, &conv2d_16_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_16_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 1, 1, 16), AI_STRIDE_INIT(4, 1, 96, 96, 96),
  1, &conv2d_16_weights_array, &conv2d_16_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_15_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_15_bias_array, &conv2d_15_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_15_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 3, 3, 1), AI_STRIDE_INIT(4, 1, 96, 288, 864),
  1, &conv2d_15_weights_array, &conv2d_15_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_13_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_13_bias_array, &conv2d_13_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_13_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 1, 1, 96), AI_STRIDE_INIT(4, 1, 16, 16, 16),
  1, &conv2d_13_weights_array, &conv2d_13_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_11_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_11_bias_array, &conv2d_11_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_11_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 1, 1, 16), AI_STRIDE_INIT(4, 1, 96, 96, 96),
  1, &conv2d_11_weights_array, &conv2d_11_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_10_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_10_bias_array, &conv2d_10_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_10_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 96, 3, 3, 1), AI_STRIDE_INIT(4, 1, 96, 288, 864),
  1, &conv2d_10_weights_array, &conv2d_10_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_9_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 1, 1), AI_STRIDE_INIT(4, 4, 4, 384, 384),
  1, &conv2d_9_bias_array, &conv2d_9_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_9_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 1, 1, 96), AI_STRIDE_INIT(4, 1, 16, 16, 16),
  1, &conv2d_9_weights_array, &conv2d_9_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_8_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_8_bias_array, &conv2d_8_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_8_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 48, 1, 1, 16), AI_STRIDE_INIT(4, 1, 48, 48, 48),
  1, &conv2d_8_weights_array, &conv2d_8_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 1, 1), AI_STRIDE_INIT(4, 4, 4, 192, 192),
  1, &conv2d_7_bias_array, &conv2d_7_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 48, 3, 3, 1), AI_STRIDE_INIT(4, 1, 48, 144, 432),
  1, &conv2d_7_weights_array, &conv2d_7_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 1, 1), AI_STRIDE_INIT(4, 4, 4, 192, 192),
  1, &conv2d_5_bias_array, &conv2d_5_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 8, 1, 1, 48), AI_STRIDE_INIT(4, 1, 8, 8, 8),
  1, &conv2d_5_weights_array, &conv2d_5_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 8, 1, 1), AI_STRIDE_INIT(4, 4, 4, 32, 32),
  1, &conv2d_4_bias_array, &conv2d_4_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 1, 1, 8), AI_STRIDE_INIT(4, 1, 16, 16, 16),
  1, &conv2d_4_weights_array, &conv2d_4_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_3_bias_array, &conv2d_3_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 16, 3, 3, 1), AI_STRIDE_INIT(4, 1, 16, 48, 144),
  1, &conv2d_3_weights_array, &conv2d_3_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_bias, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 1, 1), AI_STRIDE_INIT(4, 4, 4, 64, 64),
  1, &conv2d_2_bias_array, &conv2d_2_bias_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_weights, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 3, 3, 3, 16), AI_STRIDE_INIT(4, 1, 3, 9, 27),
  1, &conv2d_2_weights_array, &conv2d_2_weights_intq)
AI_TENSOR_OBJ_DECLARE(
  input_0_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3, 64, 64), AI_STRIDE_INIT(4, 4, 4, 12, 768),
  1, &input_0_output_array, NULL)
AI_TENSOR_OBJ_DECLARE(
  conversion_0_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 3, 64, 64), AI_STRIDE_INIT(4, 1, 1, 3, 192),
  1, &conversion_0_output_array, &conversion_0_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_2_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 32, 32), AI_STRIDE_INIT(4, 1, 1, 16, 512),
  1, &conv2d_2_output_array, &conv2d_2_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_3_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 32, 32), AI_STRIDE_INIT(4, 1, 1, 16, 512),
  1, &conv2d_3_output_array, &conv2d_3_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 8, 32, 32), AI_STRIDE_INIT(4, 1, 1, 8, 256),
  1, &conv2d_4_output_array, &conv2d_4_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_5_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 32, 32), AI_STRIDE_INIT(4, 1, 1, 48, 1536),
  1, &conv2d_5_output_array, &conv2d_5_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 16, 16), AI_STRIDE_INIT(4, 1, 1, 48, 768),
  1, &conv2d_7_output_array, &conv2d_7_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_8_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 16, 16), AI_STRIDE_INIT(4, 1, 1, 16, 256),
  1, &conv2d_8_output_array, &conv2d_8_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_9_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 16, 16), AI_STRIDE_INIT(4, 1, 1, 96, 1536),
  1, &conv2d_9_output_array, &conv2d_9_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_10_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 16, 16), AI_STRIDE_INIT(4, 1, 1, 96, 1536),
  1, &conv2d_10_output_array, &conv2d_10_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_11_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 16, 16), AI_STRIDE_INIT(4, 1, 1, 16, 256),
  1, &conv2d_11_output_array, &conv2d_11_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_12_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 16, 16), AI_STRIDE_INIT(4, 1, 1, 16, 256),
  1, &eltwise_12_output_array, &eltwise_12_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_13_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 16, 16), AI_STRIDE_INIT(4, 1, 1, 96, 1536),
  1, &conv2d_13_output_array, &conv2d_13_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_15_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_15_output_array, &conv2d_15_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_16_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 8, 8), AI_STRIDE_INIT(4, 1, 1, 16, 128),
  1, &conv2d_16_output_array, &conv2d_16_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_17_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_17_output_array, &conv2d_17_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_18_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_18_output_array, &conv2d_18_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_19_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 8, 8), AI_STRIDE_INIT(4, 1, 1, 16, 128),
  1, &conv2d_19_output_array, &conv2d_19_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_20_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 8, 8), AI_STRIDE_INIT(4, 1, 1, 16, 128),
  1, &eltwise_20_output_array, &eltwise_20_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_21_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_21_output_array, &conv2d_21_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_22_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_22_output_array, &conv2d_22_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_23_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 8, 8), AI_STRIDE_INIT(4, 1, 1, 16, 128),
  1, &conv2d_23_output_array, &conv2d_23_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_24_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 16, 8, 8), AI_STRIDE_INIT(4, 1, 1, 16, 128),
  1, &eltwise_24_output_array, &eltwise_24_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_25_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 8, 8), AI_STRIDE_INIT(4, 1, 1, 96, 768),
  1, &conv2d_25_output_array, &conv2d_25_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_27_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 96, 4, 4), AI_STRIDE_INIT(4, 1, 1, 96, 384),
  1, &conv2d_27_output_array, &conv2d_27_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_28_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &conv2d_28_output_array, &conv2d_28_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_29_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_29_output_array, &conv2d_29_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_30_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_30_output_array, &conv2d_30_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_31_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &conv2d_31_output_array, &conv2d_31_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_32_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &eltwise_32_output_array, &eltwise_32_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_33_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_33_output_array, &conv2d_33_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_34_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_34_output_array, &conv2d_34_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_35_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &conv2d_35_output_array, &conv2d_35_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_36_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &eltwise_36_output_array, &eltwise_36_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_37_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_37_output_array, &conv2d_37_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_38_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_38_output_array, &conv2d_38_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_39_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &conv2d_39_output_array, &conv2d_39_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_40_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 32, 4, 4), AI_STRIDE_INIT(4, 1, 1, 32, 128),
  1, &eltwise_40_output_array, &eltwise_40_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_41_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_41_output_array, &conv2d_41_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_42_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 192, 4, 4), AI_STRIDE_INIT(4, 1, 1, 192, 768),
  1, &conv2d_42_output_array, &conv2d_42_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_43_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 4, 4), AI_STRIDE_INIT(4, 1, 1, 48, 192),
  1, &conv2d_43_output_array, &conv2d_43_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_44_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_44_output_array, &conv2d_44_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_45_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_45_output_array, &conv2d_45_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_46_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 4, 4), AI_STRIDE_INIT(4, 1, 1, 48, 192),
  1, &conv2d_46_output_array, &conv2d_46_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_47_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 4, 4), AI_STRIDE_INIT(4, 1, 1, 48, 192),
  1, &eltwise_47_output_array, &eltwise_47_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_48_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_48_output_array, &conv2d_48_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_49_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_49_output_array, &conv2d_49_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_50_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 4, 4), AI_STRIDE_INIT(4, 1, 1, 48, 192),
  1, &conv2d_50_output_array, &conv2d_50_output_intq)
AI_TENSOR_OBJ_DECLARE(
  eltwise_51_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 48, 4, 4), AI_STRIDE_INIT(4, 1, 1, 48, 192),
  1, &eltwise_51_output_array, &eltwise_51_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_52_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 288, 4, 4), AI_STRIDE_INIT(4, 1, 1, 288, 1152),
  1, &conv2d_52_output_array, &conv2d_52_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conv2d_53_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 35, 4, 4), AI_STRIDE_INIT(4, 1, 1, 35, 140),
  1, &conv2d_53_output_array, &conv2d_53_output_intq)
AI_TENSOR_OBJ_DECLARE(
  conversion_55_output, AI_STATIC,
  0x0, 0x0, AI_SHAPE_INIT(4, 1, 35, 4, 4), AI_STRIDE_INIT(4, 4, 4, 140, 560),
  1, &conversion_55_output_array, NULL)


/**  Layer declarations section  **********************************************/


AI_TENSOR_CHAIN_OBJ_DECLARE(
  conversion_0_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&input_0_output),
  AI_TENSOR_LIST_ENTRY(&conversion_0_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conversion_0_layer, 0,
  NL_TYPE,
  nl, node_convert,
  &AI_NET_OBJ_INSTANCE, &conv2d_2_layer, AI_STATIC,
  .tensors = &conversion_0_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_2_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_2_nl_params, AI_ARRAY_FORMAT_S8, conv2d_2_nl_params_data,
    conv2d_2_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_2_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conversion_0_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_2_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_2_weights, &conv2d_2_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_2_scratch0, &conv2d_2_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_2_layer, 2,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_3_layer, AI_STATIC,
  .tensors = &conv2d_2_chain, 
  .groups = 1, 
  .nl_params = &conv2d_2_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(2, 2), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 1, 1), 
)


AI_STATIC_CONST ai_i8 conv2d_3_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_3_nl_params, AI_ARRAY_FORMAT_S8, conv2d_3_nl_params_data,
    conv2d_3_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_3_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_2_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_3_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_3_weights, &conv2d_3_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_3_scratch0, &conv2d_3_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_3_layer, 3,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_4_layer, AI_STATIC,
  .tensors = &conv2d_3_chain, 
  .groups = 16, 
  .nl_params = &conv2d_3_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_4_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_3_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_4_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_4_weights, &conv2d_4_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_4_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_4_layer, 4,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_5_layer, AI_STATIC,
  .tensors = &conv2d_4_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_5_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_5_nl_params, AI_ARRAY_FORMAT_S8, conv2d_5_nl_params_data,
    conv2d_5_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_5_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_4_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_5_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_5_weights, &conv2d_5_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_5_scratch0, &conv2d_5_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_5_layer, 5,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_7_layer, AI_STATIC,
  .tensors = &conv2d_5_chain, 
  .groups = 1, 
  .nl_params = &conv2d_5_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_7_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_7_nl_params, AI_ARRAY_FORMAT_S8, conv2d_7_nl_params_data,
    conv2d_7_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_7_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_5_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_7_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_7_weights, &conv2d_7_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_7_scratch0, &conv2d_7_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_7_layer, 7,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_8_layer, AI_STATIC,
  .tensors = &conv2d_7_chain, 
  .groups = 48, 
  .nl_params = &conv2d_7_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(2, 2), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_8_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_7_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_8_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_8_weights, &conv2d_8_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_8_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_8_layer, 8,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_9_layer, AI_STATIC,
  .tensors = &conv2d_8_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_9_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_9_nl_params, AI_ARRAY_FORMAT_S8, conv2d_9_nl_params_data,
    conv2d_9_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_9_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_8_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_9_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_9_weights, &conv2d_9_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_9_scratch0, &conv2d_9_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_9_layer, 9,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_10_layer, AI_STATIC,
  .tensors = &conv2d_9_chain, 
  .groups = 1, 
  .nl_params = &conv2d_9_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_10_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_10_nl_params, AI_ARRAY_FORMAT_S8, conv2d_10_nl_params_data,
    conv2d_10_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_10_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_9_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_10_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_10_weights, &conv2d_10_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_10_scratch0, &conv2d_10_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_10_layer, 10,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_11_layer, AI_STATIC,
  .tensors = &conv2d_10_chain, 
  .groups = 96, 
  .nl_params = &conv2d_10_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_11_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_10_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_11_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_11_weights, &conv2d_11_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_11_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_11_layer, 11,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_12_layer, AI_STATIC,
  .tensors = &conv2d_11_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_12_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_8_output, &conv2d_11_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_12_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_12_layer, 12,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_13_layer, AI_STATIC,
  .tensors = &eltwise_12_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_13_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_13_nl_params, AI_ARRAY_FORMAT_S8, conv2d_13_nl_params_data,
    conv2d_13_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_13_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_12_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_13_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_13_weights, &conv2d_13_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_13_scratch0, &conv2d_13_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_13_layer, 13,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_15_layer, AI_STATIC,
  .tensors = &conv2d_13_chain, 
  .groups = 1, 
  .nl_params = &conv2d_13_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_15_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_15_nl_params, AI_ARRAY_FORMAT_S8, conv2d_15_nl_params_data,
    conv2d_15_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_15_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_13_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_15_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_15_weights, &conv2d_15_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_15_scratch0, &conv2d_15_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_15_layer, 15,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_16_layer, AI_STATIC,
  .tensors = &conv2d_15_chain, 
  .groups = 96, 
  .nl_params = &conv2d_15_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(2, 2), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_16_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_15_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_16_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_16_weights, &conv2d_16_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_16_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_16_layer, 16,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_17_layer, AI_STATIC,
  .tensors = &conv2d_16_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_17_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_17_nl_params, AI_ARRAY_FORMAT_S8, conv2d_17_nl_params_data,
    conv2d_17_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_17_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_16_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_17_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_17_weights, &conv2d_17_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_17_scratch0, &conv2d_17_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_17_layer, 17,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_18_layer, AI_STATIC,
  .tensors = &conv2d_17_chain, 
  .groups = 1, 
  .nl_params = &conv2d_17_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_18_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_18_nl_params, AI_ARRAY_FORMAT_S8, conv2d_18_nl_params_data,
    conv2d_18_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_18_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_17_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_18_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_18_weights, &conv2d_18_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_18_scratch0, &conv2d_18_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_18_layer, 18,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_19_layer, AI_STATIC,
  .tensors = &conv2d_18_chain, 
  .groups = 96, 
  .nl_params = &conv2d_18_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_19_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_18_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_19_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_19_weights, &conv2d_19_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_19_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_19_layer, 19,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_20_layer, AI_STATIC,
  .tensors = &conv2d_19_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_20_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_16_output, &conv2d_19_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_20_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_20_layer, 20,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_21_layer, AI_STATIC,
  .tensors = &eltwise_20_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_21_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_21_nl_params, AI_ARRAY_FORMAT_S8, conv2d_21_nl_params_data,
    conv2d_21_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_21_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_20_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_21_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_21_weights, &conv2d_21_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_21_scratch0, &conv2d_21_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_21_layer, 21,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_22_layer, AI_STATIC,
  .tensors = &conv2d_21_chain, 
  .groups = 1, 
  .nl_params = &conv2d_21_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_22_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_22_nl_params, AI_ARRAY_FORMAT_S8, conv2d_22_nl_params_data,
    conv2d_22_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_22_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_21_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_22_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_22_weights, &conv2d_22_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_22_scratch0, &conv2d_22_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_22_layer, 22,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_23_layer, AI_STATIC,
  .tensors = &conv2d_22_chain, 
  .groups = 96, 
  .nl_params = &conv2d_22_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_23_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_22_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_23_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_23_weights, &conv2d_23_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_23_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_23_layer, 23,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_24_layer, AI_STATIC,
  .tensors = &conv2d_23_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_24_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_20_output, &conv2d_23_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_24_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_24_layer, 24,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_25_layer, AI_STATIC,
  .tensors = &eltwise_24_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_25_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_25_nl_params, AI_ARRAY_FORMAT_S8, conv2d_25_nl_params_data,
    conv2d_25_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_25_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_24_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_25_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_25_weights, &conv2d_25_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_25_scratch0, &conv2d_25_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_25_layer, 25,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_27_layer, AI_STATIC,
  .tensors = &conv2d_25_chain, 
  .groups = 1, 
  .nl_params = &conv2d_25_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_27_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_27_nl_params, AI_ARRAY_FORMAT_S8, conv2d_27_nl_params_data,
    conv2d_27_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_27_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_25_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_27_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_27_weights, &conv2d_27_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_27_scratch0, &conv2d_27_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_27_layer, 27,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_28_layer, AI_STATIC,
  .tensors = &conv2d_27_chain, 
  .groups = 96, 
  .nl_params = &conv2d_27_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(2, 2), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_28_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_27_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_28_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_28_weights, &conv2d_28_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_28_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_28_layer, 28,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_29_layer, AI_STATIC,
  .tensors = &conv2d_28_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_29_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_29_nl_params, AI_ARRAY_FORMAT_S8, conv2d_29_nl_params_data,
    conv2d_29_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_29_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_28_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_29_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_29_weights, &conv2d_29_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_29_scratch0, &conv2d_29_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_29_layer, 29,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_30_layer, AI_STATIC,
  .tensors = &conv2d_29_chain, 
  .groups = 1, 
  .nl_params = &conv2d_29_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_30_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_30_nl_params, AI_ARRAY_FORMAT_S8, conv2d_30_nl_params_data,
    conv2d_30_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_30_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_29_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_30_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_30_weights, &conv2d_30_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_30_scratch0, &conv2d_30_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_30_layer, 30,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_31_layer, AI_STATIC,
  .tensors = &conv2d_30_chain, 
  .groups = 192, 
  .nl_params = &conv2d_30_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_31_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_30_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_31_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_31_weights, &conv2d_31_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_31_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_31_layer, 31,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_32_layer, AI_STATIC,
  .tensors = &conv2d_31_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_32_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_28_output, &conv2d_31_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_32_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_32_layer, 32,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_33_layer, AI_STATIC,
  .tensors = &eltwise_32_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_33_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_33_nl_params, AI_ARRAY_FORMAT_S8, conv2d_33_nl_params_data,
    conv2d_33_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_33_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_32_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_33_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_33_weights, &conv2d_33_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_33_scratch0, &conv2d_33_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_33_layer, 33,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_34_layer, AI_STATIC,
  .tensors = &conv2d_33_chain, 
  .groups = 1, 
  .nl_params = &conv2d_33_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_34_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_34_nl_params, AI_ARRAY_FORMAT_S8, conv2d_34_nl_params_data,
    conv2d_34_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_34_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_33_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_34_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_34_weights, &conv2d_34_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_34_scratch0, &conv2d_34_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_34_layer, 34,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_35_layer, AI_STATIC,
  .tensors = &conv2d_34_chain, 
  .groups = 192, 
  .nl_params = &conv2d_34_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_35_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_34_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_35_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_35_weights, &conv2d_35_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_35_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_35_layer, 35,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_36_layer, AI_STATIC,
  .tensors = &conv2d_35_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_36_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_32_output, &conv2d_35_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_36_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_36_layer, 36,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_37_layer, AI_STATIC,
  .tensors = &eltwise_36_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_37_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_37_nl_params, AI_ARRAY_FORMAT_S8, conv2d_37_nl_params_data,
    conv2d_37_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_37_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_36_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_37_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_37_weights, &conv2d_37_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_37_scratch0, &conv2d_37_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_37_layer, 37,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_38_layer, AI_STATIC,
  .tensors = &conv2d_37_chain, 
  .groups = 1, 
  .nl_params = &conv2d_37_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_38_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_38_nl_params, AI_ARRAY_FORMAT_S8, conv2d_38_nl_params_data,
    conv2d_38_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_38_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_37_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_38_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_38_weights, &conv2d_38_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_38_scratch0, &conv2d_38_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_38_layer, 38,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_39_layer, AI_STATIC,
  .tensors = &conv2d_38_chain, 
  .groups = 192, 
  .nl_params = &conv2d_38_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_39_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_38_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_39_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_39_weights, &conv2d_39_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_39_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_39_layer, 39,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_40_layer, AI_STATIC,
  .tensors = &conv2d_39_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_40_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_36_output, &conv2d_39_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_40_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_40_layer, 40,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_41_layer, AI_STATIC,
  .tensors = &eltwise_40_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_41_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_41_nl_params, AI_ARRAY_FORMAT_S8, conv2d_41_nl_params_data,
    conv2d_41_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_41_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_40_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_41_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_41_weights, &conv2d_41_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_41_scratch0, &conv2d_41_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_41_layer, 41,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_42_layer, AI_STATIC,
  .tensors = &conv2d_41_chain, 
  .groups = 1, 
  .nl_params = &conv2d_41_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_42_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_42_nl_params, AI_ARRAY_FORMAT_S8, conv2d_42_nl_params_data,
    conv2d_42_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_42_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_41_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_42_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_42_weights, &conv2d_42_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_42_scratch0, &conv2d_42_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_42_layer, 42,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_43_layer, AI_STATIC,
  .tensors = &conv2d_42_chain, 
  .groups = 192, 
  .nl_params = &conv2d_42_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_43_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_42_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_43_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_43_weights, &conv2d_43_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_43_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_43_layer, 43,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_44_layer, AI_STATIC,
  .tensors = &conv2d_43_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_44_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_44_nl_params, AI_ARRAY_FORMAT_S8, conv2d_44_nl_params_data,
    conv2d_44_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_44_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_43_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_44_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_44_weights, &conv2d_44_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_44_scratch0, &conv2d_44_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_44_layer, 44,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_45_layer, AI_STATIC,
  .tensors = &conv2d_44_chain, 
  .groups = 1, 
  .nl_params = &conv2d_44_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_45_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_45_nl_params, AI_ARRAY_FORMAT_S8, conv2d_45_nl_params_data,
    conv2d_45_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_45_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_44_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_45_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_45_weights, &conv2d_45_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_45_scratch0, &conv2d_45_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_45_layer, 45,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_46_layer, AI_STATIC,
  .tensors = &conv2d_45_chain, 
  .groups = 288, 
  .nl_params = &conv2d_45_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_46_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_45_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_46_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_46_weights, &conv2d_46_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_46_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_46_layer, 46,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_47_layer, AI_STATIC,
  .tensors = &conv2d_46_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_47_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_43_output, &conv2d_46_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_47_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_47_layer, 47,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_48_layer, AI_STATIC,
  .tensors = &eltwise_47_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_48_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_48_nl_params, AI_ARRAY_FORMAT_S8, conv2d_48_nl_params_data,
    conv2d_48_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_48_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_47_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_48_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_48_weights, &conv2d_48_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_48_scratch0, &conv2d_48_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_48_layer, 48,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_49_layer, AI_STATIC,
  .tensors = &conv2d_48_chain, 
  .groups = 1, 
  .nl_params = &conv2d_48_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)


AI_STATIC_CONST ai_i8 conv2d_49_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_49_nl_params, AI_ARRAY_FORMAT_S8, conv2d_49_nl_params_data,
    conv2d_49_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_49_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_48_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_49_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_49_weights, &conv2d_49_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_49_scratch0, &conv2d_49_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_49_layer, 49,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_50_layer, AI_STATIC,
  .tensors = &conv2d_49_chain, 
  .groups = 288, 
  .nl_params = &conv2d_49_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 1, 1, 1, 1), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_50_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_49_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_50_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_50_weights, &conv2d_50_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_50_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_50_layer, 50,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &eltwise_51_layer, AI_STATIC,
  .tensors = &conv2d_50_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  eltwise_51_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_47_output, &conv2d_50_output),
  AI_TENSOR_LIST_ENTRY(&eltwise_51_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  eltwise_51_layer, 51,
  ELTWISE_TYPE,
  eltwise, forward_add_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_52_layer, AI_STATIC,
  .tensors = &eltwise_51_chain, 
)


AI_STATIC_CONST ai_i8 conv2d_52_nl_params_data[] = { -128, 127 };
AI_ARRAY_OBJ_DECLARE(
    conv2d_52_nl_params, AI_ARRAY_FORMAT_S8, conv2d_52_nl_params_data,
    conv2d_52_nl_params_data, 2, AI_STATIC_CONST)
AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_52_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&eltwise_51_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_52_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_52_weights, &conv2d_52_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_52_scratch0, &conv2d_52_scratch1)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_52_layer, 52,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conv2d_53_layer, AI_STATIC,
  .tensors = &conv2d_52_chain, 
  .groups = 1, 
  .nl_params = &conv2d_52_nl_params, 
  .nl_func = nl_func_clip_array_integer, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_53_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_52_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_53_output),
  AI_TENSOR_LIST_ENTRY(&conv2d_53_weights, &conv2d_53_bias, NULL),
  AI_TENSOR_LIST_ENTRY(&conv2d_53_scratch0)
)

AI_LAYER_OBJ_DECLARE(
  conv2d_53_layer, 53,
  CONV2D_TYPE,
  conv2d, forward_conv2d_integer,
  &AI_NET_OBJ_INSTANCE, &conversion_55_layer, AI_STATIC,
  .tensors = &conv2d_53_chain, 
  .groups = 1, 
  .nl_func = NULL, 
  .filter_stride = AI_SHAPE_2D_INIT(1, 1), 
  .dilation = AI_SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conversion_55_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_ENTRY(&conv2d_53_output),
  AI_TENSOR_LIST_ENTRY(&conversion_55_output),
  AI_TENSOR_LIST_EMPTY,
  AI_TENSOR_LIST_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  conversion_55_layer, 55,
  NL_TYPE,
  nl, node_convert,
  &AI_NET_OBJ_INSTANCE, &conversion_55_layer, AI_STATIC,
  .tensors = &conversion_55_chain, 
)


AI_NETWORK_OBJ_DECLARE(
  AI_NET_OBJ_INSTANCE, AI_STATIC,
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 184860, 1,
                     NULL),
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 79200, 1,
                     NULL),
  AI_TENSOR_LIST_IO_ENTRY(AI_FLAG_NONE, AI_NETWORK_IN_NUM, &input_0_output),
  AI_TENSOR_LIST_IO_ENTRY(AI_FLAG_NONE, AI_NETWORK_OUT_NUM, &conversion_55_output),
  &conversion_0_layer, 0, NULL)



AI_DECLARE_STATIC
ai_bool network_configure_activations(
  ai_network* net_ctx, const ai_buffer* activation_buffer)
{
  AI_ASSERT(net_ctx &&  activation_buffer && activation_buffer->data)

  ai_ptr activations = AI_PTR(AI_PTR_ALIGN(activation_buffer->data, 4));
  AI_ASSERT(activations)
  AI_UNUSED(net_ctx)

  {
    /* Updating activations (byte) offsets */
    conv2d_53_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_53_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_52_scratch1_array.data = AI_PTR(activations + 4608);
    conv2d_52_scratch1_array.data_start = AI_PTR(activations + 4608);
    conv2d_52_scratch0_array.data = AI_PTR(activations + 1536);
    conv2d_52_scratch0_array.data_start = AI_PTR(activations + 1536);
    conv2d_50_scratch0_array.data = AI_PTR(activations + 5376);
    conv2d_50_scratch0_array.data_start = AI_PTR(activations + 5376);
    conv2d_49_scratch1_array.data = AI_PTR(activations + 768);
    conv2d_49_scratch1_array.data_start = AI_PTR(activations + 768);
    conv2d_49_scratch0_array.data = AI_PTR(activations + 13056);
    conv2d_49_scratch0_array.data_start = AI_PTR(activations + 13056);
    conv2d_48_scratch1_array.data = AI_PTR(activations + 3840);
    conv2d_48_scratch1_array.data_start = AI_PTR(activations + 3840);
    conv2d_48_scratch0_array.data = AI_PTR(activations + 768);
    conv2d_48_scratch0_array.data_start = AI_PTR(activations + 768);
    conv2d_46_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_46_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_45_scratch1_array.data = AI_PTR(activations + 5088);
    conv2d_45_scratch1_array.data_start = AI_PTR(activations + 5088);
    conv2d_45_scratch0_array.data = AI_PTR(activations + 14304);
    conv2d_45_scratch0_array.data_start = AI_PTR(activations + 14304);
    conv2d_44_scratch1_array.data = AI_PTR(activations + 5088);
    conv2d_44_scratch1_array.data_start = AI_PTR(activations + 5088);
    conv2d_44_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_44_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_43_scratch0_array.data = AI_PTR(activations + 3072);
    conv2d_43_scratch0_array.data_start = AI_PTR(activations + 3072);
    conv2d_42_scratch1_array.data = AI_PTR(activations + 0);
    conv2d_42_scratch1_array.data_start = AI_PTR(activations + 0);
    conv2d_42_scratch0_array.data = AI_PTR(activations + 6144);
    conv2d_42_scratch0_array.data_start = AI_PTR(activations + 6144);
    conv2d_41_scratch1_array.data = AI_PTR(activations + 3072);
    conv2d_41_scratch1_array.data_start = AI_PTR(activations + 3072);
    conv2d_41_scratch0_array.data = AI_PTR(activations + 1024);
    conv2d_41_scratch0_array.data_start = AI_PTR(activations + 1024);
    conv2d_39_scratch0_array.data = AI_PTR(activations + 3584);
    conv2d_39_scratch0_array.data_start = AI_PTR(activations + 3584);
    conv2d_38_scratch1_array.data = AI_PTR(activations + 512);
    conv2d_38_scratch1_array.data_start = AI_PTR(activations + 512);
    conv2d_38_scratch0_array.data = AI_PTR(activations + 8704);
    conv2d_38_scratch0_array.data_start = AI_PTR(activations + 8704);
    conv2d_37_scratch1_array.data = AI_PTR(activations + 2560);
    conv2d_37_scratch1_array.data_start = AI_PTR(activations + 2560);
    conv2d_37_scratch0_array.data = AI_PTR(activations + 512);
    conv2d_37_scratch0_array.data_start = AI_PTR(activations + 512);
    conv2d_35_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_35_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_34_scratch1_array.data = AI_PTR(activations + 2112);
    conv2d_34_scratch1_array.data_start = AI_PTR(activations + 2112);
    conv2d_34_scratch0_array.data = AI_PTR(activations + 10304);
    conv2d_34_scratch0_array.data_start = AI_PTR(activations + 10304);
    conv2d_33_scratch1_array.data = AI_PTR(activations + 4160);
    conv2d_33_scratch1_array.data_start = AI_PTR(activations + 4160);
    conv2d_33_scratch0_array.data = AI_PTR(activations + 2112);
    conv2d_33_scratch0_array.data_start = AI_PTR(activations + 2112);
    conv2d_31_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_31_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_30_scratch1_array.data = AI_PTR(activations + 2752);
    conv2d_30_scratch1_array.data_start = AI_PTR(activations + 2752);
    conv2d_30_scratch0_array.data = AI_PTR(activations + 8896);
    conv2d_30_scratch0_array.data_start = AI_PTR(activations + 8896);
    conv2d_29_scratch1_array.data = AI_PTR(activations + 2752);
    conv2d_29_scratch1_array.data_start = AI_PTR(activations + 2752);
    conv2d_29_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_29_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_28_scratch0_array.data = AI_PTR(activations + 1536);
    conv2d_28_scratch0_array.data_start = AI_PTR(activations + 1536);
    conv2d_27_scratch1_array.data = AI_PTR(activations + 0);
    conv2d_27_scratch1_array.data_start = AI_PTR(activations + 0);
    conv2d_27_scratch0_array.data = AI_PTR(activations + 8736);
    conv2d_27_scratch0_array.data_start = AI_PTR(activations + 8736);
    conv2d_25_scratch1_array.data = AI_PTR(activations + 2592);
    conv2d_25_scratch1_array.data_start = AI_PTR(activations + 2592);
    conv2d_25_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_25_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_23_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_23_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_22_scratch1_array.data = AI_PTR(activations + 15904);
    conv2d_22_scratch1_array.data_start = AI_PTR(activations + 15904);
    conv2d_22_scratch0_array.data = AI_PTR(activations + 3616);
    conv2d_22_scratch0_array.data_start = AI_PTR(activations + 3616);
    conv2d_21_scratch1_array.data = AI_PTR(activations + 3616);
    conv2d_21_scratch1_array.data_start = AI_PTR(activations + 3616);
    conv2d_21_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_21_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_19_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_19_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_18_scratch1_array.data = AI_PTR(activations + 14880);
    conv2d_18_scratch1_array.data_start = AI_PTR(activations + 14880);
    conv2d_18_scratch0_array.data = AI_PTR(activations + 1568);
    conv2d_18_scratch0_array.data_start = AI_PTR(activations + 1568);
    conv2d_17_scratch1_array.data = AI_PTR(activations + 2592);
    conv2d_17_scratch1_array.data_start = AI_PTR(activations + 2592);
    conv2d_17_scratch0_array.data = AI_PTR(activations + 1568);
    conv2d_17_scratch0_array.data_start = AI_PTR(activations + 1568);
    conv2d_16_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_16_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_15_scratch1_array.data = AI_PTR(activations + 3556);
    conv2d_15_scratch1_array.data_start = AI_PTR(activations + 3556);
    conv2d_15_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_15_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_13_scratch1_array.data = AI_PTR(activations + 13184);
    conv2d_13_scratch1_array.data_start = AI_PTR(activations + 13184);
    conv2d_13_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_13_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_11_scratch0_array.data = AI_PTR(activations + 4448);
    conv2d_11_scratch0_array.data_start = AI_PTR(activations + 4448);
    conv2d_10_scratch1_array.data = AI_PTR(activations + 54624);
    conv2d_10_scratch1_array.data_start = AI_PTR(activations + 54624);
    conv2d_10_scratch0_array.data = AI_PTR(activations + 4448);
    conv2d_10_scratch0_array.data_start = AI_PTR(activations + 4448);
    conv2d_9_scratch1_array.data = AI_PTR(activations + 5472);
    conv2d_9_scratch1_array.data_start = AI_PTR(activations + 5472);
    conv2d_9_scratch0_array.data = AI_PTR(activations + 4448);
    conv2d_9_scratch0_array.data_start = AI_PTR(activations + 4448);
    conv2d_8_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_8_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_7_scratch1_array.data = AI_PTR(activations + 58000);
    conv2d_7_scratch1_array.data_start = AI_PTR(activations + 58000);
    conv2d_7_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_7_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_5_scratch1_array.data = AI_PTR(activations + 8848);
    conv2d_5_scratch1_array.data_start = AI_PTR(activations + 8848);
    conv2d_5_scratch0_array.data = AI_PTR(activations + 8336);
    conv2d_5_scratch0_array.data_start = AI_PTR(activations + 8336);
    conv2d_4_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_4_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_3_scratch1_array.data = AI_PTR(activations + 29004);
    conv2d_3_scratch1_array.data_start = AI_PTR(activations + 29004);
    conv2d_3_scratch0_array.data = AI_PTR(activations + 0);
    conv2d_3_scratch0_array.data_start = AI_PTR(activations + 0);
    conv2d_2_scratch1_array.data = AI_PTR(activations + 12620);
    conv2d_2_scratch1_array.data_start = AI_PTR(activations + 12620);
    conv2d_2_scratch0_array.data = AI_PTR(activations + 12288);
    conv2d_2_scratch0_array.data_start = AI_PTR(activations + 12288);
    input_0_output_array.data = AI_PTR(NULL);
    input_0_output_array.data_start = AI_PTR(NULL);
    conversion_0_output_array.data = AI_PTR(activations + 0);
    conversion_0_output_array.data_start = AI_PTR(activations + 0);
    conv2d_2_output_array.data = AI_PTR(activations + 12620);
    conv2d_2_output_array.data_start = AI_PTR(activations + 12620);
    conv2d_3_output_array.data = AI_PTR(activations + 29004);
    conv2d_3_output_array.data_start = AI_PTR(activations + 29004);
    conv2d_4_output_array.data = AI_PTR(activations + 144);
    conv2d_4_output_array.data_start = AI_PTR(activations + 144);
    conv2d_5_output_array.data = AI_PTR(activations + 8848);
    conv2d_5_output_array.data_start = AI_PTR(activations + 8848);
    conv2d_7_output_array.data = AI_PTR(activations + 58000);
    conv2d_7_output_array.data_start = AI_PTR(activations + 58000);
    conv2d_8_output_array.data = AI_PTR(activations + 352);
    conv2d_8_output_array.data_start = AI_PTR(activations + 352);
    conv2d_9_output_array.data = AI_PTR(activations + 30048);
    conv2d_9_output_array.data_start = AI_PTR(activations + 30048);
    conv2d_10_output_array.data = AI_PTR(activations + 54624);
    conv2d_10_output_array.data_start = AI_PTR(activations + 54624);
    conv2d_11_output_array.data = AI_PTR(activations + 4992);
    conv2d_11_output_array.data_start = AI_PTR(activations + 4992);
    eltwise_12_output_array.data = AI_PTR(activations + 9088);
    eltwise_12_output_array.data_start = AI_PTR(activations + 9088);
    conv2d_13_output_array.data = AI_PTR(activations + 13184);
    conv2d_13_output_array.data_start = AI_PTR(activations + 13184);
    conv2d_15_output_array.data = AI_PTR(activations + 3556);
    conv2d_15_output_array.data_start = AI_PTR(activations + 3556);
    conv2d_16_output_array.data = AI_PTR(activations + 544);
    conv2d_16_output_array.data_start = AI_PTR(activations + 544);
    conv2d_17_output_array.data = AI_PTR(activations + 8736);
    conv2d_17_output_array.data_start = AI_PTR(activations + 8736);
    conv2d_18_output_array.data = AI_PTR(activations + 14880);
    conv2d_18_output_array.data_start = AI_PTR(activations + 14880);
    conv2d_19_output_array.data = AI_PTR(activations + 1568);
    conv2d_19_output_array.data_start = AI_PTR(activations + 1568);
    eltwise_20_output_array.data = AI_PTR(activations + 2592);
    eltwise_20_output_array.data_start = AI_PTR(activations + 2592);
    conv2d_21_output_array.data = AI_PTR(activations + 9760);
    conv2d_21_output_array.data_start = AI_PTR(activations + 9760);
    conv2d_22_output_array.data = AI_PTR(activations + 15904);
    conv2d_22_output_array.data_start = AI_PTR(activations + 15904);
    conv2d_23_output_array.data = AI_PTR(activations + 544);
    conv2d_23_output_array.data_start = AI_PTR(activations + 544);
    eltwise_24_output_array.data = AI_PTR(activations + 1568);
    eltwise_24_output_array.data_start = AI_PTR(activations + 1568);
    conv2d_25_output_array.data = AI_PTR(activations + 2592);
    conv2d_25_output_array.data_start = AI_PTR(activations + 2592);
    conv2d_27_output_array.data = AI_PTR(activations + 0);
    conv2d_27_output_array.data_start = AI_PTR(activations + 0);
    conv2d_28_output_array.data = AI_PTR(activations + 2240);
    conv2d_28_output_array.data_start = AI_PTR(activations + 2240);
    conv2d_29_output_array.data = AI_PTR(activations + 5824);
    conv2d_29_output_array.data_start = AI_PTR(activations + 5824);
    conv2d_30_output_array.data = AI_PTR(activations + 2752);
    conv2d_30_output_array.data_start = AI_PTR(activations + 2752);
    conv2d_31_output_array.data = AI_PTR(activations + 1088);
    conv2d_31_output_array.data_start = AI_PTR(activations + 1088);
    eltwise_32_output_array.data = AI_PTR(activations + 1600);
    eltwise_32_output_array.data_start = AI_PTR(activations + 1600);
    conv2d_33_output_array.data = AI_PTR(activations + 7232);
    conv2d_33_output_array.data_start = AI_PTR(activations + 7232);
    conv2d_34_output_array.data = AI_PTR(activations + 2112);
    conv2d_34_output_array.data_start = AI_PTR(activations + 2112);
    conv2d_35_output_array.data = AI_PTR(activations + 1088);
    conv2d_35_output_array.data_start = AI_PTR(activations + 1088);
    eltwise_36_output_array.data = AI_PTR(activations + 0);
    eltwise_36_output_array.data_start = AI_PTR(activations + 0);
    conv2d_37_output_array.data = AI_PTR(activations + 5632);
    conv2d_37_output_array.data_start = AI_PTR(activations + 5632);
    conv2d_38_output_array.data = AI_PTR(activations + 512);
    conv2d_38_output_array.data_start = AI_PTR(activations + 512);
    conv2d_39_output_array.data = AI_PTR(activations + 4672);
    conv2d_39_output_array.data_start = AI_PTR(activations + 4672);
    eltwise_40_output_array.data = AI_PTR(activations + 512);
    eltwise_40_output_array.data_start = AI_PTR(activations + 512);
    conv2d_41_output_array.data = AI_PTR(activations + 3072);
    conv2d_41_output_array.data_start = AI_PTR(activations + 3072);
    conv2d_42_output_array.data = AI_PTR(activations + 0);
    conv2d_42_output_array.data_start = AI_PTR(activations + 0);
    conv2d_43_output_array.data = AI_PTR(activations + 4320);
    conv2d_43_output_array.data_start = AI_PTR(activations + 4320);
    conv2d_44_output_array.data = AI_PTR(activations + 9696);
    conv2d_44_output_array.data_start = AI_PTR(activations + 9696);
    conv2d_45_output_array.data = AI_PTR(activations + 5088);
    conv2d_45_output_array.data_start = AI_PTR(activations + 5088);
    conv2d_46_output_array.data = AI_PTR(activations + 1632);
    conv2d_46_output_array.data_start = AI_PTR(activations + 1632);
    eltwise_47_output_array.data = AI_PTR(activations + 0);
    eltwise_47_output_array.data_start = AI_PTR(activations + 0);
    conv2d_48_output_array.data = AI_PTR(activations + 8448);
    conv2d_48_output_array.data_start = AI_PTR(activations + 8448);
    conv2d_49_output_array.data = AI_PTR(activations + 768);
    conv2d_49_output_array.data_start = AI_PTR(activations + 768);
    conv2d_50_output_array.data = AI_PTR(activations + 7008);
    conv2d_50_output_array.data_start = AI_PTR(activations + 7008);
    eltwise_51_output_array.data = AI_PTR(activations + 768);
    eltwise_51_output_array.data_start = AI_PTR(activations + 768);
    conv2d_52_output_array.data = AI_PTR(activations + 4608);
    conv2d_52_output_array.data_start = AI_PTR(activations + 4608);
    conv2d_53_output_array.data = AI_PTR(activations + 1504);
    conv2d_53_output_array.data_start = AI_PTR(activations + 1504);
    conversion_55_output_array.data = AI_PTR(NULL);
    conversion_55_output_array.data_start = AI_PTR(NULL);
    
  }
  return true;
}



AI_DECLARE_STATIC
ai_bool network_configure_weights(
  ai_network* net_ctx, const ai_buffer* weights_buffer)
{
  AI_ASSERT(net_ctx &&  weights_buffer && weights_buffer->data)

  ai_ptr weights = AI_PTR(weights_buffer->data);
  AI_ASSERT(weights)
  AI_UNUSED(net_ctx)

  {
    /* Updating weights (byte) offsets */
    
    conv2d_53_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_53_bias_array.data = AI_PTR(weights + 184720);
    conv2d_53_bias_array.data_start = AI_PTR(weights + 184720);
    conv2d_53_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_53_weights_array.data = AI_PTR(weights + 174640);
    conv2d_53_weights_array.data_start = AI_PTR(weights + 174640);
    conv2d_52_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_52_bias_array.data = AI_PTR(weights + 173488);
    conv2d_52_bias_array.data_start = AI_PTR(weights + 173488);
    conv2d_52_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_52_weights_array.data = AI_PTR(weights + 159664);
    conv2d_52_weights_array.data_start = AI_PTR(weights + 159664);
    conv2d_50_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_50_bias_array.data = AI_PTR(weights + 159472);
    conv2d_50_bias_array.data_start = AI_PTR(weights + 159472);
    conv2d_50_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_50_weights_array.data = AI_PTR(weights + 145648);
    conv2d_50_weights_array.data_start = AI_PTR(weights + 145648);
    conv2d_49_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_49_bias_array.data = AI_PTR(weights + 144496);
    conv2d_49_bias_array.data_start = AI_PTR(weights + 144496);
    conv2d_49_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_49_weights_array.data = AI_PTR(weights + 141904);
    conv2d_49_weights_array.data_start = AI_PTR(weights + 141904);
    conv2d_48_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_48_bias_array.data = AI_PTR(weights + 140752);
    conv2d_48_bias_array.data_start = AI_PTR(weights + 140752);
    conv2d_48_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_48_weights_array.data = AI_PTR(weights + 126928);
    conv2d_48_weights_array.data_start = AI_PTR(weights + 126928);
    conv2d_46_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_46_bias_array.data = AI_PTR(weights + 126736);
    conv2d_46_bias_array.data_start = AI_PTR(weights + 126736);
    conv2d_46_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_46_weights_array.data = AI_PTR(weights + 112912);
    conv2d_46_weights_array.data_start = AI_PTR(weights + 112912);
    conv2d_45_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_45_bias_array.data = AI_PTR(weights + 111760);
    conv2d_45_bias_array.data_start = AI_PTR(weights + 111760);
    conv2d_45_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_45_weights_array.data = AI_PTR(weights + 109168);
    conv2d_45_weights_array.data_start = AI_PTR(weights + 109168);
    conv2d_44_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_44_bias_array.data = AI_PTR(weights + 108016);
    conv2d_44_bias_array.data_start = AI_PTR(weights + 108016);
    conv2d_44_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_44_weights_array.data = AI_PTR(weights + 94192);
    conv2d_44_weights_array.data_start = AI_PTR(weights + 94192);
    conv2d_43_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_43_bias_array.data = AI_PTR(weights + 94000);
    conv2d_43_bias_array.data_start = AI_PTR(weights + 94000);
    conv2d_43_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_43_weights_array.data = AI_PTR(weights + 84784);
    conv2d_43_weights_array.data_start = AI_PTR(weights + 84784);
    conv2d_42_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_42_bias_array.data = AI_PTR(weights + 84016);
    conv2d_42_bias_array.data_start = AI_PTR(weights + 84016);
    conv2d_42_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_42_weights_array.data = AI_PTR(weights + 82288);
    conv2d_42_weights_array.data_start = AI_PTR(weights + 82288);
    conv2d_41_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_41_bias_array.data = AI_PTR(weights + 81520);
    conv2d_41_bias_array.data_start = AI_PTR(weights + 81520);
    conv2d_41_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_41_weights_array.data = AI_PTR(weights + 75376);
    conv2d_41_weights_array.data_start = AI_PTR(weights + 75376);
    conv2d_39_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_39_bias_array.data = AI_PTR(weights + 75248);
    conv2d_39_bias_array.data_start = AI_PTR(weights + 75248);
    conv2d_39_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_39_weights_array.data = AI_PTR(weights + 69104);
    conv2d_39_weights_array.data_start = AI_PTR(weights + 69104);
    conv2d_38_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_38_bias_array.data = AI_PTR(weights + 68336);
    conv2d_38_bias_array.data_start = AI_PTR(weights + 68336);
    conv2d_38_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_38_weights_array.data = AI_PTR(weights + 66608);
    conv2d_38_weights_array.data_start = AI_PTR(weights + 66608);
    conv2d_37_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_37_bias_array.data = AI_PTR(weights + 65840);
    conv2d_37_bias_array.data_start = AI_PTR(weights + 65840);
    conv2d_37_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_37_weights_array.data = AI_PTR(weights + 59696);
    conv2d_37_weights_array.data_start = AI_PTR(weights + 59696);
    conv2d_35_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_35_bias_array.data = AI_PTR(weights + 59568);
    conv2d_35_bias_array.data_start = AI_PTR(weights + 59568);
    conv2d_35_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_35_weights_array.data = AI_PTR(weights + 53424);
    conv2d_35_weights_array.data_start = AI_PTR(weights + 53424);
    conv2d_34_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_34_bias_array.data = AI_PTR(weights + 52656);
    conv2d_34_bias_array.data_start = AI_PTR(weights + 52656);
    conv2d_34_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_34_weights_array.data = AI_PTR(weights + 50928);
    conv2d_34_weights_array.data_start = AI_PTR(weights + 50928);
    conv2d_33_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_33_bias_array.data = AI_PTR(weights + 50160);
    conv2d_33_bias_array.data_start = AI_PTR(weights + 50160);
    conv2d_33_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_33_weights_array.data = AI_PTR(weights + 44016);
    conv2d_33_weights_array.data_start = AI_PTR(weights + 44016);
    conv2d_31_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_31_bias_array.data = AI_PTR(weights + 43888);
    conv2d_31_bias_array.data_start = AI_PTR(weights + 43888);
    conv2d_31_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_31_weights_array.data = AI_PTR(weights + 37744);
    conv2d_31_weights_array.data_start = AI_PTR(weights + 37744);
    conv2d_30_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_30_bias_array.data = AI_PTR(weights + 36976);
    conv2d_30_bias_array.data_start = AI_PTR(weights + 36976);
    conv2d_30_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_30_weights_array.data = AI_PTR(weights + 35248);
    conv2d_30_weights_array.data_start = AI_PTR(weights + 35248);
    conv2d_29_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_29_bias_array.data = AI_PTR(weights + 34480);
    conv2d_29_bias_array.data_start = AI_PTR(weights + 34480);
    conv2d_29_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_29_weights_array.data = AI_PTR(weights + 28336);
    conv2d_29_weights_array.data_start = AI_PTR(weights + 28336);
    conv2d_28_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_28_bias_array.data = AI_PTR(weights + 28208);
    conv2d_28_bias_array.data_start = AI_PTR(weights + 28208);
    conv2d_28_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_28_weights_array.data = AI_PTR(weights + 25136);
    conv2d_28_weights_array.data_start = AI_PTR(weights + 25136);
    conv2d_27_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_27_bias_array.data = AI_PTR(weights + 24752);
    conv2d_27_bias_array.data_start = AI_PTR(weights + 24752);
    conv2d_27_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_27_weights_array.data = AI_PTR(weights + 23888);
    conv2d_27_weights_array.data_start = AI_PTR(weights + 23888);
    conv2d_25_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_25_bias_array.data = AI_PTR(weights + 23504);
    conv2d_25_bias_array.data_start = AI_PTR(weights + 23504);
    conv2d_25_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_25_weights_array.data = AI_PTR(weights + 21968);
    conv2d_25_weights_array.data_start = AI_PTR(weights + 21968);
    conv2d_23_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_23_bias_array.data = AI_PTR(weights + 21904);
    conv2d_23_bias_array.data_start = AI_PTR(weights + 21904);
    conv2d_23_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_23_weights_array.data = AI_PTR(weights + 20368);
    conv2d_23_weights_array.data_start = AI_PTR(weights + 20368);
    conv2d_22_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_22_bias_array.data = AI_PTR(weights + 19984);
    conv2d_22_bias_array.data_start = AI_PTR(weights + 19984);
    conv2d_22_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_22_weights_array.data = AI_PTR(weights + 19120);
    conv2d_22_weights_array.data_start = AI_PTR(weights + 19120);
    conv2d_21_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_21_bias_array.data = AI_PTR(weights + 18736);
    conv2d_21_bias_array.data_start = AI_PTR(weights + 18736);
    conv2d_21_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_21_weights_array.data = AI_PTR(weights + 17200);
    conv2d_21_weights_array.data_start = AI_PTR(weights + 17200);
    conv2d_19_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_19_bias_array.data = AI_PTR(weights + 17136);
    conv2d_19_bias_array.data_start = AI_PTR(weights + 17136);
    conv2d_19_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_19_weights_array.data = AI_PTR(weights + 15600);
    conv2d_19_weights_array.data_start = AI_PTR(weights + 15600);
    conv2d_18_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_18_bias_array.data = AI_PTR(weights + 15216);
    conv2d_18_bias_array.data_start = AI_PTR(weights + 15216);
    conv2d_18_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_18_weights_array.data = AI_PTR(weights + 14352);
    conv2d_18_weights_array.data_start = AI_PTR(weights + 14352);
    conv2d_17_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_17_bias_array.data = AI_PTR(weights + 13968);
    conv2d_17_bias_array.data_start = AI_PTR(weights + 13968);
    conv2d_17_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_17_weights_array.data = AI_PTR(weights + 12432);
    conv2d_17_weights_array.data_start = AI_PTR(weights + 12432);
    conv2d_16_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_16_bias_array.data = AI_PTR(weights + 12368);
    conv2d_16_bias_array.data_start = AI_PTR(weights + 12368);
    conv2d_16_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_16_weights_array.data = AI_PTR(weights + 10832);
    conv2d_16_weights_array.data_start = AI_PTR(weights + 10832);
    conv2d_15_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_15_bias_array.data = AI_PTR(weights + 10448);
    conv2d_15_bias_array.data_start = AI_PTR(weights + 10448);
    conv2d_15_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_15_weights_array.data = AI_PTR(weights + 9584);
    conv2d_15_weights_array.data_start = AI_PTR(weights + 9584);
    conv2d_13_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_13_bias_array.data = AI_PTR(weights + 9200);
    conv2d_13_bias_array.data_start = AI_PTR(weights + 9200);
    conv2d_13_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_13_weights_array.data = AI_PTR(weights + 7664);
    conv2d_13_weights_array.data_start = AI_PTR(weights + 7664);
    conv2d_11_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_11_bias_array.data = AI_PTR(weights + 7600);
    conv2d_11_bias_array.data_start = AI_PTR(weights + 7600);
    conv2d_11_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_11_weights_array.data = AI_PTR(weights + 6064);
    conv2d_11_weights_array.data_start = AI_PTR(weights + 6064);
    conv2d_10_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_10_bias_array.data = AI_PTR(weights + 5680);
    conv2d_10_bias_array.data_start = AI_PTR(weights + 5680);
    conv2d_10_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_10_weights_array.data = AI_PTR(weights + 4816);
    conv2d_10_weights_array.data_start = AI_PTR(weights + 4816);
    conv2d_9_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_9_bias_array.data = AI_PTR(weights + 4432);
    conv2d_9_bias_array.data_start = AI_PTR(weights + 4432);
    conv2d_9_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_9_weights_array.data = AI_PTR(weights + 2896);
    conv2d_9_weights_array.data_start = AI_PTR(weights + 2896);
    conv2d_8_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_8_bias_array.data = AI_PTR(weights + 2832);
    conv2d_8_bias_array.data_start = AI_PTR(weights + 2832);
    conv2d_8_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_8_weights_array.data = AI_PTR(weights + 2064);
    conv2d_8_weights_array.data_start = AI_PTR(weights + 2064);
    conv2d_7_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_7_bias_array.data = AI_PTR(weights + 1872);
    conv2d_7_bias_array.data_start = AI_PTR(weights + 1872);
    conv2d_7_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_7_weights_array.data = AI_PTR(weights + 1440);
    conv2d_7_weights_array.data_start = AI_PTR(weights + 1440);
    conv2d_5_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_5_bias_array.data = AI_PTR(weights + 1248);
    conv2d_5_bias_array.data_start = AI_PTR(weights + 1248);
    conv2d_5_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_5_weights_array.data = AI_PTR(weights + 864);
    conv2d_5_weights_array.data_start = AI_PTR(weights + 864);
    conv2d_4_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_4_bias_array.data = AI_PTR(weights + 832);
    conv2d_4_bias_array.data_start = AI_PTR(weights + 832);
    conv2d_4_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_4_weights_array.data = AI_PTR(weights + 704);
    conv2d_4_weights_array.data_start = AI_PTR(weights + 704);
    conv2d_3_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_3_bias_array.data = AI_PTR(weights + 640);
    conv2d_3_bias_array.data_start = AI_PTR(weights + 640);
    conv2d_3_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_3_weights_array.data = AI_PTR(weights + 496);
    conv2d_3_weights_array.data_start = AI_PTR(weights + 496);
    conv2d_2_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_2_bias_array.data = AI_PTR(weights + 432);
    conv2d_2_bias_array.data_start = AI_PTR(weights + 432);
    conv2d_2_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_2_weights_array.data = AI_PTR(weights + 0);
    conv2d_2_weights_array.data_start = AI_PTR(weights + 0);
  }

  return true;
}


/**  PUBLIC APIs SECTION  *****************************************************/

AI_API_ENTRY
ai_bool ai_network_get_info(
  ai_handle network, ai_network_report* report)
{
  ai_network* net_ctx = AI_NETWORK_ACQUIRE_CTX(network);

  if ( report && net_ctx )
  {
    ai_network_report r = {
      .model_name        = AI_NETWORK_MODEL_NAME,
      .model_signature   = AI_NETWORK_MODEL_SIGNATURE,
      .model_datetime    = AI_TOOLS_DATE_TIME,
      
      .compile_datetime  = AI_TOOLS_COMPILE_TIME,
      
      .runtime_revision  = ai_platform_runtime_get_revision(),
      .runtime_version   = ai_platform_runtime_get_version(),

      .tool_revision     = AI_TOOLS_REVISION_ID,
      .tool_version      = {AI_TOOLS_VERSION_MAJOR, AI_TOOLS_VERSION_MINOR,
                            AI_TOOLS_VERSION_MICRO, 0x0},
      .tool_api_version  = {AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR,
                            AI_TOOLS_API_VERSION_MICRO, 0x0},

      .api_version            = ai_platform_api_get_version(),
      .interface_api_version  = ai_platform_interface_api_get_version(),
      
      .n_macc            = 6484427,
      .n_inputs          = 0,
      .inputs            = NULL,
      .n_outputs         = 0,
      .outputs           = NULL,
      .activations       = AI_STRUCT_INIT,
      .params            = AI_STRUCT_INIT,
      .n_nodes           = 0,
      .signature         = 0x0,
    };

    if ( !ai_platform_api_get_network_report(network, &r) ) return false;

    *report = r;
    return true;
  }

  return false;
}

AI_API_ENTRY
ai_error ai_network_get_error(ai_handle network)
{
  return ai_platform_network_get_error(network);
}

AI_API_ENTRY
ai_error ai_network_create(
  ai_handle* network, const ai_buffer* network_config)
{
  return ai_platform_network_create(
    network, network_config, 
    &AI_NET_OBJ_INSTANCE,
    AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR, AI_TOOLS_API_VERSION_MICRO);
}

AI_API_ENTRY
ai_handle ai_network_destroy(ai_handle network)
{
  return ai_platform_network_destroy(network);
}

AI_API_ENTRY
ai_bool ai_network_init(
  ai_handle network, const ai_network_params* params)
{
  ai_network* net_ctx = ai_platform_network_init(network, params);
  if ( !net_ctx ) return false;

  ai_bool ok = true;
  ok &= network_configure_weights(net_ctx, &params->params);
  ok &= network_configure_activations(net_ctx, &params->activations);

  return ok;
}


AI_API_ENTRY
ai_i32 ai_network_run(
  ai_handle network, const ai_buffer* input, ai_buffer* output)
{
  return ai_platform_network_process(network, input, output);
}

AI_API_ENTRY
ai_i32 ai_network_forward(ai_handle network, const ai_buffer* input)
{
  return ai_platform_network_process(network, input, NULL);
}

#undef AI_NETWORK_MODEL_SIGNATURE
#undef AI_NET_OBJ_INSTANCE
#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#undef AI_TOOLS_DATE_TIME
#undef AI_TOOLS_COMPILE_TIME

