#include "dma2d_drv.h"
#include "L8_LUTs.h"

DMA2D_HandleTypeDef hdma2d;
DMA2D_CLUTCfgTypeDef CLUTCfg;

__IO uint32_t CLUT_LoadComplete;
__IO uint32_t Transfer_Complete;

void TransferError(DMA2D_HandleTypeDef *hdma2d)
{
  while (1)
  {
    BSP_LED_Toggle(LED1);
    HAL_Delay(1000);
  }
}

void TransferComplete(DMA2D_HandleTypeDef *hdma2d)
{
  Transfer_Complete = 1;
}

void LUT_Set(void){
  while (HAL_DMA2D_GetState(&hdma2d) != HAL_DMA2D_STATE_READY){}
  DMA2D_CLUTCfgTypeDef CLUTCfg;
    
  CLUTCfg.CLUTColorMode = DMA2D_CCM_ARGB8888;     
  CLUTCfg.pCLUT = (uint32_t *)L8_CLUT_0;
  CLUTCfg.Size = 255;
  CLUT_LoadComplete = 0;
  HAL_DMA2D_CLUTLoad_IT(&hdma2d, CLUTCfg, 1);
  
  while(CLUT_LoadComplete == 0)
  {
    ;
  } 
}

void HAL_DMA2D_CLUTLoadingCpltCallback(DMA2D_HandleTypeDef *hdma2d)
{
  CLUT_LoadComplete = 1;
}

void DMA2D_Image_Show(void *pSrc, void *pDst, uint16_t width, uint16_t height, uint32_t original_format, uint16_t out_offs)
{
  //avoid collisions with other transfers -> if the dma2d is already in use, wait for the completion of the operation
  while (HAL_DMA2D_GetState(&hdma2d) != HAL_DMA2D_STATE_READY)
  {
  }
  /* Enable DMA2D clock */
  __HAL_RCC_DMA2D_CLK_ENABLE();

  /* Configure the DMA2D Mode, Color Mode and output offset */
  hdma2d.Init.Mode         = DMA2D_M2M_PFC;
  hdma2d.Init.ColorMode    = DMA2D_OUTPUT_RGB565;
  hdma2d.Init.OutputOffset = out_offs;

  /* Foreground Configuration */
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0xFF;
  hdma2d.LayerCfg[1].InputColorMode = original_format;
  hdma2d.LayerCfg[1].InputOffset = 0;

  hdma2d.Instance = DMA2D;

  /* Callbacks setup */
  hdma2d.XferCpltCallback  = TransferComplete;
  hdma2d.XferErrorCallback = TransferError;

  /* DMA2D Initialization */
  if(HAL_DMA2D_Init(&hdma2d) == HAL_OK)
  {
    if(HAL_DMA2D_ConfigLayer(&hdma2d, 1) == HAL_OK)
    {
        Transfer_Complete = 0;
        LUT_Set();
        HAL_DMA2D_Start_IT(&hdma2d, (uint8_t *)pSrc, (uint16_t *)pDst, width, height);
    }
  }
  else
  {
    LCD_Print("DMA2D init error!");
    while(1);
  }
}


void DMA2D_Image_Downsample(uint8_t *pSrc, uint8_t *pDst, uint16_t width, uint16_t height, uint8_t factor)
{
  while (HAL_DMA2D_GetState(&hdma2d) != HAL_DMA2D_STATE_READY)
  {
  }
  uint32_t tmp=0, tmp2=0, counter=0;
  
  while (height > counter){
    Line_Downsample((uint8_t *)(pSrc + tmp), (uint8_t *)(pDst + tmp2), factor-1, width/factor);
    tmp  = tmp + factor*width*sizeof(uint8_t); 
    tmp2 = tmp2 + width/factor*sizeof(uint32_t);
    counter+=factor;
  }
}



//TODO: change to m2m w/o pfc
//Downsample a line with a specified stride (a bit of a hack)
void Line_Downsample(void *pSrc, void *pDst, uint8_t stride, uint16_t length)
{
  /* Enable DMA2D clock */
  __HAL_RCC_DMA2D_CLK_ENABLE();

  /* Configure the DMA2D Mode, Color Mode and output offset */
  hdma2d.Init.Mode         = DMA2D_M2M_PFC;
  hdma2d.Init.ColorMode    = DMA2D_OUTPUT_ARGB8888;
  hdma2d.Init.OutputOffset = 0;

  /* Foreground Configuration */
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0xFF;
  hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_L8;
  hdma2d.LayerCfg[1].InputOffset = stride;

  hdma2d.Instance = DMA2D;

  /* DMA2D Initialization */
  if(HAL_DMA2D_Init(&hdma2d) == HAL_OK)
  {
    if(HAL_DMA2D_ConfigLayer(&hdma2d, 1) == HAL_OK)
    {
      if (HAL_DMA2D_Start(&hdma2d, (uint32_t)pSrc, (uint32_t)pDst, 1, length) == HAL_OK)
      {
        /* Polling For DMA transfer */

        HAL_DMA2D_PollForTransfer(&hdma2d, 10);
      }
    }
  }
  else
  {
    while(1);
  }
}