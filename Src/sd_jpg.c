#include "sd_jpg.h"

DMA2D_HandleTypeDef hdma2d;

static uint8_t s_nextFrameReady = 0;
static uint32_t s_lastFrameStartTimeMSec = 0;

static uint8_t *sp_lineBuffRGB888;
static FATFS   *sp_fatFs;
static FIL     *sp_fil;
static struct jpeg_compress_struct *sp_cinfo;
static struct jpeg_error_mgr       *sp_jerr;
static JSAMPROW s_jsamprow[2] = {0};
static uint32_t s_jpegQuality = JPEG_QUALITY;

static uint16_t numpic=0;

RET liveviewCtrl_capture()
{
  RET ret = RET_OK;
  char filename[10] = "";
  uint32_t start = HAL_GetTick();

  sprintf(filename, "pic%d.jpg",numpic);

  ret |= liveviewCtrl_writeFileStart(filename);
  HAL_Delay(20);
  ret |= liveviewCtrl_encodeJpegFrame();
  HAL_Delay(100);
  ret |= liveviewCtrl_writeFileFinish();
  HAL_Delay(100);

  numpic++;

  return ret;
}


RET liveviewCtrl_movieRecordStart()
{
  RET ret = RET_OK;
  char filename[14] = "VID000.AVI";

  s_nextFrameReady = 1; // the first frame is always ready because I can reuse liveview image
  s_lastFrameStartTimeMSec = HAL_GetTick();

  ret |= liveviewCtrl_generateFilename(filename, 3);
  ret |= liveviewCtrl_writeFileStart(filename);
  if(ret != RET_OK) {
    ret |= liveviewCtrl_writeFileFinish();
    LCD_Print("Movie Record End");
    return ret;
  }

  return ret;
}

RET liveviewCtrl_movieRecordFinish()
{
  RET ret = RET_OK;

  ret |= liveviewCtrl_writeFileFinish();

  return ret;
}

RET liveviewCtrl_movieRecordFrame()
{
  RET ret = RET_OK;

  if(s_nextFrameReady) {
    if(HAL_GetTick() - s_lastFrameStartTimeMSec > MOTION_JPEG_FPS_MSEC) { // control fps

      s_lastFrameStartTimeMSec = HAL_GetTick();
      /* encode one frame (do not close file yet) */
      ret |= liveviewCtrl_encodeJpegFrame();
      /* capture next frame */
      s_nextFrameReady = 1;
    } else {
      // skip for fps control
    }
  } else {
    /* not ready (copying image data from camera to display) */

    // workaround. Vsync signal sometimes doesn't come (probably because of poor hardware work)
    if(HAL_GetTick() - s_lastFrameStartTimeMSec > MOTION_JPEG_FPS_MSEC*3) {
      s_nextFrameReady = 1;
    }
  }

  return ret;
}

static RET liveviewCtrl_writeFileStart(char* filename)
{
  RET ret = RET_OK;
  sp_fatFs = ff_malloc(sizeof(FATFS));
  sp_fil   = ff_malloc(sizeof(FIL));
  if( (sp_fatFs == 0) || (sp_fil == 0) ) {
    return RET_ERR_MEMORY;
  }

  ret |= f_mount(sp_fatFs, "", 0);
  ret |= f_open(sp_fil, filename, FA_WRITE | FA_CREATE_NEW);

  return ret;
}

static RET liveviewCtrl_writeFileFinish()
{
  RET ret = RET_OK;
  ret |= f_close(sp_fil);
  ret |= f_mount(0, "", 0);
  ff_free(sp_fil);
  ff_free(sp_fatFs);
  return ret;
}

static RET liveviewCtrl_encodeJpegFrame()
{
  RET ret = RET_OK;

  /*** alloc memory ***/
  sp_cinfo = ff_malloc(sizeof(struct jpeg_compress_struct));
  sp_jerr  = ff_malloc(sizeof(struct jpeg_error_mgr));
  sp_lineBuffRGB888 = ff_malloc(IMAGE_SIZE_WIDTH * 3);

  if( (sp_cinfo == 0) || (sp_jerr == 0) || (sp_lineBuffRGB888 == 0) ){
    ff_free(sp_cinfo);
    ff_free(sp_jerr);
    ff_free(sp_lineBuffRGB888);
    return RET_ERR_MEMORY;
  }

  /*** prepare libjpeg ***/
  s_jsamprow[0] = sp_lineBuffRGB888;
  sp_cinfo->err = jpeg_std_error(sp_jerr);
  jpeg_create_compress(sp_cinfo);
  jpeg_stdio_dest(sp_cinfo, sp_fil);

  /* jpeg encode setting */
  sp_cinfo->image_width  = IMAGE_SIZE_WIDTH;
  sp_cinfo->image_height = IMAGE_SIZE_HEIGHT;
  sp_cinfo->input_components = 3;
  sp_cinfo->in_color_space = JCS_RGB;
  jpeg_set_defaults(sp_cinfo);
  jpeg_set_quality(sp_cinfo, s_jpegQuality, TRUE);
  jpeg_start_compress(sp_cinfo, TRUE);


  for(uint32_t y = 0; y < IMAGE_SIZE_HEIGHT; y++) {
    /* read one line from display device (as an external RAM) */
    LCD_LL_ConvertLineToRGB888(CAMERA_FRAME_BUFFER+y*IMAGE_SIZE_WIDTH, sp_lineBuffRGB888, IMAGE_SIZE_WIDTH);
    /* encode one line */
    if(jpeg_write_scanlines(sp_cinfo, s_jsamprow, 1) != 1) {
      return RET_NO_DATA;
      break;
    }
  }

  /*** finalize libjpeg ***/
  jpeg_finish_compress(sp_cinfo);
  jpeg_destroy_compress(sp_cinfo);

  /*** free memory ***/
  ff_free(sp_cinfo);
  ff_free(sp_jerr);
  ff_free(sp_lineBuffRGB888);

  return ret;
}

static void LCD_LL_ConvertLineToRGB888(void *pSrc, void *pDst, uint16_t width)
{
    /* Enable DMA2D clock */
    __HAL_RCC_DMA2D_CLK_ENABLE();

    /* Configure the DMA2D Mode, Color Mode and output offset */
    hdma2d.Init.Mode         = DMA2D_M2M_PFC;
    hdma2d.Init.ColorMode    = DMA2D_RGB888;
    hdma2d.Init.OutputOffset = 0;

    /* Foreground Configuration */
    hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
    hdma2d.LayerCfg[1].InputAlpha = 0xFF;
    hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_L8;
    hdma2d.LayerCfg[1].InputOffset = 0;

    hdma2d.Instance = DMA2D;

    /* DMA2D Initialization */
    if(HAL_DMA2D_Init(&hdma2d) == HAL_OK) {
        if(HAL_DMA2D_ConfigLayer(&hdma2d, 1) == HAL_OK) {
            if (HAL_DMA2D_Start(&hdma2d, (uint32_t)pSrc, (uint32_t)pDst, width, 1) == HAL_OK) {
                /* Polling For DMA transfer */
                HAL_DMA2D_PollForTransfer(&hdma2d, 10);
            }
        }
    } else {

    }
}

static RET liveviewCtrl_generateFilename(char* filename, uint8_t numPos)
{
  static uint8_t s_number = 0;

  FRESULT ret;
  FATFS *p_fatFs = ff_malloc(sizeof(FATFS));
  FIL   *p_fil   = ff_malloc(sizeof(FIL));
  if( (p_fatFs == 0) || (p_fil == 0) ) {
    ff_free(p_fil);
    ff_free(p_fatFs);
    return RET_ERR_MEMORY;
  }

  f_mount(p_fatFs, "", 0);
  do {
    uint8_t num100 = (s_number/100) % 10;
    uint8_t num10  = (s_number/10) % 10;
    uint8_t num1   = (s_number/1) % 10;
    filename[numPos + 0] = '0' + num100;
    filename[numPos + 1] = '0' + num10;
    filename[numPos + 2] = '0' + num1;
    s_number++;
    ret = f_open(p_fil, filename, FA_OPEN_EXISTING);
    f_close(p_fil);
  } while(ret == FR_OK);

  f_mount(0, "", 0);
  ff_free(p_fil);
  ff_free(p_fatFs);

  if(ret == FR_NO_FILE) {
      return RET_OK;
  }
  return RET_ERR;
}